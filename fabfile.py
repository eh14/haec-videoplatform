from fabric.api import task, local, run, env, lcd, put, settings, execute, cd, prompt, prefix
from fabric.operations import sudo
from compiler import ast
from fabric.decorators import roles, parallel
from fabric.colors import green, red, blue
import os
import re
import time
import subprocess
import sys
import uuid
import shutil
from fabric.contrib.console import confirm
import watcher

@task
def check(test='', randSeed=None):
    """
    Executes unit-tests
    """
    if randSeed == 0:
        randSeed = int(round(time.time() * 1000.0))
        
        
    testToRun = 'haec.unittests%s' % ('.' + test if len(test) > 0 else '')
    cmd = 'PYTHONPATH=$PYTHONPATH:src && cd src && trial --temp-directory=/tmp/_trial_tmp '
    
    if randSeed:
        cmd += ' --random=%s ' % randSeed  
    local(cmd + testToRun)
    
@task
def ptest(test=''):
    """
    Executes some performance tests. (not really testing, just seeing how long things take)
    """
    local('PYTHONPATH=$PYTHONPATH:%s:%s && cd ptest && trial --temp-directory=/tmp/_trial_tmp_ptest tests' % (os.path.join(os.getcwd(), 'src'), os.path.join(os.getcwd(), 'ptest')))
    
def _pythonPathAndCdPrefix(path):
    return 'PYTHONPATH="$PYTHONPATH:%s:%s" && cd %s && ' % (os.path.join(os.getcwd(), 'src'), os.path.join(os.getcwd(), path), path)

@task
def profile(test):
    """
    Runs a predetermined profile run.
    """
    local(_pythonPathAndCdPrefix('profile') + 
          ('python -m cProfile -o %s %s' % (os.path.join(os.getcwd(), 'profile', 'profiling.pyprof'), test)
           ))
    local('pyprof2calltree -i profile/profiling.pyprof -o profile/profiling.pyprof.log')
    local('kcachegrind profile/profiling.pyprof.log')
    
@task
def simulate():
    local(_pythonPathAndCdPrefix('profile/simulator') + 
          ('python -m cProfile -o %s %s' % (os.path.join(os.getcwd(), 'profile/simulator', 'simulation.pyprof'), 'simulation1.py')
           ))
    local('pyprof2calltree -i profile/simulator/simulation.pyprof -o profile/simulator/simulation.pyprof.log')
    local('kcachegrind profile/simulator/simulation.pyprof.log')
    
    
@task
def rawProfile(test):
    local(_pythonPathAndCdPrefix('profile') + 
          ('python %s' % test))
    
    
@task
def prepareFrontend():
    with lcd('src/haec/frontend/web'):
        local('mkdir -p .external')
        local('npm install requirejs bootstrap backbone jquery underscore bootstrap-slider backgrid moment')
        local('cp node_modules/jquery/dist/cdn/* .external/')
        local('cp node_modules/underscore/*.js .external/')
        local('cp node_modules/backbone/backbone*.* .external/')
        local('cp node_modules/bootstrap/dist/js/*.js .external/')
        local('cp node_modules/bootstrap/dist/css/* .external/')
        local('cp node_modules/bootstrap/dist/fonts/* .external/')
        local('cp node_modules/requirejs/require.js .external/')
        local('cp node_modules/bootstrap-slider/dist/bootstrap-slider.min.js .external/')
        local('cp node_modules/bootstrap-slider/dist/css/bootstrap-slider.min.css .external/')
        local('cp node_modules/backgrid/lib/backgrid*.* .external/')
        local('cp node_modules/moment/min/moment-with-locales.min.js .external/')
        
        
@task
def collectStatics():
    with lcd('src/haec/frontend'):
        local('python manage.py collectstatic  --noinput')

@task
@watcher.watch('src/haec/frontend/web/static', ['*.js', '*.css', '*.png', '*.jpg'])
def watchCollect():
    execute(collectStatics)
    
@task
def profileStreaming():
    path = os.path.abspath('../../../sourcevideos')
    local('python profile/streamingserver.py %s' % path)

#
# Integration test specific stuff.
#


itestPath = os.path.abspath('itest')
sys.path.insert(0, itestPath)
from tests import config


def _startMaster(out, bulkInterval=0.2, videoDir=None):
    
    if not videoDir:
        videoDir = config.video_dir
        
    options = ['master',
               '--local_node_port=%s' % config.masterNodePort,
               '--name=master',
               '--db_connectionurl=mysql://root:root@localhost:3306/vp_itests',
               '--db_drop=true',
               '--webserver_port=%s' % config.masterWebPort,
               '--cambrionix_url=tcp://127.0.0.1:9059',
               '--video_dir=%s' % videoDir,
               '--cambrionix_postinit_delay=0',
               '--sensor_sample_interval=%f' % bulkInterval,  # reduce the collecting intervals
               '--video_bulk_interval=%f' % bulkInterval,  # ...
               '--cambrionix_collectinterval=%f' % bulkInterval,  # ...
               '--slave_dnslookup=localhost',
               '--sensor_collectinterval=%f' % bulkInterval,  # so we don't have to wait too long
               '--portmapping_initialize_delay=%f' % bulkInterval,
               ]
    
    args = {}
    if out:
        args['stdout'] = out
        args['stderr'] = subprocess.STDOUT
    return subprocess.Popen(['python', 'start.py'] + options, cwd='src', **args)

def _startCambrionixMock(out):
    args = {}
    if out:
        args['stdout'] = out
        args['stderr'] = subprocess.STDOUT
    return subprocess.Popen(['python', 'haec/unittests/services/cambrionixmock.py', 'slave-y1'], cwd='src', **args)

        
def _startSlave(out, bulkInterval=0.2, videoDir=None, resetVideoDir=True):
    
    if not videoDir:
        videoDir = config.video_dir
    if resetVideoDir:
        with settings(warn_only=True):
            local('rm -r ' + videoDir)
    
    local('cp -r %s %s' % (config.originalVidDir, videoDir))
    
    options = ['slave',
               '--local_node_port=%s' % config.slaveNodePort,
               '--parent_node_port=%s' % config.masterNodePort,
               '--name=slave-y1',
               '--webserver_port=%s' % config.slaveWebPort,
               '--video_dir=%s' % videoDir,
               '--sensor_sample_interval=%f' % bulkInterval,  # reduce the collecting intervals
               '--sensor_collectinterval=%f' % bulkInterval,  # so we don't have to wait too long
               ]
    
    args = {}
    if out:
        args['stdout'] = out
        args['stderr'] = subprocess.STDOUT
        
    return subprocess.Popen(['python', 'start.py'] + options, cwd='src', **args)


@task
def startMockCluster(videoDir):
    master = None
    slave = None

    killIt = True
    try:
        out = None
        cambrionix = _startCambrionixMock(out)
        master = _startMaster(out, bulkInterval=5.0, videoDir=videoDir)
        slave = _startSlave(out, bulkInterval=5.0, videoDir=videoDir, resetVideoDir=False)
        killIt = confirm('Kill it?', default=True)
    finally:
        if not killIt:
            return
        if master:
            master.kill()
        if slave:
            slave.kill()
        if cambrionix:
            cambrionix.kill()
        if out:
            out.close()

@task
def itest(quiet=1, wait=5, test=''):
    """
    Run integration tests
    @param quiet: 0 --> print server output during tests
    """
    
    quiet = int(quiet)
    wait = float(wait)
    execute(collectStatics)
    
    master = None
    slave = None
    
    try:
        
        out = None
        if quiet:
            out = open(os.devnull, 'w')

        cambrionix = _startCambrionixMock(out)
        master = _startMaster(out)
        time.sleep(1)
        slave = _startSlave(out)
        # let it start
        time.sleep(wait)
        
        absSrc = os.path.abspath('src')
        # run the tests
        local('PYTHONPATH=$PYTHONPATH:itest:' + absSrc + ' && cd itest && trial --temp-directory=%s tests%s' % (config.itest_tmp, '.' + test if len(test) > 0 else ''))
        
    finally:
        if master:
            master.kill()
        if slave:
            slave.kill()
        if cambrionix:
            cambrionix.kill()
        if out:
            out.close()

@task
def test():
    """
    Runs ALL possible tests.
    """
    execute(check)
    execute(itest)

def makeSubfolderString(subfolder, elementList):
    return ' '.join([os.path.join(subfolder, element) for element in elementList])


@task
def cloc():
    haecSubfolders = ['kplane', 'network', 'optimization', 'protomixins', 'services', 'unittests', 'utils', '*.py', 'frontend/*.py']
    
    frontendweb = ['*.py', 'services', 'settings', 'tests', 'views', 'templates']
    frontendStatic = ['charts.js', 'common.js',
                      'dashboard.js', 'dashboard.css',
                      'livestats.js',
                      'models.js', 'search.js', 'slavelist.js',
                      'sysicons.*',
                       'tasks.js', 'template.js',
                       'videoDetails.js',
                       'videostats.js',
                       'zonestats.js']
    
    cmd = 'cloc itest profile *.py %s %s %s' % (makeSubfolderString('src/haec', haecSubfolders),
                                            makeSubfolderString('src/haec/frontend/web', frontendweb),
                                            makeSubfolderString('src/haec/frontend/web/static', frontendStatic))
    
    local(cmd)
    
