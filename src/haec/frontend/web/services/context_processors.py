'''

@author: eh14
'''
import pythonioc
from haec import config as _config

_cfg = pythonioc.Inject(_config.Config)

def config(request):
    return {'config':_cfg.vals}