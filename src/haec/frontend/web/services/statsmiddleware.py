import time
from twisted.python import log
import collections

_times = collections.deque(maxlen=30)


class StatsMiddleware(object):
    """
    Displays the time, a request took to render the view
    """
    
    def process_view(self, request, view_func, view_args, view_kwargs):
        # time the view
        start = time.time()
        response = view_func(request, *view_args, **view_kwargs)
        rendertime = ((time.time() - start) * 1000)
        _times.append(rendertime)
        avg = sum(_times) / len(_times)
        if rendertime > avg * 2:
            log.msg('rendered %s in %.2fms (avg=%.2fms)' % (request.path, rendertime, avg))
        
        return response
