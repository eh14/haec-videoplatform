"""
Video specific views
"""
import viewutils
from django.views.decorators.http import require_GET
from django.http.response import HttpResponse, HttpResponseRedirect, \
    HttpResponseNotFound, HttpResponseServerError, HttpResponseBadRequest
from django.template.context import Context
from django.template.loader import get_template
from django.core.urlresolvers import reverse
import random
import time
from haec import config
from haec.kplane import hitcounter
from requests.exceptions import ConnectionError
import pythonioc
from haec.services import localtaskservice, portmapperservice, profileservice, \
    eventservice
import json
from web.views.viewutils import HttpResponseUnavailable

cfg = pythonioc.Inject(config.Config)
__rand = random.Random(time.time())
portmapperService = pythonioc.Inject(portmapperservice.PortMapperService)

profileService = pythonioc.Inject(profileservice.ProfileService)

videoDao = pythonioc.Inject('videoDao')
hitTracker = pythonioc.Inject('hitTracker')
events = pythonioc.Inject(eventservice.EventService)

def showSearch(request):
    template = get_template('search.html')
    ctx = Context({'codecs':videoDao.getAudioVideoCodecNames()})
    return HttpResponse(template.render(ctx))

taskDao = pythonioc.Inject('taskDao')
taskService = pythonioc.Inject('TaskService')

@require_GET
@viewutils.get_params('namePattern', 'audio_codec', 'video_codec', 'format')
def doSearch(request, namePattern=None, video_codec=None, audio_codec=None, format='json'):
    videos = videoDao.searchVideos(namePattern=namePattern,
                            videoCodec=video_codec, audioCodec=audio_codec)
    
    # create a list of dicts of the returned domain objects
    vidList = viewutils.domainObjectsToList(videos)
    for vid in vidList:
        vid['detailUrl'] = reverse(showDetails) + ('?name=%s' % vid['name'])
        
    if format == 'json':
        return viewutils.createJsonResponse(request, vidList)
    else:
        return viewutils.createCsvResponse(request, vidList)

@viewutils.json_response
@require_GET
@viewutils.get_params('name')
def variantsForName(request, name):
    videos = videoDao.getAllVideosForName(name)
    
    return viewutils.domainObjectsToList(videos)

@require_GET
@viewutils.get_params('name')
def showDetails(request, name=None):
    if not name:
        return HttpResponseRedirect(reverse(showSearch))
    
    # retrieve all variants with that name
    videos = videoDao.getVideoVariantsForName(name)
    
    # something should be found
    if len(videos) == 0:
        return HttpResponseNotFound('Could not find any video with name %s' % name)

    # the origin is the one with variant == ''
    origin = None
    for video in list(videos):
        if video.variant == '':
            origin = video
            
            # remove the origin from the list
            videos.remove(video)
            
    ctx = Context({'origin':origin,
                   'transcoded':videos,
                   'profiles':viewutils.blockingCallInReactor(profileService.getProfileDictList),
                   'qualityModes':profileService.getQualityModes()
                   }
                  )
    template = get_template('videoDetails.html')
    return HttpResponse(template.render(ctx))

@require_GET
@viewutils.get_params('name', 'profile', 'qualityMode', priority=viewutils.boolConverter, redirect=viewutils.boolConverter)
def requestVideo(request, name, profile, qualityMode, priority=False, redirect=True):
    """
    Requests a video in a specific profile and quality mode. If the profile is already existent,
    the client will be redirected. Otherwise, a 404 will be returned.
    """
    
    
    if priority:
        print "------- PRIORITY REQUET"
    
    # (1) try to extract the profile for passed name.
    try:
        profile = viewutils.blockingCallInReactor(profileService.getProfileForNameOrId, profile)
    except profileservice.NoSuchProfileException as e:
        return HttpResponseBadRequest('requested profile %s not found' % profile)
    
    # (2) search videos matching the profile
    variant = _findVideoVariantForProfile(name, profile)
    
    if variant is not None:
        redirectUrl = reverse(requestVariant) + ('?name=%s&variant=%s' % (name, variant))
        if redirect:
            return HttpResponseRedirect(redirectUrl)
        else:
            return HttpResponse(redirectUrl)
        
    else:
        return HttpResponseNotFound("Video not available in requested profile")

def _findVideoVariantForProfile(name, profile):
    """
    Tries to find a video variant matching the profile for the given name.
    @returns: the variant ID in case of success, None otherwise.
    """
    vids = videoDao.searchVideos(name=name,
                    videoCodec=profile.vCodec,
                    audioCodec=profile.aCodec,
                    width=profile.width,
                    height=profile.height,
                    loadSlaves=True)
    
    # there are some matches
    if len(vids):
        
        # try to find candidates on online slaves
        candidates = [v for v in vids if any([s.online for s in v.slaves])]
        
        # no online candidate, just take one by random.
        if len(candidates) == 0:
            candidates = vids
        
        return __rand.choice(candidates).variant
        
    return None
        
@require_GET
@viewutils.get_params('name', 'variant', download=viewutils.boolConverter, redirect=viewutils.boolConverter, details=viewutils.boolConverter)
def requestVariant(request, name, variant, download=False, redirect=True, details=False):
    """
    Requests a video. This method either returns a redirect to the 
    slave which is supposed do do the streaming OR it returns its url.
    """
    vid = videoDao.findVideo(name, variant, loadSlaves=True)
    if not vid:
        return HttpResponseNotFound('Video not found in requested variant.')
    
    if len(vid.slaves) == 0:
        return HttpResponseNotFound('Video seems not to be stored on any slave.')
    # get online slaves
    onlineSlaves = filter(lambda x:x.online, vid.slaves)
    
    # no slave only
    if len(onlineSlaves) == 0:
        return _attemptBootSlave(vid)
    

    targetSlave = __rand.choice(onlineSlaves)
    
    redirectUrl = 'http://%s/%s?download=%s' % (portmapperService.getMappedSlave(targetSlave.name, request.META['REMOTE_ADDR']), vid.fname, download)
    hitTracker.hit(vid.id)
    if redirect:
        response = HttpResponseRedirect(redirectUrl)
        if details:
            response.content = json.dumps(viewutils.domainObjectToJson(vid), default=viewutils._date_handler)
        return response
    else:
        return HttpResponse(redirectUrl)
    
def _attemptBootSlave(video):
    """
    Tries to boot a slave that contains passed video (assuming the video's slaves are
    already loaded from db and passed to the method.
    """
    # avoid booting multiple servers for the video
    if not any(slave.status == 'boot' for slave in video.slaves):
        
        # extract "offline" servers
        bootableSlaves = [s for s in video.slaves if s.status == 'offline']
        
        # error if nothing found
        if len(bootableSlaves) == 0:
            return HttpResponseNotFound("No server with that video is currently available (and functioning). Sorry.")
        else:
            # select one and boot.
            slave = __rand.choice(bootableSlaves)
            viewutils.blockingCallInReactor(taskDao.addLocalActionTask, 'Booting slave %s' % slave.name, localtaskservice.LocalTaskService.turnOnSlave, args=[slave.name])
            
    # notify with status=102
    return viewutils.HttpResponseProcessing("Server is being started. Wait sometime... You'll be redirected if the slave is up.")

def createVideoStats():
    now = time.time()
    hitMatrix = hitTracker.constructHitMatrix(now)
    histogram = hitcounter.createHitHistogram(hitMatrix)
    
    totalVideos = videoDao.getTotalNumberOfVideos()
    missingVids = max(0, totalVideos - histogram.index.size)
    histogram.loc[0, 'total'] += missingVids
    histogram.loc[0, 'minute'] += missingVids
    histogram.loc[0, '30minutes'] += missingVids
    histogram.loc[0, 'hour'] += missingVids
    histogram.loc[0, 'day'] += missingVids
    
    return histogram

@require_GET
@viewutils.json_response
def getVideoStats(request):
    
    hist = createVideoStats()
    
    values = {
        'lastminute':zip(hist.index.values, hist['minute']),
        'last30minutes':zip(hist.index.values, hist['30minutes']),
        'lasthour':zip(hist.index.values, hist['hour']),
        'total':zip(hist.index.values, hist['total']),
        }
    
    return viewutils.createJsonResponse(request, values)

@require_GET
def showVideoStats(request):
    histogram = createVideoStats()
     
    template = get_template('videostats.html')
    ctx = Context({
                   'lastminute':zip(histogram.index.values, histogram['minute']),
                   'last30minutes':zip(histogram.index.values, histogram['30minutes']),
                   'lasthour':zip(histogram.index.values, histogram['hour']),
                   'total':zip(histogram.index.values, histogram['total']),
                   })
    return HttpResponse(template.render(ctx))
