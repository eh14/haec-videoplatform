'''
Created on Jun 6, 2014

@author: eh14
'''
from django.http.response import HttpResponseRedirect, HttpResponse
from django.template.loader import get_template
from django.template.context import Context
from django.views.decorators.http import require_GET
from haec import config
from django.core.urlresolvers import reverse
import datetime
import time
from haec.services import looperservice, optimizerservice, portmapperservice, \
    sensorservice
import pythonioc
import json
from web.views import statistics, opt, viewutils
from haec.frontend import events

cfg = pythonioc.Inject(config.Config)
looperService = pythonioc.Inject(looperservice.LooperService)

sensorstore = pythonioc.Inject('sensorStore')
sensorService = pythonioc.Inject(sensorservice.MasterSensorService)
optimizerService = pythonioc.Inject(optimizerservice.OptimizerService)
portmapperService = pythonioc.Inject(portmapperservice.PortMapperService)
eventBuffer = pythonioc.Inject(events.EventBroadcastBuffer)

def redirectToHome(request):
    return HttpResponseRedirect(reverse(showHome))

@require_GET
def showHome(request):
    
    template = get_template('home/index.html')
    
    
    ctx = Context({'cfg':cfg.vals,
                   'systemstart_time': datetime.datetime.fromtimestamp(cfg.vals.system_start),
                   'system_uptime': int(time.time() - cfg.vals.system_start),
                   'slave_mapping':portmapperService.getSlaveMapping(),
                   'internal_net':portmapperService.getInternalAddress(),
                   'external_net':portmapperService.getExternalAddress(),
                   })
    
    return HttpResponse(template.render(ctx))


@require_GET
def showDashboard(request):
    template = get_template('home/dashboard.html')
    
    
    wlgenWebUrl = 'http://%s:%s' % (request.session.get('wlgen_host', cfg.vals.wlgen_defaulthost),
                                      request.session.get('wlgen_webport', cfg.vals.wlgen_defaultport))
    
    wlgenSocketUrl = 'http://%s:%s' % (request.session.get('wlgen_host', cfg.vals.wlgen_defaulthost),
                                      request.session.get('wlgen_socketport', str(int(cfg.vals.wlgen_defaultport) + 1)))
    
    # URL to the videoplatform socket
    vpWebsocketUrl = 'http://%s:%s' % (portmapperService.getSystemAddress(request.META['REMOTE_ADDR']), cfg.vals.websocket_port)
    
    ctx = Context({
                   'powervalues': json.dumps(viewutils.blockingCallInReactor(sensorService.getAllPowerSum)),
                   'avg_cpu_usage': json.dumps(viewutils.blockingCallInReactor(sensorService.getAllCpuUsage)),
                   'sum_network_out':json.dumps(viewutils.blockingCallInReactor(sensorService.getAllNwOutSum)),
                   'currentStrategyName':optimizerService.getCurrentStrategyName(),
                   'optStrategies' : optimizerService.getStrategyNames(),
                   'wlgenWebUrl':wlgenWebUrl,
                   'wlgenSocketUrl':wlgenSocketUrl,
                   'websocketUrl':vpWebsocketUrl,
                   'initialClusterState':json.dumps(statistics._getClusterState()),
                   'bufferedEvents':json.dumps(viewutils.blockingCallInReactor(eventBuffer.getAllEvents))
                   })
    
    return HttpResponse(template.render(ctx))
