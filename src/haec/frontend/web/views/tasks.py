'''
Views/methods to manage tasks.
'''
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods, require_GET
import viewutils
from django.http.response import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.template.context import Context
from django.template.loader import get_template
import pythonioc
from haec import services
from haec.services import slaveconnservice, eventservice, localtaskservice
from haec.protomixins import video


taskDao = pythonioc.Inject('taskDao')
slaveDao = pythonioc.Inject('slaveDao')
timeProvider = pythonioc.Inject(services.TimeProvider)
slaveConnService = pythonioc.Inject(slaveconnservice.SlaveConnService)
events = pythonioc.Inject(eventservice.EventService)
localTaskService = pythonioc.Inject(localtaskservice.LocalTaskService)

@require_GET
def index(request):
    return HttpResponseRedirect(reverse(showMasterTasks))

@require_GET
def showMasterTasks(request):
    template = get_template('mastertasks.html')
    ctx = Context()
    return HttpResponse(template.render(ctx))

@csrf_exempt
@require_http_methods(['PUT'])
def removeAllTasks(request):
    taskDao.deleteAll()
    return HttpResponse('ok')

@require_GET
@viewutils.get_params('format')
def listTasks(request, format='json'):
    tasks = taskDao.getAllTasks()
        
    taskList = viewutils.domainObjectsToList(tasks)

    if format == 'json':
        return viewutils.createJsonResponse(request, taskList)
    else:
        return viewutils.createCsvResponse(request, taskList)
    
@require_GET
@viewutils.json_response
def masterTaskList(request):
    tasks = taskDao.getLocalTasks()
    return viewutils.domainObjectsToList(tasks)
    

@require_GET
@viewutils.get_params('taskId', 'format')
def taskDetails(request, taskId, format='json'):
    task = taskDao.getTaskById(taskId)
    
    taskList = viewutils.domainObjectsToList([task])
    
    if format == 'json':
        return viewutils.createJsonResponse(request, taskList[0])
    else:
        return viewutils.createCsvResponse(request, taskList)
    
