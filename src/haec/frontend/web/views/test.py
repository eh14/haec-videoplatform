from django.http.response import HttpResponse
from django.template.loader import get_template
from django.template.context import Context
from django.views.decorators.http import require_GET
from web.views import statistics
import json

@require_GET
def test(request, name):
    if name == 'sysiconstest':
        return sysiconstest(request)
    
    template = get_template('tests/%s.html' % name)
    
    ctx = Context({})
    return HttpResponse(template.render(ctx))


@require_GET
def sysiconstest(request):
    template = get_template('tests/sysiconstest.html')
    
    ctx = Context({'initialState':json.dumps(statistics._getClusterState())
                   })
    return HttpResponse(template.render(ctx))