'''

@author: eh14
'''
import pythonioc
from haec.services import mastervideoservice, optimizerservice
from django.views.decorators.http import require_GET
from web.views import viewutils
import web.views.video as video_view
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect, HttpResponseNotFound

__samples = []

NUM_SAMPLES = 1000

videoService = pythonioc.Inject(mastervideoservice.MasterVideoService)
optimizerService = pythonioc.Inject(optimizerservice.OptimizerService)
sensorStore = pythonioc.Inject('sensorStore')

@require_GET
def randomVideo(request):
    try:
        videoName = viewutils.blockingCallInReactor(videoService.getRandomVideoName)
    except RuntimeError:
        return HttpResponseNotFound('No videos available.')
    
    args = []
    for argKey, argVals in request.GET.iteritems():
        for argVal in argVals:
            args.append('%s=%s' % (argKey, argVal))
            
    redirectUrl = reverse(video_view.requestVariant) + ('?name=' + videoName + '&' + '&'.join(args))
    response = HttpResponseRedirect(redirectUrl)
    response.content = videoName
    return response

@viewutils.json_response
@require_GET
def serverStats(request):
    values = sensorStore.getRecentPowerValues()
    values['average_cpu'] = sensorStore.cpuAvg.getLast()
    values['network_load'] = sensorStore.nwOutSum.getLast()
    
    return values
