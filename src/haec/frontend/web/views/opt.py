'''

@author: eh14
'''
from django.views.decorators.http import require_GET, require_POST
from django.template.loader import get_template
from django.template.context import Context
from django.http.response import HttpResponse, HttpResponseRedirect
import pythonioc
from haec.optimization import optmodel, snapshotcreator
from haec.services import activityservice, optimizerservice, looperservice, \
    statisticservice, topologyservice
import json
from web.views import viewutils
from django.views.decorators.csrf import csrf_exempt
from django.core.urlresolvers import reverse
from haec.frontend import migrationevents
import collections

slaveDao = pythonioc.Inject('slaveDao')

optProvider = pythonioc.Inject(optmodel.OptModelProvider)
activityService = pythonioc.Inject(activityservice.ActivityService)
statisticService = pythonioc.Inject(statisticservice.StatisticService)
timeProvider = pythonioc.Inject('timeProvider')

optimizerService = pythonioc.Inject(optimizerservice.OptimizerService)
looperService = pythonioc.Inject(looperservice.LooperService)
snapshotCreator = pythonioc.Inject(snapshotcreator.SystemSnapshotCreator)
migrationEvents = pythonioc.Inject(migrationevents.MigrationEventService)
topologyService = pythonioc.Inject(topologyservice.TopologyService)
@require_GET
def showOverview(request):
    template = get_template('opt/overview.html')
    ctx = Context({
                   'currentStrategyName':optimizerService.getCurrentStrategyName(),
                   'optStrategies' : optimizerService.getStrategyNames(),
                   'resurrection_running':looperService.slaveResurrectorLooper.running,
                   })
    return HttpResponse(template.render(ctx))


@require_GET
def showStatistics(request):
    template = get_template('opt/stats.html')
    
    slaveClasses = activityService.getSlaveClasses()
    migEvents = migrationEvents.getMigrations()
    # count the number of videos mgirated from one slave to another
    migrationCounter = collections.defaultdict(lambda: 0)
    
    # collect source and target slave names
    srcSlaves = set()
    destSlaves = set()

    # count them
    for migEvent in migEvents:
        migrationCounter[(migEvent['oldSlave'], migEvent['newSlave'])] += 1
        srcSlaves.add(migEvent['oldSlave'])
        destSlaves.add(migEvent['newSlave'])
        
    # sort both lists    
    srcSlaves = list(srcSlaves)
    srcSlaves.sort()
    destSlaves = list(destSlaves)
    destSlaves.sort()
    links = []
    for key, num in migrationCounter.iteritems():
        links.append({'source':srcSlaves.index(key[0]),
                     'target':destSlaves.index(key[1]) + len(srcSlaves),
                     'value':num
                     })
        
        
    snap = viewutils.blockingCallInReactor(snapshotCreator.getLastSnapshot)
    
    if snap is not None:
        popVids, numUnpopularVids = _getPopularityValues(snap)
    else:
        popVids = []
        numUnpopularVids = 0
        
    
    ctx = Context({'numberMigrations':len(migEvents),
                   'nodes':json.dumps([{'name':n,
                                        'popClass':slaveClasses.get(n, 'B')
                                        } for n in srcSlaves + destSlaves]),
                   'links':json.dumps(links),
                   'popVids':json.dumps(popVids),
                   'numUnpopularVids':numUnpopularVids,
                   'optmodel':optProvider.model,
                   })
    
    return HttpResponse(template.render(ctx))
    
def _getPopularityValues(snapshot):
    """
    @return: a tuple (<desc sorted list of popularities>, <num all videos>)
    """
    
    popVids = snapshot.mapping.loc[snapshot.mapping['popularity'] > 0, 'popularity'].values.tolist()
    return (popVids, snapshot.mapping.index.size - len(popVids))
    
@require_POST
@csrf_exempt
@viewutils.post_params(enabled=viewutils.boolConverter)
def enableResurrection(request, enabled):
    if enabled:
        looperService.startResurrectorLooper()
    else:
        looperService.stopResurrectorLooper()
        
    return HttpResponseRedirect(reverse(showOverview))

@require_POST
@csrf_exempt
@viewutils.post_params('optStrategy')
@viewutils.get_params('optStrategy')
def changeStrategy(request, optStrategy):
    viewutils.blockingCallInReactor(optimizerService.activateStrategy, optStrategy)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
