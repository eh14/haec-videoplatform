"""
Video specific views
"""
from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST, require_GET, \
    require_http_methods
import pythonioc
from twisted.internet import defer

from haec.protomixins import video
from haec.services import taskservice, slaveconnservice
import viewutils


taskDao = pythonioc.Inject('taskDao')
taskService = pythonioc.Inject(taskservice.TaskService)
videoDao = pythonioc.Inject('videoDao')
slaveConnService = pythonioc.Inject(slaveconnservice.SlaveConnService)
videoHitTracker = pythonioc.Inject('hitTracker')


@require_POST
@csrf_exempt
@viewutils.get_params('slaveName')
def rescanVideoDir(request, slaveName=None):
    @defer.inlineCallbacks
    def sendRequest():
        if slaveName:
            slaves = [slaveConnService.getConnectedSlaveByName(slaveName)]
        else:
            slaves = slaveConnService.getAllConnectedSlaves()
            
        for slave in slaves:
            yield video.MasterVideoMixin.sendVideoListRequest(slave)
        
    viewutils.callInReactor(sendRequest)
    return HttpResponse('Ok, but that might take a while...')

@require_GET
@viewutils.get_params('format')
def getAllVideos(request, format='json'):
    vidList = videoDao.getVideos()
    dictVidList = viewutils.domainObjectsToList(vidList)
    if format == 'json':
        return viewutils.createJsonResponse(request, dictVidList)
    else:
        return viewutils.createCsvResponse(request, dictVidList)

@require_POST
@csrf_exempt
@viewutils.get_params('taskId', 'srcName')
def taskAdd(request, taskId, srcName):
    viewutils.blockingCallInReactor(taskService.addExtrinsicTask, taskId, name='ITEST-Task',
                                   srcName=srcName)
    
    return HttpResponse('ok')


@csrf_exempt
@require_http_methods(["PUT"])
@viewutils.get_params('taskId', 'estimatedTime', 'hosts')
def taskStart(request, taskId, estimatedTime=0, hosts=''):
    host = hosts.split(',')[0]
    
    viewutils.callInReactor(taskDao.updateTaskStarted, taskId, estimatedTime, location=host)
    
    return HttpResponse('ok')

@csrf_exempt
@require_http_methods(["PUT"])
@viewutils.get_params('taskId', 'file', 'slaveName', 'message', success=viewutils.boolConverter)
def taskFinish(request, taskId, success, file='', slaveName='', message='Unknown'):
    viewutils.callInReactor(taskDao.updateTaskFinished,
                            taskId,
                            success=success,
                            message=message)
            
    return HttpResponse("ok")

@require_POST
@csrf_exempt
def resetVideoHits(request):
    """
    Resets the video hit counter in order to create a defined
    distribution of video hits for experiments.
    """
    viewutils.callInReactor(videoHitTracker.reset)
    
    return HttpResponse('OK')
