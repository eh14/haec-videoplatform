"""
Some utils...
"""
from django.http.response import HttpResponse, HttpResponseBadRequest
import json
from functools import wraps
from haec import config, utils
import sys
import traceback
import pythonioc
from twisted.python import threadable
from twisted.internet import threads
cfg = pythonioc.Inject(config.Config)

def _date_handler(obj):
    """
    Handles converting datetimes to json.
    Taken from
    http://blog.codevariety.com/2012/01/06/python-serializing-dates-datetime-datetime-into-json/
    """
    return obj.isoformat(' ') if hasattr(obj, 'isoformat') else obj

def toPrettyJson(value):
    return json.dumps(value, indent=4, separators=(',', ': '))


def createJsonResponse(request, response):
    if isinstance(response, HttpResponse):
        return response
    data = json.dumps(response, default=_date_handler)
    if 'callback' in request.REQUEST:
        # a jsonp response!
        data = '%s(%s);' % (request.REQUEST['callback'], data)
        return HttpResponse(data, "text/javascript")
    
    return HttpResponse(data, "application/json")


def createCsvResponse(request, objList):
    if len(objList) == 0:
        return HttpResponse('')
    
    headers = objList[0].keys()
    response = [';'.join(headers)]
    for obj in objList:
        vals = [str(obj[h])if (h in obj) else '' for h in headers]
        response.append(';'.join(vals))
        
    return HttpResponse('\n'.join(response))
#
# django decorator that transforms simple return values into 
# a json response.
# taken from https://coderwall.com/p/k8vb_a
def json_response(func):
    """
    A decorator thats takes a view response and turns it
    into json. If a callback is added through GET or POST
    the response is JSONP.
    """
    @wraps(func)
    def decorator(request, *args, **kwargs):
        response = func(request, *args, **kwargs)
        return createJsonResponse(request, response)
        
    return decorator

def boolConverter(value):
    return value.lower() in ['true', '1', 'yes']

def intConverter(value, default=0):
    return int(round(float(value)))

def _createParamExtractor(paramType, *params, **typedParams):
    assert paramType in ['GET', 'POST']
    
    def extractor(func):
        
        errorHandler = lambda error: HttpResponseBadRequest(error)
        if '__errorHandler__' in typedParams:
            errorHandler = typedParams['__errorHandler__']
            del typedParams['__errorHandler__']
        
        @wraps(func)
        def decorator(request, *origArgs, **origKwargs):
            
            valueDict = getattr(request, paramType)
            kwargs = {name:valueDict.get(name, None) for name in params if name in valueDict}
            
            def addValue(convItem):
                (name, conv) = convItem
                if name in valueDict:
                    kwargs[name] = conv(valueDict.get(name, None))
                
            map(addValue, typedParams.iteritems())

            try:
                kwargs.update(origKwargs)
                return func(request, *origArgs, **kwargs)
            except TypeError as e:
                tb = traceback.extract_tb(sys.exc_info()[2])
                if len(tb) > 1:
                    raise
                return errorHandler(e)
            
        return decorator
    
    return extractor

def get_params(*params, **typedParams):
    return _createParamExtractor('GET', *params, **typedParams)

def post_params(*params, **typedParams):
    return _createParamExtractor('POST', *params, **typedParams)

def domainObjectToJson(obj):
    return {col.name : getattr(obj, col.name) 
             for col in obj.__table__.columns}
def domainObjectsToList(domainObjects):
    return [ domainObjectToJson(obj) for obj in domainObjects]

def getDomainObjectColumns(domainObject):
    return [col.name for col in domainObject.__table__.columns]


_reactorOverride = utils.ThreadLocalBool()

reactor = pythonioc.Inject('reactor')
def callInReactor(func, *args, **kwargs):
    if threadable.isInIOThread():# or _reactorOverride.getValue():
        reactor.callLater(0, func, *args, **kwargs)
    else:
        reactor.callFromThread(func, *args, **kwargs)
        
def blockingCallInReactor(func, *args, **kwargs):
    if threadable.isInIOThread():# or _reactorOverride.getValue():
        return func(*args, **kwargs)
    else:
        return threads.blockingCallFromThread(reactor, func, *args, **kwargs)
        
        
class HttpResponseProcessing(HttpResponse):
    """
    Response 102 "Processing" (http://tools.ietf.org/html/rfc2518)
    
    The server is still prcoessing the request.
    """
    
    status_code = 102
    
class HttpResponseUnavailable(HttpResponse):
    """
    Response 503 "Service unavailable".
    """
    status_code = 503
