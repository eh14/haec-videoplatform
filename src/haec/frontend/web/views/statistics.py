'''
Created on Jun 6, 2014

@author: eh14
'''


from django.template.loader import get_template
from django.template.context import Context
from django.http.response import HttpResponse, HttpResponseBadRequest, \
    HttpResponseRedirect
import viewutils
from django.views.decorators.http import require_GET, require_POST
import time
from django.views.decorators.csrf import csrf_exempt
from django.core.urlresolvers import reverse
from twisted.python import log
import json
import pythonioc
from haec.services import slaveconnservice, cambrionixservice, activityservice, \
    localtaskservice, topologyservice, statisticservice
from haec.protomixins import hardware
from twisted.internet import defer
from haec.optimization import optmodel

cambrionixConnector = cambrionixservice.CambrionixConnector()

slaveDao = pythonioc.Inject('slaveDao')
taskDao = pythonioc.Inject('taskDao')
slaveConnService = pythonioc.Inject(slaveconnservice.SlaveConnService)
activityService = pythonioc.Inject(activityservice.ActivityService)
reactor = pythonioc.Inject('reactor')
optModel = pythonioc.Inject(optmodel.OptModelProvider)
sensorStore = pythonioc.Inject('sensorStore')
timeProvider = pythonioc.Inject('timeProvider')
topologyService = pythonioc.Inject(topologyservice.TopologyService)
statisticService = pythonioc.Inject(statisticservice.StatisticService)

@require_GET
def index(request):
    return HttpResponseRedirect(reverse(showSlaveList))
    
def showStatistics(request):
    template = get_template('dashboard/statistics.html')
    
    ctx = Context({'slaveBootTimes':viewutils.blockingCallInReactor(statisticService.getSlaveBootTimes),
                   })
    
    return HttpResponse(template.render(ctx))

def showSlaveList(request):
    template = get_template('dashboard/slavelist.html')
    ctx = Context()
    return HttpResponse(template.render(ctx))


@viewutils.json_response
@require_GET
def getSlaveStatus(request):
    slaveStatus = viewutils.blockingCallInReactor(slaveDao.getAllSlaveStatus)
    slaveNames = topologyService.getNodeNameList()
    
    # get newest sensor/power data.
    sensorData = sensorStore.getRecentSensorValues()
    powerData = sensorStore.getRecentPowerValues()
    
    slaveData = []
    for slaveName in slaveNames:
        slavePowerStatus = powerData.get(slaveName, {'status':False})['status']
        s = {'name':slaveName,
           'id':slaveName,
           'status':slavePowerStatus,
           'current':powerData.get(slaveName, {'current':0})['current'],
           'energy':powerData.get(slaveName, {'energy':0})['energy'],
           'cpu_usage':sensorData.get(slaveName, {'cpu_usage':0})['cpu_usage'],
           'memory_usage':sensorData.get(slaveName, {'memory_usage':0})['memory_usage'],
           'network_out':sensorData.get(slaveName, {'network_out':0})['network_out'],
           'network_in':sensorData.get(slaveName, {'network_in':0})['network_in']}
        
        # if it is a real slave
        if slaveName in slaveStatus:
            s['slavestatus'] = slaveStatus[slaveName]
            
        # if it is a switch, use the power info to 
        # simulate its status.
        else:
            s['slavestatus'] = 'online' if slavePowerStatus else 'offline'
        # the frontend uses "poweron", so let's just copy the "status"-field 
        # to match the name.
        s['poweron'] = s['status']
        
        slaveData.append(s)
    return slaveData

@csrf_exempt
@require_POST
@viewutils.get_params('slaveNames', 'status')
def setSlaveStatus(request, slaveNames, status):
    try:
        for slaveName in slaveNames.split(','):
            slaveName = slaveName.strip()
            if not slaveName:
                continue
            
            viewutils.blockingCallInReactor(_setSingleSlaveStatus, slaveName, status)
        return HttpResponse('ok')
    except cambrionixservice.CambrionixException as e:
        log.msg('Error setting slave status ' + str(e))
        return HttpResponseBadRequest(str(e))
    except AttributeError as e:
        log.msg('Error setting slave status ' + str(e))
        return HttpResponseBadRequest(str(e))

@defer.inlineCallbacks        
def _setSingleSlaveStatus(slaveName, status):
    if status == 'on':
        yield topologyService.activateNode(slaveName)
    elif status == 'off':
        yield topologyService.deactivateNode(slaveName)
    elif status == 'shutdown':
        yield taskDao.addLocalActionTask('Shutting down Slave %s' % slaveName,
                  localtaskservice.LocalTaskService.shutdownSlave,
                  args=(slaveName,))
    else:
        raise AttributeError('Invalid Slave status %s requested' % status)

@csrf_exempt
@require_POST
@viewutils.get_params('slaveNames', 'led', 'mode')
def slaveBlink(request, slaveNames, led='blue', mode='timer'):
    for slaveName in slaveNames.split(','):
        slaveName = slaveName.strip()
        if not slaveName:
            continue
        
        def blink():
            slave = slaveConnService.getConnectedSlaveByName(slaveName)
            hardware.blinkSlave(slave)
        viewutils.callInReactor(blink)
            
    return HttpResponse('ok')

@viewutils.json_response    
@require_GET
def getClusterState(request):
    return _getClusterState()

def _getClusterState():
    powerValues = sensorStore.getRecentPowerValues()
    topology = topologyService.getTopology()
    tasksPerSlave = taskDao.getTasksPerLocation()
    slaveStatus = viewutils.blockingCallInReactor(slaveDao.getAllSlaveStatus)
    sensorData = viewutils.blockingCallInReactor(sensorStore.getRecentSensorValues)
    data = []
    
    getSlaveStatus = lambda name: slaveStatus.get(name, 'offline')
    isSlaveOnline = lambda name: getSlaveStatus(name) == 'online'
    
    sortedSwitchList = list(topology.keys())
    sortedSwitchList.sort()
    
    defaultSensorData = {'cpu_usage':0, 'network_out':0, 'memory_usage':0}
    
    
    for switchName in sortedSwitchList:
        slaves = topology[switchName]
        slaves.sort()
        
        
        children = []
        for slaveName in slaves:
            slaveActivity = activityService.getSlaveActivity(slaveName)
            
            if slaveName not in sensorData or sensorData[slaveName] is None:
                slaveSensorData = defaultSensorData
            else:
                slaveSensorData = sensorData[slaveName]
            
            shutdownIn = -1
            idleTimeout = slaveActivity.getIdleTimeout() 
            if idleTimeout:
                shutdownIn = round(slaveActivity.getIdleTimeout() - timeProvider.now())
            power = powerValues[slaveName]['current'] if slaveName in powerValues else 0
            slaveData = {'name':slaveName,
                         'power':power if getSlaveStatus(slaveName) != 'offline' else 0,
                         'status':getSlaveStatus(slaveName),
                         'popClass': slaveActivity.getClass(),
                         'download': slaveActivity.getNumStreams(),
                         'transcode': tasksPerSlave.get(slaveName, 0),
                         'shutdown':shutdownIn,
                         'cpu':slaveSensorData['cpu_usage'] if isSlaveOnline(slaveName) else 0,
                         'nw':(slaveSensorData['network_out'] / optModel.model.maxBs) if isSlaveOnline(slaveName) else 0,
                         'mem':slaveSensorData['memory_usage'] if isSlaveOnline(slaveName) else 0,
                         }
            children.append(slaveData)
            
        switchStatus = powerValues[switchName]['status'] if switchName in powerValues else 0
        switchPower = powerValues[switchName]['current'] if switchName in powerValues else 0
        switchData = {'id':switchName,
                      'name':switchName,
                      'status':switchStatus,
                      'current':switchPower,
                      'children':children
                      }
        data.append(switchData)
        
    return data
    