'''

@author: eh14
'''
from django.views.decorators.http import require_GET, require_POST
from django.template.loader import get_template
from django.http.response import HttpResponse, HttpResponseRedirect
from django.template.context import Context
from django.views.decorators.csrf import csrf_exempt
from django.core.urlresolvers import reverse
import pythonioc
from haec import config
from web.views import viewutils

cfg = pythonioc.Inject(config.Config)

@require_GET
def show(request):
    template = get_template('configuration.html')
    
    ctx = Context({
        'wlgen_host':request.session.get('wlgen_host', cfg.vals.wlgen_defaulthost),
        'wlgen_webport':request.session.get('wlgen_webport', cfg.vals.wlgen_defaultport),
        'wlgen_socketport':request.session.get('wlgen_socketport', str(int(cfg.vals.wlgen_defaultport) + 1))
    })
    
    return HttpResponse(template.render(ctx))


@csrf_exempt
@viewutils.post_params('wlgenHost', 'wlgenWebPort', 'wlgenSocketPort')
@require_POST
def change(request, wlgenHost, wlgenWebPort, wlgenSocketPort):
    request.session['wlgen_host'] = wlgenHost
    request.session['wlgen_webport'] = wlgenWebPort
    request.session['wlgen_socketport'] = wlgenSocketPort
    
    return HttpResponseRedirect(reverse(show))
