define(
        [ 'jquery', 'underscore', 'backbone', 'highcharts', 'highchartsmore' ],
        function($, _, Backbone) {
            c = {};

            Highcharts.setOptions({
                global : {
                    useUTC : false,
                }
            });

            c.DiagramView = Backbone.View
                    .extend({

                        initFinished : false,
                        initialize : function(options) {
                            this.chartOpts = options;
                            if (typeof this.chartOpts.series === 'undefined') {
                                this.chartOpts.series = [ 'max', 'avg', 'min' ];
                            }
                            this.listenTo(this.collection, 'add',
                                    this.addHandler);
                        },
                        makePoint : function(time, values, i) {
                            return {
                                'id' : time,
                                x : time,
                                y : (values instanceof Array) ? values[i]
                                        : values,
                            };
                        },
                        render : function() {
                            var chart = this.$el.highcharts({
                                chart : {
                                    type : 'line',
                                    // type : 'spline',
                                    animation : Highcharts.svg, // don't animate
                                    // in old IE
                                    height : 250,
                                },
                                plotOptions : {
                                    line : {
                                        marker : {
                                            enabled : false
                                        }
                                    },
                                },
                                title : {
                                    text : this.chartOpts.title,
                                    x : -20
                                // center
                                },
                                yAxis : {
                                    title : {
                                        text : this.chartOpts.yaxisTitle
                                    },
                                    plotLines : [ {
                                        value : 0,
                                        width : 1,
                                        color : '#808080'
                                    } ]
                                },

                                xAxis : {
                                    title : {
                                        text : 'time',
                                    },
                                    type : 'datetime',
                                },

                                legend : {
                                    layout : 'horizontal',
                                    align : 'center',
                                    verticalAlign : 'bottom',
                                    borderWidth : 0
                                }
                            });

                            var chart = this.$el.highcharts();
                            if (typeof this.chartOpts.minValue !== 'undefined'
                                    || typeof this.chartOpts.maxValue !== 'undefined') {
                                chart.yAxis[0].setExtremes(
                                        this.chartOpts.minValue,
                                        this.chartOpts.maxValue);
                            }

                            var initialSeries = [];
                            for (var i = 0; i < this.chartOpts.series.length; ++i) {
                                initialSeries.push([]);
                            }

                            this.chartOpts.initial
                                    .each(
                                            function(data) {
                                                var time = data.get('time');
                                                var values = data
                                                        .get(this.chartOpts.dataField);

                                                for (var i = 0; i < this.chartOpts.series.length; ++i) {
                                                    initialSeries[i].push(this
                                                            .makePoint(time,
                                                                    values, i));
                                                }
                                            }, this);

                            for (var i = 0; i < this.chartOpts.series.length; ++i) {
                                chart.addSeries({
                                    name : this.chartOpts.series[i],
                                    data : initialSeries[i],
                                    redraw : false,
                                });
                            }
                            this.initFinished = true;
                        },

                        addHandler : function(newItem) {
                            if (!this.initFinished) {
                                return;
                            }
                            var chart = this.$el.highcharts();
                            var time = newItem.get('time');
                            var data = newItem.get(this.chartOpts.dataField);

                            var length = 1;
                            if (data instanceof Array) {
                                length = this.chartOpts.series.length;
                                for (var i = 0; i < length; ++i) {
                                    var shift = false;
                                    if (chart.series[i].data.length > 0
                                            && chart.series[i].data[0].x < newItem.collection
                                                    .oldest()) {
                                        shift = true;
                                    }
                                    chart.series[i].addPoint(this.makePoint(
                                            time, data, i), false, shift);
                                }
                            } else {
                            }
                        },

                        redraw : function() {
                            this.$el.highcharts().redraw();
                        },

                    });

            c.MultiValueDiagram = Backbone.View
                    .extend({

                        initialize : function(options) {
                            _.extend(this, options);
                        },
                        render : function() {

                            var createAxes = function(a) {
                                var axis = {
                                    title : {
                                        text : a.name,
                                        style : {
                                            color : Highcharts.getOptions().colors[0]
                                        },
                                    }
                                };
                                _
                                        .each(
                                                [ 'min', 'max', 'opposite',
                                                        'alignTicks',
                                                        'gridLineWidth' ],
                                                function(elem, index,
                                                        collection) {
                                                    if (typeof a[elem] !== 'undefined') {
                                                        axis[elem] = a[elem];
                                                    }
                                                });
                                return axis;
                            };
                            this.$el
                                    .highcharts({
                                        chart : {
                                            type : 'line',
                                            // type : 'spline',
                                            // animation : Highcharts.svg, //
                                            // don't
                                            // animate in old IE
                                            height : 250,
                                        },
                                        title : {
                                            text : _.result(this, 'title'),
                                            x : -30
                                        },
                                        plotOptions : {
                                            line : {
                                                marker : {
                                                    enabled : false
                                                }
                                            },
                                        },
                                        xAxis : {
                                            title : {
                                                text : 'time',
                                            },
                                            type : 'datetime',
                                        },
                                        tooltip : {
                                            shared : true,
                                        },
                                        yAxis : _.map(this.axes, createAxes),
                                        legend : {
                                            layout : 'horizontal',
                                            align : 'center',
                                            verticalAlign : 'bottom',
                                            floating : true,
                                            backgroundColor : (Highcharts.theme && Highcharts.theme.legendBackgroundColor)
                                                    || '#FFFFFF'
                                        },

                                    });

                            var series = this.series;
                            var chart = this.$el.highcharts();
                            var initialSeries = [];
                            for (var i = 0; i < series.length; ++i) {
                                initialSeries.push([]);
                            }
                            _.each(this.initial, function(data, index,
                                    collection) {
                                var time = data['time'];
                                for (var i = 0; i < series.length; ++i) {
                                    initialSeries[i].push([ time,
                                            data[series[i].key] ]);
                                }
                            });

                            for (var i = 0; i < series.length; ++i) {
                                chart.addSeries({
                                    name : series[i].name,
                                    data : initialSeries[i],
                                    redraw : false,
                                    yAxis : series[i].axis
                                });
                            }
                        },

                    });

            c.RangeValueDiagram = Backbone.View
                    .extend({

                        initialize : function(options) {
                            _.extend(this, options);

                            this.listenTo(this.collection, 'add',
                                    this.addHandler);
                        },

                        extractSeriesValues : function(stat) {
                            v = stat.get(this.series_key);
                            time = stat.get('time');
                            return [ [ time, v[0], v[2] ], [ time, v[1] ] ];
                        },
                        render : function() {

                            var rangeValues = [];
                            var pointValues = [];

                            this.collection.each(
                                    function(stat) {
                                        var serieValue = this
                                                .extractSeriesValues(stat);
                                        rangeValues.push(serieValue[0]);
                                        pointValues.push(serieValue[1]);
                                    }, this);

                            this.$el.highcharts({
                                title : {
                                    text : this.title,
                                },
                                tooltip : {
                                    shared : true,
                                },
                                xAxis : {
                                    title : {
                                        text : 'time',
                                    },
                                    type : 'datetime',
                                },
                                yAxis : {
                                    title : {
                                        text : '',
                                    },
                                    min : this.minYValue,
                                    max : this.maxYValue,
                                },
                                legend : {},
                                series : [ {
                                    name : this.point_title,
                                    data : pointValues,
                                    zIndex : 1,
                                    marker : {
                                        enabled : false,
                                    },
                                }, {
                                    name : this.range_title,
                                    data : rangeValues,
                                    type : 'arearange',
                                    lineWidth : 0,
                                    linkedTo : ':previous',
                                    color : Highcharts.getOptions().colors[0],
                                    fillOpacity : 0.3,
                                    zIndex : 0,
                                } ],
                            });
                        },
                        addHandler : function(newItem) {
                            var chart = this.$el.highcharts();
                            var shift = false;
                            if (newItem.collection.length >= newItem.collection.max_entries) {
                                var shift = true;
                            }
                            var serieValue = this.extractSeriesValues(newItem);
                            chart.series[0].addPoint(serieValue[0], false,
                                    shift);
                            chart.series[1].addPoint(serieValue[1], false,
                                    shift);
                        },

                        redraw : function() {
                            this.$el.highcharts().redraw();
                        },
                    });

            c.SingleValueDiagram = Backbone.View
                    .extend({

                        initialize : function(options) {
                            _.extend(this, options);

                            this.listenTo(this.collection, 'add',
                                    this.addHandler);
                        },

                        extractSeriesValues : function(stat) {
                            v = stat.get(this.series_key);
                            time = stat.get('time');
                            return [ time, v ];
                        },
                        render : function() {

                            var pointValues = [];

                            this.collection.each(
                                    function(stat) {
                                        var serieValue = this
                                                .extractSeriesValues(stat);
                                        pointValues.push(serieValue);
                                    }, this);

                            this.$el.highcharts({
                                title : {
                                    text : this.title,
                                },
                                xAxis : {
                                    title : {
                                        text : 'time',
                                    },
                                    type : 'datetime',
                                },
                                yAxis : {
                                    title : {
                                        text : '',
                                    },
                                    min : this.minYValue,
                                    max : this.maxYValue,
                                },
                                legend : {},
                                series : [ {
                                    name : this.point_title,
                                    data : pointValues,
                                    marker : {
                                        enabled : false,
                                    },
                                } ],
                            });
                        },
                        addHandler : function(newItem) {
                            var chart = this.$el.highcharts();
                            var shift = false;
                            if (newItem.collection.length >= newItem.collection.max_entries) {
                                var shift = true;
                            }
                            var serieValue = this.extractSeriesValues(newItem);
                            chart.series[0].addPoint(serieValue, false, shift);
                        },

                        redraw : function() {
                            this.$el.highcharts().redraw();
                        },
                    });

            return c;
        });
