/**
 * draw the charts showing the experiment outcomes. This is static and quite
 * expensive. Still nicer than having it on pdf/printed or whatever
 */

require(
        [ 'common' ],
        function() {
            require(
                    [ 'underscore', 'jquery', 'highcharts', 'demoplots/expdata' ],
                    function(_, $, Highcharts, expData) {

                        var createPowerLatencyChartOpts = function(title,
                                maxLatency) {
                            var opts = {
                                chart : {
                                    type : 'line',
                                    height : 350,
                                },
                                title : {
                                    text : title,
                                },
                                plotOptions : {
                                    line : {
                                        marker : {
                                            enabled : false,
                                        },
                                        tooltip : {
                                            headerFormat : '<span style="font-size: 10px">Power Consumption</span><br/>',
                                            pointFormat : '<span style="color:{series.color}">\u25CF</span>: <b>{point.y}W</b><br/>',
                                        },
                                    },
                                    scatter : {
                                        tooltip : {
                                            headerFormat : '<span style="font-size: 10px">Latency</span><br/>',
                                            pointFormat : '<span style="color:{series.color}">\u25CF</span>: <b>{point.y}s</b><br/>',
                                        },
                                    },
                                },
                                yAxis : [
                                        {
                                            min : 0,
                                            title : {
                                                text : 'Power'
                                            },
                                            labels : {
                                                format : '{value}W',
                                            },
                                            opposite : false,
                                        },
                                        {
                                            min : 0,
                                            max : typeof maxLatency !== undefined ? maxLatency
                                                    : null,
                                            title : {
                                                text : 'Latency'
                                            },
                                            labels : {
                                                format : '{value}s',
                                            },
                                            opposite : true,
                                        } ],
                                series : [],
                            };

                            var optMaker = {
                                get : function() {
                                    return opts;
                                },
                                addLatency : function(data) {
                                    opts.series.push({
                                        name : 'Latency',
                                        type : 'scatter',
                                        data : _.sortBy(data, function(value) {
                                            return value[0];
                                        }),
                                        yAxis : 1,
                                    });
                                },
                                addPower : function(data) {
                                    opts.series.push({
                                        name : 'Power Consumption',
                                        data : data,
                                        yAxis : 0,
                                    });
                                }
                            }
                            return optMaker;
                        };

                        // All - On
                        var flatOptMaker = createPowerLatencyChartOpts('Flat Workload - All On', 5);
                        flatOptMaker.addPower(expData.allon_flat.server_power);
                        flatOptMaker
                                .addLatency(expData.allon_flat.wlgen_latency);
                        $('#allonFlatChartBox').highcharts(flatOptMaker.get());

                        var sineOptMaker = createPowerLatencyChartOpts('Sine Workload - All On', 5);
                        sineOptMaker.addPower(expData.allon_sine.server_power);
                        sineOptMaker
                                .addLatency(expData.allon_sine.wlgen_latency);
                        $('#allonSineChartBox').highcharts(sineOptMaker.get());

                        // Pop-based, without adaptation
                        var flatOptMaker = createPowerLatencyChartOpts('Flat Workload - Popularity Strategy');
                        flatOptMaker
                                .addPower(expData.pop_flat_noopt.server_power);
                        flatOptMaker
                                .addLatency(expData.pop_flat_noopt.wlgen_latency);
                        $('#popNooptFlatChartBox').highcharts(
                                flatOptMaker.get());

                        var sineOptMaker = createPowerLatencyChartOpts('Sine Workload - Popularity Strategy');
                        sineOptMaker
                                .addPower(expData.pop_sine_noopt.server_power);
                        sineOptMaker
                                .addLatency(expData.pop_sine_noopt.wlgen_latency);
                        $('#popNooptSineChartBox').highcharts(
                                sineOptMaker.get());

                        // Pop-based and Adaptation
                        var flatOptMaker = createPowerLatencyChartOpts('Flat Workload - Popularity Strategy - Adapted');
                        flatOptMaker
                                .addPower(expData.pop_flat_opt.server_power);
                        flatOptMaker
                                .addLatency(expData.pop_flat_opt.wlgen_latency);
                        $('#popOptFlatChartBox').highcharts(flatOptMaker.get());

                        var sineOptMaker = createPowerLatencyChartOpts('Sine Workload - Popularity Strategy - Adapted');
                        sineOptMaker
                                .addPower(expData.pop_sine_opt.server_power);
                        sineOptMaker
                                .addLatency(expData.pop_sine_opt.wlgen_latency);
                        $('#popOptSineChartBox').highcharts(sineOptMaker.get());
                    });
        });