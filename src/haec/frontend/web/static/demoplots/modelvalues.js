define(function() {

    var pmcValues = {
        'index' : [ 0.0, 0.01, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4,
                0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95,
                0.99 ],
        'continue as is' : [ 34.0, 34.0, 34.0, 37.0, 39.0, 41.0, 44.0, 48.0,
                null, null, null, null, null, null, null, null, null, null,
                null, null, null, null ],
        'cooldown++' : [ 33.0, 34.0, 34.0, 37.0, 38.0, 38.0, 40.0, 41.0, 41.0,
                43.0, 44.0, 45.0, 46.0, 48.0, 52.0, null, null, null, null,
                null, null, null ],
        'power up' : [ 35.0, 35.0, 35.0, 36.0, 39.0, 39.0, 41.0, 43.0, 44.0,
                46.0, 49.0, null, null, null, null, null, null, null, null,
                null, null, null ],
        'power up, cooldown++' : [ 35.0, 35.0, 35.0, 36.0, 37.0, 39.0, 40.0,
                40.0, 41.0, 42.0, 43.0, 44.0, 45.0, 46.0, 47.0, 49.0, 50.0,
                52.0, 56.0, 63.0, null, null ],
        'theoretical optimum' : [ 30.0, 32.0, 33.0, 34.0, 35.0, 36.0, 37.0,
                37.0, 38.0, 39.0, 40.0, 41.0, 42.0, 43.0, 44.0, 45.0, 46.0,
                48.0, 50.0, 54.0, 62.0, null ]
    };
    return function(target) {

        var chartOpts = {
            chart : {
                type : 'line',
                height : 400,
                width : 800,
            },
            title : {
                text : null,
            },
            plotOptions : {
                line : {
                    marker : {
                        enabled : false
                    }
                },
            },
            xAxis : {
                title : {
                    text : 'probability threshold p',
                },
                min : 0,
                max : 1,
            },
            tooltip : {
                shared : true,
                crosshairs : false,
            },
            yAxis : [ {
                title : {
                    text : 'minimal energy e',
                },
                // min : 0,
                // max : 100,
                opposite : false,
                labels : {
                    format : '{value}',
                },
            } ],
            series : [],
            legend : {
                enabled : true,
                layout : 'horizontal',
                align : 'center',
                verticalAlign : 'bottom',
                floating : false,
            },
        };

        var pmcSeries = [ {
            name : 'continue as is',
            color : '#1C4AD4',
            lineWidth : 2,
        }, {
            name : 'power up',
            color : '#829DED',
            lineWidth : 2,
            dashStyle : 'longdash'
        }, {
            name : 'cooldown++',
            color : '#118204',
            lineWidth : 2,
        }, {
            name : 'power up, cooldown++',
            color : '#3BCC2B',
            lineWidth : 2,
            dashStyle : 'longdash'
        }, {
            name : 'theoretical optimum',
            color : '#8C8C8C',
            lineWidth : 3
        } ];
        _.each(pmcSeries, function(series) {
            chartOpts.series.push({
                name : series.name,
                data : _.zip(pmcValues.index, pmcValues[series.name]),
                color : series.color,
                lineWidth : series.lineWidth,
                dashStyle : series.dashStyle,
            });
        });

        $(target).highcharts(chartOpts);
    };
});
