require([ 'common' ], function() {
    require([ 'jquery', 'underscore', 'backgrid', 'backbone' ], function($) {

        $(function() {

            var models = {};

            models.Video = Backbone.Model.extend({
                defaults : {
                    id : 0,
                    name : '',
                    variant : '',
                    width : '',
                    height : '',
                    video_codec : '',
                    audio_codec : '',
                    duration : '',
                    bitrate : '',
                    fps : '',
                    format : '',
                    fname : '',
                },

                initialize : function() {
                    this.set('size', (this.get('width') + 'x' + this
                            .get('height')));
                },

            });

            models.SearchResult = Backbone.Collection.extend({
                model : models.Video,
                url : urls('searchVideo'),

            });
            var resultCollection = new models.SearchResult();

            var SearchForm = Backbone.View.extend({
                el : '#searchForm',

                // form fields
                namePatternInput : $('#namePatternInput'),
                videoCodec : $('#videoCodecSelect'),
                audioCodec : $('#audioCodecSelect'),

                // some events
                events : {
                    'click #submitSearch' : function() {
                        resultCollection.fetch({
                            // reset : true,
                            error : function(model, xhr, options) {
                                console.log('Fetch error');
                            },
                            data : $.param({
                                'namePattern' : this.namePatternInput.val(),
                                'audio_codec' : this.audioCodec.find(
                                        ':selected').val(),
                                'video_codec' : this.videoCodec.find(
                                        ':selected').val(),
                            })
                        });
                    }
                },

            });

            var grid = new Backgrid.Grid({
                columns : [ {
                    name : "detailUrl",
                    label : 'Name',
                    cell : "string",
                    direction : "ascending",
                    editable : false,
                    cell : Backgrid.UriCell.extend({
                        formatter : {
                            fromRaw : function(rawData, model) {
                                return model.get('name');
                            },
                        },
                        target : '_self',
                    }),

                }, {
                    name : "size",
                    label : 'Size',
                    cell : "string",
                    editable : false,
                }, {
                    name : "video_codec",
                    label : 'Video',
                    cell : "string",
                    editable : false,
                }, {
                    name : "audio_codec",
                    label : 'Audio',
                    cell : "string",
                    editable : false,
                }, {
                    name : "format",
                    label : 'Container',
                    cell : "string",
                    editable : false,
                }, {
                    name : "fps",
                    label : 'Frames per sec.',
                    cell : "string",
                    editable : false,
                }, {
                    name : "bitrate",
                    label : 'Bitrate',
                    cell : "string",
                    editable : false,
                }, ],
                collection : resultCollection,
                className : "backgrid table",
            });

            // var paginator = new Backgrid.Extension.Paginator({
            // collection: resultCollection
            // });

            $("#searchResults").append(grid.render().el);
            // console.log(paginator.render());
            // $("#searchResultPaginator").append();

            searchForm = new SearchForm();
        });
    });
});