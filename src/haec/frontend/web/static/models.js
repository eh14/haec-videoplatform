define([ 'jquery', 'backbone', 'underscore' ], function($, Backbone, _) {

    var models = {}
    var defaultAutoshiftLength = 60 * 1000;
    models.AutoShiftingCollection = Backbone.Collection.extend({
        period : defaultAutoshiftLength,
        latest : $.now() - defaultAutoshiftLength,

        initialize : function(data, options) {
            this.listenTo(this, 'add', this.addHandler)
            _.extend(this, options);
        },
        addHandler : function(added) {
            if (added.get('time') > this.latest) {
                this.latest = added.get('time') + 1000;
            }
            var earliest = this.oldest();
            while (1) {
                if (this.length > 0 && this.at(0).get('time') < earliest) {
                    this.shift();
                } else {
                    break;
                }
            }
        },
        fetchLatest : function(doneCb) {
            this.fetch({
                remove : false,
                // reset : true,
                data : {
                    rangeFrom : this.latest / 1000,
                },
                success : doneCb,
            });
        },

        oldest : function() {
            return $.now() - _.result(this, 'period');
        }
    });

    models.StatisticValue = Backbone.Model.extend({
        allowToEdit : function(account) {
            return false;
        }
    });

    models.StatisticValues = models.AutoShiftingCollection.extend({
        model : models.StatisticValue,
    });

    models.TaskStatValue = Backbone.Model.extend({
        allowToEdit : function(account) {
            return false;
        }
    });

    models.TaskStatValues = models.AutoShiftingCollection.extend({
        period : 600000,
        model : models.StatisticValue
    });

    return models;
});