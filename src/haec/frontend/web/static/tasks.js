require([ 'common' ], function() {

    require([ 'jquery', 'backgrid', 'backbone', 'moment' ], function($, Backgrid,
            Backbone, moment) {
        $(function() {

            var models = {};

            models.Task = Backbone.Model.extend({
                allowedToEdit : function(account) {
                    return false;
                }
            });

            models.TaskList = Backbone.Collection.extend({
                model : models.Task,

                url : function(param) {
                    return urls('masterTaskList');
                }
            });

            var lastDateFormatter = {
                fromRaw : function(rawValue, model) {
                    lastTime = model.get('finished') || model.get('started')
                            || model.get('created');
                    if (lastTime == null || lastTime.length == 0) {
                        return '';
                    }
                    var m = moment(new Date(lastTime));
                    return m.format('HH:mm:ss') + ' (' + m.fromNow() + ')';
                },
                toRaw : function(formatted, model) {
                    return formatted;
                }
            };

            var taskList = new models.TaskList();

            var taskColumns = [
                    {
                        name : "name",
                        label : "name",
                        cell : "string",
                        editable : false,
                    },
                    {
                        name : "status",
                        label : "Status",
                        cell : "string",
                        editable : false,
                    },
                    {
                        name : "created",
                        label : "Date",
                        editable : false,
                        cell : Backgrid.DatetimeCell.extend({
                            formatter : lastDateFormatter,
                        }),
                    },
                    {
                        name : 'location',
                        label : 'Location',
                        editable : false,
                        cell : 'string',
                    },
                    {
                        name : "message",
                        label : "Message",
                        editable : false,
                        cell : Backgrid.StringCell.extend({
                            formatter : {
                                fromRaw : function(rawValue, model) {
                                    rawValue = new Backgrid.StringFormatter()
                                            .fromRaw(rawValue, model);
                                    if (rawValue.length > 40) {
                                        rawValue = rawValue.substring(0, 40)
                                                + '...';
                                    }
                                    return rawValue;
                                },
                                toRaw : function(formatted, model) {
                                    return formatted;
                                }
                            },
                        })
                    }, ];

            // Initialize a new Grid instance
            var grid = new Backgrid.Grid({
                columns : taskColumns,
                collection : taskList,
                className : 'table table-condensed backgrid'
            });
            grid.sort('created', 'descending');

            // Render the grid and attach the root to your HTML document
            $("#taskTable").append(grid.render().el);

            taskList.fetch({
                reset : true,
                error : function(model, xhr, options) {
                    console.log('Fetch error');
                }
            });

            window.setInterval(function() {
                taskList.fetch();
            }, 5000);

            $('#deleteAllTasks').click(function() {
                $.ajax({
                    type : 'PUT',
                    url : urls('removeAllTasks'),
                    success : function(result, status, xhr) {
                        taskList.fetch();
                    }
                });
            });

        });
    });
})
