require(
        [ 'common' ],
        function() {
            require(
                    [ 'underscore', 'backbone', 'backgrid', 'backgridSelect' ],
                    function() {
                        $(function() {

                            var models = {};

                            models.SlaveStatus = Backbone.Model.extend({
                                allowedToEdit : function(account) {
                                    return false;
                                }
                            });

                            models.SlaveStatusList = Backbone.Collection
                                    .extend({
                                        model : models.SlaveStatus,

                                        url : function(param) {
                                            return urls('dashboardSlaveStatus');
                                        },

                                        defaults : {
                                            id : 0,
                                            name : '',
                                        }
                                    });

                            var slaveStatusList = new models.SlaveStatusList();

                            var ButtonCell = Backgrid.Cell
                                    .extend({

                                        buttonText : 'ButtonText',
                                        url : '',
                                        method : 'POST',
                                        enabled : true,
                                        template : _
                                                .template('<button class="btn btn-default" <%=disabled%>><%=buttonText%></button>'),
                                        events : {
                                            "click" : "execute"
                                        },

                                        execute : function() {
                                            $.ajax(_.result(this, 'url'),
                                                    {
                                                        type : _.result(this,
                                                                'method'),
                                                    });
                                        },
                                        render : function() {
                                            this.$el.html(this.template({
                                                buttonText : _.result(this,
                                                        'buttonText'),
                                                disabled : _.result(this,
                                                        'enabled') ? ''
                                                        : 'disabled',
                                            }));
                                            this.delegateEvents();
                                            return this;
                                        }
                                    });

                            var slaveColumns = [
                                    {
                                        name : "",
                                        cell : "select-row",
                                        headerCell : "select-all",
                                    },
                                    {
                                        name : "name",
                                        label : "Name",
                                        cell : "string",
                                        editable : false,
                                    },
                                    {
                                        name : "cpu_usage",
                                        label : "CPU Utilization",
                                        editable : false,
                                        cell : Backgrid.PercentCell.extend({
                                            multiplier : 100
                                        }),
                                    },
                                    {
                                        name : "memory_usage",
                                        label : "Memory",
                                        editable : false,
                                        cell : Backgrid.PercentCell.extend({
                                            multiplier : 100
                                        }),
                                    },
                                    {
                                        name : "current",
                                        label : "Current drawn (mA)",
                                        editable : false,
                                        cell : Backgrid.IntegerCell.extend({
                                            decimals : 0
                                        })
                                    },
                                    {
                                        name : "energy",
                                        label : "Consumed Energy(Wh)",
                                        editable : false,
                                        cell : Backgrid.IntegerCell.extend({
                                            decimals : 0
                                        })
                                    },
                                    {
                                        name : "slavestatus",
                                        label : "Status",
                                        editable : false,
                                        cell : "string"
                                    },
                                    {
                                        name : "poweron",
                                        label : "Power On",
                                        editable : false,
                                        cell : "string"
                                    },
                                    {
                                        name : 'online',
                                        label : '',
                                        editable : false,
                                        cell : ButtonCell
                                                .extend({
                                                    isOnline : function() {
                                                        return this.model
                                                                .get('poweron');
                                                    },
                                                    buttonText : function() {
                                                        return this.isOnline() ? 'turn OFF'
                                                                : 'turn ON';
                                                    },
                                                    url : function() {
                                                        var statusParam = this
                                                                .isOnline() ? 'off'
                                                                : 'on';
                                                        return urls('dashboardSetSlaveStatus')
                                                                + "?slaveNames="
                                                                + this.model
                                                                        .get('name')
                                                                + "&status="
                                                                + statusParam
                                                    },
                                                })
                                    },
                                    {
                                        name : 'shutdown',
                                        label : '',
                                        editable : false,
                                        cell : ButtonCell
                                                .extend({
                                                    enabled : function() {
                                                        return this.model
                                                                .get('poweron');
                                                    },
                                                    buttonText : 'Shutdown',
                                                    url : function() {
                                                        return urls('dashboardSetSlaveStatus')
                                                                + "?slaveNames="
                                                                + this.model
                                                                        .get('name')
                                                                + "&status=shutdown"
                                                    },
                                                })
                                    },
                                    {
                                        name : 'online',
                                        label : '',
                                        editable : false,
                                        cell : ButtonCell
                                                .extend({
                                                    buttonText : 'Blink',
                                                    enabled : function() {
                                                        return this.model
                                                                .get('slavestatus') == 'online';
                                                    },
                                                    url : function() {
                                                        return urls('dashboardSlaveBlink')
                                                                + "?slaveNames="
                                                                + this.model
                                                                        .get('name');
                                                    }
                                                })
                                    } ];

                            // Initialize a new Grid instance
                            var grid = new Backgrid.Grid({
                                columns : slaveColumns,
                                collection : slaveStatusList,
                                className : 'table table-condensed backgrid'
                            });

                            // Render the grid and attach the root to your HTML
                            // document
                            $("#slaves-table").append(grid.render().el);

                            slaveStatusList.fetch({
                                reset : true,
                                error : function(model, xhr, options) {
                                    console.log('Fetch error');
                                }
                            });

                            grid.sort('name', 'ascending');

                            window.setInterval(function() {
                                slaveStatusList.fetch();
                            }, 2500);

                            var createButtonExecutor = function(what,
                                    filterFunc) {

                                return function(event) {
                                    if (!window.confirm('Are you sure?')) {
                                        return;
                                    }

                                    var models = grid.getSelectedModels();

                                    if (typeof filterFunc !== 'undefined') {
                                        models = _.filter(models, filterFunc);
                                    }
                                    var names = _.map(models, function(m) {
                                        return m.get('name');
                                    });
                                    $.ajax(what(names), {
                                        type : 'POST',
                                    });
                                }
                            };

                            $('#btn_turnAllOn').click(
                                    createButtonExecutor(function(names) {
                                        return urls('dashboardSetSlaveStatus')
                                                + "?status=on&slaveNames="
                                                + names.join(',');
                                    }));

                            $('#btn_turnAllOff').click(
                                    createButtonExecutor(function(names) {
                                        return urls('dashboardSetSlaveStatus')
                                                + "?status=off&slaveNames="
                                                + names.join(',');
                                    }));

                            $('#btn_shutAllDown')
                                    .click(
                                            createButtonExecutor(function(names) {
                                                return urls('dashboardSetSlaveStatus')
                                                        + "?status=shutdown&slaveNames="
                                                        + names.join(',');
                                            }));

                            var onlineFilter = function(model) {
                                return model.get('online') == 'True';
                            }

                            $('#btn_blinkAll').click(
                                    createButtonExecutor(function(names) {
                                        return urls('dashboardSlaveBlink')
                                                + "?slaveNames="
                                                + names.join(',');
                                    }, onlineFilter));

                            $('#btn_ledOffAll').click(
                                    createButtonExecutor(function(names) {
                                        return urls('dashboardSlaveBlink')
                                                + "?slaveNames="
                                                + names.join(',');
                                    }, onlineFilter));
                        });

                    });
        });