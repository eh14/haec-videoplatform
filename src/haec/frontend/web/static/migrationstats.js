require([ 'common' ], function() {
    require([ 'underscore', 'jquery', 'd3', 'sankey', 'highcharts' ], function(
            _, $, d3, sankey, Highcharts) {
        if (numberMigrations == 0) {
            $('#migrationChart').text('No Migrations done');
        } else {

            var margin = {
                top : 1,
                right : 1,
                bottom : 6,
                left : 1
            };
            var width = 400 - margin.left - margin.right;
            var height = 300 - margin.top - margin.bottom;

            var formatNumber = d3.format(",.0f");
            var format = function(d) {
                return formatNumber(d) + " videos migrated";
            };

            var color = d3.scale.category20();

            var svg = d3.select("#migrationChart").append("svg").attr("width",
                    width + margin.left + margin.right).attr("height",
                    height + margin.top + margin.bottom).append("g").attr(
                    "transform",
                    "translate(" + margin.left + "," + margin.top + ")");

            var sankeychart = sankey().size([ width, height ]).nodeWidth(15)
                    .nodePadding(10).nodes(nodes).links(links).layout(32);
            var path = sankeychart.link();

            var link = svg.append("g").selectAll(".link").data(links).enter()
                    .append("path").attr("class", "link").attr("d", path)
                    .style("stroke-width", function(d) {
                        return Math.max(1, d.dy);
                    }).sort(function(a, b) {
                        return b.dy - a.dy;
                    });

            link.append("title").text(
                    function(d) {
                        return "Slave " + d.source.name + " migrated "
                                + formatNumber(d.value) + " videos to "
                                + d.target.name + "\n";
                    });

            var node = svg.append("g").selectAll(".node").data(nodes).enter()
                    .append("g").attr("class", "node").attr("transform",
                            function(d) {
                                return "translate(" + d.x + "," + d.y + ")";
                            }).call(d3.behavior.drag().origin(function(d) {
                        return d;
                    }).on("dragstart", function() {
                        this.parentNode.appendChild(this);
                    }).on("drag", dragmove));

            node.append("rect").attr("height", function(d) {
                return d.dy;
            }).attr("width", sankeychart.nodeWidth()).style({
                fill : function(d) {
                    if (d.popClass == 'A') {
                        return '#5cb85c';
                    } else {
                        return '#5bc0de';
                    }
                }
            }).style("stroke", function(d) {
                return d3.rgb(d.color).darker(2);
            }).append("title").text(
                    function(d) {
                        return d.name + "\n Class " + d.popClass + "\n"
                                + format(d.value);
                    });

            node.append("text").attr("x", -6).attr("y", function(d) {
                return d.dy / 2;
            }).attr("dy", ".35em").attr("text-anchor", "end").attr("transform",
                    null).text(function(d) {
                return d.name;
            }).filter(function(d) {
                return d.x < width / 2;
            }).attr("x", 6 + sankeychart.nodeWidth()).attr("text-anchor",
                    "start");

            function dragmove(d) {
                d3.select(this).attr(
                        "transform",
                        "translate("
                                + d.x
                                + ","
                                + (d.y = Math.max(0, Math.min(height - d.dy,
                                        d3.event.y))) + ")");
                sankeychart.relayout();
                link.attr("d", path);
            }
        }

        /**
         * Popularity Chart...
         * 
         */

        popVids.sort(function(a, b) {
            return b - a;
        });
        _.each(_.range(numUnpopularVids), function() {
            popVids.push(0);
        });

        $('#popThresholdChart').highcharts({
            chart : {
                type : 'area',
                height : 300,
            },
            title : {
                text : null,
            },
            plotOptions : {
                area : {
                    marker : {
                        enabled : false,
                    }
                }
            },
            yAxis : {
                min : 0,
                title : {
                    text : 'Popularity'
                }
            },
            series : [ {
                name : 'Threshold',
                data : _.map(_.range(popVids.length), function() {
                    return popThreshold
                }),
                color : '#5bc0de',
                fillOpacity : 0.5,
            }, {
                name : 'Video Popularity',
                data : popVids,
                color : '#5cb85c',
                fillOpacity : 0.5,
            } ],
        });

    });
});