require([ 'common' ], function() {
    require([ 'jquery', 'underscore', 'backbone', 'highcharts'],
            function($, _, Backbone, Highcharts) {

                // //////////////////////
                // Total popularity
                // //////////////////////
                $(function() {
                    var DiagramView = Backbone.View.extend({
                        initialize : function(options) {
                            this.chartOpts = options;
                        },
                        render : function() {
                            var chart = this.$el.highcharts({
                                chart : {
                                    type : 'column',
                                },
                                plotOptions : {
                                    line : {
                                        marker : {
                                            enabled : false
                                        }
                                    }
                                },
                                title : {
                                    text : this.chartOpts.title,
                                // x : -20
                                },
                                yAxis : {
                                    title : {
                                        text : this.chartOpts.yaxisTitle
                                    },
                                    plotLines : [ {
                                        value : 0,
                                        width : 1,
                                        color : '#808080'
                                    } ]
                                },

                                xAxis : {
                                    title : {
                                        text : null,
                                    },
                                },
                                series : [ {
                                    name : 'total',
                                    data : this.chartOpts.values,
                                } ],
                            });

                        },

                    });

                    var diagrams = [];
                    diagrams.push(new DiagramView({
                        el : '#lastMinuteVideoPopularityChart',
                        values : _hitDataLastMinute,
                        title : 'Last Minute',
                    }));

                    diagrams.push(new DiagramView({
                        el : '#last30MinuteVideoPopularityChart',
                        values : _hitDataLast30Minutes,
                        title : 'Last 30 Minutes',
                    }));

                    diagrams.push(new DiagramView({
                        el : '#lastHourVideoPopularityChart',
                        values : _hitDataLastHour,
                        title : 'Last Hour',
                    }));

                    diagrams.push(new DiagramView({
                        el : '#totalVideoPopularityChart',
                        values : _hitDataTotalHits,
                        title : 'Total',
                    }));

                    _.each(diagrams, function(element, index, list) {
                        element.render();
                    });
                });
            });
});