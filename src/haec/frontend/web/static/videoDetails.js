require(
        [ 'common' ],
        function() {
            var initView = function() {
                var Video = Backbone.Model.extend({
                    defaults : {
                        name : '',
                        variant : '',
                        format : '',
                    },
                });

                var VideoView = Backbone.View.extend({
                    initialize : function(options) {
                        this.$el.hide();
                        this.listenTo(this.model, 'change', this.render);
                        this.player = videojs(options.vidContainer, {},
                                function() {
                                });
                    },

                    loadVideo : function(src) {
                        this.player.src(src);

                        this.$el.show();
                    },

                    render : function() {
                        var url = urls('streamVideo', this.model.get('name'),
                                this.model.get('variant'));
                        var _this = this;
                        $.get(url, {}, function(data, textStatus, xhr) {
                            if (textStatus == 'success') {
                                _this.loadVideo(data);
                            } else {
                                console.log('Error loading video url');
                            }
                        });
                    },
                });

                var watchVideoHandler = function(i, btn) {
                    $(btn).click(
                            function() {
                                var button = $(btn);
                                video.set({
                                    name : button.attr('data-videoname'),
                                    variant : button.attr('data-videovariant'),
                                    format : 'video/'
                                            + button.attr('data-videoformat'),
                                });
                            });
                };

                var TranscodingForm = Backbone.View
                        .extend({
                            el : '#transcodeForm',

                            // form fields
                            profile : $('#profileSelect'),
                            qualityMode : $('input[name=qualityMode]'),
                            btn : $('#submitRequest'),
                            transcodingInfo : $('#transcodingRunningInfo'),

                            initialize : function() {
                                this.transcodingInfo.hide();

                            },

                            transcodingSuccess : function(result, status, xhr) {

                                if (result.indexOf('stream') != -1) {
                                    var info = $('#transcodingRunningInfo');
                                    info.text('Transcoding already done.')
                                    window.setInterval(function() {
                                        window.location.reload();
                                    }, 1000);
                                    return;
                                }

                                var checkSuccessAndReload = function(data,
                                        status, xhr) {
                                    if (data.status == 'success'
                                            || data.status == 'failure') {
                                        // do a quick refresh here,
                                        // since we cannot
                                        // reload
                                        // the list with ajax.
                                        // kill the checker-interval
                                        window.clearInterval(checkerTimer);
                                        console
                                                .log('Transcoding success. Reloading in 1.5s');
                                        window.setInterval(function() {
                                            window.location.reload();
                                        }, 1500);
                                    } else {
                                        console
                                                .log('No success yet. Continue polling...');
                                    }
                                };
                                var errorCallback = function(xhr, status, error) {
                                    console.log(error);
                                };
                                var taskId = result;
                                console.log('TaskID is ' + taskId);
                                // now poll for the task-Id to
                                // finish
                                var checker = function() {
                                    $.ajax({
                                        dataType : 'json',
                                        url : urls('taskDetails',
                                                taskId),
                                        success : checkSuccessAndReload,
                                        error : errorCallback
                                    });
                                };
                                var checkerTimer = window.setInterval(checker,
                                        1000);
                            },

                            transcodingError : function(xhr, status, error) {
                                console.log('Error transcoding...');
                                var info = $('#transcodingRunningInfo');
                                info
                                        .text('Error performing the transcoding...');
                                window.setInterval(function() {
                                    $('#submitRequest').removeAttr('disabled');
                                    info.hide();
                                }, 1000);
                            },

                            // some events
                            events : {
                                'click #submitRequest' : function() {
                                    this.btn.attr('disabled', 'disabled');
                                    this.transcodingInfo
                                            .text('Transcoding started...');
                                    this.transcodingInfo.show();

                                    var profile = this.profile
                                            .find(':selected').val();
                                    var qualityMode = this.qualityMode.filter(
                                            ':checked').val();
                                    var url = urls('requestVideo',
                                            watchOrigButton
                                                    .attr('data-videoname'),
                                            profile, qualityMode, 0);
                                    $.ajax({
                                        url : url,
                                        type : 'GET',
                                        success : this.transcodingSuccess,
                                        error : this.transcodingError,
                                        timeout : 5000,
                                    });
                                }
                            },

                        });

                var video = new Video();

                var vidView = new VideoView({
                    el : $('#watchVideoContainer'),
                    vidContainer : 'videoContainer',
                    model : video,
                });

                var watchOrigButton = $('#watchOriginalVideo');
                watchOrigButton.click(function() {
                    video.set({
                        name : watchOrigButton.attr('data-videoname'),
                        variant : watchOrigButton.attr('data-videovariant'),
                        format : 'video/'
                                + watchOrigButton.attr('data-videoformat'),
                    });
                });

                $('#videoTranscodes').find('button').each(watchVideoHandler);

                var transForm = new TranscodingForm();
            };

            require([ 'jquery', 'backbone', 'bootstrapslider' ], function($) {
                $(function() {
                    initView();
                });

            });
        });