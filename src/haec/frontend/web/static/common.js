requirejs
        .config({
            baseUrl : '/static',
            paths : {
                bootstrap : [
                        'http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min',
                        'bootstrap' ],
                jquery : [
                        'http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min',
                        'jquery-2.1.1.min' ],
                underscore : 'underscore-min',
                backbone : 'backbone-min',
                d3 : [ 'http://d3js.org/d3.v3.min' ],
                highcharts : [ 'highcharts' ],
                highstock : [ 'http://code.highcharts.com/stock/highstock.src',
                        'highstock' ],
                highchartsmore : [
                        'http://code.highcharts.com/highcharts-more',
                        'highcharts-more' ],
                bootstrapslider : 'bootstrap-slider.min',
                backgrid : 'backgrid.min',
                backgridSelect : 'backgrid-select-all',
                moment : 'moment-with-locales.min',
                highslide : [ 'highslide-full.min' ],
            },
            shim : {
                underscore : {
                    exports : "_"
                },
                backbone : {
                    deps : [ 'underscore', 'jquery' ],
                    exports : 'Backbone'
                },
                highslide : {
                    exports : 'hs',
                },
                highcharts : {
                    deps : [ 'jquery' ],
                    exports : "Highcharts"
                },
                highstock : {
                    deps : [ 'jquery', ],
                },
                highchartsmore : {
                    deps : [ 'jquery', 'highcharts' ],
                },
                bootstrap : {
                    deps : [ 'jquery' ],
                },
                d3 : {
                    deps : [ 'jquery' ],
                    exports : "d3",
                },
                jquery : {
                    exports : "$",
                },
                bootstrapslider : {
                    depends : [ 'jquery' ],
                },
                backgrid : {
                    depends : [ 'backbone' ],
                    exports : "Backgrid",
                },
                backgridSelect : {
                    depends : [ 'backgrid' ],
                },
            }

        });

require([ 'bootstrap' ], function() {
});