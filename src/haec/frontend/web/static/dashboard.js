require(
        [ 'common' ],
        function() {
            require(
                    [ 'underscore', 'jquery', 'sysicons', 'highstock',
                            'models', 'highslide', 'bootstrapslider',
                            'demoplots/modelvalues' ],
                    function(_, $, sysicons, _a, models, hs, _b, pmcValues) {
                        hs.graphicsDir = '/static/graphics/';
                        hs.outlineType = 'rounded-white';
                        hs.wrapperClassName = 'draggable-header';
                        hs.captionEval = 'this.a.title';
                        hs.showCredits = false;
                        hs.marginTop = 20;
                        hs.marginRight = 20;
                        hs.marginBottom = 20;
                        hs.marginLeft = 20;

                        var MAX_LOAD_POINTS = 1800;

                        Highcharts.setOptions({
                            global : {
                                useUTC : false
                            }
                        });

                        var lineChartHeight = 400;

                        // ////////////// Time to keep values in chart (in
                        // seconds)
                        // //
                        var maxDisplayTime = 600;

                        var legendBackgroundColor = (Highcharts.theme && Highcharts.theme.legendBackgroundColor)
                                || '#FFFFFF';

                        var eventDetailsRenderer = {
                            defaultRenderer : function(point) {
                                var template = _.template($(
                                        '#templateGeneralEventDetails').html());

                                return template({
                                    details : 'Event '
                                            + point.eventData.name
                                            + ' occured. No further details known.',
                                    date : Highcharts.dateFormat(
                                            '%A, %b %e, %Y %H:%M:%S', point.x)
                                });
                            },
                            'situation-adaptation' : function(point) {
                                var template = _.template($(
                                        '#templateSituationEventDetails')
                                        .html());

                                return template({
                                    details : point.eventData.summary,
                                    date : Highcharts.dateFormat(
                                            '%A, %b %e, %Y %H:%M:%S', point.x),
                                    situationQuery : $('#demo-situation')
                                            .html(),
                                    loggedInUsers : point.eventData.userIds.length,
                                    beforeThreshold : point.eventData.beforeThreshold
                                            .toFixed(5),
                                    afterThreshold : point.eventData.afterThreshold
                                            .toFixed(5),
                                    contextUserIds : _.map(
                                            point.eventData.userIds,
                                            function(element) {
                                                return '<li>' + element
                                                        + '</li>';
                                            }).join('\n'),
                                });
                            },
                            'transcoding-started' : function(point) {
                                var template = _.template($(
                                        '#templateTranscodingEventDetails')
                                        .html());
                                return template({
                                    date : Highcharts.dateFormat(
                                            '%A, %b %e, %Y %H:%M:%S', point.x),
                                    location : point.eventData.location,
                                    estimatedTime : point.eventData.estimatedTime
                                });
                            }
                        };

                        var eventHasDetailsTooltip = function(eventName) {
                            return _.has(eventDetailsRenderer, eventName);
                        }

                        var showEventTooltip = function(e) {
                            var eventName = e.point.eventData.name;

                            // Use the default-renderer, if we don't have a
                            // specialized one.
                            var detailRenderer = null;

                            // use a special renderer, if we have.
                            if (eventHasDetailsTooltip(eventName)) {
                                detailRenderer = eventDetailsRenderer[eventName];
                            }

                            // no renderer, don't display anything!
                            if (detailRenderer == null) {
                                return;
                            }
                            // build the details-box
                            // remove the old details box. This is probably bad
                            // style, but we're using
                            // unique IDs inside the box, so that's a quick
                            // solution.
                            $('#situationEventDetailsBox').remove();
                            hs.htmlExpand(null, {
                                width : 1200,
                                height : 800,
                                headingText : 'Event Details',
                                maincontentText : detailRenderer(e.point),
                            });

                            var chartDrawTimer = setInterval(function() {
                                if ($('#pmcPlotBox').length) {
                                    pmcValues('#pmcPlotBox');
                                    clearInterval(chartDrawTimer);
                                }
                            }, 100);
                        }

                        var cpuColor = '#E88C85';
                        var powerColor = '#000000';
                        var nwColor = '#9FC2FF';

                        var chartOpts = {
                            chart : {
                                type : 'spline',
                                height : lineChartHeight,
                                alignTicks : false,
                            },
                            navigator : {
                                baseSeries : 1,
                            },
                            rangeSelector : {
                                buttons : [ {
                                    count : 1,
                                    type : 'minute',
                                    text : '1M'
                                }, {
                                    count : 5,
                                    type : 'minute',
                                    text : '5M'
                                }, {
                                    type : 'all',
                                    text : 'All'
                                } ],
                                inputEnabled : false,
                                selected : 1
                            },
                            title : {
                                text : null,
                            },
                            plotOptions : {
                                line : {
                                    marker : {
                                        enabled : false
                                    }
                                },
                                series : {
                                    cursor : 'pointer',
                                    point : {
                                        events : {
                                            click : function(e) {
                                                // display if event and the
                                                // event has a details-renderer
                                                if (e.point.name == 'event'
                                                        && eventHasDetailsTooltip(e.point.eventData.name)) {
                                                    showEventTooltip(e);
                                                }
                                                return false;
                                            }
                                        }
                                    },
                                }
                            },
                            xAxis : {
                                title : {
                                    text : 'time',
                                },
                                type : 'datetime',
                            },
                            tooltip : {
                                shared : false,
                                crosshairs : false,
                                formatter : function(tooltip) {
                                    // for the energy/sysload series, print the
                                    // default tooltip
                                    if (this.series.index <= 2) {
                                        return Highcharts.Tooltip.prototype.defaultFormatter
                                                .apply(this, [ tooltip ]);
                                    }

                                    // otherwise, print our event-tooltip.
                                    return "<b>"
                                            + this.series.name
                                            + " Event</b>"
                                            + (eventHasDetailsTooltip(this.point.eventData.name) ? ' (click for details) '
                                                    : '') + "<br>"
                                            + this.point.tooltip;
                                }
                            },
                            yAxis : [ {
                                title : {
                                    text : 'CPU Load',
                                    style : {
                                        color : cpuColor,
                                    },
                                },
                                min : 0,
                                max : 100,
                                opposite : false,
                                labels : {
                                    format : '{value}%',
                                    style : {
                                        color : cpuColor,
                                    },
                                },
                                top : '15%',
                                height : '85%',
                                tickInterval : 25,
                                ceiling : 100,
                                offset : 0,
                            }, {
                                top : '15%',
                                height : '85%',
                                title : {
                                    text : 'Power',
                                    offset : 22,
                                    style : {
                                        color : powerColor,
                                    },
                                },
                                labels : {
                                    format : '{value}W',
                                    align : 'right',
                                    style : {
                                        color : powerColor,
                                    },
                                },
                                gridLineWidth : 0,
                                min : 0,
                                opposite : true,
                                offset : 20,
                            }, {
                                title : {
                                    text : 'Events'
                                },
                                labels : {
                                    enabled : false,
                                },
                                height : '15%',
                                gridLineWidth : 1,
                                min : 1,
                                max : 3,
                                tickInterval : 1,
                                showEmpty : true,
                                offset : 30,
                            }, {
                                title : {
                                    text : 'Network Bandwidth',
                                    style : {
                                        color : nwColor,
                                    },
                                },
                                min : 0,
                                labels : {
                                    enabled : true,
                                    style : {
                                        color : nwColor,
                                    },
                                },
                                top : '15%',
                                height : '85%',

                            } ],
                            series : [ {
                                id : 'series-sysload',
                                name : 'CPU Load',
                                data : _initialAvgCpuUsage.map(function(elem) {
                                    return [ elem[0] * 1000, // convert to ms
                                    elem[1] * 100 // convert 0..1-range to
                                    // percent
                                    ];
                                }),
                                min : 0,
                                yAxis : 0,
                                color : cpuColor,
                                tooltip : {
                                    valueSuffix : '%',
                                    valueDecimals : 0
                                },
                                visible : true,
                            }, {
                                id : 'series-energy',
                                name : 'Energy',
                                data : _initialPowerValues.map(function(elem) {
                                    return [ elem[0] * 1000, // convert to ms
                                    elem[1] * 0.00522 // convert to Watt
                                    ];
                                }),
                                yAxis : 1,
                                floor : 0, // don't display below 0
                                min : 0,
                                color : powerColor,
                                tooltip : {
                                    valueSuffix : 'W',
                                    valueDecimals : 2
                                },
                            }, {
                                id : 'series-nwout',
                                name : 'Network',
                                data : _initialNwOut.map(function(elem) {
                                    return [ elem[0] * 1000, elem[1] / 1024 ];
                                }),
                                yAxis : 3,
                                floor : 0,
                                min : 0,
                                color : nwColor,
                                tooltip : {
                                    valueSuffix : 'kB/s',
                                    valueDecimals : 2,
                                }
                            }, {
                                id : 'context-events',
                                name : 'Context-Events (B02/B03)',
                                yAxis : 2,
                                type : 'scatter',
                                color : '#f0ad4e',
                                marker : {
                                    symbol : 'diamond',
                                    radius : 5,
                                },
                                visible : false,
                            }, {
                                id : 'transcoding-events',
                                name : 'Transcoding Events (B01)',
                                yAxis : 2,
                                type : 'scatter',
                                color : '#D14719',
                                marker : {
                                    symbol : 'triangle',
                                },
                                visible : false,
                            }, {
                                id : 'system-events',
                                name : 'System-Events',
                                yAxis : 2,
                                type : 'scatter',
                                color : '#000',
                                marker : {
                                    symbol : 'circle',
                                },
                                visible : false,
                            }, ],
                            legend : {
                                enabled : true,
                                layout : 'horizontal',
                                align : 'center',
                                verticalAlign : 'bottom',
                                floating : false,
                                backgroundColor : legendBackgroundColor
                            },
                        };
                        $('#loadChart').highcharts("StockChart", chartOpts);

                        var eventTooltipMapper = {
                            'slave-online' : function(data) {
                                return 'Slave ' + data.args[0] + ' went online';
                            },
                            'slave-offline' : function(data) {
                                return 'Slave ' + data.args[0]
                                        + ' went offline';
                            },
                            'situation-adaptation' : function(data) {
                                return data.summary;
                            },
                            'transcoding-started' : function(data) {
                                return 'Transcoding started'
                            }
                        };

                        /**
                         * Handler to system events
                         */
                        var handleSystemEvent = (function() {
                            // private ref to chart, used by handleSystemEvent
                            var chart = $('#loadChart').highcharts();
                            var inner = function(data, refresh) {
                                // set some default data
                                var p = {
                                    x : data.time * 1000,
                                    y : 0,
                                    name : 'event',
                                    eventData : data,
                                }

                                if (typeof refresh === undefined) {
                                    refresh = true;
                                }

                                var seriesId = null;

                                // set the tooltip using the
                                // event-tooltip-mapping
                                if (_.has(eventTooltipMapper, data.name)) {
                                    p['tooltip'] = eventTooltipMapper[data.name]
                                            (data);
                                } else {
                                    // was not mapped, just put in the event
                                    // name
                                    p['tooltip'] = 'Event ' + data.name
                                            + ' occured.';
                                }
                                // categorize the events into the appropriate
                                // series

                                if (_.contains([ 'situation-adaptation' ],
                                        data.name)) {
                                    seriesId = 'context-events';
                                    p.y = 1;
                                } else if (_.contains([ 'slave-online',
                                        'slave-offline', ], data.name)) {
                                    seriesId = 'system-events';
                                    p.y = 2;
                                } else if (_.contains(
                                        [ 'transcoding-started' ], data.name)) {
                                    seriesId = 'transcoding-events';
                                    p.y = 3;
                                } else if (data.name == 'powersum') {
                                    seriesId = 'series-energy';
                                    p = [ data.time * 1000,
                                            data.value * 0.00522 ];
                                } else if (data.name == 'avgCpu') {
                                    seriesId = 'series-sysload';
                                    p = [ data.time * 1000, data.value * 100 ];
                                } else if (data.name == 'sumNwOut') {
                                    seriesId = 'series-nwout';
                                    p = [ data.time * 1000, data.value / 1024 ];
                                }
                                if (seriesId != null) {
                                    var series = chart.get(seriesId);
                                    var shift = series.data.length >= MAX_LOAD_POINTS;
                                    series.addPoint(p, refresh, shift);
                                }
                            };
                            return inner;
                        })();

                        /**
                         * Listen to system events from the videoplatform. Tries
                         * to avoid double-listening using the module pattern.
                         */
                        (function() {
                            var sock = null;
                            var inner = function() {
                                // avoid double-connect
                                if (sock !== null) {
                                    return;
                                }
                                sock = new SockJS(_websocketUrl);

                                sock.onmessage = function(e) {
                                    var data = $.parseJSON(e.data);
                                    handleSystemEvent(data);
                                };

                                // retry if closed
                                sock.onclose = function() {
                                    sock = null;
                                    window.setTimeout(inner, 1000);
                                };
                            };
                            inner();
                        })();

                        /**
                         * Load initial events by passing each into the same
                         * method that's also handling live events.
                         */
                        _.each(_bufferedEvents, function(element) {
                            // don't update the chart for every event
                            handleSystemEvent(element, false);
                        });
                        // now update the chart
                        $('#loadChart').highcharts().redraw();

                        /**
                         * Crossloading of the workload-generator
                         */
                        var WorkloadModel = Backbone.Model.extend({
                            url : _wlgenWebUrl + '/settings'
                        });

                        var slider = $('#requestsPerHourSlider').slider({
                            tooltip : 'hide'
                        }).on(
                                'slide',
                                function(ev) {
                                    $('#rphValue').html(
                                            ev.value.toFixed(0)
                                                    + ' requests per hour.');
                                });

                        // we need to know when the wlgen is first contacted.
                        // first time we reset the settings but don't change,
                        // otherwise
                        // the user cannot draw the slider without it jumping.
                        var initialUpdate = true;

                        var onError = function(mode, response, options) {
                            $('#infoErrorWlgenConnection')
                                    .removeClass('hidden');
                            $('#updateWorkloadBtn')
                                    .attr('disabled', 'disabled');

                            initialUpdate = true;

                        };

                        var periodSlider = $('#period').slider({
                            tooltip : 'hide'
                        }).on('slide', function(ev) {
                            $('#periodValue').html(ev.value + ' seconds');
                        });

                        var varianceSlider = $('#variance').slider({
                            tooltip : 'hide'
                        }).on(
                                'slide',
                                function(ev) {
                                    $('#varianceValue').html(
                                            (ev.value * 100).toFixed(0) + '%');
                                });

                        $('#loadShapeFlat').change(function(a, b, c) {
                            $('#sineOptionGroups').addClass('hidden');
                        });

                        $('#loadShapeSine').change(function(a, b, c) {
                            $('#sineOptionGroups').removeClass('hidden');
                        });

                        var StatisticModel = Backbone.Model.extend({});
                        var StatisticCollection = Backbone.Collection.extend({
                            model : StatisticModel,
                        });

                        var statCollection = new StatisticCollection();

                        var streamWlgenResults = function() {
                            var sock = new SockJS(_wlgenSocketUrl);
                            sock.onmessage = function(e) {
                                var data = $.parseJSON(e.data);

                                var chart = $('#workloadChart').highcharts();
                                var pointTime = data.time * 1000;

                                var oldest = Date.now() - maxDisplayTime * 1000;

                                var runningSeries = chart
                                        .get('series_runningRequests');
                                var rphSeries = chart.get('series_rph');
                                var latencySeries = chart
                                        .get('series_avgLatency');
                                var errorSeries = chart.get('series_errors');

                                var shift = function(series) {
                                    return series.data[0]
                                            && series.data[0].x < oldest;
                                }
                                runningSeries.addPoint([ pointTime,
                                        data.runningRequests ], false,
                                        shift(runningSeries));

                                rphSeries.addPoint([ pointTime, data.rph ],
                                        false, shift(rphSeries));

                                // if latency is < 0 (no records were done), we
                                // don't
                                // plot a value...
                                if (data.avgLatency >= 0) {
                                    var avgLatency = data.avgLatency * 1000;
                                    var avgPoint = [ pointTime, avgLatency ];
                                    latencySeries.addPoint(avgPoint, false,
                                            shift(latencySeries));
                                } else {
                                    if (shift(latencySeries)) {
                                        latencySeries.data.shift();
                                    }
                                }

                                errorSeries.addPoint(
                                        [ pointTime, data.errors ], true,
                                        shift(errorSeries));

                            };

                            // retry if closed
                            sock.onclose = function() {
                                window.setTimeout(streamWlgenResults, 1000);
                            };
                        };

                        // initialize the slider only once
                        var initialUpdate = true;
                        var updateWlgenValues = function(model, response,
                                options) {
                            if (initialUpdate) {
                                // start streaming the wlgen-events coming by
                                // websocket
                                streamWlgenResults();

                                $('#requestsPerHour').val(
                                        model.get('requestsPerHour'));
                                slider.slider('setValue', model
                                        .get('requestsPerHour'));

                                $('#rphValue').html(
                                        model.get('requestsPerHour').toFixed(0)
                                                + ' requests per hour.');

                                periodSlider.slider('setValue', model
                                        .get('loadPeriod'));
                                $('#periodValue').html(
                                        model.get('loadPeriod') + ' seconds');

                                varianceSlider.slider('setValue', model
                                        .get('loadVariance'));

                                $('#varianceValue').html(

                                        (model.get('loadVariance') * 100)
                                                .toFixed(0)
                                                + '%');

                                if (model.get('loadShape') == 'flat') {
                                    $('#loadShapeFlat').attr('checked',
                                            'checked');
                                    $('#sineOptionGroups').addClass('hidden');
                                } else {
                                    $('#loadShapeSine').attr('checked',
                                            'checked');
                                    $('#sineOptionGroups')
                                            .removeClass('hidden');
                                }
                                slider.slider('setValue', model
                                        .get('requestsPerHour'));

                                initialUpdate = false;
                            }
                            var interval = 3600 / model.get('requestsPerHour');
                            $('#wlgenInfo_interval').html(
                                    interval.toFixed(2) + ' s.');
                            $('#wlgenInfo_currentReqs').html(
                                    model.get('concurrentRequests'));

                            $('#infoErrorWlgenConnection').addClass('hidden');
                            $('#updateWorkloadBtn').removeAttr('disabled');
                        };

                        var workload = new WorkloadModel();

                        window.setInterval(function() {
                            workload.fetch({
                                success : updateWlgenValues,
                                error : onError
                            });
                        }, 800);

                        var latencyColor = '#5cb85c';
                        var numberColor = '#428bca';

                        var chartOpts = {
                            chart : {
                                height : lineChartHeight,
                                alignTicks : false,
                            },
                            plotOptions : {
                                series : {
                                    cursor : 'pointer',
                                },
                            },
                            navigator : {
                                baseSeries : 3,
                            },
                            rangeSelector : {
                                buttons : [ {
                                    count : 1,
                                    type : 'minute',
                                    text : '1M'
                                }, {
                                    count : 5,
                                    type : 'minute',
                                    text : '5M'
                                }, {
                                    type : 'all',
                                    text : 'All'
                                } ],
                                inputEnabled : false,
                                selected : 1
                            },
                            title : {
                                text : null,
                            },
                            xAxis : {
                                title : {
                                    text : 'time',
                                },
                                type : 'datetime',
                            },
                            yAxis : [ {
                                title : {
                                    text : 'Latency (ms)',
                                    style : {
                                        color : latencyColor,
                                    },
                                },
                                labels : {
                                    style : {
                                        color : latencyColor,
                                    },
                                },
                                min : 0,
                                opposite : true,
                            }, {
                                name : 'RpH',
                                title : {
                                    text : 'Requests per Hour',
                                },
                                min : 0,
                                opposite : false,
                            }, {
                                name : 'Number',
                                title : {
                                    text : 'Number',
                                    style : {
                                        color : numberColor,
                                    },
                                },
                                min : 0,
                                labels : {
                                    style : {
                                        color : numberColor,
                                    },
                                },
                                opposite : true,
                            } ],
                            tooltip : {
                                shared : true,
                            },
                            legend : {
                                enabled : true,
                            },
                            series : [ {
                                id : 'series_avgLatency',
                                type : 'spline',
                                name : 'Avg. Latency',
                                color : latencyColor,
                                tooltip : {
                                    valueDecimals : 2,
                                    valueSuffix : 'ms',
                                },
                                data : [],
                                yAxis : 0,
                                visible : false,
                            }, {
                                id : 'series_runningRequests',
                                type : 'spline',
                                name : 'Running Requests',
                                color : '#428bca',
                                data : [],
                                yAxis : 2,
                            }, {
                                id : 'series_errors',
                                type : 'spline',
                                name : 'Errors',
                                color : '#d9534f',
                                data : [],
                                yAxis : 2,
                                visible : false,
                            }, {
                                id : 'series_rph',
                                name : 'RpH',
                                tooltip : {
                                    valueDecimals : 2,
                                },
                                type : 'spline',
                                color : '#dddddd',
                                lineWidth : 8,
                                data : [],
                                yAxis : 1,
                                zIndex : -1,
                            } ]
                        };

                        $('#workloadChart').highcharts('StockChart', chartOpts);

                        /*******************************************************
                         * Wlgen live changes to the server
                         */

                        // requests per hour
                        slider.on('slideStop', function(event) {
                            var baseUrl = _wlgenWebUrl + '/requestsPerHour';
                            $.post(baseUrl + '?reqPerHour=' + event.value)
                                    .fail(onError);
                        });

                        // load Shape
                        $('#loadShapeFlat').change(function(event) {
                            var baseUrl = _wlgenWebUrl + '/loadShape';
                            $.post(baseUrl + '?loadShape=flat').fail(onError);
                        });

                        $('#loadShapeSine').change(function(event) {
                            var baseUrl = _wlgenWebUrl + '/loadShape';
                            $.post(baseUrl + '?loadShape=sine').fail(onError);
                        });

                        // load period
                        periodSlider.on('slideStop', function(event) {
                            var baseUrl = _wlgenWebUrl + '/loadPeriod';
                            $.post(baseUrl + '?loadPeriod=' + event.value)
                                    .fail(onError);
                        });

                        // load variance
                        varianceSlider.on('slideStop', function(event) {
                            var baseUrl = _wlgenWebUrl + '/loadVariance';
                            $.post(baseUrl + '?loadVariance=' + event.value)
                                    .fail(onError);
                        });

                        /**
                         * Demo Dummy for B2/B3 alarms
                         */

                        // shows a modal alarm, as if initiated from the b2
                        $('#alarmButton').click(function() {
                            $('#alarmContainer').modal({
                                show : true
                            });
                        });

                        $(sysicons.init('#sysicons', _initialClusterState));

                        var showChangedMessage = function(msg) {
                            $('#migrationChangedMessage').text(msg)
                                    .removeClass('hidden');
                            window.setTimeout(function() {
                                $('#migrationChangedMessage')
                                        .addClass('hidden');
                            }, 3000);
                        };

                        var submitStrategyChange = function() {
                            var url = urls('dashboardChangeStrategy', $(
                                    'input[name=optStrategy]:checked',
                                    '#changeMigrationForm').val());
                            $.post(url).success(function() {
                                showChangedMessage('Changed!');
                            }).fail(function() {
                                showChangedMessage('Error!');
                            });
                        }

                        /* Change migration */
                        $('#changeMigrationFormSubmit').click(
                                submitStrategyChange);
                    });
        });