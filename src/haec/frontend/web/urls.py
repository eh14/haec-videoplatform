import os

from django.conf import settings
from django.conf.urls import patterns, url, include
from django.conf.urls.static import static
from django.contrib import admin

admin.autodiscover()

from views import home, statistics, video, tasks, \
itest, opt, experiment, configuration, test


settings.TEMPLATE_DIRS.append(os.path.join(os.path.abspath(__file__), 'templates')) 

urlpatterns = patterns('',
    url(r'^admin/?', include(admin.site.urls)),
    
    url(r'^/?$', home.redirectToHome),
    url(r'^home/?$', home.showHome),
    url(r'^home/dashboard/?$', home.showDashboard),
    
    url(r'^statistics/?$', statistics.index),
    url(r'^statistics/show?$', statistics.showStatistics),
    url(r'^statistics/slaveList/?$', statistics.showSlaveList),
    url(r'^statistics/slaveStatus/?$', statistics.getSlaveStatus),
    url(r'^statistics/setSlaveStatus/?$', statistics.setSlaveStatus),
    url(r'^statistics/slaveBlink/?$', statistics.slaveBlink),
    url(r'^statistics/clusterstate/?$', statistics.getClusterState),
    
    url(r'^opt/show/?$', opt.showOverview),
    url(r'^opt/stats/?$', opt.showStatistics),
    url(r'^opt/changeStrategy/?$', opt.changeStrategy),
    url(r'^opt/enableResurrection/?$', opt.enableResurrection),
    
    url(r'^config/?$', configuration.show),
    url(r'^change/?$', configuration.change),
    
    url(r'^video/search/?$', video.showSearch),
    url(r'^video/_doSearch/?$', video.doSearch),
    url(r'^video/details/?$', video.showDetails),
    url(r'^video/stream/?$', video.requestVariant),
    url(r'^video/transcoding/?$', video.requestVideo),
    url(r'^video/variantsForName/?$', video.variantsForName),
    url(r'^video/stats/?$', video.showVideoStats),
    url(r'^video/rawstats/?$', video.getVideoStats),
    
    url(r'^tasks/?$', tasks.showMasterTasks),
    url(r'^tasks/master/?$', tasks.showMasterTasks),
    url(r'^tasks/list/?$', tasks.listTasks),
    url(r'^tasks/masterlist/?$', tasks.masterTaskList),
    url(r'^tasks/details/?$', tasks.taskDetails),
    url(r'^tasks/removeAllTasks/?$', tasks.removeAllTasks),
    
    # helper/debug functions for the integration test
    url(r'^_itest/rescanvideodir/?$', itest.rescanVideoDir),
    url(r'^_itest/getallvideos/?$', itest.getAllVideos),
    url(r'^_itest/taskAdd/?$', itest.taskAdd),
    url(r'^_itest/taskStart/?$', itest.taskStart),
    url(r'^_itest/taskFinish/?$', itest.taskFinish),
    url(r'^_itest/resetVideoHits/?$', itest.resetVideoHits),
    
    url(r'^exp/randomVideo/?$', experiment.randomVideo),
    url(r'^exp/serverStats/?$', experiment.serverStats),
        
    #
    # unit tests/frontend tests
    url(r'^__test/(?P<name>[a-zA-Z]+)/?$', test.test),
)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
