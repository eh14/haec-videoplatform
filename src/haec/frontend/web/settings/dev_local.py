from common import *
import logging

DEBUG = True
TEMPLATE_DEBUG = DEBUG

MEDIA_ROOT = '/tmp/'

MIDDLEWARE_CLASSES += (
    'django_pdb.middleware.PdbMiddleware',
                       )

INSTALLED_APPS += (
    'django_pdb',
                  )
logging

LOGGING['loggers']['frontend']['level'] = 'INFO'


