from twisted.python import log
from twisted.web import server

class LoggingSite(server.Site):
    def log(self, request):
        """
        Log a request's result to the logfile, by default in combined log format.
        """
        if request.uri.startswith('/static'):
            return
        
        line = '%s (%s) -> %s' % (request.uri,
                                  request.method,
                                  request.code)
        log.msg(line)
