from twisted.internet import protocol
from twisted.python import log
import json
import pythonioc
from haec.services import eventservice, sensorservice
from haec import services, config
from haec.kplane import sensorstore

_cfg = pythonioc.Inject(config.Config)

class EventBroadcastBuffer(protocol.Protocol):
    """
    Buffers the events that should be sent to the frontend.
    All events are added as they come, each time, events that are too
    old are removed.
    It is necessary to have monotonic times, otherwise events might 
    be removed too late or too early.
    
    We just assume that all events are being added by the EventPushFactory
    which adds the time using the timeprovider.
    """
    timeProvider = pythonioc.Inject(services.TimeProvider)
    
    def __init__(self):
        self._events = []
        self._storetime = _cfg.vals.sensor_storetime
        
    def add(self, event):
        if not isinstance(event, dict):
            raise AttributeError("Expected type dict, got %s" % type(event))
        if 'time' not in event:
            raise AttributeError("All events must have a time-attribute")
        
        self._events.append(event)
        
        earliest = self.timeProvider.now() - self._storetime
        
        while len(self._events) and self._events[0]['time'] < earliest:
            del self._events[0]
            
    def getAllEvents(self):
        return list(self._events)
            
class EventPushProtocol(protocol.Protocol):
    def connectionMade(self):
        log.msg('Connection made')        
        self.factory.connections.add(self)
        
    def connectionLost(self, reason):
        log.msg('Connection lost %s' % str(reason))
        self.factory.connections.remove(self)
        
class EventPushFactory(protocol.Factory):
    
    _SUBSCRIPTIONS = ['system-events', 'avg-cpu', 'power-sum']
    
    protocol = EventPushProtocol
    events = pythonioc.Inject(eventservice.EventService)
    connections = set()
    timeProvider = pythonioc.Inject(services.TimeProvider)
    buffer = pythonioc.Inject(EventBroadcastBuffer)
    sensorStore = pythonioc.Inject('sensorStore')
    sensorService = pythonioc.Inject(sensorservice.MasterSensorService)
    
    def handleSystemEvent(self, event, *args, **kwargs):
        if event == 'powerdata-available':
            if not self.hasConnectedListeners():
                return
            
            rawMsg = {'name':'powersum',
                   'value':self.sensorService.getRecentPowerSum(),
                   'time':kwargs.get('now', self.timeProvider.now()) 
                   }
            
            self.sendToAllListeners(rawMsg)
        elif event == 'sensors-available':
            if not self.hasConnectedListeners():
                return
            
            avgCpuMsg = {'name':'avgCpu',
                      'value':self.sensorStore.cpuAvg.getLast(),
                      'time':kwargs.get('now', self.timeProvider.now())}
            
            sumNwOutMsg = {'name':'sumNwOut',
                      'value':self.sensorStore.nwOutSum.getLast(),
                      'time':kwargs.get('now', self.timeProvider.now())}
            
            self.sendToAllListeners(avgCpuMsg)
            self.sendToAllListeners(sumNwOutMsg)
        else:
            rawMsg = {'name': event,
              'time':self.timeProvider.now()}
            rawMsg['args'] = args
            for k, w in kwargs.iteritems():
                rawMsg[k] = w
                    
            self.buffer.add(rawMsg)
            self.sendToAllListeners(rawMsg)
          
    def __init__(self):  
        self.events.subscribeUniversal(self.handleSystemEvent)
        
    def __del__(self):
        self.events.unsubscribeUniversal(self.handleSystemEvent)

    def sendToAllListeners(self, rawMessage):
        msg = json.dumps(rawMessage)
        for conn in self.connections:
            conn.transport.write(msg)
            
            
    def hasConnectedListeners(self):
        return len(self.connections) > 0