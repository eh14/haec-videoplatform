'''

@author: eh14
'''
from haec.services import eventservice
import pythonioc
from haec import services
import collections


class MigrationEventService(object):
    
    MIGRATIONS_STORE_TIME = 60
    
    events = pythonioc.Inject(eventservice.EventService)
    timeProvider = pythonioc.Inject(services.TimeProvider)
    def __init__(self):
        self._migrations = collections.deque(maxlen=50)
        
    def postInit(self):
        self.events.subscribeEvent('video-moved', self._addMovedVideo)
        self.events.subscribeEvent('optimizer-run', self._cleanupOldMigrations)
    
    def _addMovedVideo(self, videoId, oldSlave, newSlave):
        self._migrations.append({'videoId':videoId,
                                 'oldSlave':oldSlave,
                                 'newSlave':newSlave,
                                 'time':self.timeProvider.now()
                                 })
        
    def _cleanupOldMigrations(self, *args, **kwargs):
        """
        Cleans migrations older than n seconds.
        """
        oldest = self.timeProvider.now() - self.MIGRATIONS_STORE_TIME
        while len(self._migrations) and self._migrations[0]['time'] < oldest:
            del self._migrations[0]
        
    def getMigrations(self):
        return list(self._migrations)