#!/usr/bin/env python
"""
Thread that uses a django frontend application
"""

import os
import sys
from twisted.python import threadpool
import pythonioc
sys.path.append(os.path.dirname(__file__))

from haec import config
from twisted.web.wsgi import WSGIResource

cfg = pythonioc.Inject(config.Config)

def createServer(reactor):
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", cfg.vals.django_settings)
        
    from django.core.wsgi import get_wsgi_application
    application = get_wsgi_application()
    tpool = threadpool.ThreadPool()
    tpool.start()
    return (tpool, WSGIResource(reactor, tpool, application))