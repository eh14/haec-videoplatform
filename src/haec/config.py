import argparse
import pprint
import pythonioc
import socket
import time
from twisted.python import log


def _registerOption(argparser, name, **kwargs):
    """
    Helper method to generate the option from the name.
    """
    argparser.add_argument('--' + name, dest=name, **kwargs)
    
def _ipType(value):
    
    # check 
    values = [int(v) for v in value.split('.')]
    if len(values) != 4 or not all([0 <= v <= 255 for v in values]):
        raise TypeError('Invalid IP address given (%s)' % value)
    return value

def _boolType(value):
    return value.lower() in ['yes', 'true', '1', 'y', 't']
    
class Config(object):
    def __init__(self):
        """
        Initializes the haec configuration.
        """
        
        # configuration values. These are the actual values.
        self._configValues = None
        
        # little buffer so we don't need to do the lookup multiple times
        self.__slaveIpBuffer = {}
        
        # knowledgeplanecreator.. we must not create it more than once.
        # TODO: remove when the creator-thing is removed..
        self._kpcreator = None
        
        self._argparser = argparse.ArgumentParser(description='Demonstrator for the HAEC-Cubie.')
        
        # shortcut
        ap = self._argparser
        
        ap.add_argument('role', help='role to start the server in', choices=['master', 'slave'])
        
        # ##############################
        # port options, environment etc.
        # ##############################
        envOpts = ap.add_argument_group('General', 'Ports, Environment options etc')
        _registerOption(envOpts, 'name', help="Name of the instance. Default is the hostname.", type=str, default=socket.gethostname())
        _registerOption(envOpts, 'local_node_port', help="Port where other nodes can connect.", type=int, default=6900)
        _registerOption(envOpts, 'temp_dir', help="Temp folder", type=str, default='/tmp')
        _registerOption(envOpts, 'system_start', help="System start time", type=float, default=time.time())
        # root option removed!
        
        # slave specific options
        slaveOpts = ap.add_argument_group('Slave', 'Options only applicable to the slave')        
        _registerOption(slaveOpts, 'parent_host', help="Parent host IP.", type=_ipType, default='127.0.0.1')
        _registerOption(slaveOpts, 'parent_node_port', help="Port used to connect to the parent.", type=int, default=6900)
        _registerOption(slaveOpts, 'video_dir', help="Slave video folder", type=str, default='/tmp')
        
        # #################
        # Topology related
        # #################
        topoOpts = ap.add_argument_group('Topology', 'Options related to topology handling.')        
        _registerOption(topoOpts, 'heartbeat_interval', help="interval when to send heart beats", type=float, default=2.0)
        _registerOption(topoOpts, 'heartbeat_timeout', help="Timeout after a child is considered disconnected", type=float, default=20.0)
        _registerOption(topoOpts, 'peer_slave_user', help="Username for slave peers", type=str, default='linaro')
        _registerOption(topoOpts, 'peer_slave_password', help="Password for slave peers", type=str, default='linaro')
        _registerOption(topoOpts, 'slave_dnslookup', help="Slave host resolution. Default is python hostlookup. Provide string for fixed host.", default=socket.gethostbyname)
        _registerOption(topoOpts, 'slave_resurrect_interval', help="interval in which failed slaves are tried to be resurrected (by turning them off and on again.)", type=float, default=180)
        _registerOption(topoOpts, 'portmapping_initialize_delay', help="delay, the upnp-mapping-service waits until it attempts to map a slave.", type=float, default=10)
        
        ######################################
        # Configuration for external services
        #####################################
        extOpts = ap.add_argument_group('External Services', 'External services configuration.')        
        
        _registerOption(extOpts, 'db_connectionurl', help="Database connection URL", type=str, default='mysql://root:root@localhost:3306/videoplatform')
        _registerOption(extOpts, 'db_echo', help="Print all sql requests.", type=_boolType, default=False)
        _registerOption(extOpts, 'db_create', help="Create database on startup.", type=_boolType, default=True)
        _registerOption(extOpts, 'db_drop', help="Drop database on startup. Only really makes sense with db_created turned on.", type=_boolType, default=False)
        _registerOption(extOpts, 'cambrionix_url', help="Cambrionix connection URL", type=str, default='tcp://127.0.0.1:9059')
        _registerOption(extOpts, 'cambrionix_collectinterval', help="Cambrionix value collection interval", type=float, default=1)
        _registerOption(extOpts, 'cambrionix_postinit_delay', help="Delay before we do some post-initialization with the cambrionix", type=float, default=10)
        
        # #################
        # frontend settings
        # #################
        frontOpts = ap.add_argument_group('Frontend', 'Some options to configure the web interface.')        
        _registerOption(frontOpts, 'django_settings', help="Django frontend app settings", type=str, default='web.settings.common')
        _registerOption(frontOpts, 'webserver_port', help="Port where the frontend is served.", type=int, default=8980)
        _registerOption(frontOpts, 'websocket_port', help="Port for websockets.", type=int, default=0)
        _registerOption(frontOpts, 'wlgen_defaulthost', help="Host used for the workload generator connection by default.", type=str, default='localhost')
        _registerOption(frontOpts, 'wlgen_defaultport', help="Port used for the workload generator connection by default.", type=str, default='10310')
        
        # #######################
        # Job- and Taskmanagement
        # #######################
        jobOpts = ap.add_argument_group('Job- and Taskmanagement', 'Some options to configure job/task handling')
        #
        # time in seconds to wait between a new job is executed.
        # mainly to spare the CPU and the event-loop
        # when massive amounts of jobs are getting scheduled. Not sure
        # whether this is a good idea.
        _registerOption(jobOpts, 'scheduler_job_waitinterval', help="Interval to wait between working on several multiple jobs.", type=float, default=0.89)
        _registerOption(jobOpts, 'task_runtasks_interval', help="Interval when tasks are tried to be executed / or marked as failed.", type=float, default=20)
        _registerOption(jobOpts, 'tasks_cleantasks_interval', help="Interval when timeout+deletion of tasks is executed.", type=float, default=45)
        _registerOption(jobOpts, 'task_max_waittime', help="Maximum time a task can wait until getting marked as 'timeout'.", type=float, default=600)
        _registerOption(jobOpts, 'task_max_runtime', help="Maximum time a task can run until getting marked as 'timeout'.", type=float, default=600)
        _registerOption(jobOpts, 'task_maxage', help="Time when tasks are cleaned up so the DB does not fill up.", type=float, default=1320)
        
        
                    
        ###############
        # Video related
        ###############
        vidOpts = ap.add_argument_group('Video', 'Video handling options')
        _registerOption(vidOpts, 'video_bulk_interval', help="When to insert collected video reports", type=float, default=2.0)
        
        #######################################################
        # sensor-related                                      #
        # #####################################################
        sensorOpts = ap.add_argument_group('Sensors', 'Sensor options')
        # interval in seconds when slaves read the sensors
        _registerOption(sensorOpts, 'sensor_collectinterval', help="Interval in seconds when slaves read the sensors", type=float, default=1.0)
        _registerOption(sensorOpts, 'sensor_sample_interval', help="Interval the server samples the data from slaves/power", type=float, default=1.0)
        _registerOption(sensorOpts, 'sensor_storetime', help="Time after which sensor values are cleared up internally.", type=float, default=900.0)
        
        ######################
        # Optimization related
        ######################
        optOpts = ap.add_argument_group('Optimization', 'Optimization Options')
        _registerOption(optOpts, 'opt_runinterval', help="If set to > 0, will schedule the optimization.", type=float, default=55)
        _registerOption(optOpts, 'opt_max_boottime', help="When slaves are not up by then, let's consider them failed.", type=float, default=60)
        # Note that this is only the initial value for the optimization model.
        # This value will adapt according to the slave's average boot time.
        _registerOption(optOpts, 'opt_initial_slave_idle_timeout', help="number of seconds, a slave can remain in idle mode (without any streaming requests) before it is shutdown.", type=float, default=45)
        
        #########################
        # Some statistic settings
        #########################
        statOpts = ap.add_argument_group('Statistics', 'Statistic Options')
        # interval when statistics are being recorded
        _registerOption(statOpts, 'statistics_record_interval', help="Interval when statistics are being recorded.", type=float, default=15)
        _registerOption(statOpts, 'statistics_preserve_duration', help="Number of seconds a statistic will be held in database. Also cleaning interval", type=float, default=1800)
        
        ################
        # PSO-Settings #
        ################
        psoOpts = ap.add_argument_group('PSO', 'Options for the particle swarm optimization approach.')
        _registerOption(psoOpts, 'pso_opt_interval', help="Interval when slaves try to optimize", type=float, default=10)
        #
        # PSO is based on a speed in which all slaves 
        # perform their actions in the appropriate direction.
        # The purpose of this is to avoid overswinging.
        _registerOption(psoOpts, 'pso_initial_speed', help="Transition speed in PSO", type=float, default=0.6)
            
            
    def isInitialized(self):
        return self._configValues is not None
    
    def change(self, **kwargs):
        """
        Allows to change values in the configuration. Needs an initialized
        config though, otherwise the role would not be known.
        """
        
        if not self._configValues:
            raise AttributeError('Cannot change, initialize first!')
        
        try:
            updatedArgs = self._argparser.parse_args([self.vals.role] + ['--%s=%s' % (k, v) for k, v in kwargs.iteritems()])
            currentValues = vars(self._configValues).items()
            newValues = vars(updatedArgs).items()
        
            mergedValues = dict(currentValues + newValues)
            self._configValues = argparse.Namespace(**mergedValues)
            
        except SystemExit:
            raise AttributeError('Cannot change config. Parsing the new arguments failed')
        
    def initFromCmd(self, cmdLine=None, exit=False):
        """
        Initialize the config values from a list of strings as would be
        passed as command line
        """
        try:
            self._configValues = self._argparser.parse_args(cmdLine)
            if self._configValues is None:
                raise Exception("init with none??")
        except SystemExit:
            if exit:
                raise
            else:
                raise AttributeError('Cannot parse configuration')
        
    def init(self, role='master', **kwargs):
        # we just assume it's the master. This method is mostly used in testing anyway.
        self.initFromCmd([role])
        self.change(**kwargs)
    
    @property
    def vals(self):
        if self._configValues is None:
            raise Exception('Config uninitialized. Call an init function')
        
        return self._configValues
    
    
    def getSlaveIp(self, slaveName):
        
        if slaveName not in self.__slaveIpBuffer:
            try:
                if callable(self.vals.slave_dnslookup):
                    self.__slaveIpBuffer[slaveName] = self.vals.slave_dnslookup(slaveName)
                else:
                    self.__slaveIpBuffer[slaveName] = self.vals.slave_dnslookup
            except Exception as e:
                log.msg('Error resolving IP address for %s (%s)' % (slaveName, e))
                self.__slaveIpBuffer[slaveName] = ""
        
        return self.__slaveIpBuffer[slaveName]
    
    def getDump(self):
        return vars(self._configValues)
    
    def dump(self):
        log.msg("***ENVIRONMENT DUMP ***\n" + 
        str(pprint.pformat(self.getDump())))


pythonioc.registerService(Config)