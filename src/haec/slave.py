from haec import config

from twisted.internet import reactor
from twisted.python import log
from haec.protomixins import topology, sensors, hardware, \
    taskrunner, activity, video, psoptimizer
from haec.network import multiproto
from haec.services import jobscheduler, videostreamer, \
    videoservice, activitytracker
from twisted.internet.protocol import ReconnectingClientFactory
from twisted.manhole.telnet import ShellFactory
import pythonioc
from haec.frontend import siteutils
import os
cfg = pythonioc.Inject(config.Config)

class BaseSlaveFactory(ReconnectingClientFactory):
    protocol = None
    
    _reactor = pythonioc.Inject('reactor')
    
    def __init__(self):
        
        # we are in a tiny network, so let's not waste time on waiting
        # until the client tries to reconnect again.
        self.maxDelay = 1.3
        self.jitter = None
        
        self._jobsched = None
        
    def startFactory(self):
        ReconnectingClientFactory.startFactory(self)
        self._jobsched = jobscheduler.JobScheduler(8)
        pythonioc.registerServiceInstance(self._jobsched, serviceName='taskRunner', overwrite=True)
        
    def stopFactory(self):
        self._jobsched.shutdown()
        
    def buildProtocol(self, addr):
        proto = ReconnectingClientFactory.buildProtocol(self, addr)
        
        return proto
    
class SlaveFactory(BaseSlaveFactory):
    protocol = multiproto.MultiProtocolBuilder(video.SlaveVideoMixin,
                                               topology.SlaveTopologyMixin,
                                               sensors.SlaveSensorsMixin,
                                               hardware.SlaveHardwareMixin,
                                               taskrunner.SlaveTaskRunnerMixin,
                                               psoptimizer.SlavePsoMixin,
                                               activity.SlaveActivityMixin)
    
def run():
    
    # register the reactor as reactor.
    pythonioc.registerServiceInstance(reactor, 'reactor')
    pythonioc.registerService(activitytracker.ActivityTracker)
    pythonioc.registerService(videoservice.SlaveVideoService)
    
    
    #
    # Create some directories if not existing
    # 
    for d in [cfg.vals.video_dir, cfg.vals.temp_dir]:
        if not os.path.exists(d):
            log.msg('Creating necessary directory %s' % d)
            os.makedirs(d)
        
        assert os.path.isdir(d)
    
    root = videostreamer.CORSResource(cfg.vals.video_dir)
    
    slaveFactory = SlaveFactory()
        
    reactor.connectTCP(cfg.vals.parent_host, cfg.vals.parent_node_port,  # @UndefinedVariable
                       slaveFactory)
    
    # is this necessary???
    # The video registry slave should have its own job scheduler
    reactor.addSystemEventTrigger('before', 'shutdown', pythonioc.cleanServiceRegistry)  # @UndefinedVariable
    
    manholePort = reactor.listenTCP(0, ShellFactory())  # @UndefinedVariable
    log.msg('Started manhole on port %s' % str(manholePort.getHost().port))
    reactor.listenTCP(cfg.vals.webserver_port, siteutils.LoggingSite(root))  # @UndefinedVariable
    
    reactor.run()  # @UndefinedVariable
    
