'''
Created on Jul 28, 2014

@author: eh14
'''

from twisted.python import log
import optmodel
import pythonioc
from twisted.internet import defer
from haec.services import localtaskservice, activityservice, eventservice
from haec import config
from haec.protomixins import psoptimizer
import pandas
from haec.services import slaveconnservice
from haec.optimization import snapshotcreator
import math

cfg = pythonioc.Inject(config.Config)


class _SlaveBootMixin(object):
    """
    Strategy Mixin taking care of booting a slave or all slaves.
    """
    taskDao = pythonioc.Inject('taskDao')
    
    @defer.inlineCallbacks
    def _bootSlave(self, slaveName):
        log.msg('Scheduling booting of slave %s' % slaveName)
        yield self.taskDao.addLocalActionTask('Booting slave %s' % slaveName,
                                             localtaskservice.LocalTaskService.turnOnSlave,
                                             args=[slaveName])
        
        
    @defer.inlineCallbacks
    def _bootAllSlaves(self):
        slaves = yield self.slaveDao.getSlaves()
        for slave in slaves:
            if slave.status == 'offline':
                yield self._bootSlave(slave.name)
        
        defer.returnValue(slaves)

class _SlaveClassDataMixin(object):
    """
    Strategy Mixin taking care of 
    creating slave class data.
    """
    
    
    snapshotCreator = pythonioc.Inject(snapshotcreator.SystemSnapshotCreator)
    activityService = pythonioc.Inject(activityservice.ActivityService)
    slaveDao = pythonioc.Inject('slaveDao')
    
    # can be used to reuse the expensive calculation of slave data and snapshots etc.
    _currentSlaveClassData = None
    
    @defer.inlineCallbacks
    def _createSlaveClassData(self, aSlaves, setByteseconds=False, snapshot=None):
        slaves = yield self.slaveDao.getSlavesAsDict()
        
        byteSeconds = {}
        if setByteseconds:
            if not snapshot:
                snapshot = yield self.snapshotCreator.createSnapshot(self.optModel.model)
            byteSeconds = snapshot.getSlaveByteSeconds(allSlaves=True)
        def _createSlaveData(name):
            if not snapshot or (snapshot and name in snapshot.onlineSlaveNames):
                online = True
            else:
                online = False
            return {'online': online,
                    'class': 'A' if name in aSlaves else 'B',
                    'bs': byteSeconds.get(name, 0),
                    'ip' : cfg.getSlaveIp(name),
                    'viddir':slaves[name].videodir,
                    }
        self._currentSlaveClassData = {s:_createSlaveData(s) for s in slaves.keys()}
        
        self.activityService.updatePopClasses({name:s['class'] for name, s in self._currentSlaveClassData.iteritems()})
        
        defer.returnValue(self._currentSlaveClassData)
        
    def _broadcastSlaveClassData(self):
        
        if self._currentSlaveClassData is None:
            return
            
        slaveDataMessage = psoptimizer.MasterPsoMixin.prepareSlaveClassData(slaveData=self._currentSlaveClassData,
                                                          speed=self.optModel.model.initialSpeed)
        
        for slave in self.slaveConnService.getAllConnectedSlaves():
            self.reactor.callLater(0, slave.sendObject, slaveDataMessage)
        
    
class _VideoDataMixin(object):
    """
    Strategy Mixin creating video statistics
    and sending that data filtered to slaves.
    """
    videoDao = pythonioc.Inject('videoDao')
    
    _currentVideoData = None
    slaveConnService = pythonioc.Inject(slaveconnservice.SlaveConnService)
    optModel = pythonioc.Inject(optmodel.OptModelProvider)
    
    @defer.inlineCallbacks
    def _createVideoData(self, snapshot):
        assert snapshot, "Provide a snapshot!"
        names = []
        for vidId in snapshot.mapping.index.values:
            name = yield self.videoDao.getFilenameForId(vidId)
            names.append(name)
            
        fileNames = pandas.Series(names, index=snapshot.mapping.index)
        
        videoData = snapshot.mapping.copy()
        videoData['fname'] = fileNames
        self._currentVideoData = videoData
        
        defer.returnValue(videoData)
        
        
    def _sendVideoDataToSlave(self, slave):
        model = self.optModel.model
        if self._currentVideoData is None:
            return

        slaveName = slave.getSlaveName()
        slaveVids = self._currentVideoData.loc[self._currentVideoData[slaveName] == 1, ('fname', 'size', 'totalbyteseconds', 'popularity')]
        v = slaveVids.fillna(0).to_json(orient='split')
        
        psoptimizer.MasterPsoMixin.sendVideoDataToSlave(slave, videoData=v,
                                                        popularityThreshold=float(model.videoPopularityThreshold))
        
    def sendVideodataToSlaveByName(self, slaveName):
        slave = self.slaveConnService.getConnectedSlaveByName(slaveName)
        self._sendVideoDataToSlave(slave)

class Strategy(object):
    
    reactor = pythonioc.Inject('reactor')
    optModel = pythonioc.Inject(optmodel.OptModelProvider)
    eventService = pythonioc.Inject(eventservice.EventService)
    name = None
    
    def deploy(self):
        """
        Called to initially deploy the strategy cluster wide (or activate it).
        """        
        pass
    
    
    def undeploy(self):
        """
        Called to perform actions before removing the strategy.
        """
        pass
    
    
    def doOptimize(self):
        raise NotImplementedError('abstract')
    
    def getName(self):
        if self.name is not None:
            return self.name
        else:
            return str(self)
        
    def getPsoMode(self):
        """
        Returns the mode for the psoptimizer.
        """
        return psoptimizer.MODE_DISABLED

    
class NoStrategy(Strategy):
    name = "Disabled"
    
    def deploy(self):
        pass
    def undeploy(self):
        pass
    
    def doOptimize(self):
        pass
    
    def getPsoMode(self):
        """
        Stop all slaves from doing anything.
        """
        return psoptimizer.MODE_DISABLED
    
class AllOnStrategy(Strategy, _SlaveClassDataMixin, _SlaveBootMixin):
    
    name = 'All On'
    
    slaveDao = pythonioc.Inject('slaveDao')
    slaveConnService = pythonioc.Inject(slaveconnservice.SlaveConnService)
    
    @defer.inlineCallbacks
    def deploy(self):
        yield self._bootAllSlaves()
        
        slaveNames = yield self.slaveDao.getSlaveNames()
        slaveClassData = yield self._createSlaveClassData(slaveNames, False)
        self._slaveDataMessage = psoptimizer.MasterPsoMixin.prepareSlaveClassData(slaveData=slaveClassData,
                                                          speed=0)
        
        # send the data to all that are online now
        for slave in self.slaveConnService.getAllConnectedSlaves():
            self.reactor.callLater(0, slave.sendObject, self._slaveDataMessage)
        
        # subscribe for later events and send the same
        self.eventService.subscribeEvent('slave-online', self._sendSlaveClassData)
        
    def undeploy(self):
        self.eventService.unsubscribeEvent('slave-online', self._sendSlaveClassData)
        
    def _sendSlaveClassData(self, slaveName):
        slave = self.slaveConnService.getConnectedSlaveByName(slaveName)
        slave.sendObject(self._slaveDataMessage)
        
    @defer.inlineCallbacks
    def doOptimize(self):
        yield self._bootAllSlaves()
        
    def getPsoMode(self):
        """
        They're all on, nothing is optimized.
        """
        return psoptimizer.MODE_DISABLED
    
class AllOnBalancedStrategy(AllOnStrategy, _VideoDataMixin):
    """
    Simply treats all slaves as A-slaves and all videos as A-videos. All slaves are thus pure by definition
    and just do some load balancing.
    """
    name = 'All On (Balanced)'
    
    optModel = pythonioc.Inject(optmodel.OptModelProvider)
    
    slaveDao = pythonioc.Inject('slaveDao')
    slaveConnService = pythonioc.Inject(slaveconnservice.SlaveConnService)
    
    def __init__(self):
        AllOnStrategy.__init__(self)
        
        self._speed = 0.8
    
    @defer.inlineCallbacks
    def deploy(self):
        yield self._bootAllSlaves()
        
        # subscribe for later events and send the same
        self.eventService.subscribeEvent('slave-online', self._sendSlaveClassData)
        
        # every video is an A-video now
        self.optModel.model.videoPopularityThreshold = 0
        
    def undeploy(self):
        self.eventService.unsubscribeEvent('slave-online', self._sendSlaveClassData)
        
    def _sendSlaveClassData(self, slaveName):
        
        if self._currentSlaveClassData:
            slaveDataMessage = psoptimizer.MasterPsoMixin.prepareSlaveClassData(self._currentSlaveClassData, self._speed)
        
            slave = self.slaveConnService.getConnectedSlaveByName(slaveName)
            slave.sendObject(slaveDataMessage)
            self._sendVideoDataToSlave(slave)
        
    @defer.inlineCallbacks
    def doOptimize(self):
        # (1) boot all slaves
        slaves = yield self._bootAllSlaves()
        
        slaveClassData = yield self._createSlaveClassData([s.name for s in slaves], True)
        
        # (3) prepare a message to send the data to the slaves 
        slaveDataMessage = psoptimizer.MasterPsoMixin.prepareSlaveClassData(slaveClassData, self._speed)
        
        # (4) send it to all connected slaves
        for slave in self.slaveConnService.getAllConnectedSlaves():
            self.reactor.callLater(0, slave.sendObject, slaveDataMessage)
            
        # (5) create video data and send it to all online slaves
        
        snapshot = yield self.snapshotCreator.createSnapshot(self.optModel.model)
        yield self._createVideoData(snapshot)
        for slave in self.slaveConnService.getAllConnectedSlaves():
            self._sendVideoDataToSlave(slave)
            
    def getPsoMode(self):
        """
        They're all on, but they do load balancing
        """
        return psoptimizer.MODE_BALANCE
            
# class LoadOnlyStrategy__(Strategy, _VideoDataMixin, _SlaveClassDataMixin, _SlaveBootMixin):
#     name = 'Load Only Strategy'
# 
#     def __init__(self):
#         self._currentASlaves = []
#         self._currentSlavesWithVids = []
#         
#     def deploy(self):
#         self.eventService.subscribeEvent('slave-online', self._onSlaveOnline)
#         
#     def undeploy(self):
#         self.eventService.unsubscribeEvent('slave-online', self._onSlaveOnline)
#         
#     @defer.inlineCallbacks
#     def _onSlaveOnline(self, slaveName):
#         # optimizer did not run yet, so let's wait until it runs.
#         if not self._currentASlaves:
#             return
#         snapshot = yield self.snapshotCreator.createSnapshot(self.optModel.model)
#         
#         yield self._createSlaveClassData(self._currentASlaves, setByteseconds=True, snapshot=snapshot)
#         yield self._broadcastSlaveClassData()
#         
#     @defer.inlineCallbacks        
#     def doOptimize(self):
#         snapshot = yield self.snapshotCreator.createSnapshot(self.optModel.model)
#         self._refreshASlaves(snapshot)
#         
#         slaveData = yield self._createSlaveClassData(self._currentASlaves, setByteseconds=True, snapshot=snapshot)
#         yield self._broadcastSlaveClassData()
#         
#         # boot all slaves that have any videos but aren't online.
#         for slaveName, slave in slaveData.iteritems():
#             if (slave in self._currentSlavesWithVids and 
#                 not slave['online'] and
#                 not snapshot.isSlaveBooting(slaveName) and
#                 not snapshot.isSlaveFailed(slaveName)):
#                 yield self._bootSlave(slaveName)
#         
#         self.optModel.model.videoPopularityThreshold = 0
#         yield self._createVideoData(snapshot)
#         print snapshot.mapping
#         print self._currentASlaves
#         print self._currentSlavesWithVids
#         self.activityService.setAllSlavesUnpure()
#         for slave in self.slaveConnService.getAllConnectedSlaves():
#             self._sendVideoDataToSlave(slave)
#                 
#     def getPsoMode(self):
#         """
#         They're all on, but they do load balancing
#         """
#         return psoptimizer.MODE_BALANCE | psoptimizer.MODE_PURITY
#     
#     def _refreshASlaves(self, snapshot):
#         """
#         Returns the names of slaves classed A and slaves that contain any A-Videos.
#         """
#         model = self.optModel.model
#         
#         # check how many slaves we need per class
#         requiredASlaves = snapshot.mapping['totalbyteseconds'].sum() / model.maxBs
#         # round to int
#         requiredASlaves = int(math.ceil(requiredASlaves))
# 
#         # no a slaves required.
#         if requiredASlaves == 0:
#             self._currentASlaves = []
#             self._currentSlavesWithVids = []
#             return
#         
#         # number of videos per slave
#         slaveVids = snapshot.mapping.loc[:, snapshot.allSlaveNames].sum(axis=0).fillna(0).to_dict().items()
#         # sort descending
#         slaveVids.sort(lambda l, r: cmp(r[1], l[1]))
#         
#         classASlaves = slaveVids[:requiredASlaves]
#         allSlavesWithVids = [s[0] for s in slaveVids if s[1] > 0]
#         
#         self._currentASlaves = [s[0] for s in classASlaves]
#         self._currentSlavesWithVids = allSlavesWithVids
        
class PopularityStrategy(Strategy, _VideoDataMixin, _SlaveClassDataMixin, _SlaveBootMixin):
    """
    The bin packing strategy basically divides all videos into two classes:
    present: class A
    on-demand: class B
    
    The video popularity determines the class a video belongs to.
    
    Slaves are divided into three classes
    (i) class-A-provider
    (ii) class-B-provider
    
    Each slave basically tries to get rid of the videos of the other class,
    pushing them to other slaves. When class-B slaves are finished, they notify
    the master which then decides whether to turn them off.           
    """
    
    name = 'Popularity Strategy'
    
    slaveConnService = pythonioc.Inject(slaveconnservice.SlaveConnService)
    reactor = pythonioc.Inject('reactor')
    taskDao = pythonioc.Inject('taskDao')
    videoDao = pythonioc.Inject('videoDao')
    activityService = pythonioc.Inject(activityservice.ActivityService)
    optModel = pythonioc.Inject(optmodel.OptModelProvider)
    snapshotCreator = pythonioc.Inject(snapshotcreator.SystemSnapshotCreator)
    def __init__(self):
        self._currentASlaves = None
        self._currentSlavesWithAVids = None
        
    def deploy(self):
        self.eventService.subscribeEvent('slave-online', self._onSlaveOnline)
        
    def undeploy(self):
        self.eventService.unsubscribeEvent('slave-online', self._onSlaveOnline)
        
    def getPsoMode(self):
        """
        Returns the mode for the psoptimizer.
        """
        return psoptimizer.MODE_PURITY | psoptimizer.MODE_BALANCE
            
    @defer.inlineCallbacks
    def _onSlaveOnline(self, slaveName):
        # optimizer did not run yet, so let's wait until it runs.
        if not self._currentASlaves:
            return
        snapshot = yield self.snapshotCreator.createSnapshot(self.optModel.model)
        
        yield self._createSlaveClassData(self._currentASlaves, setByteseconds=True, snapshot=snapshot)
        yield self._broadcastSlaveClassData()
        
    @defer.inlineCallbacks
    def doOptimize(self):
        snapshot = yield self.snapshotCreator.createSnapshot(self.optModel.model)

        self._refreshASlaves(snapshot)
        
        slaveData = yield self._createSlaveClassData(self._currentASlaves, setByteseconds=True, snapshot=snapshot)
        yield self._broadcastSlaveClassData()
        
        
        # boot all slaves that are in class A but not online.
        for slaveName, slave in slaveData.iteritems():
            if (slave in self._currentSlavesWithAVids and 
                not slave['online'] and
                not snapshot.isSlaveBooting(slaveName) and
                not snapshot.isSlaveFailed(slaveName)):
                yield self._bootSlave(slaveName)
                
        self._updateVideoThreshold(snapshot)
        
        yield self._createVideoData(snapshot)
        
        for slave in self.slaveConnService.getAllConnectedSlaves():
            self._sendVideoDataToSlave(slave)
            
    def _refreshASlaves(self, snapshot):
        """
        Returns the names of slaves classed A and slaves that contain any A-Videos.
        """
        model = self.optModel.model
        
        classA = snapshot.mapping.loc[snapshot.mapping['popularity'] > model.videoPopularityThreshold, :]
               
        if classA.index.size == 0:
            self._currentASlaves = []
            self._currentSlavesWithAVids = []
        
        # check how many slaves we need per class
        requiredASlaves = classA['totalbyteseconds'].sum() / model.maxBs
        # round to int
        requiredASlaves = int(math.ceil(requiredASlaves))

        # no a slaves required.
        if requiredASlaves == 0:
            self._currentASlaves = []
            self._currentSlavesWithAVids = []
            return
        
        # number of videos per slave
        slaveVids = classA.loc[:, snapshot.allSlaveNames].sum(axis=0).fillna(0).to_dict().items()
        # sort descending
        slaveVids.sort(lambda l, r: cmp(r[1], l[1]))
        
        classASlaves = slaveVids[:requiredASlaves]
        allSlavesWithAVids = [s[0] for s in slaveVids if s[1] > 0]
        
        self._currentASlaves = [s[0] for s in classASlaves]
        self._currentSlavesWithAVids = allSlavesWithAVids
    
    def _updateVideoThreshold(self, snapshot):
        model = self.optModel.model
        if snapshot.mapping.index.size == 0:
            model.videoPopularityThreshold = 0
            return
        
        popularity = snapshot.mapping['popularity']
        minPop = popularity.min()
        maxPop = popularity.max()
        
        # if theyre all the same,
        # make the threshold higher than the highest, so all
        # belong to class B
        if minPop == maxPop:
            threshold = minPop + 1
        else:
            threshold = (maxPop - minPop) * model.classBVideoFraction + minPop
        
        model.videoPopularityThreshold = threshold

class LoadOnlyStrategy(PopularityStrategy):
    """
    Strategy that basically turns on as many slaves as needed for serving
    videos.
    """
    name = 'Load Only Strategy'
    
    def __init__(self):
        PopularityStrategy.__init__(self)
        
    def deploy(self):
        PopularityStrategy.deploy(self)
        self._updateVideoThreshold(None)
        
    def _updateVideoThreshold(self, snapshot):
        self.optModel.model.videoPopularityThreshold = 0