'''
Created on Jul 28, 2014

@author: eh14
'''

import numpy
from haec.optimization import optmodel

class SnapshotModifier(object):
    def __init__(self):
        
        self._appliedOperators = []
        
    def addOperator(self, operator):
        canApply = operator.canApply(self)
        if canApply:
            operator.apply(self)
            self._appliedOperators.append(operator)
            
        return canApply

    def getAvgLength(self):
        return sum([op.duration(self) for op in self._appliedOperators])
    
    def getOperators(self):
        return list(self._appliedOperators)
    
    def hasOperators(self):
        return len(self._appliedOperators) > 0
    
class SystemSnapshot(SnapshotModifier):
    def __init__(self,
                 fromTimestamp,
                 toTimestamp,
                 slaveStates,
                 optmodel,
                 mapping):
        
        SnapshotModifier.__init__(self)
        
        self.fromTimestamp = fromTimestamp
        self.toTimestamp = toTimestamp
        self.allSlaveNames = slaveStates.keys()
        self.allSlaveNames.sort()
        self.onlineSlaveNames = [name for name, status in slaveStates.iteritems() if status == 'online']
        self.slaveStates = slaveStates
        self.mapping = mapping
        self.model = optmodel
        
        # some static derived metrics
        self.mapping['replication'] = self.getMappingForOnlineSlaves().sum(axis=1)
        self.mapping['maxstreams'] = self.mapping.loc[:, 'totalbyteseconds'] / self.model.maxVidStreamBs
        self.mapping['optimalReplication'] = self.mapping.loc[:, 'maxstreams'] + self.mapping.loc[:, 'concurrentRequests']
        self.mapping['optimalReplication'] = self.mapping.loc[:, 'optimalReplication'].apply(lambda x:max(1.0, x))
            
        self.updateVideoStats()
        
    def getOptimizationModel(self):
        return self.model
        
    def getNumOnlineSlaves(self):
        return len(self.onlineSlaveNames)
    
    def getNumOfflineSlaves(self):
        return len(self.allSlaveNames) - self.getNumOnlineSlaves()
    
    def getNumSlaves(self):
        return len(self.allSlaveNames)
    
    def isSlaveBooting(self, slave):
        return self.slaveStates[slave] == 'boot'
    
    def isSlaveFailed(self, slave):
        return self.slaveStates[slave] == 'failure'
        
    def getMappingForOnlineSlaves(self):
        return self.mapping.loc[:, self.onlineSlaveNames]
    
    def getMappingForAllSlaves(self):
        return self.mapping.loc[:, self.allSlaveNames]
    
    def _getOnlineSlaveNames(self):
        return self.onlineSlaveNames
    
    def getOfflineSlaveNames(self):
        return [name for name in self.allSlaveNames if name not in self.onlineSlaveNames]
    
    def getUsedSpace(self):
        """
        Returns a list of all active slave's free space
        """
        usedSizes = self.getMappingForOnlineSlaves().multiply(self.mapping['size'], axis=0)
        return (usedSizes.sum(axis=0) / self.model.MB).to_dict()
    
    def getAvgPopularity(self, asDict=True):
        """
        average popularity, grouped by slave.
        
        @param asDict: if true, the results are returned as dict {<slaveName>:avg popularity}.
        """
        
        avgPopularity = self.getMappingForOnlineSlaves().multiply(self.mapping['popularity'], axis=0).mean(axis=0)
        if asDict:
            return avgPopularity.to_dict()
        else:
            return avgPopularity
        
    
    def getTotalVideoPopularity(self):
        return self.mapping.loc[:, 'popularity'].mean(axis=0)
    
    def updateVideoStats(self, videoName=None):
        if videoName is None:
            self.mapping['avgbyteseconds'] = self.mapping.loc[:, 'totalbyteseconds'] / self.mapping.loc[:, 'replication']
        else:
            self.mapping.loc[videoName, 'avgbyteseconds'] = (self.mapping.loc[videoName, 'totalbyteseconds'] / 
                                                             self.mapping.loc[videoName, 'replication'])
        

    def getSlaveByteSeconds(self, raw=False, allSlaves=False):
        
        if allSlaves:
            slaves = self.getMappingForAllSlaves()
        else:
            slaves = self.getMappingForOnlineSlaves()
        #print self.mapping
        #print slaves.mul(self.mapping.loc[:, 'avgbyteseconds'], axis='index').sum(axis=0).fillna(0)
        result = slaves.mul(self.mapping.loc[:, 'avgbyteseconds'], axis='index').sum(axis=0).fillna(0)
        if raw:
            return result
        else:
            return result.to_dict()
        
    def getMByteSeconds(self):
        return {key: value / self.model.MB for key, value in self.getSlaveByteSeconds().iteritems()}
    
    def getNumberOfVideos(self, asDict=True):
        numVids = self.getMappingForOnlineSlaves().sum(axis=0)
        
        if asDict:
            return numVids.to_dict()
        else:
            return numVids
    
    def getSumOfVideoSize(self, videoIds):
        return self.mapping.loc[videoIds, 'size'].sum()
    
    def getVideoSize(self, videoId):
        return self.mapping.loc[videoId, 'size']
    
    def getMappedVideoReplication(self):
        """
        Returns a dataframe of all videos and their replication values for each slave
        (which is the same for each slave, but there's no point in splitting it for a slave
        right now.
        """
        onlineSlaves = self.getMappingForOnlineSlaves()
        return onlineSlaves.multiply(self.mapping.loc[:, 'replication'], axis=0)
    
    def assertAllVideosAvailable(self):
        assert not numpy.isnan(self.mapping.loc[:, 'replication']).any(), "some video is not available."
    
    def createMapForNonReplicatedVideos(self):
        """
        Returns a Series mapping slaves to the number of videos stored
        on that slave, whose replication value is 1 (i.e. it only resides
        on this slave)
        """
        
        onlineSlaves = self.getMappingForOnlineSlaves()
        replication = self.mapping.loc[:, 'replication']
        nonReplicatedVids = replication.where(replication == 1).dropna()
        return onlineSlaves.reindex(nonReplicatedVids.index)
    
    def clone(self):
      
        clone = SystemSnapshot(self.fromTimestamp,
                              self.toTimestamp,
                              dict(self.slaveStates),
                              self.model,
                              self.mapping.copy(True))
        
        # copy operator list
        clone._appliedOperators = list(self._appliedOperators)
        return clone
        
