'''
Created on Jul 25, 2014

@author: eh14
'''
from functools import wraps

# http://stackoverflow.com/questions/1094841/reusable-library-to-get-human-readable-version-of-file-size
def sizeof_fmt(num):
    for x in ['bytes', 'KB', 'MB', 'GB', 'TB']:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0


def find(function, items, msg=''):
    for item in items:
        if function(item):
            return item
        
    raise IndexError('Item not found in the collection (%s)' % msg)

def weightedSum(summands, weights):
    sumWeight = float(sum(weights))
    return sum([float(w) / sumWeight * float(v) for (w, v) in zip(weights, summands)])
    

def cached(cacheKey=0):
    
    def funcWrapper(func):
        valueCache = {}
        setattr(func, '__cache', valueCache)
        setattr(func, 'clear_cache', lambda *args:valueCache.clear())
        
        @wraps(func)
        def wrappee(self, *args, **kwargs):
            key = args[cacheKey]
            if key in valueCache:
                return valueCache[key]
            else:
                result = func(self, *args, **kwargs)
                valueCache[key] = result
                
                return result
            
        return wrappee
    
    return funcWrapper