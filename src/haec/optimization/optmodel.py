"""
Represents a model of all parameters used and learned by the system used for optimization.
"""
from haec import config
import pythonioc
from haec.services import statisticservice

cfg = pythonioc.Inject(config.Config)

class OptModelProvider(object):
    
    statisticService = pythonioc.Inject(statisticservice.StatisticService)
    def __init__(self):
        self._model = OptimizationModel()
        
    @property
    def model(self):
        return self._model
    
    def update(self):
        avgBootTime = self.statisticService.getAverageSlaveBootTime()
        if avgBootTime is not None:
            self._model.bootTime = avgBootTime 
        
    
class OptimizationModel(object):
    MB = 1024 * 1024
    def __init__(self):
        # maximum of 5MB/s streaming transfer per slave
        self.maxBs = 5 * self.MB
        
        
        self.bootTime = 30
        
        self.shutdownTime = 15
        
        self.systemLoadMargin = 0.1
        
        # if we do not limit this size,
        # all slaves except one would be switched off if 
        # the system is idle.
        self.slaveSpace = 1000 * self.MB
        
        #
        # number of seconds we consider a period
        # where hits are "recent"
        self.recentHitPeriod = 120
        
        #
        # set or learn that.
        # It's used to estimate the fitness of migration duration,
        # since we generally prefer a short migration plan.
        # Also this should be less than the optimization interval
        # so the optimization is done, when the next round is analyzed.
        self.maxMigrationTime = 120.0
        
        # power consumption of a cubieboard
        # replace with function
        self.slaveConsumption = 300
        
        # switch-consumption
        self.switchConsumption = 150
        
        # limit minimum number of online slaves,
        # so the optimizer does not shut down all slaves.
        self.minimumOnlineSlaves = 10
        
        #
        # number of seconds where the last hitrate is 
        # taken into account
        # unused yet
        self.hitratePeriod = None
        
        # max number of bytes/s that should be used by one
        # video. Used to determine the optimal video replication level.
        self.maxVidStreamBs = 0.5 * self.MB

        # something for the video balancing strategy (evoluationary computing)
        self.numMutationSteps = range(1, 10)
        self.rateSurvivors = 0.3
        self.numGenerations = 30
        self.populationSize = 12
        
        
        # #############
        # PSO Options #
        # #############
        
        self.classBVideoFraction = 0.25
        
        self.videoPopularityThreshold = 0
        
        #
        # time in seconds, a slave needs to be idle before
        # a shutdown is initiated.
        # Note that the shutdown might way to for current streamings/transcodings
        # to terminate before it is performed.
        self.idleTimeBeforeShutdown = cfg.vals.opt_initial_slave_idle_timeout
        
        #
        # PSO is based on a speed in which all slaves 
        # perform their actions in the appropriate direction.
        # The purpose of this is to avoid overswinging.
        self.initialSpeed = cfg.vals.pso_initial_speed
