'''

@author: eh14
'''
import time
from haec.optimization import snapshots
import pythonioc
from twisted.python import log
from haec import utils
import snapshotextensions

class SystemSnapshotCreator(object):
    
    slaveDao = pythonioc.Inject('slaveDao')
    videoDao = pythonioc.Inject('videoDao')
    hitTracker = pythonioc.Inject('hitTracker')
    
    def __init__(self):
        self._optmodel = None
        
        self._lastSnapshot = None
    
    @utils.runAsDeferredThread
    def createSnapshot(self, optmodel):
        self._optmodel = optmodel
        
        toTimestamp = time.time()
        fromTimestamp = toTimestamp - self._optmodel.recentHitPeriod
        
        # start = time.time()
        slaves = self.slaveDao.getSlaves()
        # log.msg('get slaves %s' % (time.time() - start))
        
        start = time.time()
        mapping = self.createVideoMapping()
        log.msg('create video mapping %s ' % (time.time() - start))
        
        self._lastSnapshot = snapshots.SystemSnapshot(fromTimestamp,
                    toTimestamp,
                    {s.name:s.status for s in slaves},
                    self._optmodel,
                    mapping)
        
        return self._lastSnapshot
        
    @utils.runAsDeferredThread
    def createVideoMapping(self):
        start = time.time()
        hitMatrix = self.hitTracker.constructHitMatrix()
        
        slaves = self.slaveDao.getSlaves()
        videoSlaveMapping = self.videoDao.getVideoSlaveMapping()
        
        vidIds = []
        sizes = []
        lengthes = []
        for vid in self.videoDao.getVideos():
            vidIds.append(vid.id)
            sizes.append(vid.fsize)
            lengthes.append(vid.duration)
            
        log.msg('prepare snapshot information %s' % (time.time() - start))
        
        start = time.time()
        snappi = snapshotextensions.createSnapshotMapping(self._optmodel.recentHitPeriod, hitMatrix, vidIds, sizes, lengthes, slaves, videoSlaveMapping)
        log.msg('create snapshot took %s' % (time.time() - start))
        return snappi
        
    def getLastSnapshot(self):
        return self._lastSnapshot