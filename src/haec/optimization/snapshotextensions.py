import pandas

class Log(object):
    def msg(self, msg):
        print msg
        
log = Log()

def createSnapshotMapping(hitInterval, hitMatrix, vidIds, sizes, lengthes, slaves, videoSlaveMapping):
    """
    @param hitInterval: period in seconds we want to create the snapshot for. This applies especially to popularity and hitsintime for all videos.
    """
    hitInterval = float(hitInterval)
    assert hitInterval > 0, 'Invalid hitinterval returned'    
    hitMinutes = hitInterval / 60.0
    
    
    # start = time.time()
    if not hitMatrix.empty:
        # popularity is defined by the number of hits in the time we consider "recent" + the last 30 minutes.
        # the two values are linear combined: 25% last 30minutes + 75% the "recent hit time".
        # the last 30 minutes is rounded so we don't end up waiting 30 minutes until a video becomes
        # unpopular again (pop == 0)
        hitMatrix['popularity'] = (((hitMatrix['30minutes'] * ((hitMinutes * 0.25) / 30.0))) + (hitMatrix['minute'] * (hitMinutes * 0.75))).round(5)
        # 
        # hits in time is the number of hits per second according to the popularity
        hitMatrix['hitsintime'] = hitMatrix['popularity'] / hitInterval
        
    else:
        hitMatrix['popularity'] = pandas.Series()
        hitMatrix['hitsintime'] = pandas.Series()
        
    # log.msg('Calculating popularity and hits/time %s ' % (time.time() - start))
        
    # start = time.time()
    hitsintime = hitMatrix['hitsintime'].reindex(vidIds).fillna(0)
    # log.msg('adding videos to mapping %s ' % (time.time() - start))
                
    # start = time.time()
    videoMapping = pandas.DataFrame(index=vidIds)
    videoMapping['size'] = pandas.Series(data=sizes, index=vidIds)
    # log.msg('size %s ' % (time.time() - start))
    videoMapping['length'] = pandas.Series(data=lengthes, index=vidIds)
    # log.msg('length %s ' % (time.time() - start))
    videoMapping['popularity'] = hitMatrix['popularity'].reindex(vidIds).fillna(0)
    # log.msg('popularity %s ' % (time.time() - start))
    videoMapping['concurrentRequests'] = hitMatrix['hitsintime'] * videoMapping['length']
    videoMapping['hitsintime'] = hitsintime
    # log.msg('concurrentRequests %s ' % (time.time() - start))
    videoMapping['totalbyteseconds'] = (videoMapping['size'] * hitsintime)
    # log.msg('totalbyteseconds %s ' % (time.time() - start))
        
    # adding columns for all slaves.
    for slave in slaves:
        videoMapping[slave.name] = pandas.Series()
        
    if len(videoSlaveMapping):
        # start = time.time()
        # add the mapping
        df = pandas.DataFrame(videoSlaveMapping)
        df[2] = 1
        actualMapping = df.pivot(0, 1, 2)
        # log.msg('do pivoting %s' % (time.time() - start))
        # start = time.time()        
        for col in actualMapping.columns:
            videoMapping[col] = actualMapping[col]
        # log.msg('append to mapping %s ' % (time.time() - start))
    
    return videoMapping

