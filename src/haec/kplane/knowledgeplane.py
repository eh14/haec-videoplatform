import collections
from contextlib import contextmanager
import pythonioc
import sqlalchemy
from sqlalchemy.engine import reflection
from sqlalchemy.orm.scoping import scoped_session
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.schema import (
    MetaData,
    Table,
    DropTable,
    ForeignKeyConstraint,
    DropConstraint,
    )
import time
from twisted.python import log

from haec import config
from haec.kplane.domain import Base


cfg = pythonioc.Inject(config.Config)

class DbConnectionProvider(object):
    
    reactor = pythonioc.Inject('reactor') 
    def postInit(self):
        self._engine = sqlalchemy.create_engine(cfg.vals.db_connectionurl,
                                                echo=cfg.vals.db_echo)
        self._sessionMaker = scoped_session(sessionmaker(bind=self._engine))
        
        self._initialized = False
        self.reset()
        self._initialized = True
        
        self._times = collections.deque(maxlen=50)
        
    def reset(self):
        
        if cfg.vals.db_drop:
            # drop table dropping foreign keys first.
            # idea taken from https://bitbucket.org/zzzeek/sqlalchemy/wiki/UsageRecipes/DropEverything
            createDropEngine = sqlalchemy.create_engine(cfg.vals.db_connectionurl,
                                                    echo=cfg.vals.db_echo)
            maker = sessionmaker(bind=createDropEngine)
            db = maker()
            inspector = reflection.Inspector.from_engine(createDropEngine)
              
            metadata = MetaData()
            # gather all data first before dropping anything.
            # some DBs lock after things have been dropped in 
            # a transaction.
            tbs = []
            all_fks = []
            
            for table_name in inspector.get_table_names():
                fks = []
                for fk in inspector.get_foreign_keys(table_name):
                    if not fk['name']:
                        continue
                    fks.append(
                        ForeignKeyConstraint((), (), name=fk['name'])
                        )
                t = Table(table_name, metadata, *fks)
                tbs.append(t)
                all_fks.extend(fks)
              
            for fkc in all_fks:
                db.execute(DropConstraint(fkc))
              
            for table in tbs:
                db.execute(DropTable(table))

            db.flush()
            
        if cfg.vals.db_create:
            Base.metadata.create_all(sqlalchemy.create_engine(cfg.vals.db_connectionurl,  # @UndefinedVariable  
                                                              echo=cfg.vals.db_echo)) 
        
    @contextmanager    
    def __call__(self):
        """
        Decorator that keeps track of creating, commiting/rollingback 
        and closing the session.
        Idea taken from sqlalchemy documentation.
        """
        if not self._initialized:
            raise Exception("Don't use the creator until it's initialized!")
        
        if self._sessionMaker.registry.has():
            session = self._sessionMaker()
            newSession = False
        else:
            session = self._sessionMaker(autocommit=True, autoflush=False, expire_on_commit=False)
            newSession = session.transaction is None or not session.transaction.is_active
            
        try:
            start = time.time()
            if newSession:
                session.begin()
                
            yield session
            
            if newSession:
                session.commit()
            else:
                session.flush()
        except:
            session.rollback()
            raise
        finally:
            
            self._logOrPrintTime(start)
            if newSession:
                session.close()
                self._sessionMaker.registry.clear()
                
    def _logOrPrintTime(self, start):
        rendertime = ((time.time() - start) * 1000)
        self._times.append(rendertime)
        avg = sum(self._times) / len(self._times)
        if rendertime > avg * 2:
            log.msg('db session took %.2fms (avg %.2fms)' % (rendertime, avg))
    