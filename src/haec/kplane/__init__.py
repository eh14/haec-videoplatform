import pythonioc

def registerInGlobalRegistry(overwrite=True):
    
    from haec.kplane import taskdao, knowledgeplane, profiledao, \
        videodao, slavedao, sensorstore, hitcounter
    
    pythonioc.registerService(videodao.VideoDao, overwrite=overwrite)
    pythonioc.registerService(slavedao.SlaveDao, overwrite=overwrite)
    pythonioc.registerService(taskdao.TaskDao, overwrite=overwrite)
    pythonioc.registerService(sensorstore.SensorStore, overwrite=overwrite)
    pythonioc.registerService(hitcounter.HitTracker, overwrite=overwrite)
    pythonioc.registerService(profiledao.ProfileDao, overwrite=overwrite)
    pythonioc.registerService(knowledgeplane.DbConnectionProvider, serviceName='dbprovider', overwrite=overwrite)
    