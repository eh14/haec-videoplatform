from haec.kplane import domain
from sqlalchemy.sql.operators import like_op
from sqlalchemy.orm import joinedload
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.sql import functions
from twisted.python import log
import pythonioc
from haec import utils
from twisted.internet import defer
import threading

class VideoDao(object):
    
    db = pythonioc.Inject('dbprovider')
    slaveDao = pythonioc.Inject('slaveDao')
    """
    Mixin class backing an SQLAlchemy with sqlite interface to
    the knowledgeplane
    """
    def __init__(self):
        self._addVideoLock = threading.Lock()
        self._videoIdFilenameBuffer = None
     
    @utils.runAsDeferredThread   
    def addVideo(self, slaveName, video):
        vids = self.addVideosToSlave(slaveName, [video])
        return vids[0].id
    
    def syncAddVideo(self, db, video):
        with self._addVideoLock:
            variant = '' if not video.variant else video.variant
            q = db.query(domain.Video)
            q = q.filter(domain.Video.name == video.name)
            q = q.filter(domain.Video.variant == variant)
            res = q.all()
            if len(res):
                return res[0]
            else:
                db.add(video)
                return video
    
    @utils.runAsDeferredThread    
    def getUnreplicatedVideosForSlave(self, slaveName):
        with self.db() as db:
            
            sub = db.query(domain.video2slaveTable).join(domain.Slave)
            sub = sub.filter(domain.Slave.status == 'online')
            sub = sub.group_by('video_id').having(functions.count('video_id') == 1).subquery()
            
            q = db.query(domain.Video).join('slaves').join(sub).filter(domain.Slave.name == slaveName)
            vids = q.all()
            db.expunge_all()
            return vids
        
    @utils.runAsDeferredThread
    def addVideosToSlave(self, slaveName, videos):
        with self.db() as db:
            slave = self.slaveDao.syncAddOrUpdateSlave(db, domain.Slave(name=slaveName))
            outVids = []
            for video in videos:
                video = self.syncAddVideo(db, video)
                slave.videos.append(video)
                outVids.append(video)
            
            db.flush()
            db.expunge_all()
            return outVids
        
    @utils.runAsDeferredThread
    def addVideoToSlave(self, videoId, slaveName):  # , failOnError=True):
        with self.db() as db:
            db.execute(domain.video2slaveTable.insert().prefix_with('IGNORE')
                                .values({'video_id':videoId,
                                        'slave_name':slaveName
                                        })
                       )
    @utils.runAsDeferredThread           
    def _updateVideoIdFilenameBuffer(self):
        with self.db() as db:
            values = db.query(domain.Video.id, domain.Video.fname).all()
            self._videoIdFilenameBuffer = {r[0]:r[1] for r in values}
            
    @utils.runAsDeferredThread
    def _getFilenameForId(self, videoId):
        with self.db() as db:
            names = db.query(domain.Video.fname).filter(domain.Video.id == videoId).all()
            if len(names) == 0:
                return None
            else:
                return names[0][0]
        
    @defer.inlineCallbacks
    def getFilenameForId(self, videoId):
        """
        Returns all videoIds and corresponding file names. Either loaded from db (first time)
        or just a copy of the buffer.
        """
        if self._videoIdFilenameBuffer is None:
            self._videoIdFilenameBuffer = {}
            yield self._updateVideoIdFilenameBuffer()
                
        if videoId in self._videoIdFilenameBuffer:
            defer.returnValue(self._videoIdFilenameBuffer[videoId])
        else:
            name = yield self._getFilenameForId(videoId)
            if name:
                self._videoIdFilenameBuffer[videoId] = name
                defer.returnValue(name)
        
        defer.returnValue(None)
    
    @utils.runAsDeferredThread
    def removeVideoFromSlave(self, videoId, slaveName):
        with self.db() as db:
            vid = db.query(domain.Video).get(videoId)
            vid.slaves.remove(db.query(domain.Slave).get(slaveName))
            
            if len(vid.slaves) == 0:
                db.delete(vid)
    
    @utils.runAsDeferredThread
    def isVideoOnSlave(self, videoId, slaveName):
        with self.db() as db:
            cnt = db.query(domain.video2slaveTable).filter_by(video_id=int(videoId), slave_name=slaveName).count()
            return cnt > 0
            
    @utils.runAsDeferredThread
    def getVideoSlaveMapping(self):
        with self.db() as db:
            return db.query(domain.video2slaveTable.c.video_id,
                            domain.video2slaveTable.c.slave_name).all()
        
    @utils.runAsDeferredThread
    def getVideoNames(self):
        """
       Returns all video names in the database. Mostly used for testing.
        """
        with self.db() as db:
            return [x[0] for x in db.query(domain.Video.name).
                                        distinct(domain.Video.name).
                                        order_by(domain.Video.id).
                                        all()]
        
    @utils.runAsDeferredThread
    def getSlavesForVideo(self, name, variant):
        
        with self.db() as db:
            vid = db.query(domain.Video).filter_by(name=name, variant=variant).options(joinedload('slaves')).one()
            
            db.expunge_all()
            return vid.slaves
        
    @utils.runAsDeferredThread
    def getVideo(self, name, variant, loadSlaves=False):
        with self.db() as db:
            q = db.query(domain.Video).filter_by(name=name, variant=variant)
            if loadSlaves:
                q = q.options(joinedload('slaves'))
                
            vid = q.one()
            
            db.expunge_all()
            return vid
        
    @utils.runAsDeferredThread
    def getVideoById(self, videoId):
        with self.db() as db:
            vid = db.query(domain.Video).get(int(videoId))
            db.expunge(vid)
            return vid
        
    @utils.runAsDeferredThread
    def findVideo(self, name, variant, loadSlaves=False):
        try:
            return self.getVideo(name, variant, loadSlaves)
        except NoResultFound:
            pass
        return None
        
    @utils.runAsDeferredThread
    def getVideos(self):
        """
        Really returns ALL videos. Use with care, only for testing/debugging
        """
        with self.db() as db:
            vids = db.query(domain.Video).all()
            db.expunge_all()
            return vids
        
    @utils.runAsDeferredThread
    def getVideosBySlaveName(self, slaveName):
        with self.db() as db:
            slaves = db.query(domain.Slave).filter_by(name=slaveName).options(joinedload('videos')).limit(1).all()
            
            if len(slaves) == 0:
                return []
            else:
                slave = slaves[0]
            
            db.expunge_all()
            return slave.videos
    @utils.runAsDeferredThread
    def removeVideos(self, slaveName, videoPaths):
        if len(videoPaths) == 0:
            return
        
        with self.db() as db:
            slave = db.query(domain.Slave).get(slaveName)
            
            for vid in list(slave.videos):
                if vid.fname in videoPaths:
                    slave.videos.remove(vid)
                if len(vid.slaves) == 0:
                    db.delete(vid)
    
    @utils.runAsDeferredThread
    def searchVideos(self, name=None, namePattern=None, videoCodec=None,
                     audioCodec=None, height=None, width=None, loadSlaves=False):
        with self.db() as db:
            q = db.query(domain.Video)
            if namePattern:
                q = q.filter(like_op(domain.Video.name, namePattern.replace('*', '%')))
                
            if name:
                q = q.filter_by(name=name)
                
            if videoCodec:
                q = q.filter_by(video_codec=videoCodec)
                
            if audioCodec:
                q = q.filter_by(audio_codec=audioCodec)
                
            if height:
                q = q.filter_by(height=height)
                
            if width:
                q = q.filter_by(width=width)
                
            if loadSlaves:
                q = q.options(joinedload('slaves'))
                
            q = q.limit(30)
                
                
            result = q.all()
            db.expunge_all()
            
            return result
        
    @utils.runAsDeferredThread
    def getAudioVideoCodecNames(self):
        with self.db() as db:
            vidCodecs = db.query(domain.Video.video_codec).distinct().all()
            audioCodecs = db.query(domain.Video.audio_codec).distinct().all()
            
            db.expunge_all()
            
            return {'video' : [x[0] for x in vidCodecs],
                    'audio' : [x[0] for x in audioCodecs]}
            
    @utils.runAsDeferredThread
    def getVideoVariantsForName(self, name):
        with self.db() as db:
            vids = db.query(domain.Video).filter(domain.Video.name == name).group_by(domain.Video.variant).all()
            
            db.expunge_all()
            return vids
        
    @utils.runAsDeferredThread
    def getVideosForVariant(self, name, variant):
        with self.db() as db:
            vids = db.query(domain.Video).filter(domain.Video.name == name).filter(domain.Video.variant == variant).all()
            
            db.expunge_all()
            return vids

    @utils.runAsDeferredThread
    def getAllVideosForName(self, name, loadSlaves=False):
        with self.db() as db:
            q = db.query(domain.Video).filter(like_op(domain.Video.name, name.replace('*', '%')))
            if loadSlaves:
                q = q.options(joinedload('slaves'))
            vids = q.all()
            db.expunge_all()
            return vids
        
    @utils.runAsDeferredThread
    def getTotalNumberOfVideos(self):
        with self.db() as db:
            return db.query(domain.Video).count()
        

    @utils.runAsDeferredThread
    def cleanOrphanVideos(self):
        """
        Sometimes, video-slave relations get deleted without deleting the video.
        This method deletes all orphan videos at startup.
        
        Use the following query:
        delete from videos where not exists (select * from video2slave where videos.id = video2slave.video_id);
        """
        with self.db() as db:
            db.execute('''delete from videos 
                             where not exists 
                                 (select * from video2slave 
                                  where videos.id = video2slave.video_id)''')
            
            
    
    @utils.runAsDeferredThread
    def deleteAll(self):
        with self.db() as db:
            db.query(domain.Video).delete()