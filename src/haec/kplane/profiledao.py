'''

@author: eh14
'''
import pythonioc
from haec.kplane import domain
from haec import utils


class ProfileDao(object):
    db = pythonioc.Inject('dbprovider')
    
    
    @utils.runAsDeferredThread
    def getNames(self):
        with self.db() as db:
            names = db.query(domain.Profile.name).order_by(domain.Profile.name).all()
            
            return [n[0] for n in names]
        
    @utils.runAsDeferredThread
    def add(self, **kwargs):
        with self.db() as db:
            prof = domain.Profile(**kwargs)
            db.add(prof)
    
    @utils.runAsDeferredThread
    def getAll(self):
        with self.db() as db:
            profs = db.query(domain.Profile).all()
            db.expunge_all()
            return profs