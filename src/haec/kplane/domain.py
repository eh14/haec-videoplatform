from sqlalchemy import Integer, Sequence, Column, String, \
                        Enum, DateTime, Table, types, event

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import ForeignKey, UniqueConstraint
from sqlalchemy.sql.ddl import DDL
import datetime
import json
import itertools
Base = declarative_base()



class NumberTuple(types.TypeDecorator):
    def __init__(self, count=3, precision=5, converter=float):
        self._formatStr = '|'.join(itertools.repeat('%%.%df' % precision, 3))
        self.impl = types.String(precision * 2 * count)
        
        self._count = count
        self._precision = precision
        self._converter = converter

    def process_bind_param(self, value, dialect):
        value = tuple(value)
        assert len(value) == self._count, "Need %d values!" % self._count
        return self._formatStr % tuple(map(self._converter, value))
                            
    def process_result_value(self, value, dialect):
        if value is None:
            return None
        else:
            values = value.split('|')
            assert len(values) == self._count, "Expected %d values, got %s" % (self._count, value)
            return [self._converter(x) for x in values]
    
    def copy(self):
        return NumberTuple(self._count, self._precision, self._converter)

class JsonCol(types.TypeDecorator):
    """
    Type wrapper that encodes and decodes list or dict to a json-string and back
    for storing in the database.
    """
    def __init__(self, maxSize):
        self.impl = types.String(1024)
    
    def process_bind_param(self, value, dialect):
        return json.dumps(value)
                            
    def process_result_value(self, value, dialect):
        if value is None:
            return None
        return json.loads(value)
    
    def copy(self):
        return JsonCol(self.impl.length)

#
# M:M-Table mapping videos to slaves.
# 
video2slaveTable = Table('video2slave', Base.metadata,
    Column('video_id', Integer(), ForeignKey('videos.id', ondelete='CASCADE'), primary_key=True),
    Column('slave_name', String(64), ForeignKey('slaves.name', ondelete='CASCADE'), primary_key=True),
    # mysql_engine='MEMORY'
)

class Video(Base):
    __tablename__ = 'videos'
    
    #
    # normal sequence integer ID
    id = Column(Integer(), Sequence('video_id_seq'), primary_key=True)
    
    #
    # name of the video
    # Several videos can have the same name, 
    # in this case however must have different
    # variants
    name = Column(String(64), default='', nullable=False)
    # video variant (mostly generated using a timestamp)
    variant = Column(String(64), default='', nullable=False)
    
    #
    # video picture dimensions
    width = Column(Integer(), default=0)
    height = Column(Integer(), default=0)
    
    #
    # used codecs
    video_codec = Column(String(32), default='')
    audio_codec = Column(String(32), default='')
    
    #
    # length in seconds
    duration = Column(Integer(), default=0)
    
    #
    # video bitrate as extracted by avprobe
    bitrate = Column(String(32), default=0)
    
    #
    # more details
    fps = Column(String(32), default=0)
    
    #
    # format extracted by avprobe. This is not
    # necessarily the file extension.
    format = Column(String(32), default='')
    
    #
    # file name
    fname = Column(String(128), default='')
    
    #
    # file size
    fsize = Column(Integer(), default=0)
    
    uploadtime = Column(DateTime(), default=lambda:datetime.datetime.now())
    
    __table_args__ = (
                      UniqueConstraint('name', 'variant', name='unique_variantname'),
                      # {'mysql_engine':'MEMORY'}
                      )

class Switch(Base):
    __tablename__ = 'switches'
    
    name = Column(String(64), primary_key=True)
    slaves = relationship('Slave', order_by='Slave.name', backref='switch')
    
    # __table_args__ = {'mysql_engine':'MEMORY'}
    
    
slaveStates = ['online',  # online (registered by VP)
               'boot',  # slave is booting
               'shutdown',  # slave is about to shutdown and should not receive any more requests
               'failure',  # slave failed to boot and will be considered broken.
               'offline'  # not connected (being powered is a different thing)
               ]
class Slave(Base):
    __tablename__ = 'slaves'
    
    
    name = Column(String(64), primary_key=True)
    
    webserver_port = Column(String(5), default='0')
    videodir = Column(String(128), default='')
    
    status = Column(Enum(*slaveStates), default='offline') 
    
    videos = relationship("Video", secondary=video2slaveTable, backref="slaves")
    
    switch_name = Column(String(64), ForeignKey('switches.name'))
    
    @property
    def online(self):
        return self.status == 'online'
    
    def getStatusOrDefault(self):
        """
        Sometimes during query execution, the status might not be set but also the
        default value will not be set yet. This method selects the default when 
        the value is not set yet.
        """
        
        if self.status is None:
            return Slave.status.default.arg
        else:
            return self.status
    
    # __table_args__ = {'mysql_engine':'MEMORY'}
    
task2taskDependencyTable = Table('task2task', Base.metadata,
    Column('task', String(64), ForeignKey('tasks.taskId', ondelete='CASCADE')),
    Column('dependend', String(64), ForeignKey('tasks.taskId', ondelete='CASCADE')),
    mysql_engine='MEMORY'
)

class Task(Base):
    __tablename__ = 'tasks'
     
    taskId = Column(String(64), primary_key=True)
    name = Column(String(64), default='')
    location = Column(String(64), default='')
    type = Column(Enum('intrinsic', 'extrinsic'), default='intrinsic')
    rank = Column(Enum('primary', 'secondary'), default='primary')
    status = Column(Enum('waiting', 'running', 'success', 'failure', 'cancelled', 'timeout'), default='waiting')
    message = Column(String(512))
    
    created = Column(DateTime(True), default=None)
    started = Column(DateTime(True), default=None)
    finished = Column(DateTime(True), default=None)
    
    # some details that are task-specific and are
    # simply slaved as json-string.
    details = Column(JsonCol(1024), default={})
    
    estimatedTime = Column(Integer(), default=0)
    srcName = Column(String(64))
    action = Column(String(256), default='')
    
    # sequence ID we need for chunked batch updates.
    sequence_id = Column(Integer(),
                         index=True,
                         nullable=False)
    
    __table_args__ = {'mysql_engine':'MEMORY'}
    
# nasty workaround since sqlalchemy does not handle
# non-primary-key auto_increment columns for mysql.
event.listen(Task.__table__, 'after_create', DDL("""
ALTER TABLE tasks CHANGE sequence_id sequence_id INT AUTO_INCREMENT NOT NULL;
""", on='mysql'))
    
    
class Profile(Base):
    __tablename__ = 'profiles'
    
    id = Column(Integer(), Sequence('profile_seq'), primary_key=True)
    
    name = Column(String(64), nullable=False)
    width = Column(Integer(), nullable=True)
    height = Column(Integer(), nullable=True)
    vCodec = Column(String(32), nullable=True)
    aCodec = Column(String(32), nullable=True)
    fileFormat = Column(String(32), nullable=True)
    
    __table_args__ = {'mysql_engine':'MEMORY'}
    
