'''
Created on Jul 4, 2014

@author: eh14
'''
import collections
from haec.services import eventservice, valueaggregator
import pythonioc
from haec import config
import time
from twisted.python import log

cfg = pythonioc.Inject(config.Config)

class PowerValue(object):
    
    _powerTypes = {'current':float,
                   'energy':float,
                   'uptime':float,
                   'status':lambda status: str(status) == '1'}
    
    
    def __init__(self, ts, values):
        self._ts = ts
        
        self._slaveValues = {}
        for (nodeName, powerInfo) in values.iteritems():
            self._slaveValues[nodeName] = {key:converter(powerInfo[key]) for (key, converter) in self._powerTypes.iteritems()}
            self._slaveValues[nodeName]['slave'] = nodeName
            
        
        self._aggregated = {'current':sum(map(lambda x: x['current'], self._slaveValues.values()))}
    
    def getTime(self):
        return self._ts
    
    def getCurrentSum(self):
        return self._aggregated['current']
    
    def getSlaveValues(self):
        return self._slaveValues
            
class SensorStore(object):
    _sensorTypes = ['cpu_usage', 'memory_usage', 'network_in', 'network_out', 'disk_io_bytes', 'disk_io_time']

    eventService = pythonioc.Inject(eventservice.EventService)

    def __init__(self):
        #
        # buffers saving the values collected by slaves
        # 
        self.slaveValues = None
        
        #
        # cpu average and cpu min/avg/max values. 
        self.cpuAvg = None
        self.nwOutSum = None
        
        #
        # list of power values collected. 
        # We don't need to use the valuebuffer here since
        # the values are collected for all switches/slaves together
        self.powervalues = collections.deque(maxlen=cfg.vals.sensor_storetime / cfg.vals.sensor_sample_interval)

        # 
        # state variable. Will be set to True when
        # the buffers for all nodes/slaves etc have been initialized.
        self._initialized = False
        
    def initBuffers(self, topology):
        sensors = {}
        
        for slaves in topology.itervalues():
            for slave in slaves:
                sensors[slave] = valueaggregator.ValueBuffer(None, 5)
       
        self._sensorBuffers = sensors
        
        sourceNames = self._sensorBuffers.keys()
        self.slaveValues = valueaggregator.HashCollectingAggregator(size=cfg.vals.sensor_storetime,
                                                                  sourceNames=sourceNames,
                                                                  default={n:None for n in sourceNames},
                                                                  sources=[self._sensorBuffers[s] for s in sourceNames])
        
        self.cpuAvg = valueaggregator.AvgAggregator(valueExtractor=lambda v: v['cpu_usage'],
                                                     valueFilter=lambda v: v is not None,
                                                     size=cfg.vals.sensor_storetime / cfg.vals.sensor_sample_interval,
                                                     sources=self._sensorBuffers.values())
        
        self.nwOutSum = valueaggregator.SumAggregator(valueExtractor=lambda v: v['network_out'],
                                                                 valueFilter=lambda v:v is not None,
                                                                 size=cfg.vals.sensor_storetime / cfg.vals.sensor_sample_interval,
                                                                 sources=self._sensorBuffers.values())
        self._initialized = True
        
    def sampleValues(self, now):
        
        if not self._initialized:
            return
        
        start = time.time()
        self.slaveValues.flush(now)
        self.cpuAvg.flush(now)
        self.nwOutSum.flush(now)
        log.msg('sampling the values took %fms' % ((time.time() - start) * 1000))
        
    def getNumSensorValues(self):
        if not self._initialized:
            return 0 
        
        return len(self.slaveValues)
    
    def getNumPowerValues(self):
        if not self._initialized:
            return 0 
                
        return len(self.powervalues)
        
    def getRecentSensorValues(self):
        if not self._initialized:
            return RuntimeError('Sensor Store not initialized yet.') 
        
        return {k:v for k, v in self.slaveValues.getLast().iteritems() if v is not None}
    
    def getRecentPowerValues(self):
        if not self._initialized:
            return RuntimeError('Sensor Store not initialized yet.') 
        
        if len(self.powervalues) == 0:
            raise RuntimeError("No Power values available")
        
        return self.powervalues[-1].getSlaveValues()
        
    def addSensorValues(self, slave, sensorValues):
        if not self._initialized:
            return 
        
        assert isinstance(sensorValues, dict), "Expected dict, got %s" % type(sensorValues)
        self._sensorBuffers[slave].setValue(sensorValues)
       
    def addPowerValues(self, now, powerValues):
        if not self._initialized:
            return
        
        self.powervalues.append(PowerValue(now, powerValues))

