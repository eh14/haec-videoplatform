'''
Created on Jul 9, 2014

@author: eh14
'''
from haec.kplane.exceptions import ItemNotFoundError
import pythonioc
from haec import config
from twisted.python import log
from haec.services import jobscheduler
import uuid
import json
from haec import utils, services
from haec.kplane import domain
from sqlalchemy import func, between, or_

cfg = pythonioc.Inject(config.Config)

class TaskDao(object):
    db = pythonioc.Inject('dbprovider')
    
    localTaskService = pythonioc.Inject('localTaskService')
    taskService = pythonioc.Inject('taskService')
    
    timeProvider = pythonioc.Inject(services.TimeProvider)
            
    @utils.runAsDeferredThread
    def getNumberOfWaitingTasks(self):
        """
        Returns the number of waiting intrinsic tasks 
        """
        with self.db() as db:
            return db.query(func.count(domain.Task.taskId)).filter(domain.Task.type == 'intrinsic').filter(domain.Task.status == 'waiting').one()[0]
        
    @utils.runAsDeferredThread
    def getAllTasks(self):
        with self.db() as db:
            tasks = db.query(domain.Task).all()
            db.expunge_all()
            return tasks
        
    @utils.runAsDeferredThread
    def getNumberOfRunningTasksPerSlave(self):
        """
        Returns a dictionary of the form {<slavename>:<runningTasks>}.
        Only slaves that have at least one running tasks are listed, so
        there won't be any entries {<slavename>:0}. Match those entries using
        the slaveDao
        """
        with self.db() as db:
            running = db.query(domain.Task.location, func.count(domain.Task.location)).\
                            filter(domain.Task.status == 'running').\
                            filter(domain.Task.type == 'intrinsic').\
                            filter(domain.Task.location != 'local').\
                            group_by(domain.Task.location).all()
            
            # all rows are tuples, so extract the first and count
            # their occurence
            return {row[0]:row[1] for row in running}
        
    @utils.runAsDeferredThread    
    def deleteAll(self):
        with self.db() as db:
            db.query(domain.Task).delete()
                
    @utils.runAsDeferredThread    
    def getTaskById(self, taskId):
        with self.db() as db:
            tasks = db.query(domain.Task).filter(domain.Task.taskId == taskId).all()
            if len(tasks) != 1:
                raise ItemNotFoundError('Cannot find task with ID %s' % taskId)
            db.expunge_all()
            return tasks[0]

    
    def newTaskId(self):
        return str(uuid.uuid4())
    
    @utils.runAsDeferredThread
    def addTask(self, taskId, name, location, taskType, action='', rank='primary', taskDeps=[], **kwargs):
        
        assert type(taskDeps) in [list, tuple]
        status = 'waiting'
        
        if 'status' in kwargs:
            status = kwargs['status']
            del kwargs['status']
        with self.db() as db:
            task = domain.Task(taskId=taskId,
                             created=self.timeProvider.nowDate(),
                             name=name,
                             location=location,
                             type=taskType,
                             status=status,
                             action=action, **kwargs)
            db.add(task)
            
            taskDeps = [dep.taskId if isinstance(dep, domain.Task) else dep 
                            for dep in taskDeps]
            if len(taskDeps):
                insStatement = domain.task2taskDependencyTable.insert()
                for dep in taskDeps:
                    insStatement = insStatement.values(task=task.taskId,
                                                       dependend=dep)
    
                db.execute(insStatement)
            
        self.taskService.wakeup()
        
        return task
            
    @utils.runAsDeferredThread    
    def addCommandActionTask(self, name, location, command, taskDeps=[]):
        
        task = self.addTask(taskId=self.newTaskId(),
               name=name,
               location=location,
               taskType='intrinsic',
               action=self._createAction4Command(command),
               taskDeps=taskDeps
           )
        
        return task
    
    @utils.runAsDeferredThread
    def getRunnableTasks(self):
            
        with self.db() as db:
            
            result = db.execute("""SELECT tasks.taskId, sub.taskId, sub.cnt FROM tasks
                                        LEFT OUTER JOIN
                                (SELECT t2t.task as taskId, count(dep.taskId) as cnt 
                                FROM task2task as t2t, tasks as dep 
                                WHERE t2t.dependend = dep.taskId AND 
                                        dep.status <> 'success' AND
                                        dep.type = 'intrinsic'
                                GROUP BY t2t.task
                                ) as sub ON tasks.taskId = sub.taskId
                                
                        WHERE tasks.status='waiting' AND
                              tasks.type='intrinsic' AND
                              (sub.taskId IS NULL OR sub.cnt = 0) 
                            """)
            tasks = []
            for r in result:
                tasks.append(self.getTaskById(r[0]))
            return tasks
             
    @utils.runAsDeferredThread
    def getLocalTasks(self):   
        with self.db() as db:
            q = db.query(domain.Task)
            q = q.filter(or_(domain.Task.location == 'local', domain.Task.type == 'extrinsic'))
            q = q.filter(domain.Task.rank == 'primary') 
            tasks = q.order_by(domain.Task.finished.desc(),
                           domain.Task.started.desc(),
                           domain.Task.created.desc()).all()
            db.expunge_all()
            return tasks
                
    @utils.runAsDeferredThread
    def addLocalActionTask(self, name, function, args=(), kwargs={}, taskDeps=[]):
        
        if not isinstance(function, str):
            function = function.__name__
            
        return self.addTask(taskId=self.newTaskId(),
           name=name,
           location='local',
           taskType='intrinsic',
           action=self._createAction4LocalTask(function, args, kwargs),
           taskDeps=taskDeps
           )
        
     
    def _createAction4Command(self, command):
        return json.dumps({'type':'command', 'command':command})
    
    def _createAction4LocalTask(self, taskName, args, kwargs):
        assert self.localTaskService.hasTask(taskName), "unknown local task " + taskName
        
        assert type(args) in [list, tuple]
        assert isinstance(kwargs, dict), "kwargs needs to be a mapping"
        return json.dumps({'type':'localtask',
                'name':taskName,
                'args':args,
                'kwargs':kwargs})
        
    def createJobFromAction(self, actionStr):
        action = json.loads(actionStr)
        
        assert 'type' in action, 'Invalid action: needs a type'
        
        if action['type'] == 'localtask':
            assert 'name' in action and 'args' in action and 'kwargs' in action, "invalid action"
            
            return jobscheduler.SyncFunctionJob(self.localTaskService.getTaskForName(action['name']),
                                  *action['args'],
                                  **action['kwargs'])
            
        elif action['type'] == 'command':
            assert 'command' in action, 'Command action needs command'
            return jobscheduler.ExecuteCommandJob(action['command'])
        else:
            raise Exception('Unknown action type %s' % action['type'])
        
    @utils.runAsDeferredThread  
    def updateTaskStarted(self, taskId, estimatedTime, location=None):
         
        with self.db() as db:
            task = db.query(domain.Task).get(taskId)
            if task is None:
                raise ItemNotFoundError('Task %s not found' % taskId)
            task.estimatedTime = estimatedTime
            if location:
                task.location = location
            task.status = 'running'
            task.started = self.timeProvider.nowDate()
            

    @utils.runAsDeferredThread
    def updateTaskFinished(self, taskId, success, message=''):
        with self.db() as db:
            task = db.query(domain.Task).get(taskId)
            if task is None:
                raise ItemNotFoundError('Task %s not found' % taskId)
            task.status = 'success' if success else 'failure'
            task.message = message
            task.finished = self.timeProvider.nowDate()
            log.msg('Task %s stopped %s' % (taskId, 'successfully' if success else ('with errors (%s)' % message)))
            
        self.taskService.wakeup()
        
    @utils.runAsDeferredThread
    def updateTaskCancelled(self, taskId):
        with self.db() as db:
            query = db.query(domain.Task).filter(domain.Task.taskId == taskId)
            query.update({'status':'cancelled',
                                   'finished':self.timeProvider.nowDate()})
            
        self.taskService.wakeup()
        
    @utils.runAsDeferredThread
    def updateTaskStates(self):
        """
        Updates task status internally.
        1. propagate failed tasks to set their dependees to cancelled
        2. <not implemented yet> set running or waiting tasks to timeout after certain timeout time.
        """
        
        with self.db() as db:
            db.execute("""UPDATE tasks as t, (SELECT t2t.task as taskId,
                                count(dep.taskId) as cnt
                                FROM task2task as t2t, tasks as dep 
                                WHERE t2t.dependend = dep.taskId AND
                                        dep.type = 'intrinsic' AND 
                                        (dep.status = 'failure' OR
                                        dep.status='timeout' OR
                                        dep.status='cancelled')
                                GROUP BY t2t.task) as sub
                             SET
                            t.status='cancelled'
                            
                          WHERE
                            sub.taskId=t.taskId AND
                            t.status='waiting' AND
                            t.type='intrinsic' AND
                            sub.cnt > 0 """)
            
    @utils.runAsDeferredThread
    def getTasksPerLocation(self, waiting=False):
        """
        Returns the number of tasks running on a location.
        @param waiting: if true, will also count tasks in status 'waiting'.
        """
        with self.db() as db:
            q = db.query(domain.Task.location, func.count(domain.Task.taskId))
            q = q.filter(domain.Task.location != '')
            if waiting:
                q = q.filter(or_(domain.Task.status == 'running',
                               domain.Task.status == 'waiting'))
            else:
                q = q.filter(domain.Task.status == 'running')
                
            q = q.group_by(domain.Task.location)
            locations = q.all()
            
            return {r[0]:r[1] for r in locations}
        
    @utils.runAsDeferredThread
    def getNumTasksForLocation(self, location):
        with self.db() as db:
            q = db.query(func.count(domain.Task.taskId))
            q = q.filter(domain.Task.location == location).filter(or_(domain.Task.status == 'running',
                                                                      domain.Task.status == 'waiting'))
            tasks = q.all()
            
            return tasks[0][0]
            
    @utils.runAsDeferredThread
    def getMinMaxId(self):
        with self.db() as db:
            return db.query(func.min(domain.Task.sequence_id),
                            func.max(domain.Task.sequence_id)).one()
    
      
    @utils.runAsDeferredThread      
    def cleanTasks(self):
        """
        Cleans old tasks by marking them timed out after waiting/running too long
        and deleting them after being updated after a certain amount of time.
        
        The updates/deletes are chunked to avoid too long locks or dead locks.
        """
        minId, maxId = self.getMinMaxId()
        maxWaitingAge = self.timeProvider.futureDate(-cfg.vals.task_max_waittime)
        maxRunningAge = self.timeProvider.futureDate(-cfg.vals.task_max_runtime)
        
        for rangeStart, rangeEnd in utils.getChunkedRanges(minId, maxId, 50):
            with self.db() as db:
                #
                # let tasks timeout if waiting/running too long
                q = db.query(domain.Task).filter(between(domain.Task.sequence_id, rangeStart, rangeEnd))
                q = q.filter(((domain.Task.status == 'waiting') & (domain.Task.created < maxWaitingAge)) | 
                            ((domain.Task.status == 'running') & (domain.Task.started < maxRunningAge)) 
                            )
                q.update({'status':'timeout',
                          'finished':self.timeProvider.nowDate()},
                         synchronize_session=False)
                    
        for rangeStart, rangeEnd in utils.getChunkedRanges(minId, maxId, 100):
            with self.db() as db:        
                # delete old tasks (according to finish-date, which is set on success/failure/timeout)
                maxAge = self.timeProvider.futureDate(-cfg.vals.task_maxage)
                q = db.query(domain.Task).filter(between(domain.Task.sequence_id, rangeStart, rangeEnd))
                q = q.filter(domain.Task.finished < maxAge)
                q.delete(synchronize_session=False) 
    


