'''
Implements 
@author: eh14
'''
import datetime
import time
import math
import collections
import pandas
import pythonioc

def _floorMinute(date):
    return date.replace(second=0)
    
def _floor30Minute(date):
    if date.minute > 30:
        return date.replace(minute=30, second=0)
    else:
        return date.replace(minute=0, second=0)

def _floorHour(date):
    return date.replace(minute=0, second=0)

def _floorDay(date):
    return date.replace(hour=0, minute=0, second=0)
    
    
class Aggregator(object):
    def __init__(self, name, source, floorMethod, secondsPerPeriod, removeOldFromSource=True):
        self.name = name
        self.collection = []
        self.source = source
        self.floorMethod = floorMethod
        self.spp = float(secondsPerPeriod)
        self.removeOldFromSource = removeOldFromSource
        
    def aggregate(self, now):
        nowDate = datetime.datetime.fromtimestamp(now)
        
        endDate = self.floorMethod(nowDate)
        endTs = time.mktime(endDate.timetuple())
        
        minStartTs = endTs - self.spp
        
        if len(self.collection):
            startTs = self.collection[-1][0]
        elif len(self.source):
            startTs = self.source.getFirstItem()[0]
        else:
            # nothing to aggregate yet
            return False
        if startTs > minStartTs or startTs >= endTs:
            return False
            
        diff = int(math.ceil((endTs - startTs) / self.spp))
        if diff == 0:
            return False
        
        for i in range(diff, 0, -1):
            pieceStart = endTs - (self.spp * i)
            pieceEnd = endTs - (self.spp * (i - 1))
            self.collection.append((pieceEnd, self.source.extractCumulatedValues(pieceStart, pieceEnd, self.removeOldFromSource)))
        
        return True
    
    def extractCumulatedValues(self, start, end, removeOld=True):
        assert start < end
        count = 0 
        for item in list(self.collection):
            timeValue = item[0]
            if timeValue <= start:
                if removeOld:
                    self.collection.pop(0)
            else:
                if timeValue > end:
                    break
            
                count += item[1]
                
        return count
    
    def __len__(self):
        return len(self.collection)
    
    def getFirstItem(self):
        return self.collection[0]
    
    def sumLastItems(self, earliest):
        itemSum = 0
        for item in reversed(self.collection):
            if earliest >= item[0]:
                break
            else:
                itemSum += item[1]
        return itemSum
    
    def __str__(self):
        return "Aggregator %s" % self.name
    
class RootAggregator(Aggregator):
    
    def aggregate(self, now):
        return True
    
    def addItem(self, ts, value):
        self.collection.append((ts, value))
        
class HitCounter(object):
    
    def __init__(self):
        
        self._totalHits = 0
        self._hits = RootAggregator(None, None, None, 0)
        self._minutes = Aggregator('minutes', self._hits, _floorMinute, 60)
        # self._30minutes = Aggregator('30minutes', self._minutes, _floor30Minute, 1800, removeOldFromSource=False)
        self._hours = Aggregator('hours', self._minutes, _floorHour, 3600)
        self._days = Aggregator('days', self._hours, _floorDay, 3600 * 24)
        
        self._aggregators = [self._hits,
                             self._minutes,
                             # self._30minutes,
                             self._hours,
                             self._days]
                
    def hit(self, now, number=1):
        self._totalHits += number
        self._hits.addItem(now, number)
        # aggregate it
        self.aggregate(now)
        
    def aggregate(self, now):
        for agg in self._aggregators:
            if not agg.aggregate(now):
                break

    def getTotalHits(self):            
        return self._totalHits
    
    def getLastMinute(self, now):
        start = now - 60
        return self._hits.sumLastItems(start)
    
    def getLast30Minutes(self, now):
        start = now - 1800
        return self._hits.sumLastItems(start)
    
    def getLastHour(self, now):
        start = now - 3600
        return self._minutes.sumLastItems(start)
    
    def getLastDay(self, now):
        start = now - (86400)
        return self._hours.sumLastItems(start)
        
class HitTracker(object):
    
    _timeProvider = pythonioc.Inject('timeProvider')
    def __init__(self):
        self._counters = None
        self.reset()
        
    def hit(self, video, now=None, number=1):
        if now is None:
            now = self._timeProvider.now()
        self._counters[video].hit(now, number)

    def constructHitMatrix(self, now=None):
        series = {'total':[], 'minute':[], '30minutes':[], 'hour':[], 'day':[]}
        names = []
        if now is None:
            now = self._timeProvider.now()
            
        for name, counter in self._counters.iteritems():
            names.append(name)
            series['total'].append(counter.getTotalHits())
            series['minute'].append(counter.getLastMinute(now))
            series['30minutes'].append(counter.getLast30Minutes(now))
            series['hour'].append(counter.getLastHour(now))
            series['day'].append(counter.getLastDay(now))
            
        return pandas.DataFrame(data=series, index=names)
        
    def synchronize(self, now):
        pass
    
    def reset(self):
        self._counters = collections.defaultdict(HitCounter)
        
def createHitHistogram(hitmatrix):
    histValues = hitmatrix.apply(pandas.Series.value_counts)
    
    if histValues.index.size == 0:
        return histValues.reindex([0]).fillna(0)
    return histValues.fillna(0)
    
    # if the value_counts is crap, use the numpy.histogram method that
    # automatically calculates correct number of buckets.
    # histValues = numpy.histogram(hitmatrix['total'])

