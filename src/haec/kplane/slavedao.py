"""
Provide different Slave-Dao-Implementations
"""
from haec.kplane import domain, exceptions
import pythonioc
from haec import utils
from sqlalchemy.orm import joinedload
from twisted.internet import defer
import threading

class SlaveDao(object):
    
    db = pythonioc.Inject('dbprovider')
    def __init__(self):
        self._addSlaveLock = threading.Lock()
        
        self._slaveStatusBuffer = {}
        
    @utils.runAsDeferredThread
    def addSlave(self, slave):
        with self.db() as db:
            db.add(slave)
            self._setStatusInSlaveBufferFromThread(slave.name, slave.getStatusOrDefault())
            
    @utils.runAsDeferredThread        
    def addOrUpdateSlave(self, **kwargs):
        with self.db() as db:
            slave = self.syncAddOrUpdateSlave(db, domain.Slave(**kwargs))
            self._setStatusInSlaveBufferFromThread(slave.name, slave.getStatusOrDefault())
            return slave
        
    def syncAddOrUpdateSlave(self, db, slave):
        with self._addSlaveLock:
            # values = {c.name: str(getattr(slave, c.name)) for c in slave.__table__.columns}
            # slave.__table__.insert().prefix_with('IGNORE').values(values)
            slave = db.merge(slave)
            db.flush()
            self._setStatusInSlaveBufferFromThread(slave.name, slave.getStatusOrDefault())
            return slave
            
    @utils.runAsDeferredThread
    def getSlaveNames(self):
        with self.db() as db:
            rows = db.query(domain.Slave.name).distinct().all()
            return map(lambda row: row[0], rows)
        
    @utils.runAsDeferredThread
    def setSlavesOffExcept(self, *names):
        """
        sets the status of all slaves to "off", that are not in the
        passed list.
        """
        
        #
        # local function that will run inside the reactor thread.
        # Sets all slaves to offline when they're in the list 
        def changeBuffer(*onlineSlaves):
            for name in self._slaveStatusBuffer.iterkeys():
                if name not in onlineSlaves:
                    self._slaveStatusBuffer[name] = 'offline'
        
        with self.db() as db:
            q = db.query(domain.Slave)
            
            # if names are given, filter them
            if len(names):
                q = q.filter(domain.Slave.name.notin_(names))
                
            # update
            q.update(values={'status':'offline'}, synchronize_session=False)
            
        utils.callInReactor(changeBuffer, *names)
            
                
    @utils.runAsDeferredThread
    def getSlaves(self):
        with self.db() as db:
            slaves = db.query(domain.Slave).all()
            db.expunge_all()
            
            return slaves
        
    @defer.inlineCallbacks
    def getSlavesAsDict(self):
        slaves = yield self.getSlaves()
        defer.returnValue({s.name:s for s in slaves})
        
    def getFailedSlaves(self):
        return defer.succeed([name for name, status in self._slaveStatusBuffer.iteritems() if status == 'failure'])
       
    @utils.runAsDeferredThread
    def getSlaveByName(self, name):
        with self.db() as db:
            q = db.query(domain.Slave)
            slave = q.get(name)
            if not slave:
                raise exceptions.ItemNotFoundError('Cannot find slave with name %s' % name)
            
            db.expunge_all()
            return slave
        
    @utils.runAsDeferredThread
    def setSlaveStatus(self, name, status):
        assert status in domain.slaveStates
        
        with self.db() as db:
            slave = db.query(domain.Slave).get(name)
            if not slave:
                raise Exception("slave not found.")
            slave.status = status
            
        self._setStatusInSlaveBufferFromThread(name, status)
            
    def getSlaveStatus(self, name):
        return defer.succeed(self._slaveStatusBuffer[name])
        
    def getAllSlaveStatus(self):
        return defer.succeed(dict(self._slaveStatusBuffer))
            
    @utils.runAsDeferredThread
    def setSlaveOnlineStatus(self, name, online):
        return self.setSlaveStatus(name, 'online' if online else 'offline')
    
    
    def _setStatusInSlaveBufferFromThread(self, name, status):
        """
        Sets the slave status in the master. This method assumes being called 
        from the threadpool and starts a blocking call into the reactor.
        """
        def setIt(name__, status__):
            self._slaveStatusBuffer[name__] = status__
         
        utils.callInReactor(setIt, name, status)
    
    @utils.runAsDeferredThread
    def initializeTopology(self, topology):
        
        with self.db() as db:
            for switch, slaves in topology.iteritems():
                switchInst = domain.Switch(name=switch)
                switchInst = db.merge(switchInst)
                
                for slave in slaves:
                    slaveInst = domain.Slave(name=slave)
                    slaveInst = self.syncAddOrUpdateSlave(db, slaveInst)
                    if slaveInst not in switchInst.slaves:
                        switchInst.slaves.append(slaveInst)
                        
                    # update the status
                    self._setStatusInSlaveBufferFromThread(slave, slaveInst.status)
            db.flush()
                    
    @utils.runAsDeferredThread
    def getSystemTopology(self):
        with self.db() as db:
            switches = db.query(domain.Switch).options(joinedload(domain.Switch.slaves))
            switches = switches.all()
            db.expunge_all()
            return switches

    @utils.runAsDeferredThread
    def deleteAll(self):
        with self.db() as db:
            db.query(domain.Slave).delete()
