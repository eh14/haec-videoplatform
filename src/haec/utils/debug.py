'''
Created on Jun 20, 2014

@author: eh14
'''
from functools import wraps
import time



def logInvoke(func):
    """
    Decorator that prints a message when entering and leaving a method.
    """
    @wraps(func)
    def wrappee(*args, **kwargs):
        print "entering %s" % str(func.__name__)
        start = time.time()
        result = func(*args, **kwargs)
        print "leaving %s (%f ms)" % (str(func.__name__), (time.time() - start) * 1000)
        return result
    
    return wrappee
