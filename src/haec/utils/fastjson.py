'''

@author: eh14
'''
from __builtin__ import __import__
from twisted.python import log
from twisted.internet import defer
from haec import utils

def _date_handler(obj):
    """
    Handles converting datetimes to json.
    Taken from
    http://blog.codevariety.com/2012/01/06/python-serializing-dates-datetime-datetime-into-json/
    """
    return obj.isoformat(' ') if hasattr(obj, 'isoformat') else obj

class _JsonJson(object):
    def __init__(self):
        self._coder = __import__('json')
        
    def encode(self, value):
        return self._coder.dumps(value, default=_date_handler)
    
    
    def decode(self, string):
        return self._coder.loads(string)
        
class _CJsonJson(object):
    def __init__(self):
        self._coder = __import__('cjson')
        
    @utils.runAsDeferredThread
    def encode(self, value):
        return self._coder.encode(value)
    
    @utils.runAsDeferredThread
    def decode(self, string):
        return self._coder.decode(string)

try:
    _coder = _CJsonJson()
    log.msg("Using 'CJSON' as json encoder/decoder")
except ImportError:
    _coder = _JsonJson()
    log.msg("Using builtin 'json' as json encoder/decoder")
    

@defer.inlineCallbacks    
def encode(value):
    result = yield _coder.encode(value)
    defer.returnValue(result)
    
@defer.inlineCallbacks    
def decode(string):
    result = yield _coder.decode(string)
    defer.returnValue(result)
