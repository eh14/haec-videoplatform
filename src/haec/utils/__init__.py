from twisted.internet import task, defer, threads, reactor
from twisted.python import log, threadable
import time
import random
from functools import wraps
import threading
from haec import services
import itertools
import traceback
import sys
import pythonioc

_rand = random.Random(time.time())
class _SafeFunctionRunner(object):
    def __init__(self, function, *args, **kwargs):
        self.func = function
        self.args = args
        self.kwargs = kwargs
        
        # backref to the looping call in case
        # we want to stop it from the inside after n trials.
        self.loopingCall = None
        self.maxfails = None
        self.fails = 0
        
        # time in milliseconds the task
        # needs to be running before
        # the looper prints its time
        self._minLogTime = 50
        
        self._running = 0
        
        self._allowOverlap = True
    
    @defer.inlineCallbacks
    def __call__(self):
        try:
            if self._running > 0 and not self._allowOverlap:
                log.msg('Looper %s is not allowed to overlap. Ignoring this round.' % (self.name,))
                return
                
            self._running += 1
            start = time.time()
            result = yield self.func(*self.args, **self.kwargs)
            self.fails = 0
            defer.returnValue(result)
        except Exception as e:
            _exc_type, _exc_value, exc_traceback = sys.exc_info()
            tb = traceback.extract_tb(exc_traceback)
            exc_location = traceback.format_list(tb[-3:])
            log.msg('Exception occured running LoopingCall %s: %s\n(%s)' % (self.name, str(e), exc_location))
            self.fails += 1
        finally:
            looperTime = round((time.time() - start) * 1000, 2)
            self._running -= 1
            if looperTime > self._minLogTime:
                log.msg('Looping call %s took %f ms' % (self.name, looperTime))
                
            if self.maxfails and self.fails >= self.maxfails:
                log.msg("Looper failed too many times (max %s). I'm giving up" % self.maxfails)
                self.loopingCall.stop()
            

    def setMinLogTime(self, minLogTime):
        self._minLogTime = minLogTime
        
        
    @property
    def __name__(self):
        return self.func.__name__
    
    @property
    def name(self):
        return self.func.__name__
    
    def setMaxFails(self, loopingCall, maxfails):
        """
        Specify max number of fails along with the job that needs to be 
        stopped when the max fails are reached.
        """
        assert loopingCall, "Need job to stop when max fails reached"
        assert maxfails > 0, "maxfails needs to be > 0"
        self.maxfails = maxfails
        self.loopingCall = loopingCall
        
    def setOverlap(self, overlap):
        assert overlap in[False, True]
        self._allowOverlap = overlap
        
    def __str__(self):
        return 'Function Runner: %s *%s **%s' % (self.func, self.args, self.kwargs)
        
    def __repr__(self):
        return str(self)
    
class _JitteredLoopingCall(task.LoopingCall):
    """
    Looping call that adds some jittering to the interval, thus avoiding to 
    load the reactor more balanced.
    """
    
    def start(self, interval, now=True, jitter=0.083, maxfails=None, minLogTime=None, allowOverlap=True):
        """
        @param interval: interval between calls in seconds
        @param now: execute immediately?
        @param jitter: modify the interval slightly to avoid overloading.
        @param maxfails: max number of fails until job will be cancelled. When None, try infinitely. Default is None.
        @param minLogTime: write a log if the job takes longer than that. In Milliseconds!
        @param allowOverlap: start new job when old has not finished. Or not. TWISTED JOBS ARE NOT OVERLAPPING ANYWAY, SO THIS DOES NOT WORK!!
        """
        
        realInterval = _rand.normalvariate(interval,
                                              interval * jitter)
        
        if maxfails:
            self.f.setMaxFails(self, maxfails)
            
        if minLogTime:
            self.f.setMinLogTime(minLogTime)
            
        self.f.setOverlap(allowOverlap)
            
        return task.LoopingCall.start(self, realInterval, now)
    
    def stop(self, ignoreErrors=True):
        assert self.running or ignoreErrors, "Tried to stop non-running looper"
        
        if self.running:
            task.LoopingCall.stop(self)
            
    @property
    def __name__(self):
        return self.f.__name__
    
    @property
    def func_name(self):
        """
        Workaround for a twisted-bug.
        """
        
        return self.f.__name__
def deferredSleep(reactor, duration):
    """
    Delays execution using deferreds. Mostly used in inline-callbacks.
    Idea taken from
    http://comments.gmane.org/gmane.comp.python.twisted/19394
    
    Use it like
    @defer.inlineCallbacks
    def foo():
        yield doSomething()
        # this will wait for 3 seconds without blocking anything
        yield testutils.deferredSleep(3)
        yield doSomethingAfterwards()
        
    """
    d = defer.Deferred()
    reactor.callLater(duration, d.callback, None)
    return d
    

def createLoopingCall(reactor, function, *args, **kwargs):
    looper = _JitteredLoopingCall(_SafeFunctionRunner(function, *args, **kwargs))
    looper.clock = reactor
    
    return looper

class _OneShotFunctionRunner(_SafeFunctionRunner):
    
    @defer.inlineCallbacks        
    def __call__(self):
        yield _SafeFunctionRunner.__call__(self)
        if self.fails == 0 and self.loopingCall.running:
            self.loopingCall.stop()

def startRetryingOneshotJob(reactor, function, args=(), kwargs={}, maxRetries=32, tryInterval=3.2, now=False):
    """
    Creates a oneshot job that executes once and tries maxRetries in case
    it crashes. After first successful execution it is being stopped.
    
    Best to use named parameters only!
    """
    
    looper = _JitteredLoopingCall(_OneShotFunctionRunner(function, *args, **kwargs))
    looper.clock = reactor
    looper.start(interval=tryInterval,
                 now=now,
                 maxfails=maxRetries,
                 allowOverlap=False)  # the oneshop job should not overlap!
    
    return looper

def runAsDeferredThread(func):
    """
    Decorates a function to run as a deferred thread when invoked in the reactor thread
    or reuse the same thread when invoked in a deferred thread already.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        if isReactorThread():
            # print "creating new thread"
            return threads.deferToThread(func, *args, **kwargs)
        else:
            # print "run it directly"
            result = func(*args, **kwargs)
            assert not isinstance(result, defer.Deferred), "The inner function must be a deferred!"
            return result
    return wrapper


class ThreadLocalBool(threading.local):
    """
    Simple thread-local boolean variable.
    """
    def __init__(self):
        threading.local.__init__(self)
        self.val = False
        
    def setValue(self, val):
        self.val = val
        
    def getValue(self):
        return self.val
#
# Allows to override the behavior of isReactorThread.
# Setting it to true causes the main-thread to be the reactor-thread
# This applies only to unit testing where the test thinks
# it's in an extra thread whereas it actually runs in the
# reactor.
_reactorThreadOverride = ThreadLocalBool()

def isReactorThread():
    return threadable.isInIOThread() or _reactorThreadOverride.getValue()

def getMinAvgMax(values):
    """
    Extracts the minimum, average and maximum of a list of values.
    @return: [0,0,0] for empty value list.
    """
    if not len(values):
        return [0, 0, 0]
    
    return [min(values), float(sum(values)) / float(len(values)), max(values)]

reactor = pythonioc.Inject('reactor')
def callInReactor(func, *args, **kwargs):
    if isReactorThread():
        reactor.callLater(0, func, *args, **kwargs)
    else:
        reactor.callFromThread(func, *args, **kwargs)
        
def blockingCallInReactor(func, *args, **kwargs):
    if isReactorThread:
        return func(*args, **kwargs)
    else:
        return threads.blockingCallFromThread(reactor, func, *args, **kwargs)
        
        

def getChunkedRanges(start, end, chunkSize):
    """
    Generates a tuple in for each iteration that spans elements of chunkSize
    from start to end. (both inclusive).
    If either start or end is None, the generator is empty.
    """
    if start is None or end is None:
        return
    for item in xrange(start, end + 1, chunkSize):
        yield (item, min(end, item + chunkSize))


def countingIter(it):
    """
    Iterates through an iterate while counting the current
    index.
    """
    i = 0
    
    for item in it:
        yield (i, item)
        i += 1
