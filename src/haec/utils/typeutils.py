'''
Created on May 16, 2014

@author: eh14
'''


def traverseNested(o, tree_types=(list, tuple, set)):
    """
    Takes a arbitrarily deeply nested value and traverses
    through all the values.
    This allows flattening a structure, not only consisting of nested
    lists. (Only lists can be flattened with `compiler.ast.flatten(..)`
    
    Idea from
    http://stackoverflow.com/a/6340578
    """
    if isinstance(o, tree_types):
        for value in o:
            for subvalue in traverseNested(value):
                yield subvalue
    else:
        yield o
        
    