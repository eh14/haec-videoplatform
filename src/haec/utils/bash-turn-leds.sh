#!/bin/bash

if [ "$1" == "green" ]
then
LED="/sys/class/leds/green:ph20:led1/trigger"
else
LED="/sys/class/leds/blue:ph21:led2/trigger"
fi

sudo echo "$2" > "$LED"
sleep 3
sudo echo "none" > "$LED"
