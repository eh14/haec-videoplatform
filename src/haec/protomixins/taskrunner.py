'''

@author: eh14
'''
from haec.network import message, multiproto
from twisted.internet import defer
import pythonioc
        
class TaskRunnerRunJob(message.Message):
    job = None
    taskId = None
    
class TaskRunnerJobFinished(message.Message):
    taskId = None
    success = None
    message = None
    
    
class SlaveTaskRunnerMixin(multiproto.MultiProtocolMixinBase):
    """
    slave protocol mixin that executes jobs on the slave.
    """
    _sched = pythonioc.Inject('taskRunner')
        
    @defer.inlineCallbacks
    def on_TaskRunnerRunJob(self, message):
        assert message.job, 'Need a job in the message!'
        assert message.taskId, 'Need a taskId in the message!'
        try:
            yield self._sched.startJob(message.job)
            self.sendObject(TaskRunnerJobFinished(taskId=message.taskId,
                                        success=True))
        except Exception as e:
            self.sendObject(TaskRunnerJobFinished(taskId=message.taskId,
                                        success=False,
                                        message=str(e)))




class MasterTaskRunnerMixin(multiproto.MultiProtocolMixinBase):
    """
    Master protocol mixin that handles starting and 
    finishing remote tasks on slaves.
    """
    
    taskDao = pythonioc.Inject('taskDao')
    
    def on_TaskRunnerJobFinished(self, message):
        self.taskDao.updateTaskFinished(message.taskId,
                                        message.success,
                                        message=message.message)
    
    @classmethod
    def startJobOnSlave(cls, slaveConnection, taskId, job):
        message = TaskRunnerRunJob(job=job, taskId=taskId)
        slaveConnection.sendObject(message)