import psutil

from haec.network import message, multiproto
from haec import config
import time
from haec import utils
import pythonioc

cfg = pythonioc.Inject(config.Config)

psutilFuncs = {
               'net_io_counters':('network_io_counters', 'net_io_counters'),
               }

def runPsutil(funcName, *args, **kwargs):
    func = None
    if funcName not in psutilFuncs:
        func = getattr(psutil, funcName)
    else:
        if psutil.version_info[0] < 2:
            func = getattr(psutil, psutilFuncs[funcName][0])
        else:
            func = getattr(psutil, psutilFuncs[funcName][1])
    assert func is not None
            
    return func(*args, **kwargs)


class SensorsReport(message.Message):
    slavename = None
    slave_time = None
    
    cpu_usage = 0
    memory_usage = 0
    network_in = 0
    network_out = 0
    disk_io_bytes = 0
    disk_io_time = 0


class SlaveSensorsMixin(multiproto.MultiProtocolMixinBase):
    """
    Slave-Side of the sensor collecting protocol
    """
    __sensorJob = None
    
    __senderJob = None
    
    __lastNetIoCounters = None
    
    __lastDiskCounters = None
    
    reactor = pythonioc.Inject('reactor')
    
    def __diffByTime(self, now, last):
        return float(now - last) / cfg.vals.sensor_collectinterval
        
    
    def _collectSensorData(self):
        args = {}
        args['slave_time'] = int(round(time.time()))
        result = runPsutil('cpu_percent')
        args['cpu_usage'] = float(result) / 100.0
        network = runPsutil('net_io_counters')
        if self.__lastNetIoCounters:
            args['network_in'] = self.__diffByTime(network.bytes_recv, self.__lastNetIoCounters.bytes_recv) 
            args['network_out'] = self.__diffByTime(network.bytes_sent, self.__lastNetIoCounters.bytes_sent)
            # print network.bytes_sent, self.__lastNetIoCounters.bytes_sent, cfg.vals.sensor_collectinterval, args['network_out']
        else:
            args['network_in'] = 0
            args['network_out'] = 0
            
        self.__lastNetIoCounters = network
            
        disk = runPsutil('disk_io_counters')
        if self.__lastDiskCounters:
            args['disk_io_bytes'] = (self.__diffByTime(disk.read_bytes, self.__lastDiskCounters.read_bytes) + 
                                      self.__diffByTime(disk.write_bytes, self.__lastDiskCounters.write_bytes))
            
            args['disk_io_time'] = (self.__diffByTime(disk.read_time, self.__lastDiskCounters.read_time) + 
                                      self.__diffByTime(disk.write_time, self.__lastDiskCounters.write_time))
            
        else:
            args['disk_io_bytes'] = 0
            args['disk_io_time'] = 0
            
        self.__lastDiskCounters = disk
            
        args['memory_usage'] = runPsutil('virtual_memory').percent / 100.0
            
        report = SensorsReport(slavename=cfg.vals.name, **args)
        self.sendObject(report)
        return report
            
    def connectionMade(self):
        # start sending stuff
        self.__sensorJob = utils.createLoopingCall(self.reactor, self._collectSensorData)
        # do not use jittering here, we need exact values!!
        self.__sensorJob.start(cfg.vals.sensor_collectinterval, False, jitter=0)
        
    def connectionLost(self, reason):
        if self.__sensorJob:
            self.__sensorJob.stop()
        
class MasterSensorsMixin(multiproto.MultiProtocolMixinBase):
    """
    Master-Side of the sensor-collection protocol
    """
    
    sensorStore = pythonioc.Inject('sensorStore')
    
    def on_SensorsReport(self, sensorReport):
        self.sensorStore.addSensorValues(sensorReport.slavename,
                                         { 'cpu_usage':sensorReport.cpu_usage,
                                          'memory_usage':sensorReport.memory_usage,
                                          'network_in':sensorReport.network_in,
                                          'network_out':sensorReport.network_out,
                                          'disk_io_bytes':sensorReport.disk_io_bytes,
                                          'disk_io_time':sensorReport.disk_io_time
                                         })
        
