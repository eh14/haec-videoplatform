'''
Created on Jul 1, 2014

@author: eh14
'''

from haec.network import multiproto, message
from twisted.python import log
import subprocess
import os

here = os.path.dirname(os.path.abspath(__file__))
    
class Blink(message.Message):
    times = 5
    led = 'blue'
    mode = 'timer'


class LedOff(message.Message):
    pass

class SlaveHardwareMixin(multiproto.MultiProtocolMixinBase):
    def on_Blink(self, message):
        p = subprocess.Popen(['sudo', self._findScript(), message.led, message.mode])
        log.msg("Blinking")
    
    def on_LedOff(self, messsage):
        log.msg('LED OFF')
        
    def _findScript(self):
        return os.path.join(here, '..', 'utils', 'bash-turn-leds.sh')


def blinkSlave(slaveConnection, times=None, led=None, mode=None):
    
    args = {}
    if times:
        args['times'] = times
    if led:
        args['led'] = led
    if mode:
        args['mode'] = mode
    slaveConnection.sendObject(Blink(**args))
