'''
Video indexing and registry functionality
'''

import os
from haec.network import message, multiproto
from haec import config
from twisted.internet import threads, defer
from twisted.python import log
from haec.kplane import domain
import time
import shutil
import pythonioc
from haec.services import videoservice, eventservice

cfg = pythonioc.Inject(config.Config)

class VideoListRequest(message.Message):
    pass

class VideoList(message.Message):
    #
    # list of dictionaries as returned by 
    # VideoSlaveProtocolMixin::_parseVideoName
    # 
    videos = None
    
    def __init__(self, *args, **kwargs):
        self.slaveName = cfg.vals.name
        message.Message.__init__(self, *args, **kwargs)
        
class VideoDataRequest(message.Message):
    videoNames = None
    
class VideoData(message.FlexMessage):
    """
    Message from slave to server indicating a video being stored on the slave.
    This message is completely flexible regarding its content and may contain arbitrary fields.
    For more information, have a look at where it's instantiated.
    
    A taskId may be part of the message, which will finish the task on the server
    successfully.
    Sending this message may not fail a task on the server. use "VideoPostProcessingFailed" for that.
    """
    pass
            
class VideoAddToCollection(message.Message):
    path = None
    videoName = None
    taskId = None
    
class VideoPostProcessingFailed(message.Message):
    taskId = None
    message = None
    
class VideoMoved(message.Message):
    videoId = None
    oldSlave = None
    newSlave = None
    
class SlaveVideoMixin(multiproto.MultiProtocolMixinBase):
    """
    Installed on the slave. This provides the list of videos and formats which are
    stored on this node.
    """
    
    slaveVideoService = pythonioc.Inject(videoservice.SlaveVideoService)
    
    def __init__(self):
        # initialize the local videos by inspecting the
        # configured directories.
        self._dir = cfg.vals.video_dir
        
    @defer.inlineCallbacks
    def connectionMade(self):
        yield self._sendVideoList()
            
    @defer.inlineCallbacks
    def on_VideoListRequest(self, msg):
        yield self._sendVideoList()
        
    @defer.inlineCallbacks
    def _sendVideoList(self):
        vids = yield self.slaveVideoService.scanVideoDir()
        log.msg("send videolist list (%d videos)" % len(vids))
        self.sendObject(VideoList(videos=vids))
        defer.returnValue(vids) 
    
    def _createVideoDataMessage(self, vidData, taskId=None):
        return VideoData(slaveName=cfg.vals.name, taskId=taskId, **vidData)
    
    
    @defer.inlineCallbacks
    def on_VideoDataRequest(self, msg):
        for name in msg.videoNames:
            try:
                vidData = yield self.slaveVideoService.scanSingleVideo(name)
                
                videoDataMessage = self._createVideoDataMessage(vidData)
                self.sendObject(videoDataMessage)
            except Exception as e:
                log.msg("Error preparing the video data %s" % e)
                raise
    
        
    @defer.inlineCallbacks
    def on_VideoAddToCollection(self, msg):
        assert msg.path and msg.videoName and msg.taskId, "message incomplete"
        
        # check if it exists, fail otherwise
        if not os.path.exists(msg.path) or not os.path.isfile(msg.path):
            self.sendObject(VideoPostProcessingFailed(
                                taskId=msg.taskId,
                                message="Cannot add video to collection. Not existing (%s)" % msg.path))
            return
        

        # generate new unique filename        
        oldFileExtension = os.path.splitext(os.path.basename(msg.path))[1]
        for _i in range(100):
            variantId = self._newVariantId()
            newFileName = msg.videoName + '__id' + variantId + oldFileExtension 

            if not os.path.exists(os.path.join(self._dir, newFileName)):
                break

            log.msg('Variant-ID conflict (name %s). Trying another one.' % newFileName)
        else:  # break never called, always conflicted...
            self.sendObject(VideoPostProcessingFailed(
                                taskId=msg.taskId,
                                message="Could not find a non-existent variant-ID. This seems like a bug")) 
            return
        
        # copy the file from temp-folder to our videofolder
        yield threads.deferToThread(shutil.copyfile, msg.path, os.path.join(self._dir, newFileName))
        
        
        # scan it, send the data back to the master
        # Also, send a notification back to the master that the task
        # has been done successfully
        try:
            vidData = yield self.slaveVideoService.scanSingleVideo(newFileName)
            videoDataMessage = self._createVideoDataMessage(vidData, taskId=msg.taskId)
            self.sendObject(videoDataMessage)
        except Exception as e:
            self.sendObject(VideoPostProcessingFailed(taskId=msg.taskId,
                                                        message=str(e)))
            
        defer.returnValue(newFileName)
        
    def _newVariantId(self):
        return str(int(round(time.time() * 1000)))
                
class MasterVideoMixin(multiproto.MultiProtocolMixinBase):
    
    taskDao = pythonioc.Inject('taskDao')
    videoDao = pythonioc.Inject('videoDao')
    videoService = pythonioc.Inject('masterVideoService')
    eventService = pythonioc.Inject(eventservice.EventService)
    @defer.inlineCallbacks
    def on_VideoList(self, videoList):
        """
        @TODO: if the video/variant is already in the database, 
        don't request it from the client. We must assume the variantID is
        unique.
        """
        storedVideos = yield self.videoDao.getVideosBySlaveName(videoList.slaveName)
        storedVideoIds = set([str(storedVid.fname) for storedVid in storedVideos])
        slaveVideoPaths = set([str(v['fileName']) for v in videoList.videos])
        slaveName = videoList.slaveName
        
        if len(storedVideoIds) != len(storedVideos):
            log.msg('knowledgebase contains duplicate videos for a slave. Not good.')
        if len(slaveVideoPaths) != len(videoList.videos):
            log.msg('slave reported domain list with duplicates.Not good.')
        
        removedVideos = storedVideoIds.difference(slaveVideoPaths)
        newVideos = slaveVideoPaths.difference(storedVideoIds)
        
        yield self.videoDao.removeVideos(slaveName, list(removedVideos))
            
        if len(newVideos) > 0:
            self.sendObject(VideoDataRequest(videoNames=newVideos))
        
    @defer.inlineCallbacks    
    def on_VideoData(self, msg):
        vidInstance = domain.Video(name=msg.name,
                                   variant=msg.variant,
                                   width=str(msg.width),
                                   height=str(msg.height),
                                   video_codec=msg.video_codec,
                                   audio_codec=msg.audio_codec,
                                   duration=msg.duration,
                                   bitrate=msg.bitrate,
                                   fps=msg.fps,
                                   format=msg.format,
                                   fname=msg.fileName,
                                   fsize=msg.fsize)
        
        # queue the video data for later batch-adding
        self.videoService.addVideoData(msg.slaveName, vidInstance)
        
        # If the message has a non-empty taskId
        if hasattr(msg, 'taskId') and msg.taskId:
            # flush the batch-queue and finish the task
            yield self.videoService.addBatchedVideo()
            yield self.taskDao.updateTaskFinished(taskId=msg.taskId,
                                      success=1,
                                      message="")
        
    @defer.inlineCallbacks
    def on_VideoPostProcessingFailed(self, msg):
        assert msg.taskId, "no task Id provided"
        
        yield self.taskDao.updateTaskFinished(taskId=msg.taskId,
                                      success=0,
                                      message=msg.message)

    @defer.inlineCallbacks
    def on_VideoMoved(self, msg):
        try:
            yield self.videoDao.addVideoToSlave(msg.videoId, msg.newSlave)
            yield self.videoDao.removeVideoFromSlave(msg.videoId, msg.oldSlave)
            self.eventService.triggerEvent('video-moved',
                                           videoId=msg.videoId,
                                           oldSlave=msg.oldSlave,
                                           newSlave=msg.newSlave)
        except Exception as e:
            log.msg('Error moving video %s in database (was moved from %s to %s)' % 
                        (msg.videoId, msg.oldSlave, msg.newSlave))
            
             
    @classmethod
    def sendVideoListRequest(cls, slave):
        slave.sendObject(VideoListRequest())
        
    @classmethod
    def sendSlaveAddVideoToCollection(self,
                                      slave,
                                      videoFile,
                                      videoName,
                                      taskId):
        
        slave.sendObject(VideoAddToCollection(path=videoFile, videoName=videoName, taskId=taskId))
