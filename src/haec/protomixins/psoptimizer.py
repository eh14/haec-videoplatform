'''

@author: eh14
'''
from haec.network import message, multiproto
from haec import utils
import pythonioc
from haec import config

from twisted.python import log
import json
import os
import random
import time
import math
from haec.services import jobscheduler
from twisted.internet import defer
from haec.protomixins import video
import subprocess

cfg = pythonioc.Inject(config.Config)


class VideoData(object):
    
    def __init__(self, data):
        self._videoIds = data['index']
        self._dataColumns = data['columns']
        self._data = data['data']
        
        self._videoDir = cfg.vals.video_dir
        
        assert len(self._videoIds) == len(self._data), "Numer of Ids does not match number of data entries."
        if len(self._data) > 0:
            assert len(self._dataColumns) == len(self._data[0]), "Column count does not match data."
            
        self._popIndex = self._dataColumns.index('popularity')
        self._fnameIndex = self._dataColumns.index('fname')
        self._bsIndex = self._dataColumns.index('totalbyteseconds')
    
    def getFilteredVideos(self, minThreshold=None, maxThreshold=None):
        """
        Returns a list of tuples <index>, <filename>, <byteseconds>
        for the videos matching the passed conditions.
        
        @param minThreshold: filters videos whose popularity is greater than this (exclusive).
        @param maxThreshold: filters videos whose popularity is smaller or equals this (inclusive).
        """
        # initialize the filter methods
        tester = []
        if minThreshold is not None:
            tester.append(lambda x: x > minThreshold)
        if maxThreshold is not None:
            tester.append(lambda x: x <= maxThreshold)
        
        filtered = []
        i = 0
        while True:
            # outside of index
            if i >= len(self._videoIds):
                break
            
            d = self._data[i]
            
            if not self._videoFileExists(d[self._fnameIndex]):
                log.msg('File for video %s does not exist anymore. Probably deleted/migrated.' % str(d))
                del self._videoIds[i]
                del self._data[i]
                continue
            
            pop = d[self._popIndex]
            
            # if all tester methods return true, we'll return the video.
            if all([t(pop) for t in tester]):
                filtered.append((self._videoIds[i], d[self._fnameIndex], d[self._bsIndex]))
                
            i += 1
            
        return filtered
    
    
    def _videoFileExists(self, fileName):
        return os.path.isfile(os.path.join(self._videoDir, fileName))
    
    def getNumberOfVideos(self):
        return len(self._videoIds)

class PsoResetVideoData(message.Message):
    """
    Sent to reset the
    Encapsulates the rather inconvenient video data format.
    The constructor expects a dict as exported from pandas
    """
    popularityThreshold = None
    videoData = None
    
class PsoResetSlaveData(message.Message):
    """
    dictionary containing data about slaves
     {slaveName: {'online':False|True,
                  'popClass':'A'|'B',
                  'byteseconds':<total number of load in terms of byteseconds>
                  'IP':<ip address, used for copying>
                 }
    }
    """
    slaveData = None
    speed = None
    
class PsoSlaveIsPure(message.Message):
    slaveName = None
    isPure = None
    
class PsoDoShutdown(message.Message):
    pass

MODE_DISABLED = 0
MODE_PURITY = 1
MODE_BALANCE = 2

class PsoSetMode(message.Message):
    """
    Configures the pso slaves to go into a specific mode of operation.
    Use MODE_DISABLED, MODE_PURITY, MODE_BALANCE
    """
    mode = None
    
class SlavePsoMixin(multiproto.MultiProtocolMixinBase):
    """
    The slave has different states it resides in. At startup its idle.
    By receiving the SetOptimizerMode message it performs state transitions.
    
    the CLEARUP state is automatically left and changed to IDLE when the
    appropriate actions are copmleted.    
    """
    
    reactor = pythonioc.Inject('reactor')
    _sched = pythonioc.Inject('TaskRunner')
    activityTracker = pythonioc.Inject('activityTracker')
    
    def __init__(self):
        self._class = 'B'
        self._rand = random.Random(time.time())
        self._videoData = None
        self._popThreshold = 0.0
        # the number of steps I do to improve my optimization
        self._speed = cfg.vals.pso_initial_speed
        self._slaveData = {}
        
        self._classATester = lambda x:x > self._popThreshold
        self._classBTester = lambda x:x <= self._popThreshold
        
        self._balanceOptimized = None
        
        self._mode = MODE_DISABLED
        
        # tracks whether the current videoData means we're pure.
        # When new video data arrives, it is reset to check again.
        self._isPure = False
        
        self._optLooper = utils.createLoopingCall(self.reactor, self._opt)
        
    def connectionMade(self):
        self._optLooper.start(cfg.vals.pso_opt_interval, now=False, allowOverlap=False)
        
    def connectionLost(self, reason):
        self._optLooper.stop()
        
    def on_PsoResetVideoData(self, msg):
        self._videoData = VideoData(json.loads(msg.videoData))
        self._popThreshold = float(msg.popularityThreshold)
        self._isPure = False
        self._balanceOptimized = False
        
    def on_PsoResetSlaveData(self, msg):
        slaveData = json.loads(msg.slaveData)
        self._class = slaveData[cfg.vals.name]['class']
        self._speed = float(msg.speed)
        self._slaveData = slaveData
        
        assert self._class in ['A', 'B'], 'invalid class %s' % self._class
        assert 0 <= self._speed <= 1, 'invalid range for particle-speed' 
        
    def on_PsoSetMode(self, message):
        self._mode = message.mode
        
    def _opt(self):
        log.msg('Started optimizing. (Class %s, Speed %f, Threshold %f)' % (self._class, self._speed, self._popThreshold))
        
        if self._sched.isBusy():
            log.msg('There are already tasks scheduled. Not optimizing.')
            return
        
        if not self._class:
            log.msg('No class defined. Not optimizing.')
            return 
        if not self._videoData or not self._slaveData:
            log.msg('No video data or no slave data. Not optimizing.')
            return
        
        # apply purity, if wished
        if(self._mode & MODE_PURITY) > 0:
            self._optForClassPurity()
        
        if(self._mode & MODE_BALANCE) > 0:
            self._optForBalancing()
            
   
            
    def _optForClassPurity(self):
        """
        This method tries to optimize class purity in terms of offloading all
        videos of different classes.
        """
        
        if self._isPure:
            self.sendObject(PsoSlaveIsPure(slaveName=cfg.vals.name, isPure=True))
            return

        # filter other videos
        if self._class == 'A':
            otherVids = self._videoData.getFilteredVideos(maxThreshold=self._popThreshold)
        else:
            otherVids = self._videoData.getFilteredVideos(minThreshold=self._popThreshold)
            
        # according to speed, calculate how many videos to migrate. 
        # round to ceiling.
        vidsToMigrate = int(math.ceil(len(otherVids) * self._speed))
        
        # do not migrate more than we have.
        vidsToMigrate = min(vidsToMigrate, len(otherVids))
        
        if vidsToMigrate < 1:
            log.msg('Nothing to migrate for purity. Nothing done this time.')
            self.sendObject(PsoSlaveIsPure(slaveName=cfg.vals.name, isPure=True))
            self._isPure = True
            return
        
        # send the master: we're not pure!
        self.sendObject(PsoSlaveIsPure(slaveName=cfg.vals.name, isPure=False))
        
        # get other slaves
        otherSlaves = [(slaveName, slave['ip'], slave['viddir']) 
                            for slaveName, slave in self._slaveData.iteritems()
                                if slave['class'] != self._class and 
                                    slave['online']]

        if len(otherSlaves) == 0:
            log.msg('Cannot improve class purity. No slave of other class is available')
            return
        
        randomVidIndexes = range(len(otherVids))
        self._rand.shuffle(randomVidIndexes)
        for _i in range(vidsToMigrate):
            vid = otherVids[randomVidIndexes.pop()]
            
            targetSlave = self._rand.choice(otherSlaves)
            self._scheduleVidForMigration(vid[0], vid[1],
                                          targetSlave[0], targetSlave[1], targetSlave[2])
            
                
    def _optForBalancing(self):
        """
        Tries to optimize load between all slaves of MY class.
        
        The idea is as follows:
        (1) calculate the average load and the offset to average for each slave.
        (3a) if I am underloaded, do nothing. We are not pulling load, just pushing
        (3b) if there is no one else of my class, stop it.
        (3c) if I am overloaded, continue
        (4) calculate how my overload should be distributed among all slaves  
        (4) assuming all slaves behave similar, each one should shift a fraction of its overload
            to the underloaded slaves in a way that the fractions sum up to the missing fraction.
        (5) Never push off too much load, otherwise the slave will send it back next round and the 
            system will never become stable.
        """
        if self._balanceOptimized:
            log.msg('Already optimized for balancing this round. Skipping it.')
            return
        
        self._balanceOptimized = True
        
        if self._class == 'B':
            log.msg('Slaves of class B do not balance. They will be shut down anyway')
            return
        
        if not self._isLocalSlaveOverloaded():
            log.msg('Local slave is not overloaded. We will not balance anything')
            return
                
        # extract all slaves from my class that are online
        peerSlaves = {slaveName:s for slaveName, s in self._slaveData.iteritems() 
                                        if s['class'] == self._class and s['online'] == True}
        
        assert peerSlaves, "No slave class data given, or invalid data"
        
        
        if len(peerSlaves) < 2:
            log.msg('I seem to be the only slave of my kind, cannot balance.')
            return
        
        meanOffset = sum([s['bs'] for s in peerSlaves.itervalues()]) / len(peerSlaves)
        slaveOffset = {slaveName:meanOffset - s['bs'] for slaveName, s in peerSlaves.iteritems()}
        
        myOffset = slaveOffset[cfg.vals.name]
        if myOffset >= 0:
            log.msg('I seem to be underloaded, cannot balance.')
            return
        
        #
        # absolute deviation from the mean of local host.
        overloadToDistribute = abs(myOffset)
        
        # the gain 
        totalGain = sum([v for v in slaveOffset.itervalues() if v > 0])
        
        # how much of my overload is allowed for each slave.
        relativeGain = [[k, self._speed * overloadToDistribute * (v / totalGain)] for k, v in slaveOffset.iteritems() if v > 0]
        
        assert sum([r[1] for r in relativeGain]) - overloadToDistribute < 0.1, "the gain calculation seems messed up"

        if self._class == 'A':
            sameVideos = self._videoData.getFilteredVideos(minThreshold=self._popThreshold)
        else:
            sameVideos = self._videoData.getFilteredVideos(maxThreshold=self._popThreshold)
            
        if len(sameVideos) == 0:
            log.msg('No videos to balance.')
            return
        
        # sort them so the ones with highest byte-seconds 
        # get to the top
        sameVideos.sort(lambda x, y:cmp(y[2], x[2]))
        def sortGain():
            relativeGain.sort(lambda x, y:cmp(y[1], x[1]))
        
        for vidId, vidFile, totalBs in sameVideos:
            for i in range(len(relativeGain)):
                
                if totalBs < relativeGain[i][1]:
                    slaveName = relativeGain[i][0]
                    self._scheduleVidForMigration(vidId, vidFile, slaveName,
                                                  peerSlaves[slaveName]['ip'],
                                                  peerSlaves[slaveName]['viddir'])
                    # subtract what is left for the slave   
                    relativeGain[i][1] -= totalBs
                    
                    # resort the list
                    sortGain()
                else:
                    # if the one with the most left space does not fit,
                    # all the others won't fit.
                    # skip that video
                    break
        
    def _isLocalSlaveOverloaded(self):
        """
        Checks wether the local slave's bs, recorded by the master
        is above the 90%-mark of the total allowed byte-seconds-value
        obtained by the model 
        (currently hard-coded).
        
        return True if overloaded, False otherwise.
        """ 
        for slaveName, slave in self._slaveData.iteritems():
            if slaveName == cfg.vals.name:
                return slave['bs'] > 1024 * 1024 * 5
        else:
            raise Exception('Local slave was not found in the slave data')
        
    def wrapWithSshpass(self, command):
        if cfg.vals.peer_slave_password:
            wrap = 'sshpass -p %s' % cfg.vals.peer_slave_password
            if isinstance(command, list):
                return wrap.strip().split(' ') + command
            else:
                return '%s %s' % (wrap.strip(), command.strip())
        else:
            return command
        
    def _scheduleVidForMigration(self, videoId, filename,
                                        targetSlaveName, targetSlaveIp,
                                        targetDir):

        log.msg('Scheduling video migration %s -> %s' % (filename, targetSlaveName))        
        # # create the command
        # do the throttling dependend on the optimization's system model.
        # it's in kBit --> 1024KByte
        scpCmd = 'scp -l 8192 -oStrictHostKeyChecking=no %s %s@%s:%s'
        
        tmpFileName = os.path.join(cfg.vals.temp_dir, filename)
        
        scpCmd = scpCmd % (os.path.join(cfg.vals.video_dir, filename),
                  cfg.vals.peer_slave_user,
                  targetSlaveIp,
                 tmpFileName)
        
        
        #
        # create ssh-command that moves the file from tmp to the real dir
        mvCmd = ["ssh",
                 "-oStrictHostKeyChecking=no",
                 cfg.vals.peer_slave_user + "@" + targetSlaveIp,
                 'mv %s %s' % (tmpFileName, os.path.join(targetDir, filename))]
                            
        job = jobscheduler.CompositeJob(jobscheduler.ExecuteCommandJob(self.wrapWithSshpass(scpCmd)),
                                    jobscheduler.ExecuteCommandJob(self.wrapWithSshpass(mvCmd)))
        d = self._sched.startJob(job)
        
        def errback(error):
            log.msg('Error executing migration commands (%s\n,%s) ->%s' % (scpCmd, mvCmd, error))
            
        def notifyMaster(result):
            self.sendObject(video.VideoMoved(videoId=videoId,
                                                  oldSlave=cfg.vals.name,
                                                  newSlave=targetSlaveName))

        @defer.inlineCallbacks            
        def deleteLocally(result):
            yield self.activityTracker.blockVideo(filename)
            try:
                job = jobscheduler.ExecuteCommandJob('rm %s' % os.path.join(cfg.vals.video_dir, filename))
                yield self._sched.startJob(job)
                self._videoData
            except:
                log.msg('Failed to locally delete file')
        
        # it is crucial that the errback is added
        # last because it does not reraise the exception
        # and would cause the normal event handlers to execute as well.
        d.addCallback(notifyMaster)
        d.addCallback(deleteLocally)
        d.addErrback(errback)
    
    
    def on_PsoDoShutdown(self, msg):
        if self.activityTracker.sumStreams() != 0:
            # we don't need to notify the server here, since the activity tracker does that automatically for us.
            log.msg('new requests arrived in the meantime. Not shutting down.')
            return
        
        # set so the streamer knows it is not supposed to handle any more requests
        self.activityTracker.isTerminating()
        
        #
        # This hopefully kills everything including this process.
        cmd = ['sudo', 'shutdown', '-h', '0']
               
        log.msg('Attempting shutdown with command "%s"' % ' '.join(cmd))
        subprocess.Popen(cmd)
        
        
class MasterPsoMixin(multiproto.MultiProtocolMixinBase):
    activityService = pythonioc.Inject('activityService')
    reactor = pythonioc.Inject('reactor')
    
    @defer.inlineCallbacks
    def on_PsoSlaveIsPure(self, msg):
        log.msg("slave %s is now pure." % msg.slaveName)
        yield self.activityService.updateSlavePurity(msg.slaveName, msg.isPure)
        
    @classmethod
    def prepareVideoDataMessage(cls, videoData, popularityThreshold):
        """
        Prepares a message to be sent to slaves containing their video data.
        @param videoData: dict or str (dict will be converted to str) containing the data.
        """
        preparedData = json.dumps(videoData) if not isinstance(videoData, str) else videoData
        return PsoResetVideoData(videoData=preparedData,
                                popularityThreshold=popularityThreshold)

    @classmethod
    def sendVideoDataToSlave(cls, slave, videoData, popularityThreshold):
        cls.reactor.callLater(0, slave.sendObject, cls.prepareVideoDataMessage(videoData, popularityThreshold))


    @classmethod
    def prepareSlaveClassData(cls, slaveData, speed):
        """
        Prepares a message to be sent to slaves containing their slave class data.
        @param videoData: dict or str (dict will be converted to str) containing the data.
        """
        preparedData = json.dumps(slaveData) if not isinstance(slaveData, str) else slaveData
        return PsoResetSlaveData(slaveData=preparedData,
                                speed=speed)
        
    @classmethod
    def sendPsoSetMode(cls, slave, mode):
        msg = PsoSetMode(mode=mode)
        slave.sendObject(msg)
