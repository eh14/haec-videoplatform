"""
Components that handle slave activities in terms of handling requests.
"""
from haec.network import multiproto, message
import pythonioc
from haec import utils
from twisted.python import log
from haec import config

cfg = pythonioc.Inject(config.Config)

class ActivityMessage(message.Message):
    streams = None
    slaveName = None
    
class SlaveActivityMixin(multiproto.MultiProtocolMixinBase):
    actTracker = pythonioc.Inject('activityTracker') 
    reactor = pythonioc.Inject('reactor')
    
    def __init__(self):
        self._sendActivityLooper = None
        self._cleanActivityLooper = None
        self._lastMessage = None
        
    def connectionMade(self):
        self._sendActivityLooper = utils.createLoopingCall(self.reactor, self._sendActivity)
        self._cleanActivityLooper = utils.createLoopingCall(self.reactor, self.actTracker.cleanZero)
        self._sendActivityLooper.start(5, now=False)
        self._cleanActivityLooper.start(120, now=False)
        
        self.actTracker.setActivityCallback(self._sendActivity)
        
    def connectionLost(self, reason):
        self._sendActivityLooper.stop()
        self._cleanActivityLooper.stop()
        self._lastMessage = None
        
    def _sendActivity(self):
        message = ActivityMessage(slaveName=cfg.vals.name, streams=self.actTracker.sumStreams())
        if not self._lastMessage or self._lastMessage != message: 
            self._lastMessage = message
            log.msg('Sending activity to master %s' % message)
            self.sendObject(message)
        else:
            log.msg('Activity did not change since last time. Not sending update to server')
            

class MasterActivityMixin(multiproto.MultiProtocolMixinBase):
    activityService = pythonioc.Inject('activityService')
    
    def on_ActivityMessage(self, message):
        self.activityService.updateSlaveActivity(message.slaveName, message.streams)

