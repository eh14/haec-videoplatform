'''
Created on Jun 5, 2014

@author: eh14
'''

from haec.network import message
from haec.network import multiproto
from haec import config
from twisted.internet import reactor, defer
import time
from haec import utils
import pythonioc
from haec.services import eventservice
from twisted.python import log

cfg = pythonioc.Inject(config.Config)

class TopoloySlaveEnvironment(message.Message):
    name = None
    webserver_port = None
    videodir = None
    
class TopologyHeartBeat(message.Message):
    pass

class SlaveTopologyMixin(multiproto.MultiProtocolMixinBase):
    
    def connectionMade(self):
        self.sendObject(TopoloySlaveEnvironment(name=cfg.vals.name,
                                         webserver_port=cfg.vals.webserver_port,
                                         videodir=cfg.vals.video_dir))
        
        # send a heartbeat
        self._heartbeat = utils.createLoopingCall(reactor, self._sendHeartbeat)
        self._heartbeat.start(cfg.vals.heartbeat_interval)
            
    def _sendHeartbeat(self):
        self.sendObject(TopologyHeartBeat())
            
    def connectionLost(self, reason):
        if self._heartbeat:
            self._heartbeat.stop()
    
        
        
class MasterTopologyMixin(multiproto.MultiProtocolMixinBase):
    
    __mySlaveName = None
    __online = None
    __lastSlaveOnline = None
    
    slaveDao = pythonioc.Inject('slaveDao')
    
    videoMapper = pythonioc.Inject('portMapperService')
    
    slaveConnService = pythonioc.Inject('slaveConnService')
    eventService = pythonioc.Inject(eventservice.EventService)
    
    def connectionMade(self):
        self._stethoscope = utils.createLoopingCall(reactor, self._checkChild)
        self._stethoscope.start(cfg.vals.heartbeat_timeout, now=False)
        self.slaveConnService.slaveConnected(self)
        
    def getSlaveName(self):
        return self.__mySlaveName
        
    def connectionLost(self, reason):
        
        self.__online = False
        self.slaveConnService.slaveDisconnected(self)
        if self._stethoscope.running:
            self._stethoscope.stop()
        
        self.eventService.triggerEvent('slave-offline', self.__mySlaveName)
        self._setMySlaveStatus(False)    
    
    @defer.inlineCallbacks
    def on_TopoloySlaveEnvironment(self, slaveEnvironment):
        self.__mySlaveName = slaveEnvironment.name
        
        yield self.slaveDao.addOrUpdateSlave(name=slaveEnvironment.name,
                                            webserver_port=slaveEnvironment.webserver_port,
                                            videodir=slaveEnvironment.videodir,
                                            status='online')
        slaveIp = cfg.getSlaveIp(self.__mySlaveName)
        if slaveIp:
            self.videoMapper.registerSlave(slaveName=self.__mySlaveName, slaveIp=slaveIp, internal=slaveEnvironment.webserver_port)
        self.__online = True
        self._setMySlaveStatus(True)
        self.eventService.triggerEvent('slave-online', self.__mySlaveName)
        
    def on_TopologyHeartBeat(self, message):
        self.__lastSlaveOnline = time.time()
        
    def _setMySlaveStatus(self, status):
        assert status in [False, True]
        
        if not self.__mySlaveName:
            log.msg('Error: slave name not set. I will ignore this one.')
            self.__online = False
            return
        self.slaveDao.setSlaveOnlineStatus(self.__mySlaveName, status)
    
    def _checkChild(self):
        if not self.__lastSlaveOnline:
            self.__lastSlaveOnline = time.time()
            return
        
        diff = time.time() - self.__lastSlaveOnline
        if diff > cfg.vals.heartbeat_timeout:
            self.transport.abortConnection()
        elif not self.__online:
            self._setMySlaveStatus(True)
