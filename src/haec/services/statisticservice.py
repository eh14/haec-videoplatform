'''

@author: eh14
'''
import pythonioc
from haec import config

cfg = pythonioc.Inject(config.Config)


class StatisticService(object):
    
#     sensorstore = pythonioc.Inject('sensorStore')
#     taskDao = pythonioc.Inject('taskDao')
#     slaveDao = pythonioc.Inject('slaveDao')
#     optmodel = pythonioc.Inject(optmodel.OptModelProvider)
    
    def __init__(self):
        self._slaveBootTimes = {}
        
    def setSlaveBootTime(self, slaveName, bootTime):
        self._slaveBootTimes[slaveName] = bootTime
        
    def getAverageSlaveBootTime(self):
        if not len(self._slaveBootTimes):
            return None
        
        else: 
            values = self._slaveBootTimes.values()
            return sum(values) / len(values)
    def getSlaveBootTimes(self):
        return dict(self._slaveBootTimes)
    
