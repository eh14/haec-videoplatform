'''

@author: eh14
'''
from haec.services import optimizerservice, eventservice
import pythonioc
import random
import time
from twisted.internet import defer
from haec.optimization import optmodel


class DemoService(object):
    optimizerService = pythonioc.Inject(optimizerservice.OptimizerService)
    events = pythonioc.Inject(eventservice.EventService)
    
    optModel = pythonioc.Inject(optmodel.OptModelProvider)
    def postInit(self):
        self.rand = random.Random(time.time())
        self.userIds = range(100, 1000)
        
        self._initialClassRatio = self.optModel.model.classBVideoFraction
        
    def triggerContextEvent(self):
        userIds = self.rand.sample(self.userIds, self.rand.choice(range(3, 15)))
        before, after = self.adaptPopClassFraction(len(userIds))
        if abs(before - after) > 0.001:
            self.events.triggerEvent('situation-adaptation',
                            summary='Paying User Logged in.',
                            userIds=userIds,
                            beforeThreshold=before,
                            afterThreshold=after)


    def adaptPopClassFraction(self, loggedInUsers=None):
        """
        Adapts the model changing the classB video fraction depending on the number of loggedin
        users.
        
        Returns a tuple (logged-in users, oldvalue, newvalue)
        """
        model = self.optModel.model
        before = model.classBVideoFraction
        
        model.classBVideoFraction = self._initialClassRatio / (1 + loggedInUsers)
        return (before, model.classBVideoFraction)
        
