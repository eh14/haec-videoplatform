'''

@author: eh14
'''
import pythonioc
from haec.services import cambrionixservice, eventservice
from twisted.internet import defer
from twisted.python import log
from haec import utils
import collections
import copy

class _Status(object):
    """
    This class encapsulates the "logic" of a node's status.
    Since setting a node's status has some delay, automatic 
    deactivation of switches might result in flapping.
    
    The status can have 4 possible values:
    off -> awake -> on -> sleepy ->
    
    State transitions can only happen in the order above.
    Off and on can also switch.
    """
    
    timeProvider = pythonioc.Inject('timeProvider')
    
    CHANGE_TIMEOUT = 5
    
    def __init__(self):
        #
        # false == off
        # true == on
        self._status = False
        
        # if a change has been requested, sets this value
        # to the current time.
        self._changing = None
        
    @property
    def status(self):
        return self._status
    
    def isChanging(self):
        return self._changing and self.timeProvider.now() - self._changing <= self.CHANGE_TIMEOUT
    
    def setStatus(self, newStatus):
        """
        Changes the status to on or off. If the status
        is different and the status was in changing mode, 
        the change-variable is reset.
        """
        assert newStatus in [False, True]
        if self._status != newStatus:
            self._status = newStatus
            self._changing = None
            
        # if the changing-flag was never released it means either the node
        # never came up/down or the mark-flag was set twice.
        if self._changing and self.timeProvider.now() - self._changing > self.CHANGE_TIMEOUT:
            self._changing = None
            
    def markChange(self):
        """
        marks the status to be changed in near future.
        """
        self._changing = self.timeProvider.now()
        
    def __str__(self):
        return "status: %s, changing: %s" % (self._status, self._changing)
    
    def __repr__(self):
        return str(self)
    
    def __nonzero__(self):
        return self._status
    
    def __bool__(self):
        return self._status
    
class _Node(object):
    def __init__(self, name, parent=None):
        self.name = name
        self.parent = parent
        self.children = []
        self.status = _Status()
        
    def __str__(self):
        return '<Node %s, %s (%d children)>' % (self.name, self.status, len(self.children))
        
    def __repr__(self):
        return str(self)
    
    def isOfflineNotChanging(self):
        """
        true if the node's status is online and it is not marked as being
        changed.
        """
        return not self.status and not self.status.isChanging()

class TopologyService(object):
    """
    Service that handles node status, turns on and off nodes. 
    This services is topology-aware, if all nodes of a switch are turned off,
    the switch will be shut off as well.
    This hierarchy is managed using the _Node-class. Although the class supports 
    deep trees, the service only assumes two layers (switches->*slaves).
    """
    
    slaveDao = pythonioc.Inject('slaveDao')
    cambrionix = pythonioc.Inject(cambrionixservice.CambrionixConnector)
    reactor = pythonioc.Inject('reactor')
    events = pythonioc.Inject(eventservice.EventService)
    
    def __init__(self):
        self._topoNodes = {}
        
        self._topology = {}

    def postInit(self):
        utils.startRetryingOneshotJob(self.reactor, self._initTopology, now=True)
        
    @defer.inlineCallbacks
    def updateNodeStates(self, nodeStates):
        for name, node in nodeStates.iteritems():
            if name not in self._topoNodes:
                log.msg('Warning: status for node %s cannot be updated, seems not registered in topology' % name)
                continue

            self._topoNodes[name].status.setStatus(node['status'] in ['1', 1 , True])
            
        yield self._checkTurnOffParent()
        
        
    @defer.inlineCallbacks
    def _checkTurnOffParent(self):
        
        # need to yield something, otherwise the decorator
        # will complain if there is nothing yielded in the
        # loop    
        yield defer.succeed(None)
        
        # check for all nodes
        for name, node in self._topoNodes.iteritems():
            # not online or changing, skip it.
            if not node.status.status or node.status.isChanging():
                continue
            
            # no children
            if not len(node.children):
                continue
            
            # if all children are only and not 
            if all([c.isOfflineNotChanging() for c in node.children]):
                yield self.cambrionix.turnOffNode(name)
        
    @defer.inlineCallbacks
    def _initTopology(self):
        topology = yield self.cambrionix.getNetworkTopology()
        
        # just collect switches and the list of their children
        # so we can return that information quickly without
        # running through all nodes every time.
        topoBuffer = collections.defaultdict(list)
        topoNodes = {}
        for switch, slaves in topology.iteritems():
            switchNode = _Node(name=switch)
            topoNodes[switch] = switchNode
            for slave in slaves:
                slaveNode = _Node(name=slave, parent=switchNode)
                switchNode.children.append(slaveNode)
                topoNodes[slave] = slaveNode
                
                topoBuffer[switch].append(slave)
        
        # set the real class values      
        self._topology = topoBuffer
        self._topoNodes = topoNodes
        
        self.events.triggerEvent('topology-initialized', self.getTopology())
                
    @defer.inlineCallbacks
    def activateNode(self, node):
        if node not in self._topoNodes:
            log.msg('Warning: Topology uninitialized or node not found. Will simply activate the node')
            yield self.cambrionix.turnOnNode(node)
        
        else:
            node = self._topoNodes[node]
            
            # if the parent is offline, turn it on
            if node.parent and not node.parent.status.status:
                node.parent.status.markChange()
                yield self.cambrionix.turnOnNode(node.parent.name)
               
            
            node.status.markChange()
             
            # turn on the node
            yield self.cambrionix.turnOnNode(node.name)
            
    def getNodeNameList(self):
        return self._topoNodes.keys()

    @defer.inlineCallbacks
    def deactivateNode(self, node):
        yield self.cambrionix.turnOffNode(node)
        
    def getTopology(self):
        """
        Returns the topology as a dictionary of lists like
        {<switch>:[<slave1>, <slave2>, ...]}
        """
        return copy.deepcopy(self._topology)
        
