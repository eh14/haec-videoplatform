'''

@author: eh14
'''
import pythonioc
from haec import config
from twisted.python import log
from haec.services import portmapperservice, cambrionixservice, slaveconnservice, \
    statisticservice, sensorservice, optimizerservice, \
    mastervideoservice, profileservice, activityservice, demoservice
from haec import utils
from twisted.internet import defer
from haec.optimization import snapshotcreator, optmodel



class LooperService(object):
    """
    Handles all recurring (or not recurring) loopers that are used in the
    master globally.
    """
    
    MAX_CAMBRIONIX_RETRIES = 100
    _reactor = pythonioc.Inject('reactor')
    cfg = pythonioc.Inject(config.Config)
    
    cambrionixService = pythonioc.Inject(cambrionixservice.CambrionixService)
    
    _portmapperservice = pythonioc.Inject(portmapperservice.PortMapperService)
    slaveDao = pythonioc.Inject('slaveDao')
    slaveConnService = pythonioc.Inject(slaveconnservice.SlaveConnService)
    snapshotCreator = pythonioc.Inject(snapshotcreator.SystemSnapshotCreator)
    statisticService = pythonioc.Inject(statisticservice.StatisticService)
    sensorService = pythonioc.Inject(sensorservice.MasterSensorService)
    videoDao = pythonioc.Inject('videoDao')
    masterVideoService = pythonioc.Inject(mastervideoservice.MasterVideoService)
    portmapperService = pythonioc.Inject(portmapperservice.PortMapperService)
    optimizerService = pythonioc.Inject(optimizerservice.OptimizerService)
    optModel = pythonioc.Inject(optmodel.OptModelProvider)
    profileService = pythonioc.Inject(profileservice.ProfileService)
    activityService = pythonioc.Inject(activityservice.ActivityService)
    demoService = pythonioc.Inject(demoservice.DemoService)
    def __init__(self):
        self.queueAdder = None
        
        self.readCambrionixJob = None
        self.sensorSampleJob = None
        
        #
        # updates the slave activity regularily depending on running tasks and decides which slaves to shut down.
        # 
        self.slaveActivityJob = None
        
        
        self.optimizerJob = None
        self.setupTopologyJob = None
        self.setupSlaveMappingJob = None
        self.slaveResurrectorLooper = None
        
        #
        # demo context events
        self.demoContextJob = None
    
    def postInit(self):
        self.queueAdder = utils.createLoopingCall(self._reactor, self.masterVideoService.addBatchedVideo)
        
        self.readCambrionixJob = utils.createLoopingCall(self._reactor, self.cambrionixService.readCambrionixValues)
        self.sensorSampleJob = utils.createLoopingCall(self._reactor, self.sensorService.sampleInSensorstore)
        
        self.optimizerJob = utils.createLoopingCall(self._reactor, self.optimizerService.optimize)
        
        self.slaveResurrectorLooper = utils.createLoopingCall(self._reactor, self.slaveConnService.resurrectSlaves)
        self.slaveActivityJob = utils.createLoopingCall(self._reactor, self.activityService.updateSlaveTasks)
        
        self.demoContextJob = utils.createLoopingCall(self._reactor, self.demoService.triggerContextEvent)
        
        utils.startRetryingOneshotJob(self._reactor, self.portmapperService.initExternalIp, tryInterval=1)
        utils.startRetryingOneshotJob(self._reactor, self.portmapperService.initInternalIp, tryInterval=1)
        
    def start(self):
        log.msg('Starting Loopers...')
        
        utils.startRetryingOneshotJob(self._reactor, self.cambrionixService.lateInit, tryInterval=self.cfg.vals.cambrionix_postinit_delay)
        
        # turn off all children that are not connected by now.
        self._reactor.callLater(self.cfg.vals.heartbeat_timeout, self.setDisconnectedChildrenOffline)
        
        
        self.queueAdder.start(self.cfg.vals.video_bulk_interval, now=False)
        
        self.readCambrionixJob.start(self.cfg.vals.cambrionix_collectinterval,
                                     now=False,
                                     jitter=0,
                                     minLogTime=400,
                                     maxfails=self.MAX_CAMBRIONIX_RETRIES)

        self.slaveActivityJob.start(self.cfg.vals.task_runtasks_interval, now=False)

        
        self.sensorSampleJob.start(self.cfg.vals.sensor_sample_interval, jitter=0, now=False)

        self.setupTopologyJob = utils.startRetryingOneshotJob(self._reactor,
                                                            self.cambrionixService.setupCambrionixTopology,
                                                            tryInterval=self.cfg.vals.cambrionix_postinit_delay,
                                                            maxRetries=self.MAX_CAMBRIONIX_RETRIES)
        
        self.setupSlaveMappingJob = utils.startRetryingOneshotJob(self._reactor,
                                                            self.portmapperService.readExistingSlaves,
                                                            tryInterval=self.cfg.vals.portmapping_initialize_delay,
                                                            maxRetries=12)
        
        
        if self.cfg.vals.opt_runinterval > 0:
            self.optimizerJob.start(self.cfg.vals.opt_runinterval,
                                    now=False,
                                    allowOverlap=False)
        self.startResurrectorLooper()
        
        self.demoContextJob.start(120, now=False, jitter=0.5)
        
    def startResurrectorLooper(self):
        if self.slaveResurrectorLooper.running:
            log.msg('resurrector already running. Should not come to this point')
            return
        self.slaveResurrectorLooper.start(self.cfg.vals.slave_resurrect_interval, now=False, allowOverlap=False)
        
    def stopResurrectorLooper(self):
        self.slaveResurrectorLooper.stop()
        
        
    def stop(self):
        self.queueAdder.stop()
        self.readCambrionixJob.stop()
        self.slaveActivityJob.stop()
        self.optimizerJob.stop()
        self.stopResurrectorLooper()
        if self.setupTopologyJob:
            self.setupTopologyJob.stop()
            
        if self.setupSlaveMappingJob:
            self.setupSlaveMappingJob.stop()
        self.demoContextJob.stop()
            
    def setDisconnectedChildrenOffline(self):
        connectedSlaveNames = [str(s.getSlaveName()) for s in self.slaveConnService.getAllConnectedSlaves() if s.getSlaveName is not None]
        self.slaveDao.setSlavesOffExcept(*connectedSlaveNames)
    
