'''

@author: eh14
'''
from twisted.internet import defer, threads
from twisted.python import log
import os
import collections
import re
from haec import config
from haec.services import jobscheduler
import pythonioc
import subprocess

cfg = pythonioc.Inject(config.Config)
#
# regex pattern that matches the filenames used to identify domain files
#
videoPattern = re.compile('([a-zA-Z_0-9-]+?)(?:__id([a-zA-Z0-9-_]+))?\.([a-z0-9A-Z]{2,4})')

        
class CollectVideoDataJob(jobscheduler.ThreadedJob):
    
    _sectionBound = re.compile('^\[\/?[\w\.\d]+\]$')
    _streamStart = re.compile('^\[stream(?:s\.stream\.[\d]+)?\]$', re.IGNORECASE)
        
    def __init__(self, directory, vidData):
        self._dir = directory
        self._vidData = vidData
        self._proc = None
        
    def _executeThreaded(self):
        log.msg('Scanning video %s...' % self._vidData['fileName'])
        abspath = os.path.join(self._dir, self._vidData['fileName'])
        m = videoPattern.match(self._vidData['fileName'])
        if not m:
            raise Exception("Invalid file name pattern %s" % self._vidData['fileName'])
        
        metaData = self._readVideoMetadata(abspath)
        stat = os.stat(abspath)
        metaData['fsize'] = stat.st_size
        log.msg('Scanning video %s... done' % self._vidData['fileName'])
        
        for k, v in metaData.iteritems():
            self._vidData[k] = v
        
        if self._vidData['variant'] is None:
            self._vidData['variant'] = ''   
        
        return self._vidData
            
    def stop(self):
        if self._proc:
            self._proc.kill()
            
    def _readVideoMetadata(self, domain):
        self._proc = subprocess.Popen(['avprobe', '-show_format', '-show_streams', domain], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        (stdout, _) = self._proc.communicate()
        
        if self._proc.returncode != 0:
            raise Exception("Error reading meta data for %s. ReturnCode %d, Error %s" % (domain, self._proc.returncode, stdout))
        
        try:
            return self._readMetaDataFromOutput(stdout)
        except:
            log.msg('Error reading meta data')
            log.msg(stdout)
            raise
        
    def _readMetaDataFromOutput(self, output):
        """
        interprets avprobe's output.
        @return: python dictionary with all meta information.
        """
        formatSect = self._getFormatSection(output)
        streams = self._getStreamSections(output)
        try:
            frameRate = eval(streams['video']['avg_frame_rate'])
        except:
            frameRate = 0
        metaData = {
            'duration':self._float2Str(formatSect['duration']),
            'bitrate':self._float2Str(formatSect['bit_rate']),
            'fps':str(frameRate),
            'width':streams['video']['width'],
            'height':streams['video']['height'],
            'video_codec':streams['video']['codec_name'],
            'audio_codec':streams['audio']['codec_name'],
            'format':formatSect['format_name']
        }
        return metaData
    
    def _float2Str(self, val):
        return str(float(val))
    
    
    def _getStreamSections(self, output):
        streams = {}
        currentStream = None
        for line in output.split('\n'):
            line = line.strip()
            if self._sectionBound.match(line) and currentStream and 'codec_type' in currentStream:
                streams[currentStream['codec_type']] = currentStream
                currentStream = None
                
            if self._streamStart.match(line):
                currentStream = {}
                
            if currentStream is not None:
                items = line.split('=')
                if len(items) == 2:
                    currentStream[items[0].strip()] = items[1].strip()
                    
        # add last one, if finished:
        if currentStream:
            streams[currentStream['codec_type']] = currentStream
        return streams
    
    def _getFormatSection(self, output):
        formatSection = {}
        inSection = False
        for line in output.split('\n'):
            line = line.strip()
            
            
            if self._sectionBound.match(line) and inSection:
                return formatSection
            
            if line.lower() == '[format]':
                inSection = True
                
            if inSection:
                items = line.split('=')
                if len(items) == 2:
                    formatSection[items[0].strip()] = items[1].strip()

        raise Exception('Error reading format section')


class SlaveVideoService(object):
    
    taskRunner = pythonioc.Inject('taskRunner')
    
    def __init__(self):
        self._dir = cfg.vals.video_dir
        
    @defer.inlineCallbacks
    def scanVideoDir(self):
        try:
            vids = yield threads.deferToThread(self._scanVideoDir)
            uniquified = yield threads.deferToThread(self._uniquifyVideoDir, vids)
            # we needed to copy videos, let's scan again.
            # but we do the scan only once more, really!
            if uniquified:
                vids = yield threads.deferToThread(self._scanVideoDir)
            defer.returnValue(vids)
        except Exception as e:
            log.msg("Error sending scanning the video dir %s" % e)
            raise
            
            
    def _scanVideoDir(self):
        vids = []
        for entry in os.listdir(self._dir):
            data = self._parseVideoName(entry)
            if not data:
                continue
            vids.append(data)
            
        return vids
    
    
    def _uniquifyVideoDir(self, parsedVideos):
        
        renamedFiles = False
        
        duplicateNames = collections.defaultdict(list)
        for entry in parsedVideos:
            if not entry['variant']:
                duplicateNames[entry['name']].append(entry)
        for items in duplicateNames.itervalues():
            if len(items) > 1:
                for item in items:
                    os.rename(os.path.join(self._dir, item['fileName']), os.path.join(self._dir, item['name'] + '__id' + item['extension'] + '.' + item['extension']))
                renamedFiles = True
        return renamedFiles
    
    @classmethod
    def _parseVideoName(cls, fileName):
        m = videoPattern.match(fileName)
        if not m:
            return None
        
        return cls._createVideoData(
                fileName=fileName,  # original filename
                name=m.group(1),  # name
                variantId=m.group(2),  # variant ID (None for source videos)
                extension=m.group(3)  # file extension (container)
                  )
    @classmethod
    def _createVideoData(cls, fileName, name, variantId, extension):
        return {
                'fileName' : fileName,
                'name' :name,
                'variant' :  variantId,
                'extension': extension 
                  }
        
    @defer.inlineCallbacks
    def scanSingleVideo(self, fileName):
        videoData = self._parseVideoName(fileName)
        scannedData = yield self.scanVideoData(videoData)
        defer.returnValue(scannedData)
        
    def scanVideoData(self, videoData):
        return self.taskRunner.startJob(CollectVideoDataJob(self._dir, videoData))
