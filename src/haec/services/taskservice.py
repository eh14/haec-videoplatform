import pythonioc
from haec.services import jobscheduler, slaveconnservice
from haec import utils
from haec import config
from twisted.internet import defer
from twisted.python import log
from haec.protomixins import taskrunner

cfg = pythonioc.Inject(config.Config)

class TaskService(object):
    """
    Manages lifecycle, execution, cleanup etc. of tasks using the task dao.
    """
    localTasks = pythonioc.Inject('LocalTaskService')
    
    slaveConnService = pythonioc.Inject(slaveconnservice.SlaveConnService)
    
    videoDao = pythonioc.Inject('videoDao')
    
    reactor = pythonioc.Inject('reactor')
    
    taskDao = pythonioc.Inject('taskDao')
    
    def __init__(self):
        self._taskIndex = None
        
        self._sched = None
        
        self._runnerLooper = None
        
        # this is set to True when we are in the runTasks-function. Since
        # the tasks are fetched from the DB asynchronously, we need this kind of locking
        # to avoid starting them multiple times.
        self._runningTasksNow = False
        
        # avoid scheduling for "immediate runTasks" we save the delayed call here.
        # If not set, we schedule.
        # On snooze, we cancel the call.
        self._runTaskDelayedCall = None
        #
        # Specifies whether tasks are tried to be run when a task finishes. If set to false,
        # tasks are only started using the timer.
        # This is mostly set to false for testing purposes.
        self.autoWakeup = True
        
    def postInit(self):
        # local scheduler to execute local tasks
        self._sched = jobscheduler.JobScheduler(30)
        self._runnerLooper = utils.createLoopingCall(self.reactor, self.runTasks)
        
        # create and start cleaner-Looper
        self._cleanerLooper = utils.createLoopingCall(self.reactor, self.taskDao.cleanTasks)
        self._cleanerLooper.start(cfg.vals.tasks_cleantasks_interval, now=False)
        
    def preDestroy(self):
        if self._sched:
            self._sched.shutdown()
             
        if self._runnerLooper and self._runnerLooper.running:
            self._runnerLooper.stop()
            
        if self._cleanerLooper and self._cleanerLooper.running:
            self._cleanerLooper.stop()
        
    @defer.inlineCallbacks
    def addExtrinsicTask(self, taskId, name, **kwargs):
        yield self.taskDao.addTask(taskId, name, '', taskType='extrinsic', **kwargs)
        
        
    @defer.inlineCallbacks
    def runTasks(self):
        if self._runningTasksNow:
            return
        
        self._runningTasksNow = True
        try:
            log.msg('Attempting to run tasks in the taskmanager')
            yield self.taskDao.updateTaskStates()
            
            tasks = yield self.taskDao.getRunnableTasks()
            log.msg('Checking for tasks to run: Got %d' % len(tasks))
            
            # no more waiting tasks, stop it
            numTasks = yield self.taskDao.getNumberOfWaitingTasks()
            if numTasks == 0:
                self.snooze()
            
            for task in tasks:
                yield self._runTask(task)
            
        finally:
            self._runningTasksNow = False
            
    def wakeup(self):
        # only if enabled (can be disabled for testing)
        if not self.autoWakeup:
            return
        
        if not self._runnerLooper.running:
            interval = cfg.vals.task_runtasks_interval
            if interval:
                self._runnerLooper.start(cfg.vals.task_runtasks_interval, False)

        # schedule it for next iteration
        self.reactor.callLater(0, self.runTasks)
        
    def snooze(self):
        log.msg('Task service is going to sleep...')
        if self._runnerLooper.running:
            self._runnerLooper.stop()
            
    @defer.inlineCallbacks
    def _runTask(self, task):
        
        log.msg('Starting task %s' % repr(task))
        
        assert task.type == 'intrinsic', "Attempting to run a task that we are not supposed to execute" 
        taskId = task.taskId
        
        yield self.taskDao.updateTaskStarted(taskId, 0, task.location)
        
        job = self.taskDao.createJobFromAction(task.action)
        
        if task.location == 'local':
            self._executeLocalJob(taskId, job)
        else:
            # send the job to the slave
            try:
                slave = self.slaveConnService.getConnectedSlaveByName(task.location)
                taskrunner.MasterTaskRunnerMixin.startJobOnSlave(slave, taskId, job)
            except Exception as e:
                yield self.taskDao.updateTaskFinished(taskId, False, message=str(e))
            finally:
                self.wakeup()
    
    @defer.inlineCallbacks
    def _executeLocalJob(self, taskId, job):
        try:
            yield self._sched.startJob(job)
            yield self.taskDao.updateTaskFinished(taskId, True)
        except defer.CancelledError as e:
            yield self.taskDao.updateTaskCancelled(taskId)
        except Exception as e:
            yield self.taskDao.updateTaskFinished(taskId, False, message=str(e))
        finally:
            self.wakeup()
