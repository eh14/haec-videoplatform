'''
Created on Apr 24, 2014

@author: eh14
'''

from twisted.python import log
from twisted.internet import threads, defer
from haec import config
import pythonioc
import subprocess
from haec import utils

cfg = pythonioc.Inject(config.Config)

def register(name):
    pass

class JobCancelled(Exception):
    """ 
    If raised, allows a job to cancel itself (if preconditions are not met anymore etc.)
    instead of being marked failed when an exception is raised.
    """
    pass

class Job(object):
    
    def run(self):
        """
        override to do something.
        """
        raise NotImplementedError('run method not implemented!')
    
    def stop(self):
        
        """
        Explicitly stop the job
        """
        pass
    
class ThreadedJob(Job):
    
    def run(self):
        return threads.deferToThread(self._executeThreaded)
    
    def _executeThreaded(self):
        raise NotImplementedError('implement _executeThreaded!')
    
class NoopJob(Job):
    def run(self):
        return
    def stop(self):
        return
    
class SyncFunctionJob(Job):
    """
    Execute a function synchronized. Note that the function's return value
    is returned, so it is possible (and intended) for the function to return
    a Deferred.
    """
    def __init__(self, function, *args, **kwargs):
        self.function = function
        self.args = args
        self.kwargs = kwargs
       
    def run(self):
        return self.function(*self.args, **self.kwargs)
        
    def stop(self):
        # cannot really kill a function call, can I? 
        pass
    
class CompositeJob(Job):
    def __init__(self, *jobs):
        self.jobs = jobs
        
    @defer.inlineCallbacks
    def run(self):
        try:
            currentJob = ""
            for job in self.jobs:
                currentJob = str(job)
                yield job.run()
        except Exception as e:
            log.msg('Exception %s while executing job %s' % (e, currentJob))
            raise
            
    @defer.inlineCallbacks
    def stop(self):
        for job in self.jobs:
            yield job.stop()
            
        
    
class ExecuteCommandJob(ThreadedJob):
    def __init__(self, command):
        self._cmd = command
        self._proc = None
    def _executeThreaded(self):
        command = self._cmd
        if not isinstance(command, list):
            command = command.split(' ')
            
        log.msg('Executing command %s' % ' '.join(command))
        self._proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        (stdout, _) = self._proc.communicate()
        
        if self._proc.returncode != 0:
            raise Exception("Error executing command %s. ReturnCode %d, Error %s" % (' '.join(command), self._proc.returncode, stdout))
    def __str__(self):
        return "ExecuteCommandJob %s" % self._cmd
    
    def stop(self):
        if self._proc and self._proc.poll() is None:
            self._proc.kill()
        
class JobScheduler(object):
    _reactor = pythonioc.Inject('reactor')
    
    def __init__(self, maxRunning=2):
        self._waiting = []
        self._running = set()
        self._done = set()
      
        if not isinstance(maxRunning, int):
            raise TypeError('Expected maxRunning to be int, got %s' % maxRunning.__class__)  
        self._maxRunning = maxRunning
        
    def startJob(self, job):
        """
        Schedules a job start. This method returns a deferred which will be
        fired when the job is done, so be careful with yielding that in a test case,
        otherwise it might not terminate.
        """
        def canceller(deferred):
            for i, (_d, _) in utils.countingIter(self._waiting):
                if _d == deferred:
                    del self._waiting[i]
                    break
                    
        d = defer.Deferred(canceller)
        self._waiting.append((d, job))
        self._reactor.callLater(0, self._startWaiting)
        return d
    
    def getWaitingDeferredList(self):
        return defer.DeferredList([w[0] for w in self._waiting])
        
    def _startWaiting(self):
        """
        starts a waiting job if available and if not _maxRunning jobs are already
        executed.
        """
        assert len(self._running) <= self._maxRunning, "this should not be called at this state"
        if len(self._running) == self._maxRunning or len(self._waiting) == 0:
            return
        (d, job) = self._waiting.pop(0)
        self._running.add(job)
        
        jobDef = defer.maybeDeferred(job.run)
        def error(err):
            if err.check(JobCancelled):
                log.msg("Job cancelled itself. Not calling any callbacks.")
                d.cancel()
            else:
                log.msg("Running job resulted in %s" % (err.getErrorMessage()))
                d.errback(err)
        
        jobDef.addCallback(lambda result: d.callback(result))
        jobDef.addErrback(error)
        jobDef.addBoth(self._jobDone, job)
        return jobDef

    def _jobDone(self, result, job):
        if job in self._running:
            self._running.remove(job)
            
        self._done.add(job)
        self._reactor.callLater(cfg.vals.scheduler_job_waitinterval, self._startWaiting)
        
        return result
            
    def shutdown(self):
        for job in self._running:
            try:
                job.stop()
            except Exception as e:
                log.msg('Error trying to stop local job')
                log.msg(e)
                
        # reset the running-list
        self._running = set()
                
    def __del__(self):
        self.shutdown()
        
    def isBusy(self):
        """
        Returns true if there are running and/or waiting tasks.
        """
        return (len(self._running) + len(self._waiting)) > 0

