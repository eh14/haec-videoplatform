'''

@author: eh14
'''
import pythonioc
from haec import config
from haec.services import eventservice
import collections

cfg = pythonioc.Inject(config.Config)


class MasterSensorService(object):
    
    sensorStore = pythonioc.Inject('sensorStore')
    timeProvider = pythonioc.Inject('timeProvider')
    eventService = pythonioc.Inject(eventservice.EventService)
    
    def postInit(self):
        self.eventService.subscribeEvent('topology-initialized', self.sensorStore.initBuffers)
        
    def sampleInSensorstore(self):
        now = self.timeProvider.now()
        self.sensorStore.sampleValues(now)
        
        self.eventService.triggerEvent('sensors-available', now=now)
        
    def getAllPowerSum(self):
        return [(powerValue.getTime(), powerValue.getCurrentSum()) for powerValue in self.sensorStore.powervalues]
        
    def getAllCpuUsage(self):
        return self.sensorStore.cpuAvg.getAll()
    
    def getAllNwOutSum(self):
        return self.sensorStore.nwOutSum.getAll()
    
    def getRecentPowerSum(self):
        if len(self.sensorStore.powervalues) == 0:
            return 0
        else:
            return self.sensorStore.powervalues[-1].getCurrentSum()
        
    def getPowerValues(self, timestampFrom, nodeNames):
        if len(self.sensorStore.powervalues) == 0:
            return []
        
        assert isinstance(timestampFrom, int), "now must be an int"
        
        results = collections.defaultdict(list)
        for powervalue in self.sensorStore.powervalues:
            ts = powervalue.getTime()
            msTime = ts * 1000
            if ts >= timestampFrom:
                for node, values in powervalue.getSlaveValues().iteritems():
                    if node not in nodeNames:
                        continue
                    values = dict(values)
                    values['time'] = msTime
                    values['id'] = msTime
                    results[node].append(values)
        return results
    
    def getAggregatedPowerValues(self, timestampFrom):
        if len(self.sensorStore.powervalues) == 0:
            return []
        
        assert isinstance(timestampFrom, int), "now must be an int"
        
        
        results = []
        for powervalue in self.sensorStore.powervalues:
            ts = powervalue.getTime()
            msTime = ts * 1000
            if ts >= timestampFrom:
                results.append({'time':msTime,
                                'id':msTime,
                                'current':powervalue.getCurrentSum()
                                })
                
        return results
