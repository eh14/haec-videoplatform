'''
classes to serve video files.
'''
from twisted.web import static, resource
from twisted.python import log
from haec import config

import time
import math
from twisted.internet import reactor
import pythonioc
from haec.services import activitytracker

cfg = pythonioc.Inject(config.Config)

class ThrottlingProducer(object):
    
    def __init__(self, producer):
        self._producer = producer
        self._request = producer.request
        
    def __getattr__(self, *args, **kwargs):
        return getattr(self._producer, *args)
    
    def resumeProducing(self):
        if self._request.dataOverflow():
            return
        else:
            return self._producer.resumeProducing()
        
    def start(self):
        self._producer.start()
        self._request.setThrottlingProducer(self)

class ThrottlingRequest(object):
    
    _delayedSendRate = 0.1
    
    activityTracker = pythonioc.Inject(activitytracker.ActivityTracker)
    
    def __init__(self, request):
        self._written = 0
        self._start = None
        self._bandwidth = float(1024 * 1024)
        
        self._request = request
        self._delayedSender = None
        self._remainingData = ''
        
        request.notifyFinish().addBoth(lambda _:self.activityTracker.streamFinished(request.prepath[0], self))
    
    def __getattr__(self, *args, **kwargs):
        return getattr(self._request, *args)
    
    def registerProducer(self, *args, **kwargs):
        self.activityTracker.streamStarted(self._request.prepath[0], self)
    
    def setThrottlingProducer(self, producer):
        self._request.registerProducer(producer, 0)
            
    def dataOverflow(self):
        return self._delayedSender is not None
    
    def stats(self):
        
        if not self._written or not self._start: 
            log.msg("Nothing sent")
            return
        
        now = time.time()
        sentKb = float(self._written) / 1024.0
        bandwidth = sentKb / (now - self._start)
        log.msg('Done sending %d kbytes with %f kB/s' % (sentKb, bandwidth))
        
    def _sendAndLog(self, data):
        self._written += len(data)
        self._request.write(data)
        
    def _delayedSend(self):
        self._delayedSender = None
        
        now = time.time()
        bytesToSend = int(math.floor(((now - self._start) * self._bandwidth) - self._written))
        if bytesToSend <= 0:
            self._scheduleDelayedSend()
            return
        
        dataToSend = self._remainingData[:bytesToSend]
        self._remainingData = self._remainingData[bytesToSend:]
        
        if self._remainingData:
            # if something is left, schedule for next round
            self._scheduleDelayedSend()
            
        if dataToSend:
            self._sendAndLog(dataToSend)
                        
        
    def _scheduleDelayedSend(self):
        self._delayedSender = reactor.callLater(self._delayedSendRate, self._delayedSend)  # @UndefinedVariable
        
        
    def write(self, data):
                
        if self._delayedSender:
            log.msg('Already waiting. How can the consumer ask for more?')
            return
        
      
        now = time.time()
        
        if self._start is None:
            self._start = now
            if len(data) > self._bandwidth:
                self._remainingData = data
                self._scheduleDelayedSend()
            else:
                self._sendAndLog(data)
            return
            
        passed = now - self._start
        bandwidth = float(self._written + len(data)) / float(passed)
        if bandwidth > self._bandwidth:
            self._remainingData = data
            self._scheduleDelayedSend()
        else:
            self._sendAndLog(data)
        
    def unregisterProducer(self):
        
        self.activityTracker.streamFinished(self._request.prepath[0], self)
        while self._delayedSender:
            self._delayedSender.cancel()
            self._delayedSender = None
            
        self._request.unregisterProducer()
        
class SlaveShuttingDownResource(resource.ErrorPage):
    """
    L{SlaveShuttingDownResource} is a specialization of L{ErrorPage} which returns the HTTP
    response code I{GONE}.
    """
    def __init__(self):
        resource.ErrorPage.__init__(self, 410, "Server is shutting down.", 'The requested file cannot be sent anymore. This server is shutting down.')
        
class VideoMovedResource(resource.ErrorPage):
    """
    Uses HttpCode 301 (Moved Permanently) indicating that the video has
    been migrated to another slave. The header-location field should contain
    the location of the master's video url so the client can request a new url,
    however the master's web port and external address are unknown, so we simply 
    send the code.
    """
    def __init__(self):
        resource.ErrorPage.__init__(self, 301, "Video has been moved.", "The requested video has been migrated to another slave. Please request a new link from the master.")
        
activityTracker = pythonioc.Inject(activitytracker.ActivityTracker)
class CORSResource(static.File):
    """
    Simple File Subclasses that allows CORS access.
    """
    
    shuttingDownResource = SlaveShuttingDownResource()
    videoMovedResource = VideoMovedResource()
    
    def getChild(self, path, request):
        if path == '__get_hostname':
            return NameResource()
        
        if activityTracker.isTerminating():
            return self.shuttingDownResource
        
        if activityTracker.isVideoBlocked(path):
            return self.videoMovedResource
        
        return static.File.getChild(self, path, request)
    
    def parseBool(self, value):
        return value.lower() in ['true', '1', 'yes']
    
    def render_GET(self, request):
        request.setHeader("Access-Control-Allow-Origin", '*')  # replace that by the real master address
        
        if 'download' in request.args and self.parseBool(request.args['download'][0]):
            request.setHeader('Content-Disposition', 'attachment; filename=%s' % request.prepath[0])
        return static.File.render_GET(self, request)
    
    def makeProducer(self, request, fileForReading):
        return ThrottlingProducer(static.File.makeProducer(self,
                                                           ThrottlingRequest(request),
                                                           fileForReading))
    
    
class NameResource(resource.Resource):
    def render_GET(self, request):
        return cfg.vals.name
