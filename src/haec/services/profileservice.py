'''

@author: eh14
'''
import pythonioc
from twisted.internet import defer

class NoSuchProfileException(Exception):
    pass

_qualityModes = [('normal', 'do what is necessary to provide requested quality.'),
                 ('best', 'no energy considerations. Best quality as fast as possible (not implemented).'),
                 ('green', 'save energy. Result might suffer from quality loss.')]

class ProfileService(object):
    
    profileDao = pythonioc.Inject('profileDao')
    reactor = pythonioc.Inject('reactor')
    
    def __init__(self):
        self._profileMap = None
        self._profileNames = None
        
        self._initializedDeferred = defer.Deferred()

    @defer.inlineCallbacks
    def postInit(self):
        yield self.addDefaultProfiles()
        
        self._initializedDeferred.callback(None)
        
    @defer.inlineCallbacks
    def addDefaultProfiles(self):
        defaultProfiles = [        
         dict(name='Any Profile',
            width=None,
            height=None,
            vCodec=None,
            aCodec=None,
            fileFormat=None),
         dict(name='Universal Smartphone Low',
            width=480,
            height=320,
            vCodec='h264',
            aCodec='aac',
            fileFormat='avi'),
         dict(name='Universal Smartphone Medium',
           width=960,
           height=640,
           vCodec='h264',
           aCodec='aac',
           fileFormat='avi'),
         dict(name='Universal Smartphone HighDef',
           width=1280,
           height=720,
           vCodec='h264',
           aCodec='aac',
           fileFormat='avi'),
         dict(name='Legacy 3GP',
            width=320,
            height=240,
            vCodec='mpeg4',
            aCodec='aac',
            fileFormat='3gp'),
         dict(name='Flash',
             width=320,
             height=240,
             vCodec='h264',
             aCodec='aac',
             fileFormat='flv')
        ]
        
        names = yield self.profileDao.getNames()
        for prof in defaultProfiles:
            if prof['name'] not in names:
                yield self.profileDao.add(**prof)
        
        yield self._updateProfiles()
        
    @defer.inlineCallbacks
    def _updateProfiles(self):
        """
        Updates the internal bufferings of profiles.
        """
        
        self._profileNames = yield self.profileDao.getNames()
        
        profs = yield self.profileDao.getAll()
        self._profileMap = {str(p.id):p for p in profs}
        
    @defer.inlineCallbacks
    def getProfileForId(self, profileId):
        
        if not self._initializedDeferred.called:
            yield self._initializedDeferred
        
        
        profileId = str(profileId)
        if profileId not in self._profileMap:
            raise NoSuchProfileException(profileId)
        
        defer.returnValue(self._profileMap[profileId])
        
    @defer.inlineCallbacks
    def getProfileForNameOrId(self, profileNameOrId):
        if not self._initializedDeferred.called:
            yield self._initializedDeferred
            
        profileNameOrId = str(profileNameOrId)
        if profileNameOrId in self._profileMap:
            defer.returnValue(self._profileMap[profileNameOrId])
        for prof in self._profileMap.itervalues():
            if self.makeShortName(prof.name) == profileNameOrId:
                defer.returnValue(prof)
        
        raise NoSuchProfileException('No profile with ID or name ' + profileNameOrId)
    
    
    @classmethod
    def makeShortName(cls, name):
        return name.replace(' ', '').lower()
    
    @defer.inlineCallbacks
    def getProfileList(self, asDict=True):
        if not self._initializedDeferred.called:
            yield self._initializedDeferred
            
        assert self._profileMap, "Profiles seem not initialized."
            
        profs = list(self._profileMap.values())
        profs.sort(lambda l, r:cmp(l.name, r.name))
        defer.returnValue(profs)
            
        
    @defer.inlineCallbacks
    def getProfileDictList(self):
        """
        Returns a list of dicts containing all profiles.
        Note that this also contains the domain-object which
        should not be used outside. But it can.
        """
        
        if not self._initializedDeferred.called:
            yield self._initializedDeferred
        
        assert self._profileMap, "Profiles seem not initialized."
        
        profs = [vars(p) for p in self._profileMap.values()]
        profs.sort(lambda l, r:cmp(l['name'], r['name']))
        defer.returnValue(profs)
    
    
    def getQualityModes(self):
        return list(_qualityModes)
