'''

@author: eh14
'''
import collections
import pythonioc
from twisted.python import log
from haec.optimization import optmodel
import localtaskservice
from twisted.internet import defer

class SlaveActivity(object):
    # number of streams
    _streams = 0
    
    # holds only videos from own class
    pure = False
    
    # class
    _class = 'B'
    
    # timestamp it became idle
    _idleTimestamp = None
    
    _timeProvider = pythonioc.Inject('timeProvider')
    
    _idleTimer = None
    
    _runningTasks = 0
    
    def idleSince(self):
        if self._idleTimestamp is None:
            return 0
        else:
            return self._timeProvider.now() - self._idleTimestamp
        
    def getIdleTimestamp(self):
        return self._idleTimestamp
        
    def update(self, streams):
        self._streams = streams
        
        if self._streams == 0:
            self._idleTimestamp = self._timeProvider.now()
        else:
            self._idleTimestamp = None
            
    def updateRunningTasks(self, runningTasks):
        self._runningTasks = runningTasks
        
    def hasRunningTasks(self):
        return self._runningTasks > 0
            
    def isIdle(self):
        return self._idleTimestamp is not None
    
    def getClass(self):
        return self._class
    
    def setClass(self, popClass):
        self._class = popClass
    
    def setIdleTimer(self, timer):
        self.cancelIdleTimer()
        self._idleTimer = timer
        
    def cancelIdleTimer(self):
        if self._idleTimer and self._idleTimer.active():
            self._idleTimer.cancel()
        self._idleTimer = None
        
    def getIdleTimeout(self):
        if self._idleTimer:
            return self._idleTimer.getTime()
        else:
            return 0
        
    def getNumStreams(self):
        return self._streams
        
    
class ActivityService(object):
    """
    Manages slave activities like streaming etc which are
    not stored in database.
    """
    optModel = pythonioc.Inject(optmodel.OptModelProvider)
    master = pythonioc.Inject('master')
    reactor = pythonioc.Inject('reactor')
    
    taskDao = pythonioc.Inject('taskDao')
    slaveDao = pythonioc.Inject('slaveDao')
    def __init__(self):
        self._slaveActivities = collections.defaultdict(SlaveActivity)
        
    def updateSlavePurity(self, slaveName, pure=True):
        self._slaveActivities[slaveName].pure = pure
        self._checkSlaveShutdown(slaveName)
        
    def setAllSlavesUnpure(self):
        for slave in self._slaveActivities.itervalues():
            slave.pure = False
        
    def updateSlaveActivity(self, slaveName, streams):
        self._slaveActivities[slaveName].update(streams)
        self._checkSlaveShutdown(slaveName)
        
    @defer.inlineCallbacks
    def updateSlaveTasks(self):
        tasks = yield self.taskDao.getTasksPerLocation(waiting=True)
        
        for slaveName, slaveActivity in self._slaveActivities.iteritems():
            if slaveName in tasks:
                slaveActivity.updateRunningTasks(tasks[slaveName])
            else:
                slaveActivity.updateRunningTasks(0)
                self._checkSlaveShutdown(slaveName)
                
        
    def updatePopClasses(self, slaveData):
        """
        Updates the popularity classes for all passed slaves.
        
        @param slaveData: dict of {slaveName:popClass}
        """
        for slave, popClass in slaveData.iteritems():
            self._slaveActivities[slave].setClass(popClass)
        
        
    @defer.inlineCallbacks
    def _checkSlaveShutdown(self, slaveName):
        """
        Checks named slave for the preconditions to shutdown
        or schedule a shutdown in future.
        If the preconditions are not met, any pending shutdown timers
        are cancelled.
        """
        slaveAct = self._slaveActivities[slaveName]
        slaveStatus = yield self.slaveDao.getSlaveStatus(slaveName)
        if (slaveStatus == 'online' and 
            slaveAct.getClass() == 'B' and 
            not slaveAct.hasRunningTasks() and
            slaveAct.pure and 
            slaveAct.isIdle()):
            
            idleTime = float(self.optModel.model.idleTimeBeforeShutdown)
            
            if slaveAct.idleSince() > idleTime:
                log.msg('--> Requesting shutdown')
                slaveAct.cancelIdleTimer()
                yield self.taskDao.addLocalActionTask('Shutting down Slave %s' % slaveName,
                                                      localtaskservice.LocalTaskService.shutdownSlave,
                                                      args=(slaveName,))
            
            else:
                waitTime = idleTime - slaveAct.idleSince()
                log.msg('Slave %s is waiting for shutdown another %d s' % (slaveName, waitTime))
                slaveAct.setIdleTimer(self.reactor.callLater(waitTime, self._checkSlaveShutdown, slaveName))
        else:
            slaveAct.cancelIdleTimer()
            
    def isSlaveIdle(self, slaveName): 
        return self._slaveActivities[slaveName].isIdle()
    

    def getSlaveActivity(self, slaveName):
        return self._slaveActivities[slaveName]

    def getSlaveClasses(self):
        return {name:activity.getClass() for name, activity in self._slaveActivities.iteritems()}
