'''

@author: eh14
'''
import pythonioc
from twisted.python import log
import miniupnpc
from haec import utils
from haec import config

import Queue
import threading
from twisted.internet import defer, threads
import requests
import netaddr
import netifaces
cfg = pythonioc.Inject(config.Config)


class _SlaveMapping(object):
    def __init__(self, name, ip, internal, external):
        self.name = name
        
        
        # internal IP
        self.ip = ip
        
        #
        # webserver port of slave
        self.internal = internal
        
        #
        # external port mapped in upnp-device
        #
        self.external = external
        
    def getInternal(self):
        if not self.ip:
            raise RuntimeError('No IP for slave %s known' % self.name)
        if not self.internal:
            raise RuntimeError('No internal mapping known for slave ' + self.name)
        return '%s:%s' % (self.ip, self.internal)

class PortMapperService(object):
    """
    The port mapper basically cares about creating port mappings
    for registering slaves while caring out the initial clean up.
    """
    
    _reactor = pythonioc.Inject('reactor')
    _slaveDao = pythonioc.Inject('slaveDao')
    
    _externalIpServices = ['http://icanhazip.com',
                           'http://ipecho.net/plain',
                            'http://ident.me',
                            'http://bot.whatismyipaddress.com']
    
    def __init__(self):
        self._newSlaves = Queue.Queue()
        # event-loop based lock.
        self._handlingNewSlaves = False
        
        self._initialized = False
        
        self._slavePorts = set(range(38000, 39000))
        self._slaveMapping = {}
        
        self._internalAddress = None
        self._externalAddress = None
        
    def initInternalIp(self):
        internal = netifaces.ifaddresses('eth0')[netifaces.AF_INET][0]
        self._internalAddress = netaddr.IPNetwork('%s/%s' % (internal['addr'], internal['netmask']))
        
    @defer.inlineCallbacks
    def initExternalIp(self):
        """
        Tries to determine the public IP using an external webservice.
        """
        for service in self._externalIpServices:
            try:
                response = yield threads.deferToThread(requests.get, service, timeout=1)
                if response.status_code != 200:
                    continue
                address = netaddr.IPAddress(response.text)
                
                if address:
                    self._externalAddress = address
                    log.msg('Successfully discovered public IP  %s using %s' % (self._externalAddress, service))
                    break
            except requests.exceptions.RequestException as e:
                log.msg('Error retrieving IP from %s. %s' % (service, e))
        else:
            raise Exception("Could not determine external IP.")
        
        
    def registerSlave(self, slaveName, slaveIp=None, internal=None, external=None):
        """
        Registers a slave to the port mapping.
        If the slave is already registered, internal information is updated.
        This can happen, depending on who comes first: slave-recognition from the UPNP-device
        or the slave registering to the master.
        """
        
        if slaveName in  self._slaveMapping:
            slave = self._slaveMapping[slaveName]
            
            if slaveIp:
                slave.ip = slaveIp
            
            if internal:
                slave.internal = internal
            
            if external:
                slave.external = external
            
        else:
            self._slaveMapping[slaveName] = _SlaveMapping(name=slaveName,
                                              ip=slaveIp,
                                              internal=internal,
                                              external=external)
            
        # if slave is not externally mapped
        if not self._slaveMapping[slaveName].external:
            # queue the slaveName so the external mapper will try to map 
            # it when ready.
            self._newSlaves.put(slaveName)
            
            # and schedule the handler immediately
            if self._initialized:
                self._reactor.callLater(0, self._handleNewSlaves)
            
    def _takeNewSlavePort(self):
        return self._slavePorts.pop()
    
    def getSystemAddress(self, client):
        """
        Returns the IP of the system (the local system) depending on the request location (internal or external).
        """
        
         
        if not self._internalAddress:
            raise RuntimeError("Cannot return system address when internal address is unknown")
        
        if not self._externalAddress:
            return str(self._internalAddress.ip)
        
        clientIp = netaddr.IPNetwork('%s/%s' % (client, self._internalAddress.netmask))
        if clientIp.network == self._internalAddress.network:
            return str(self._internalAddress.ip)
        return str(self._externalAddress)
    
    def getMappedSlave(self, slaveName, client):
        """
        Returns the IP and port of requested slave depending on where the request
        came from. 
        """
        
        if slaveName not in self._slaveMapping:
            raise AttributeError('Slave %s seems unknown to the system' % slaveName)
        
        slave = self._slaveMapping[slaveName]
        
        # not properly initialized, just return the internal and IP and port.
        if not self._externalAddress or not self._internalAddress:
            return slave.getInternal()

        clientAddress = netaddr.IPAddress(client)
        clientIp = netaddr.IPNetwork('%s/%s' % (client, self._internalAddress.netmask))
        
        # if the slave is in our network
        if clientAddress.is_loopback() or clientIp.network == self._internalAddress.network:
            return slave.getInternal()
        else:
            if not slave.external:
                raise RuntimeError('Slave %s not externally mapped (yet).' % slaveName)
            return '%s:%s' % (self._externalAddress, slave.external)
        
    def _isSlaveMapped(self, slaveName):
        assert slaveName in self._slaveMapping
        return bool(self._slaveMapping[slaveName].external)
        
    def getSlaveMapping(self):
        return self._slaveMapping

    @defer.inlineCallbacks
    def _handleNewSlaves(self):
        if self._handlingNewSlaves:
            return
        
        self._handlingNewSlaves = True
        yield self._handleNewSlavesThreaded()
        self._handlingNewSlaves = False
            
    @utils.runAsDeferredThread
    def _handleNewSlavesThreaded(self):
        
        upnpDevice = self._createUpnpDevice()
        while True:
            try: 
                slaveName = self._newSlaves.get_nowait()
                
                # do not attempt to remap an already mapped slave.
                if utils.blockingCallInReactor(self._isSlaveMapped, slaveName):
                    continue
                
                
                slave = utils.blockingCallInReactor(self._slaveMapping.get, slaveName)
                externalPort = utils.blockingCallInReactor(self._takeNewSlavePort)
                try:
                    if not upnpDevice:
                        raise Exception('no upnp device')
                    self._createPortmappingEntry(upnpDevice,
                                                 slaveName,
                                                 slave.ip,
                                                 slave.internal,
                                                 externalPort)
                    
                    # set the new attribute
                    utils.blockingCallInReactor(lambda: setattr(slave, 'external', externalPort))
                
                except Exception as e:
                    log.msg('Error trying to map slave ports (%s).' % e)
                
            except Queue.Empty:
                break
    @utils.runAsDeferredThread
    def readExistingSlaves(self):
        """
        Reads the current UPNP-Configuration and registers 
        all slaves (where the name starts with slave) in the
        environment.
        """
        
        def _removeFromSlavePorts(port):
            if port in self._slavePorts:
                self._slavePorts.remove(port)
        
        upnpDevice = self._createUpnpDevice()
        if upnpDevice:
            for i in range(500):
                mapping = upnpDevice.getgenericportmapping(i)
                
                # we assume mapping format:
                # (<external-port>, <protocol>, (<ip>, <internal-port>) , <name>, ...)  
                if mapping is None:
                    break
                name = mapping[3]
                if name.startswith('slave'):
                    port = int(mapping[0])
                    
                    # remove the port from the list of available ports
                    utils.blockingCallInReactor(_removeFromSlavePorts, port)
                    
                    # finally, register the slave
                    utils.blockingCallInReactor(self.registerSlave, slaveName=name, external=port)
        
        # all done, now check for new slaves that may have arrived in the mean time
        self._initialized = True
        self._reactor.callLater(0, self._handleNewSlaves)
        
    def _createPortmappingEntry(self, upnpDevice, slaveName, slaveIp, internalPort, externalPort):
        """
        Creates a port mapping entry for passed slave with IP and port.
        
        @return: (slaveName, port)
        """
        upnpDevice.addportmapping(int(externalPort),  # external port
                                  'TCP',  # protocol
                                  slaveIp,  # IP
                                  int(internalPort),  # internal port 
                                  slaveName,  # name
                                  '')  # lease time

    def _createUpnpDevice(self):
        """
        Helper method executing the boiler plate to get the upnp device.
        """
        upnpDevice = miniupnpc.UPnP()
        devicesDiscovered = upnpDevice.discover()
        if devicesDiscovered != 1:
            log.msg('Expect exactly one router to be discovered by UPnP, got %d' % devicesDiscovered)
            return None
        
        # select the igd-device
        upnpDevice.selectigd()
        
        return upnpDevice
        
    def getInternalAddress(self):
        return self._internalAddress

    def getExternalAddress(self):
        return self._externalAddress
