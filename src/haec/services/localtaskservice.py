'''
Local tasks used by tasks in the graphdatabase.
It is separated here to avoid import dependency cycles.
'''
import pythonioc
from haec.services import jobscheduler, topologyservice, statisticservice
from twisted.internet import defer
from haec import utils, services
from haec import config;cfg = pythonioc.Inject(config.Config)
from twisted.python import log
from haec.optimization import optmodel

class LocalTaskService(object):
    """
    Service class that allows to execute local tasks.
    """
    videoDao = pythonioc.Inject('videoDao')
    slaveDao = pythonioc.Inject('slaveDao')
    topologyService = pythonioc.Inject(topologyservice.TopologyService)
    reactor = pythonioc.Inject('reactor')
    slaveConnService = pythonioc.Inject('slaveConnService')
    sensorstore = pythonioc.Inject('sensorStore')
    timeProvider = pythonioc.Inject(services.TimeProvider)
    statisticService = pythonioc.Inject(statisticservice.StatisticService)
    activityService = pythonioc.Inject('activityService')
    optModel = pythonioc.Inject(optmodel.OptModelProvider)
    def __init__(self):
        self._localTaskData = {}
    
    def hasTask(self, taskName):
        return hasattr(self, taskName)
    
    def getTaskForName(self, taskName):
        assert self.hasTask(taskName), "Unknown Task %s" % taskName
        return getattr(self, taskName)
    
    def executeTask(self, taskName, *args, **kwargs):
        assert self.hasTask(taskName)
        return getattr(LocalTaskService, taskName)(self, *args, **kwargs)
    
    def addVideoToSlave(self, slaveName, videoId):
        return self.videoDao.addVideoToSlave(videoId, slaveName)
    
    def removeVideoFromSlave(self, slaveName, videoId):
        return self.videoDao.removeVideoFromSlave(videoId, slaveName)
        
    @defer.inlineCallbacks
    def turnOnSlave(self, slaveName):
        if self.slaveConnService.isConnectedSlave(slaveName):
            # all done.
            return
        yield self.slaveDao.setSlaveStatus(slaveName, 'boot')
        yield self.topologyService.activateNode(slaveName)
        
        start = self.timeProvider.now()
        abortTime = start + cfg.vals.opt_max_boottime
        while True:
            # check if slave is online
            # if not, schedule new check
            yield utils.deferredSleep(self.reactor, 1)
            if self.slaveConnService.isConnectedSlave(slaveName):
                #
                # register the new boot time in the statistic service
                self.statisticService.setSlaveBootTime(slaveName, self.timeProvider.now() - start)
                
                # 
                # make the optmodel update itself.
                #
                self.optModel.update()
                break
            if self.timeProvider.now() > abortTime:
                # set slave status to failed and fail task.
                self.slaveDao.setSlaveStatus(slaveName, 'failure')
                raise Exception('Slave %s did not show up' % slaveName)
        
    def turnOffSlave(self, slaveName):
        return self.topologyService.deactivateNode(slaveName)
        
    @defer.inlineCallbacks
    def shutdownSlave(self, slaveName):
        from haec.protomixins import psoptimizer
        start = self.timeProvider.now()
        
        abortTime = start + self.optModel.model.shutdownTime
        
        if not self.slaveConnService.isConnectedSlave(slaveName):
            yield self.topologyService.deactivateNode(slaveName)
            return
            
        slave = self.slaveConnService.getConnectedSlaveByName(slaveName)
        yield self.slaveDao.setSlaveStatus(slaveName, 'shutdown')
        slave.sendObject(psoptimizer.PsoDoShutdown())
        while True:
            # check if slave is online
            # if not, schedule new check
            yield utils.deferredSleep(self.reactor, 1)
            
            # if the slave became idle again, let's cancel the job
            if not self.activityService.isSlaveIdle(slaveName):
                log.msg('Slave %s became busy again. Shutdown cancelled.' % slaveName)
                self.slaveDao.setSlaveStatus(slaveName, 'online')
                raise jobscheduler.JobCancelled()
            
            # if not connected anymore, do further check
            if not self.slaveConnService.isConnectedSlave(slaveName):
                
                # does it still consume power?
                sensors = self.sensorstore.getRecentPowerValues()
                if slaveName not in sensors:
                    log.msg("Warning: No sensor values, we cannot check if the slave is still on.")
                    continue
                
                # if consuming less than 20mA, let's turn it off.
                if sensors[slaveName]['current'] < 20:
                    yield self.topologyService.deactivateNode(slaveName)
                    break
                else:
                    #log.msg("Slave disconnected but still consuming energy")
                    continue
            if self.timeProvider.now() > abortTime:
                # set slave status to failed and fail task.
                # turn off the node anyway
                yield self.topologyService.deactivateNode(slaveName)
                raise Exception('Slave %s did not shutdown in time. Killed it.' % slaveName)
                

    @defer.inlineCallbacks
    def resurrectSlave(self, slaveName):
        self.turnOffSlave(slaveName)
        yield utils.deferredSleep(self.reactor, 5.0)
        yield self.turnOnSlave(slaveName)
        
        
