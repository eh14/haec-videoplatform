'''

@author: eh14
'''
from twisted.internet import defer
import pythonioc
from twisted.python import log
from haec import utils
import numpy as np
import collections
import random
import time
from haec.services import profileservice
import math


class Sampler(object):
    
    videoDao = pythonioc.Inject('videoDao')
    
    def __init__(self, numSamples):
        self._numSamples = numSamples
        
        self._samples = []
        self._videoNames = []
        
    @defer.inlineCallbacks
    def _refresh(self):        
        videoNames = yield self.videoDao.getVideoNames()
        
        # if the samples have been refreshed by another handler already,
        # don't do it again.
        if len(self._samples):
            print "double-refresh. Will skip this one"
            return
        
        if not len(videoNames):
            raise RuntimeError("No Videos registered. Cannot generate random")
        
        
        samples = self._createSamples(self._numSamples)
        # normalize to indexes
        minSample = min(samples)
        if minSample < 0:
            offset = abs(minSample)
            samples = [offset + s for s in samples]
            
        maxSample = max(samples)
        numVids = float(len(videoNames) - 1)
        self._samples = [int(round((s / maxSample) * numVids)) for s in samples]
        self._videoNames = videoNames
     
    @defer.inlineCallbacks
    def getNext(self):
        """
        This method returns a deferred.
        """
        if len(self._samples) == 0:
            # no samples, try to refresh first.
            yield self._refresh()
            
        assert len(self._videoNames)
        # get random.
        vidIndex = self._samples.pop()
        defer.returnValue(self._videoNames[vidIndex])
        
    def _createSamples(self):
        raise NotImplementedError('abstract...')
    
class GumbelSampler(Sampler):
    name = 'Gumbel'    
    def _createSamples(self, numSamples):
        return [np.random.gumbel(0, 1) for _x in range(numSamples)]
            
class NormSampler(Sampler):
    name = 'Normal'
    def _createSamples(self, numSamples):
        return np.random.normal(0, 1, numSamples)
    
    
class MasterVideoService(object):
    
    videoDao = pythonioc.Inject('videoDao')
    reactor = pythonioc.Inject('reactor')
    db = pythonioc.Inject('dbprovider')
    profileService = pythonioc.Inject(profileservice.ProfileService)
    
    NUM_SAMPLES = 1000
    
    timeProvider = pythonioc.Inject('timeProvider')
    
    rand = random.Random(time.time())
    
    def __init__(self):
        self._videoDataQueue = []
        
        self._videoNames = []
        
        self._tags = None
        self._userIds = None
        
        self._sampler = NormSampler(self.NUM_SAMPLES)
        
    def postInit(self):
        self.reactor.callLater(0, self.videoDao.cleanOrphanVideos)
        
    def addVideoData(self, slaveName, videoInstance):
        self._videoDataQueue.append((slaveName, videoInstance))
        
    @utils.runAsDeferredThread
    def addBatchedVideo(self):
        if len(self._videoDataQueue) == 0:
            return
        log.msg('Adding %d batched videos' % len(self._videoDataQueue))
        
        vidsToAdd = self._videoDataQueue
        self._videoDataQueue = []
        
        # group by slave
        grouped = collections.defaultdict(list)
        for slaveName, video in vidsToAdd:
            grouped[slaveName].append(video)
             
        outVids = []
        for slaveName, vids in grouped.iteritems(): 
            out = self.videoDao.addVideosToSlave(slaveName, vids)
            outVids.extend(out)
            
        return outVids
            
    def getRandomVideoName(self):
        # assert utils.isReactorThread()
        return self._sampler.getNext()
        
