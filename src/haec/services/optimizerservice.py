'''

@author: eh14
'''
from haec.optimization import strategies, optmodel, snapshotcreator
from haec.protomixins import psoptimizer
import pythonioc
from haec.services import slaveconnservice, eventservice, mastervideoservice
from twisted.python import log
from twisted.internet import defer

class OptimizerService(object):
    
    slaveConnService = pythonioc.Inject(slaveconnservice.SlaveConnService)
    eventService = pythonioc.Inject(eventservice.EventService)
    
    _optmodel = pythonioc.Inject(optmodel.OptModelProvider)
    events = pythonioc.Inject(eventservice.EventService)
    _snapshotCreator = pythonioc.Inject(snapshotcreator.SystemSnapshotCreator)
    masterVideoService = pythonioc.Inject(mastervideoservice.MasterVideoService)
    def __init__(self):
        stratImpls = [strategies.NoStrategy(),
                      strategies.AllOnStrategy(),
                      strategies.AllOnBalancedStrategy(),
                      strategies.LoadOnlyStrategy(),
                      strategies.PopularityStrategy()]
        
        self._stratNames = [s.getName() for s in stratImpls]
        self._strategies = {s.getName():s for s in stratImpls}
        
        self._currentStrategy = self._strategies[strategies.NoStrategy.name]
        
        self._initialClassRatio = self._optmodel.model.classBVideoFraction
        
        
        #
        # configures if the class adaptation is enabled.
        self._enableClassAdaptation = True
        
    def postInit(self):
        self._currentStrategy.deploy()
        
        self.eventService.subscribeEvent('slave-online', self._sendUpcomingSlavesPsoMode)
        
    def getStrategyNames(self):
        return list(self._stratNames)
    
    @defer.inlineCallbacks
    def activateStrategy(self, strategyName):
        log.msg('Changing strategy to %s' % strategyName)
        if strategyName == self._currentStrategy.getName():
            log.msg('Already using %s, not changing' % strategyName)
            return
        
        yield self._currentStrategy.undeploy()
        self._currentStrategy = self._strategies[strategyName]
        self._sendConnectedSlavesPsoMode()
        yield self._currentStrategy.deploy()
        
        self.eventService.triggerEvent('strategy-activated', strategyName)
        
    def _sendUpcomingSlavesPsoMode(self, slaveName):    
        mode = self._currentStrategy.getPsoMode()
        slave = self.slaveConnService.getConnectedSlaveByName(slaveName)
        psoptimizer.MasterPsoMixin.sendPsoSetMode(slave, mode)
        
    def _sendConnectedSlavesPsoMode(self):
        mode = self._currentStrategy.getPsoMode()
        for slave in self.slaveConnService.getAllConnectedSlaves():
            psoptimizer.MasterPsoMixin.sendPsoSetMode(slave, mode)
            
    def optimize(self):
        self.eventService.triggerEvent('optimizer-run', self._currentStrategy.getName())
        self._currentStrategy.doOptimize()
        
    def getCurrentStrategyName(self):
        return self._currentStrategy.getName()
