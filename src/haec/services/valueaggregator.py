import collections
import itertools
from exceptions import NotImplementedError

class ValueBuffer(object):
    def __init__(self, default=0, maxMisses=10):
        self._default = default
        self._currentValue = self._default
        self._lastValue = self._default
        self._missed = 0
        self._maxMisses = maxMisses
        
    def setValue(self, value):
        self._currentValue = value
    
    def getLast(self):
        """
        Addds the current value to the list of values.
        """
        if self._currentValue:
            self._missed = 0
            self._lastValue = self._currentValue
            self._currentValue = None
            return self._lastValue 
        elif self._currentValue is None:
            self._missed += 1
            if self._missed > self._maxMisses:
                return self._default
            else:
                return self._lastValue

class ValueAggregator(object):
    """
    Aggregates the values of all sources when flushing
    using an abstract aggregate-method. 
    Each flush stores the value along with a time in a tuple <time, value>.
    """
    def __init__(self, size,
                 sources,
                 sourceNames=None,
                 default=0,
                 valueExtractor=lambda x:x,
                 valueFilter=lambda x:True):
        if len(sources) < 1:
            raise AttributeError("Need at least on source for the aggregator")
        self._sources = list(sources)
        self._sourceNames = sourceNames
        self._valueExtractor = valueExtractor
        self._valueFilter = valueFilter
        self._values = collections.deque(maxlen=size)
        self._default = default
        
    def flush(self, now):
        values = [s.getLast() for s in self._sources]
        
        valuesToFlush = [self._valueExtractor(v) for v in  values if self._valueFilter(v)]
        self._values.append((now, self.aggregate(valuesToFlush, self._sourceNames)))
    
    def aggregate(self, values, sourceNames=None):
        raise NotImplementedError("Aggregate must be implemented by subclasses")
    
    def getLast(self):
        if len(self._values) == 0:
            return self._default
        else:
            return self._values[-1][1]
    
    def getAll(self):
        return list(self._values)
    
    def __len__(self):
        return len(self._values)
    
class AvgAggregator(ValueAggregator):
    def aggregate(self, values, sourceNames=None):
        values = map(float, values)
        if len(values) == 0:
            return 0
        return sum(values) / len(values)
    

class SumAggregator(ValueAggregator):
    def aggregate(self, values, sourceNames=None):
        return sum(values)
    
class MinMaxAvgAggregator(ValueAggregator):
    def aggregate(self, values, *args):
        values = map(float, values)
        if len(values) == 0:
            return (0, 0, 0)
        return (min(values), sum(values) / len(values), max(values))
    
class HashCollectingAggregator(ValueAggregator):
    
    def aggregate(self, values, sourceNames):
        assert len(values) == len(sourceNames), "source names do not match the list of values." 
        return {v[0]:v[1] for v in itertools.izip(sourceNames, values)}
