'''

@author: eh14
'''
import collections
from twisted.internet import defer
import weakref

class ActivityTracker(object):
    """
    Slave's side of a service to track activities and send updates to the server.
    """
    
    def __init__(self):
        self._currentStreams = collections.defaultdict(weakref.WeakSet)
        
        self._activityCallback = None
        
        self._blockedVideos = collections.defaultdict(defer.Deferred)
        
        self._terminating = False
        
    def clear(self):
        self._currentStreams.clear()
        self._terminating = False
        self._blockedVideos.clear()
        
    def setActivityCallback(self, cb):
        self._activityCallback = cb
        
    def blockVideo(self, fileName):
        """
        Requests to block a video. Returns a deferred which fires
        when the video becomes unused.
        """
        
        # if the video is not streamed, call the callback without leaving 
        # this loop iteration
        if fileName not in self._currentStreams or len(self._currentStreams[fileName]) == 0:
            return defer.succeed(fileName)
        else:
            return self._blockedVideos[fileName]
            
    def isVideoBlocked(self, fileName):
        return fileName in self._blockedVideos
        
    def streamStarted(self, fileName, request):
        self._currentStreams[fileName].add(request)
        
        # from 0 to 1
        if self._activityCallback and self.sumStreams() == 1:
            self._activityCallback()
        
    def streamFinished(self, fileName, request):
        self._currentStreams[fileName].discard(request)
        
        if len(self._currentStreams[fileName]) == 0 and fileName in self._blockedVideos:
            self._blockedVideos.pop(fileName).callback(fileName)
                

        if self._activityCallback and self.sumStreams() == 0:
            self._activityCallback()
            
    def sumStreams(self):
        return sum([len(v) for v in self._currentStreams.itervalues()])
        
    def cleanZero(self):
        for fileName in self._currentStreams.keys():
            if len(self._currentStreams[fileName]) == 0:
                # if blocked, unblock it.
                if fileName in self._blockedVideos:
                    self._blockedVideos.pop(fileName).callback(fileName)
                    
                del self._currentStreams[fileName]
        
        # trigger the callback even if it didn't change,
        # because we cannot know!        
        self._activityCallback()
            
    def setTerminating(self):
        self._terminating = True
        
    def isTerminating(self):
        return self._terminating
