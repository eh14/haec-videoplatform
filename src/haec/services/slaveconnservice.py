'''

@author: eh14
'''
from twisted.internet import defer
import pythonioc
from twisted.python import log
from haec.services import localtaskservice


class SlaveConnService(object):
    """
    Manages active connections to slaves etc.
    """
    
    slaveDao = pythonioc.Inject('slaveDao')
    taskDao = pythonioc.Inject('taskDao')
    
    def __init__(self):
        self.connectedSlaves = set()
        
    def slaveConnected(self, protocol):
        self.connectedSlaves.add(protocol)
        
    def slaveDisconnected(self, protocol):
        self.connectedSlaves.remove(protocol)
        
    def getConnectedSlaveByName(self, name):
        for slave in self.connectedSlaves:
            if slave.getSlaveName() == name:
                return slave
        else:
            raise Exception('Cannot find slave with name %s' % name)
        
    def isConnectedSlave(self, name):
        for slave in self.connectedSlaves:
            if slave.getSlaveName() == name:
                return True
        return False
        
    def getAllConnectedSlaves(self):
        return list(self.connectedSlaves)
    
    
    
    @defer.inlineCallbacks
    def resurrectSlaves(self):
        slaves = yield self.slaveDao.getFailedSlaves()
        for slave in slaves:
            log.msg('Trying to resourrect slave ' + slave)
            yield self.taskDao.addLocalActionTask('Resurrecting slave ' + slave,
                                             localtaskservice.LocalTaskService.resurrectSlave,
                                             args=[slave])
