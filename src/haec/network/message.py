"""
Implements a convenience base class for messages to initialize them
without specifying a constructor.

Use as follows:

class MyMessage(Message):
    someValue = None

Now a message can be instantiated with:

msg = MyMessage(someValue="someValue")

Passing non-defined variables in the constructor raises an AttributeError
"""
import inspect
import itertools

class Message(object):
    def __init__(self, **kwargs):
        for (key, value) in kwargs.iteritems():
            if not hasattr(self, key):
                raise AttributeError("unknown key " + key)
            setattr(self, key, value)
            
    def __repr__(self):
        
        def noMethodsPredicate(member):
            return (not inspect.isfunction(member) and 
                    not inspect.ismethod(member)
                    )
        
        strAttributes = []
        for (attrName, attr) in inspect.getmembers(self, noMethodsPredicate):
            if '__' not in attrName:
                strAttributes.append(u"%s: %s" % (attrName, unicode(attr)))
            
        attrString = (', '.join(strAttributes))[:50] + '...'
                             
        return "<%s: %s>" % (self.__class__.__name__, attrString)
                             
    def __eq__(self, other):
        """
        Compares this message to any other object by simply inspecting
        all public members, comparing their values.
        If the values are named differently or the values differ,
        the test fails.
        """
        for left, right in itertools.izip_longest(inspect.getmembers(self), inspect.getmembers(other)):
            if not (left or right):
                return False
            if inspect.isroutine(left) or inspect.isroutine(right):
                continue
            if left[0] != right[0]:
                return False
            if left[0].startswith('_') or right[0].startswith('_'):
                continue
                
            if left[1] != right[1]:
                return False
            
        return True
    
    def __ne__(self, other):
        return not self == other
    
class FlexMessage(Message):
    """
    Message that can simply hold any attributes that are passed to
    the constructor.
    """
    
    def __init__(self, **data):
        
        for key in data.iterkeys():
            setattr(self, key, None)
            
        Message.__init__(self, **data)
        