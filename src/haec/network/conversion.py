import jsonpickle

#
# Handles numpy types in jsonpickle. 
# Idea taken from 
# http://stackoverflow.com/a/24464260/3774566
#
class NumpyIntHandler(jsonpickle.handlers.BaseHandler):
    """
    Automatic conversion of numpy int  to python ints
    Required for jsonpickle to work correctly
    """
    def flatten(self, obj, data):
        """
        Converts and rounds a Numpy.float* to Python float
        """
        return int(obj)



class NumpyFloatHandler(jsonpickle.handlers.BaseHandler):
    """
    Automatic conversion of numpy float  to python floats
    Required for jsonpickle to work correctly
    """
    def flatten(self, obj, data):
        """
        Converts and rounds a Numpy.float* to Python float
        """
        return round(obj,6)
