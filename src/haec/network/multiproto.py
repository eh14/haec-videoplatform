import jsonprotocol
import inspect
from _collections import defaultdict
from twisted.internet import defer

class MultiProtocolMixinBase(object):
    
    __factoryGetter = None
   
    def setFactoryGetter(self, factoryGetter):
        self.__factoryGetter = factoryGetter
    
    @property
    def factory(self):
        if not self.__factoryGetter:
            return None
        return self.__factoryGetter()
    
    @factory.setter
    def factory(self, value):
        self.__factoryGetter = lambda: value
        
        
    def sendObject(self, msg):
        raise AssertionError('This method should have been monkey-patched when adding the protocol mixin to the multiprotocol')
        

def MultiProtocolBuilder(*protocolMixinClasses):
    def create(_ignored):
        return MultiProtocol(*[mixin() for mixin in protocolMixinClasses])

    return create


class MultiProtocol(jsonprotocol.JsonProtocol):
    
    def __init__(self, *protoMixins):
        self.__connectionMadeListeners = []
        self.__connectionLostListeners = []
        self.__handlers = defaultdict(list)
        self.__rawObservers = []
        
        self._mixins = protoMixins
        
        for proto in protoMixins:
            self._addProtocolMixin(proto)
            
    def _addProtocolMixin(self, mixin):
        
        def handlerPred(member):
            return (inspect.ismethod(member) and
                    jsonprotocol.JsonProtocol.isHandlerFunc(member.__name__))
            
        for (name, value) in inspect.getmembers(mixin, handlerPred):
            typeName = jsonprotocol.JsonProtocol.getTypeForHandler(name)
            self.__handlers[typeName].append(value)
            
        if hasattr(mixin, 'on_Raw'):
            self.__rawObservers.append(mixin.on_Raw)
            
        if hasattr(mixin, 'connectionMade'):
            self.__connectionMadeListeners.append(mixin.connectionMade)
            
        if hasattr(mixin, 'connectionLost'):
            self.__connectionLostListeners.append(mixin.connectionLost)
            
        # replace some methods of the MultiProtocolBuilder
        
        def sendObjectProxy(obj):
            self.sendObject(obj)
            
        setattr(mixin, 'sendObject', sendObjectProxy)
        
        mixin.setFactoryGetter(lambda: self.factory)
        
            
    def connectionMade(self):
        for mixin in self._mixins:
            mixin.transport = self.transport
        map(lambda f: f(), self.__connectionMadeListeners)
        
    def connectionLost(self, reason):
        map(lambda f: f(reason), self.__connectionLostListeners)
        
    @defer.inlineCallbacks
    def receiveRaw(self, message):
        typeName = message.__class__.__name__
        
        for observer in self.__rawObservers:
            yield observer(message)
        
        for handler in self.__handlers[typeName]:
            yield handler(message)
