from twisted.python import log

from twisted.protocols.basic import Int32StringReceiver, IntNStringReceiver
import jsonpickle
import message
import uuid
import re
import time

# import conversion
# import numpy as np
# jsonpickle.handlers.registry.register(np.integer, conversion.NumpyIntHandler)
# jsonpickle.handlers.registry.register(np.int64, conversion.NumpyIntHandler)
# jsonpickle.handlers.registry.register(np.int32, conversion.NumpyIntHandler)
# jsonpickle.handlers.registry.register(np.float, conversion.NumpyFloatHandler)
# jsonpickle.handlers.registry.register(np.float64, conversion.NumpyFloatHandler)
# jsonpickle.handlers.registry.register(np.float32, conversion.NumpyFloatHandler)

class _Shattered_Message__(message.Message):
    pileId = None
    sequenceId = None
    totalChunks = None
    chunkData = None
    
    
class Pile(object):
    
    @staticmethod
    def createPileID():
        return str(uuid.uuid1())
    
    def __init__(self, pileId, totalChunks):
        self._pileId = pileId
        self._totalChunks = totalChunks
        
        self._chunks = [None] * self._totalChunks
        
    def addChunk(self, chunk):
        if self.totalChunks != chunk.totalChunks:
            log.msg('Inconsistent number of chunks for shattered message. Not good.')
        if chunk.sequenceId >= self.totalChunks:
            log.msg('Invalid sequence number. Message dropped')
            
        self._chunks[chunk.sequenceId] = chunk
        
    def isPileComplete(self):
        return None not in self._chunks
    
    def getAssembledChunks(self):
        assert self.isPileComplete()
        return ''.join([chunk.chunkData for chunk in self._chunks])
            
    @property
    def pileId(self):
        return self._pileId
    
    @property
    def totalChunks(self):
        return self._totalChunks
    

class JsonProtocol(Int32StringReceiver):
    
   
    _piles = {}
    
    _maxLength = int(float(IntNStringReceiver.MAX_LENGTH) / 2.0)
    
    # make sure the computation does not eliminate length value
    # and the new max length is not accidentally larger than the subclasses
    # assert self._maxLength > 0 and self._maxLength <= self.MAX_LENGTH
    
    # todo: schedule job that removes old incomplete piles (save start time of pile in the pile)
    # todo: limit number of chunks/size of chunks so we don't run out of memory.
    
    HANDLER_PATTERN = re.compile('on_([_a-zA-Z0-9]+)')
    
    @classmethod
    def isHandlerFunc(cls, funcName):
        m = JsonProtocol.HANDLER_PATTERN.match(funcName)
        return m is not None
    
    @classmethod
    def getHandlerForType(cls, typeName):
        handler = 'on_' + typeName
        assert cls.isHandlerFunc(handler), "invalid handler func" + handler
        
        return handler
    
    @classmethod
    def getTypeForHandler(cls, funcName):
        m = JsonProtocol.HANDLER_PATTERN.match(funcName)
        assert m, "invalid handler name"
        return m.group(1)
        
    
    def stringReceived(self, string):
        try:
            recvObj = jsonpickle.decode(string)
        except AttributeError as e:
            log.msg('Invalid message. Cannot decode json (%s)' % string)
            return
        if not isinstance(recvObj, message.Message):
            log.msg("can only receive messages of type message.Message, was %s\n%s" % (recvObj.__class__, str(recvObj)))
            return
        typeName = recvObj.__class__.__name__
        start = time.time()
        try:
            funcName = self.getHandlerForType(typeName)
            if hasattr(self, funcName):
                getattr(self, funcName)(recvObj)
            else:
                self.receiveRaw(recvObj)
        except Exception as e:
            log.msg('Exception while handling received message %s (%s)' % (str(e), str(recvObj)))
            log.err()
        finally:
            handleTime = round((time.time() - start) * 1000, 2)
            if handleTime > 50:
                log.msg(u"Received: %s. Handling took %f ms" % (unicode(recvObj), handleTime))
            
    def receiveRaw(self, raw):
        """
        Called when the message received is not handled by any
        'typed' receiver function.
        """
        log.msg('Received message %s is unhandled.' % str(raw))
        
    def sendObject(self, messageObj):
        assert isinstance(messageObj, message.Message), "can only send messages of type message.Message, was %s" % messageObj.__class__
        pickled = jsonpickle.encode(messageObj)
        if len(pickled) > self._maxLength:
            self._sendShattered(pickled)
        else:
            self.sendString(pickled)
            
    def _sendShattered(self, stringMessage):
        pileId = Pile.createPileID()
        sequenceId = 0
        chunkSizes = self._maxLength / 2
        subIndices = [(i, i + chunkSizes) for i in range(0, len(stringMessage), chunkSizes)]
        totalChunks = len(subIndices)
        for (subStart, subEnd) in subIndices:
            self.sendObject(_Shattered_Message__(pileId=pileId,
                                                 sequenceId=sequenceId,
                                                 totalChunks=totalChunks,
                                                 chunkData=stringMessage[subStart:subEnd]))
            sequenceId += 1
            
    def on__Shattered_Message__(self, message):
        if message.pileId not in self._piles:
            pile = Pile(message.pileId, message.totalChunks)
            self._piles[message.pileId] = pile
        else:
            pile = self._piles[message.pileId]

        pile.addChunk(message)
        
        if pile.isPileComplete():
            self.stringReceived(pile.getAssembledChunks())
            del self._piles[message.pileId]
                    
    def lengthLimitExceeded(self, length):
        raise Exception("Message size exceeded, was (%d)" % length)
        

