from twisted.internet.protocol import Factory
from twisted.python import log
from twisted.internet import reactor

from haec import kplane, services
from haec.network import multiproto
from haec.protomixins import topology, sensors, \
    taskrunner, video, activity, psoptimizer

from twisted.manhole.telnet import ShellFactory

from frontend import server as frontend_server
from haec.optimization import snapshotcreator, optmodel
import pythonioc
from haec.services import looperservice, taskservice, \
    cambrionixservice, eventservice, activityservice, statisticservice, \
    localtaskservice, slaveconnservice, portmapperservice, videoservice, \
    sensorservice, optimizerservice, mastervideoservice, profileservice, \
    topologyservice, demoservice
from haec.frontend import siteutils, events, migrationevents
from haec import config
from txsockjs.factory import SockJSFactory
from haec.kplane import sensorstore

cfg = pythonioc.Inject(config.Config)

class MasterFactory(Factory):
    
    protocol = multiproto.MultiProtocolBuilder(video.MasterVideoMixin,
                                               topology.MasterTopologyMixin,
                                               sensors.MasterSensorsMixin,
                                               taskrunner.MasterTaskRunnerMixin,
                                               psoptimizer.MasterPsoMixin,
                                               activity.MasterActivityMixin,
                                               )
    
    looperService = pythonioc.Inject(looperservice.LooperService)
    
    def startFactory(self):
        self.looperService.start()
        
    def stopFactory(self):
        self.looperService.stop()
        
        
def registerMasterServices():
    pythonioc.registerService(slaveconnservice.SlaveConnService)
    pythonioc.registerService(portmapperservice.PortMapperService)
    pythonioc.registerService(taskservice.TaskService)
    pythonioc.registerService(looperservice.LooperService)
    pythonioc.registerService(services.TimeProvider)
    pythonioc.registerService(statisticservice.StatisticService)
    pythonioc.registerService(snapshotcreator.SystemSnapshotCreator)
    pythonioc.registerService(cambrionixservice.CambrionixService)
    pythonioc.registerService(cambrionixservice.CambrionixConnector)
    pythonioc.registerService(eventservice.EventService)
    pythonioc.registerService(optmodel.OptModelProvider)
    pythonioc.registerService(mastervideoservice.MasterVideoService)
    pythonioc.registerService(sensorservice.MasterSensorService)
    pythonioc.registerService(localtaskservice.LocalTaskService)
    pythonioc.registerService(activityservice.ActivityService)
    pythonioc.registerService(optimizerservice.OptimizerService)
    pythonioc.registerService(profileservice.ProfileService)
    pythonioc.registerService(topologyservice.TopologyService)
    pythonioc.registerService(events.EventBroadcastBuffer)
    pythonioc.registerService(migrationevents.MigrationEventService)
    pythonioc.registerService(demoservice.DemoService)
    
    
def run():
    
    registerMasterServices()
    
        
    # register the reactor as reactor.
    pythonioc.registerServiceInstance(reactor, 'reactor')
    
    kplane.registerInGlobalRegistry(overwrite=False)
    
    masterFactory = MasterFactory()
        
    pythonioc.registerServiceInstance(masterFactory, 'master')
        
    reactor.listenTCP(cfg.vals.local_node_port,  # @UndefinedVariable
                      masterFactory)  
    
    serverThreadPool, serverInstance = frontend_server.createServer(reactor)
    reactor.listenTCP(cfg.vals.webserver_port,  # @UndefinedVariable
                      siteutils.LoggingSite(serverInstance))
    
    eventFactory = events.EventPushFactory()
    
    cfg.vals.websocket_port = cfg.vals.webserver_port + 1
    reactor.listenTCP(cfg.vals.websocket_port, SockJSFactory(eventFactory))  # @UndefinedVariable
    
    
    # clean the whole service registry so we get rid of all schedulers ets,
    # delayed calls etc.
    reactor.addSystemEventTrigger('before', 'shutdown', pythonioc.cleanServiceRegistry)  # @UndefinedVariable
    reactor.addSystemEventTrigger('before', 'shutdown', serverThreadPool.stop)  # @UndefinedVariable
    
    manholePort = reactor.listenTCP(0, ShellFactory())  # @UndefinedVariable
    log.msg('Started manhole on port %s' % str(manholePort.getHost().port))
    reactor.run()  # @UndefinedVariable
    
