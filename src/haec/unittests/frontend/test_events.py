'''

@author: eh14
'''
from twisted.trial import unittest
import pythonioc
from haec import config
from haec.frontend import events
from haec.unittests import testutils

cfg = pythonioc.Inject(config.Config)

class TestEventBroadcastBuffer(unittest.TestCase):
    timeProvider = pythonioc.Inject('timeProvider')
    def setUp(self):
        pythonioc.registerServiceInstance(testutils.FixedTimeProvider(), 'timeProvider', overwrite=True)
        
        cfg.init(sensor_storetime=1)
        self.buffer = events.EventBroadcastBuffer()
        
    def test_add(self):
        self.assertEquals(0, len(self.buffer._events))
        self.buffer.add({'time':self.timeProvider.now(), 'data':0})
        self.assertEquals(1, len(self.buffer._events))
        self.buffer.add({'time':self.timeProvider.now() + 0.5, 'data':1})
        
        # move forward in time a few seconds
        self.timeProvider.advance(3)
        self.buffer.add({'time':self.timeProvider.now(), 'data':3})
        # all old events should be removed
        self.assertEquals(1, len(self.buffer._events))
        self.assertEquals(3, self.buffer._events[0]['data'])
        
