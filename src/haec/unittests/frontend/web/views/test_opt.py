import pythonioc
import basetests
from twisted.internet import defer
from haec.kplane import domain
from haec.optimization import optmodel, snapshotcreator
from haec import config

class TestOptView(basetests.WebTest):
    
    vidDao = pythonioc.Inject('videoDao')
    slaveDao = pythonioc.Inject('slaveDao')
    hittacker = pythonioc.Inject('hitTracker')
    cfg = pythonioc.Inject(config.Config)
    snapshotcreator = pythonioc.Inject(snapshotcreator.SystemSnapshotCreator)
    
    def setUp(self):
        basetests.WebTest.setUp(self, 'opt')
        
        self.optmodel = optmodel.OptimizationModel()
        
    @defer.inlineCallbacks
    def test_getPopularityValues(self):
                               
        size = 10 * 1024 * 1024
        vid1Id = yield self.vidDao.addVideo('slaveA', domain.Video(name='vid1', duration=60, fsize=size))
        vid2Id = yield self.vidDao.addVideo('slaveA', domain.Video(name='vid2', duration=30, fsize=size))
        yield self.vidDao.addVideo('slaveA', domain.Video(name='vid3', duration=10, fsize=size))
        yield self.vidDao.addVideo('slaveB', domain.Video(name='vid3'))
        yield self.vidDao.addVideo('slaveC', domain.Video(name='vid3'))
        
        hitperiod = 180
        self.optmodel.hitratePeriod = 180
        
        for _i in range(hitperiod):
            self.hittacker.hit(vid1Id)
            self.hittacker.hit(vid2Id)
            
        snap = yield self.snapshotcreator.createSnapshot(self.optmodel)
        popVids, numUnpopularVids = self.view._getPopularityValues(snap)
        
        self.assertEquals(2, len(popVids))
        self.assertEquals(1, numUnpopularVids)