'''

@author: eh14
'''
from haec.unittests.kplane import basetests
import pythonioc
from haec import config
import os
import sys
from web.views import viewutils

class WebTest(basetests.BaseDbTest):
    """
    Sets up a testcase for testing the django views.
    Notable difference: a view name can be passed, which will be imported,
    after django has been initialized (would fail before)
    """
    timeout = 5
    
    cfg = pythonioc.Inject(config.Config)
    
    def setUp(self, modName=None, *args, **kwargs):
        basetests.BaseDbTest.setUp(self, *args, **kwargs)
        
        viewutils._reactorOverride.setValue(True)
        frontendPath = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..', '..', 'frontend')
        sys.path.append(frontendPath)
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", self.cfg.vals.django_settings) 
        
        # if the subclass requires a view-module, load that
        if modName:
            imported = __import__('haec.frontend.web.views', globals(), locals(), fromlist=[modName], level=-1)
            self.view = getattr(imported, modName)
            
