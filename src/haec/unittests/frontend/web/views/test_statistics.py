import time
import pythonioc
import basetests
from haec.protomixins import sensors

class TestStatistics(basetests.WebTest):
    
    sensorstore = pythonioc.Inject('sensorStore')
    def setUp(self):
        basetests.WebTest.setUp(self, 'statistics')
        
    def test_getClusterState(self):
        data = self.view._getClusterState()
        
