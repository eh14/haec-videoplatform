'''
Created on Jun 5, 2014

@author: eh14
'''
from twisted.trial import unittest
from haec.frontend.web.views import viewutils
from django.http.response import HttpResponseBadRequest
from haec.unittests import testutils
from haec.unittests.frontend.web import views
import basetests

def _errorHandler(e):
    raise AssertionError(e)

@viewutils.get_params('param', 'optParam', paramTyped=int, paramTyped2=float, optTypedParam=viewutils.boolConverter)  # , __errorHandler__=_errorHandler)
def testParams(request, param, paramTyped, paramTyped2, optParam=None, optTypedParam=None):
    return (param, paramTyped, paramTyped2, optParam, optTypedParam)

@viewutils.get_params('param', 'nonexisting')
def testMissingParams(request, param):
    return param

@viewutils.get_params('param')
def nestedfailureFunction(request, param):
    # this call fails, since we forgot to pass parameters
    calledFunction()

def calledFunction(param):
    return param

class RequestMock(object):
    def __init__(self, **values):
        self.GET = values
        self.method = 'GET'
        
class TestUtils(basetests.WebTest):
    def setUp(self):
        basetests.WebTest.setUp(self)
        
    def test_get_params_decorator(self):
        
        # no params, raises exception
        self.assertTrue(isinstance(testParams(RequestMock()), HttpResponseBadRequest))
        self.assertTrue(isinstance(testParams(RequestMock(param='hello')), HttpResponseBadRequest))
        
        self.assertEquals(('hello', 33412, 120.125, None, None), testParams(RequestMock(param='hello', paramTyped='33412', paramTyped2='120.125')))
        self.assertEquals(('hello', 33412, 120.125, 'something', False), testParams(RequestMock(param='hello',
                                                                                               paramTyped='33412',
                                                                                               paramTyped2='120.125',
                                                                                               optParam='something',
                                                                                               optTypedParam='false',
                                                                                               )))

        self.assertTrue(isinstance(testParams(RequestMock(param='value', nonexisting='errorvalue')), HttpResponseBadRequest))
        

    def test_boolConverter(self):
        self.assertTrue(viewutils.boolConverter("true"))
        self.assertTrue(viewutils.boolConverter("True"))
        self.assertTrue(viewutils.boolConverter("1"))
        self.assertTrue(viewutils.boolConverter("yes"))
        
        self.assertFalse(viewutils.boolConverter("False"))
        self.assertFalse(viewutils.boolConverter("false"))
        self.assertFalse(viewutils.boolConverter("0"))
        self.assertFalse(viewutils.boolConverter("no"))
        
        
    def test_get_params_decorator_nestedexception(self):
        self.assertRaises(TypeError, nestedfailureFunction, RequestMock(param='something'))

    