from django.core.urlresolvers import reverse
import pythonioc
from haec.kplane import domain
import basetests
from twisted.internet import reactor, defer
from haec.services import portmapperservice, profileservice
from haec.unittests import domaincreator

class TestVideo(basetests.WebTest):
    videoDao = pythonioc.Inject('videoDao')
    slaveDao = pythonioc.Inject('slaveDao')
    profileService = pythonioc.Inject(profileservice.ProfileService)
    
    portmapperService = pythonioc.Inject(portmapperservice.PortMapperService)
    
    def setUp(self):
        basetests.WebTest.setUp(self, 'video')
        #pythonioc.registerServiceInstance(reactor, 'reactor', overwrite=True)
        pythonioc.registerService(portmapperservice.PortMapperService)
    
    @defer.inlineCallbacks
    def test_findVideoVariantForProfile(self):
        
        yield self.videoDao.deleteAll()
        allProfs = yield self.profileService.getProfileList()
        someProf = allProfs[1]
        
        # create some videos
        yield self.videoDao.addVideo('slaveA', domain.Video(name='tiny_test', variant='var'))
        vid = domaincreator.createVideoForProfile('vid1', someProf, variant='1234')
        yield self.videoDao.addVideo('slaveB', vid)
        
        # load with any profile
        anyprofile = yield self.profileService.getProfileForNameOrId('anyprofile')
        variant = self.view._findVideoVariantForProfile('tiny_test', anyprofile)
        self.assertEquals('var', variant)
        
        variant = self.view._findVideoVariantForProfile('vid1', anyprofile)
        self.assertEquals('1234', variant)
        variant = self.view._findVideoVariantForProfile('vid1', someProf)
        self.assertEquals('1234', variant)
        
        # tiny_test is not available in that profile
        variant = self.view._findVideoVariantForProfile('tiny_test', someProf)
        self.assertEquals(None, variant)
