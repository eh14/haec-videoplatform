'''

@author: eh14
'''
from twisted.trial import unittest
from haec.unittests.protomixins import mixinhelpers
from haec.protomixins import psoptimizer
from haec.unittests import testutils
from haec import config
import pythonioc
from haec.unittests.services import servicemocks
import json
import os
from haec.unittests.kplane import basetests
from haec.kplane import domain
from twisted.internet import defer
from haec.services import activityservice

cfg = pythonioc.Inject(config.Config)

class TestSlavePsoMixin(unittest.TestCase, mixinhelpers.ProtocolMixinTestMixin):
    
    taskSched = pythonioc.Inject('TaskRunner')
    def setUp(self):
        testutils.setUpIocRegistry()
        self.setUpMixin(psoptimizer.SlavePsoMixin)
        
        pythonioc.registerServiceInstance(servicemocks.JobSchedulerMock(),
                                          'TaskRunner', overwrite=True)
        
    def test_on_PsoResetVideoData(self):
        
        testVidData = {'index':[1],
                      'columns':['size', 'fname', 'totalbyteseconds', 'popularity'],
                      'data':[(1, 'tiny_test.mp4', 150, 0)]
                      }
        self.proto.receiveRaw(psoptimizer.MasterPsoMixin.prepareVideoDataMessage(popularityThreshold=1.024,
                                                            videoData=json.dumps(testVidData)))
        self.assertEquals(1.024, self.mixin._popThreshold)
        self.assertEquals(1, self.mixin._videoData.getNumberOfVideos())
        
    def test_on_PsoResetSlaveData(self):
        
        self.assertEquals('B', self.mixin._class)
        self.assertEquals(0.6, self.mixin._speed)
        
        self.proto.receiveRaw(psoptimizer.MasterPsoMixin.prepareSlaveClassData(slaveData={cfg.vals.name:{'class':'A'}},
                                                            speed=0.3))
        self.assertEquals('A', self.mixin._class)
        self.assertEquals(0.3, self.mixin._speed)
        
    def test_optForClassPurity(self):
        """
        Runs the purity optimization function.
        First, video data and slave data is sent to the mixin that suggest one video
        to be migrated to another slave.
        The existence of that job is verified.
        
        Second, a pure state of videodata is sent to the mixin and the appropriate
        message back to the master is verified.
        """
        
        # #
        # Step 1 (--> non-pure data)
        # #
        videoData = {'index':[1, 2, 3],
                     'columns':['size', 'fname', 'totalbyteseconds', 'popularity'],
                     'data':[(1, 'tiny_test.mp4', 150, 0),
                             (2, 'tiny_test.mp4', 320, 0),
                             (3, 'tiny_test.mp4', 1, 2)]
                             }
        self.sendToMixin(psoptimizer.MasterPsoMixin.prepareVideoDataMessage(popularityThreshold=1.024,
                                                       videoData=videoData))
        
        slaveData = {
                     cfg.vals.name:{'class':'B', 'online':True, 'viddir':'/tmp'},
                     'otherslave':{'class':'A', 'online':True, 'viddir':'/tmp', 'ip':'127.0.0.1'}
                     }
        self.sendToMixin(psoptimizer.MasterPsoMixin.prepareSlaveClassData(slaveData=slaveData,
                                                       speed=1.0))
        self.mixin._videoData._videoDir = os.path.join(os.path.dirname(__file__), 'testvideos')
        
        self.mixin._optForClassPurity()
        self.assertEquals(1, len(self.taskSched.deferredJobs))
        
        # empty the jobs
        self.taskSched.reset()
        
        
        # #
        # Step 2 (--> pure data)
        # #
        self.sendToMixin(psoptimizer.MasterPsoMixin.prepareVideoDataMessage(popularityThreshold=3,
                                                       videoData=videoData))
        
        self.mixin._optForClassPurity()
        self.assertEquals(0, len(self.taskSched.deferredJobs))
        msg = self.getMsgSentByMixin()
        self.assertTrue(isinstance(msg, psoptimizer.PsoSlaveIsPure))
        
    def test_scheduleVidForMigration_success(self):
        # schedule the migration job
        self.mixin._scheduleVidForMigration(1, 'filename', 'target', 'target-ip', 'target-dir')
        
        # let the job succeed
        self.taskSched.deferredJobs[0][0].callback(None)
        
        # make sure, the message has been sent to the master
        msg = self.getMsgSentByMixin()
        
        self.assertEquals(1, msg.videoId)
        self.assertEquals(cfg.vals.name, msg.oldSlave)
        self.assertEquals('target', msg.newSlave)
        
    def test_scheduleVidForMigration_fail(self):
        # schedule the migration job
        self.mixin._scheduleVidForMigration(1, 'filename', 'target', 'target-ip', 'target-dir')
        
        # let the job succeed
        self.taskSched.deferredJobs[0][0].errback(Exception("whatever"))
        
        msg = self.getMsgSentByMixin()
        self.assertIsNone(msg)
        
    def test_on_PsoDoShutdown(self):
        self.sendToMixin(psoptimizer.PsoDoShutdown)
        
    def test_optForBalancing(self):
        """
        Runs the balancing optimization.
        """
        videoData = {'index':[1, 2, 3],
                     'columns':['size', 'fname', 'totalbyteseconds', 'popularity'],
                     'data':[(1, 'tiny_test.mp4', 1024 * 256, 3),
                             (2, 'tiny_test.mp4', 1024 * 1024, 3),
                             (3, 'tiny_test.mp4', 1024 * 1024, 2)]
                             }
        self.sendToMixin(psoptimizer.MasterPsoMixin.prepareVideoDataMessage(popularityThreshold=0,
                                                       videoData=videoData))
        
        slaveData = {
                     cfg.vals.name:{'class':'A', 'online':True, 'bs':1024 * 1024 * 11, 'viddir':'/tmp'},
                     'otherslave':{'class':'A', 'online':True, 'bs': 1024 * 1024 * 10, 'viddir':'/tmp', 'ip':'127.0.0.1'}
                     }
        self.sendToMixin(psoptimizer.MasterPsoMixin.prepareSlaveClassData(slaveData=slaveData,
                                                                          speed=0.5))
        self.mixin._videoData._videoDir = os.path.join(os.path.dirname(__file__), 'testvideos')
        
        self.mixin._optForBalancing()
        self.assertEquals(0, len(self.taskSched.deferredJobs))
        
        # empty the jobs
        self.taskSched.reset()
        
class TestVideoData(unittest.TestCase):
    def test_getFilteredVideos(self):
        d = psoptimizer.VideoData({'index':[1, 2, 3],
                                   'columns':['size', 'fname', 'totalbyteseconds', 'popularity'],
                                   'data':[(1, 'tiny_test.mp4', 150, 0),
                                           (2, 'tiny_test.mp4', 320, 0),
                                           (3, 'tiny_test.mp4', 1, 2)
                                           ]
                                   })
        d._videoDir = os.path.join(os.path.dirname(__file__), 'testvideos')
        self.assertEquals(3, len(d.getFilteredVideos()))
        
        # return only vid with ID 3
        self.assertEquals(3, d.getFilteredVideos(minThreshold=0)[0][0])
        
        # return only vids 1 and 2
        self.assertEquals(1, d.getFilteredVideos(maxThreshold=0)[0][0])
        self.assertEquals(2, d.getFilteredVideos(maxThreshold=0)[1][0])
        
    def test_videoFileExists(self):
        d = psoptimizer.VideoData({'index':[],
                                   'columns':['size', 'fname', 'totalbyteseconds', 'popularity'],
                                   'data':[]
                                   })
        d._videoDir = os.path.join(os.path.dirname(__file__), 'testvideos')
        
        self.assertTrue(d._videoFileExists('tiny_test.mp4'))
        self.assertFalse(d._videoFileExists('nonexistingfile'))
        
class TestMasterPsoMixin(basetests.BaseDbTest, mixinhelpers.ProtocolMixinTestMixin):
    slaveDao = pythonioc.Inject('slaveDao')
    def setUp(self):
        basetests.BaseDbTest.setUp(self, True)
        self.setUpMixin(psoptimizer.MasterPsoMixin)
        pythonioc.registerService(activityservice.ActivityService)
        
    @defer.inlineCallbacks
    def test_on_PsoSlaveIsPure(self):
        yield self.slaveDao.addSlave(domain.Slave(name="someslave"))
        self.sendToMixin(psoptimizer.PsoSlaveIsPure(slaveName='someslave'))
        
