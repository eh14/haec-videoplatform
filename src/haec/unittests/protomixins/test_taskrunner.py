'''
Tests the task management
'''


from haec.protomixins import taskrunner
from haec.unittests.protomixins import mixinhelpers
import pythonioc
from haec.unittests import testutils
from twisted.internet import defer
from haec.network import multiproto
from twisted.test import proto_helpers
from haec.unittests.kplane import basetests
from haec.services import taskservice
from twisted.trial import unittest
from haec.unittests.services import servicemocks

class TestMasterTaskRunnerMixin(basetests.BaseDbTest):
    timeout = 5
    
    taskService = pythonioc.Inject(taskservice.TaskService)
    taskDao = pythonioc.Inject('taskDao')
    
    clock = pythonioc.Inject('reactor')
    
    def setUp(self):
        pythonioc.registerService(taskservice.TaskService)
        self.connectedSlaves = set([mixinhelpers.SlaveConnectionMock('slave-y1')])
        basetests.BaseDbTest.setUp(self, True)
        self.taskService.autoWakeup = False
        
        self.master = multiproto.MultiProtocol(taskrunner.MasterTaskRunnerMixin())
        self.tr = proto_helpers.StringTransport()
        self.master.makeConnection(self.tr)
        self.slave = multiproto.MultiProtocol(taskrunner.SlaveTaskRunnerMixin())
        self.slave.makeConnection(self.tr)
        
    @defer.inlineCallbacks
    def test_startJobOnSlave(self):
        task = yield self.taskDao.addCommandActionTask('sometest', 'testslave', 'echo "hello world"')
        
        connectionMock = mixinhelpers.SlaveConnectionMock('testslave')
        taskrunner.MasterTaskRunnerMixin.startJobOnSlave(connectionMock, taskId=task.taskId,
                                                                      job=self.taskDao.createJobFromAction(task.action))

        self.assertTrue('hello world' in connectionMock.obj.job._cmd)
        self.assertEquals(task.taskId, connectionMock.obj.taskId)
        
class TestSlaveTaskRunnerMixin(unittest.TestCase, mixinhelpers.ProtocolMixinTestMixin):
    taskRunner = pythonioc.Inject('taskRunner')
    
    def setUp(self):
        testutils.setUpIocRegistry()
        self.setUpMixin(taskrunner.SlaveTaskRunnerMixin)
        pythonioc.registerServiceInstance(servicemocks.JobSchedulerMock(),
                                          'taskRunner', overwrite=True)
    @defer.inlineCallbacks
    def test_onTaskRunnerRunJob_success(self):
        yield self.sendToMixin(taskrunner.TaskRunnerRunJob(job="Not-a-real-job",
                                                           taskId="123"))
        
        self.taskRunner.deferredJobs[0][0].callback(None)
        
        msg = self.getMsgSentByMixin()
        self.assertEquals(True, msg.success)
        self.assertEquals('123', msg.taskId)

    @defer.inlineCallbacks        
    def test_onTaskRunnerRunJob_fail(self):
        yield self.sendToMixin(taskrunner.TaskRunnerRunJob(job="Not-a-real-job",
                                                           taskId="123"))
        
        self.taskRunner.deferredJobs[0][0].errback(Exception("failed"))
        
        msg = self.getMsgSentByMixin()
        self.assertEquals(False, msg.success)
        self.assertEquals('123', msg.taskId)
        self.assertEquals('failed', msg.message)