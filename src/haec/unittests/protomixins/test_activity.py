'''

@author: eh14
'''
from twisted.trial import unittest
from haec.protomixins import activity
from twisted.test import proto_helpers
import pythonioc
from haec.services import activityservice, activitytracker
from haec.unittests.protomixins import mixinhelpers
from haec import config
from haec.unittests import testutils
from haec.unittests.kplane import basetests
from twisted.internet import defer
from haec.kplane import domain

cfg = pythonioc.Inject(config.Config)

class R(object):
    pass

class TestSlaveActivityMixin(unittest.TestCase):
    actTracker = pythonioc.Inject(activitytracker.ActivityTracker)
    def setUp(self):
        testutils.setUpIocRegistry()
        pythonioc.registerService(activitytracker.ActivityTracker)
        self.receiver = mixinhelpers.JsonProtocolMock()
        
    def test_sendActivity(self):
        tr = proto_helpers.StringTransport()
        mixin = activity.SlaveActivityMixin()
        proto = mixinhelpers.createMultiProto(mixin)
        proto.connectionMade()
        proto.transport = tr
        
        mixin._sendActivity()
        self.receiver.dataReceived(tr.value())
        tr.clear()
        self.assertEquals(0, self.receiver.received.streams)
        self.assertEquals(cfg.vals.name, self.receiver.received.slaveName)
        
        request = R()
        self.actTracker.streamStarted('somevideo', request)
        
        mixin._sendActivity()
        self.receiver.dataReceived(tr.value())
        tr.clear()
        self.assertEquals(1, self.receiver.received.streams)
        self.assertEquals(cfg.vals.name, self.receiver.received.slaveName)
        self.actTracker.streamFinished('somevideo', request)
        del request
        # note that we do not need to call _sendActivity since this is automatically
        # triggered, once the activity tracker reaches 0
        self.receiver.dataReceived(tr.value())
        self.assertEquals(0, self.receiver.received.streams)
        self.assertEquals(cfg.vals.name, self.receiver.received.slaveName)
        
class TestMasterActivityMixin(basetests.BaseDbTest):
    
    slaveDao = pythonioc.Inject('slaveDao')
    activityService = pythonioc.Inject(activityservice.ActivityService)
    
    def setUp(self):
        basetests.BaseDbTest.setUp(self, False)
        self.clearDatabase()
        pythonioc.registerService(activityservice.ActivityService)
        
    @defer.inlineCallbacks
    def test_onSlaveActivity(self):
        tr = proto_helpers.StringTransport()
        mixin = activity.MasterActivityMixin()
        proto = mixinhelpers.createMultiProto(mixin)
        proto.connectionMade()
        proto.transport = tr
        
        yield self.slaveDao.addSlave(domain.Slave(name="slave"))
        proto.receiveRaw(activity.ActivityMessage(streams=100, slaveName='slave'))
        
        self.assertEquals(100, self.activityService._slaveActivities['slave']._streams)
        
        
        
