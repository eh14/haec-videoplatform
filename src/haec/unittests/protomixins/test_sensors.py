from twisted.trial import unittest
import time
from haec.protomixins import sensors
from haec.unittests.protomixins import mixinhelpers
from haec.unittests import testutils
from haec.protomixins.sensors import SensorsReport

class TestSensors(unittest.TestCase, mixinhelpers.ProtocolMixinTestMixin):
    
    def setUp(self):
        testutils.setUpIocRegistry()
        self.setUpMixin(sensors.SlaveSensorsMixin)
        
    def test_collectSensorData(self):
        now = time.time() - 1
        
        report = self.mixin._collectSensorData()

        self.assertTrue(report.slave_time >= now)
        self.assertTrue(report.cpu_usage >= 0 and report.cpu_usage <= 1.0, 'was %s' % (str(report.cpu_usage)))  # # it may really zero sometimes :-/
        self.assertTrue(report.memory_usage > 0.001 and report.memory_usage <= 1.0)
        self.assertTrue(report.network_in >= 0 and report.network_in <= 1024 * 1024 * 1024)
        self.assertTrue(report.network_out >= 0 and report.network_out <= 1024 * 1024 * 1024)
        self.assertTrue(report.disk_io_bytes >= 0)
        self.assertTrue(report.disk_io_time >= 0)
        
        msg = self.getMsgSentByMixin()
        self.assertEquals(report.network_in, msg.network_in)

    def test_runPsutil(self):
        
        value = sensors.runPsutil('cpu_percent') 
        self.assertTrue(0 <= value <= 100, "unexpected cpu_percent value %f" % value)
        
        self.assertRaises(AttributeError, sensors.runPsutil, 'non-existing')
        
    def test_sendBulkSensorReports(self):
        raise unittest.SkipTest('messages are sent by collectSensorData already')
        self.mixin._currentData.reports.append(SensorsReport(network_in=123))
        self.mixin._sendBulkSensorReports()
        
        msg = self.getMsgSentByMixin()
        
        self.assertEquals(123, msg.reports[0].network_in)
        
