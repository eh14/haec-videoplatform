'''

@author: eh14
'''
from twisted.trial import unittest
from haec.protomixins import hardware
from haec.unittests.protomixins import mixinhelpers
import os

class TestHardware(unittest.TestCase, mixinhelpers.ProtocolMixinTestMixin):
    
    def setUp(self):
        self.setUpMixin(hardware.SlaveHardwareMixin)

    def test_findScript(self):
        self.assertTrue(os.path.isfile(self.mixin._findScript()))
        
    def test_blinkSlave(self):
        hardware.blinkSlave(self.mixin, 15, 'red')
        msg = self.getMsgSentByMixin()
        
        self.assertEquals(15, msg.times)
        self.assertEquals('red', msg.led)
        self.assertEquals('timer', msg.mode)