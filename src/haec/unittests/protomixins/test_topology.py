from haec.network import multiproto, jsonprotocol
from haec.protomixins import topology
from twisted.test import proto_helpers
from haec import master
import pythonioc
from haec.unittests.kplane import basetests
from twisted.internet import defer
from haec.services import portmapperservice
from haec.unittests.protomixins import mixinhelpers
from haec.unittests import testutils

class TestTopologyMaster(basetests.BaseDbTest):
    slaveDao = pythonioc.Inject('slaveDao')
    
    def setUp(self):
        pythonioc.registerService(portmapperservice.PortMapperService)
        basetests.BaseDbTest.setUp(self, dropTables=True)
        
    @defer.inlineCallbacks 
    def test_onSlaveEnvironment(self):
        
        fact = master.MasterFactory()
        pythonioc.registerServiceInstance(fact, 'master', True)
        
        proto = fact.buildProtocol(None)
        
        receiver = mixinhelpers.JsonProtocolMock()
        
        sender = jsonprotocol.JsonProtocol()
        sender.transport = proto_helpers.StringTransport()
        sender.sendObject(topology.TopoloySlaveEnvironment(name='test', webserver_port='1'))
        
        receiver.dataReceived(sender.transport.value())
        yield proto.receiveRaw(receiver.received)
        
        slaveNames = yield self.slaveDao.getSlaveNames()        
        self.assertEquals(['test'], slaveNames)
        slaves = yield self.slaveDao.getSlaves()
        self.assertEquals(1, len(slaves))
        self.assertEquals('1', slaves[0].webserver_port)

        sender.transport.clear()

        sender.sendObject(topology.TopoloySlaveEnvironment(name='test', webserver_port='2'))
        receiver.dataReceived(sender.transport.value())
        yield proto.receiveRaw(receiver.received)
            
        slaves = yield self.slaveDao.getSlaves()
        self.assertEquals(1, len(slaves))
        self.assertEquals('2', slaves[0].webserver_port)
