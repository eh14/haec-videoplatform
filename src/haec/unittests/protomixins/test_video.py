from twisted.trial import unittest
from haec import config, utils
import os
from haec.kplane import domain
from twisted.internet.protocol import Factory
from haec.network import jsonprotocol
from twisted.internet import defer, reactor
import pythonioc
from haec.unittests import testutils
from haec.protomixins import video
from haec.services import jobscheduler, videoservice, mastervideoservice
from haec.unittests.protomixins import mixinhelpers
from haec.protomixins.video import SlaveVideoMixin
from haec.unittests.kplane import basetests

here = os.path.dirname(os.path.abspath(__file__))

videoTestDir = os.path.join(here, 'testvideos')

class MockVidListReceiver(jsonprotocol.JsonProtocol):
    received = None
    def on_VideoList(self, message):
        self.received = message
        
        
class VideoSlaveProtocol(jsonprotocol.JsonProtocol, video.SlaveVideoMixin):
    def __init__(self):
        video.SlaveVideoMixin.__init__(self)
        
        
class VideoSlaveFactory(Factory):
    protocol = VideoSlaveProtocol
    jobscheduler = None

class TestSlaveVideoMixin(unittest.TestCase, mixinhelpers.ProtocolMixinTestMixin):
    timeout = 5
    
    clock = pythonioc.Inject('reactor')
    sched = pythonioc.Inject('taskRunner')
    
    slaveVideoService = pythonioc.Inject(videoservice.SlaveVideoService)
    cfg = pythonioc.Inject(config.Config)
    def setUp(self):
        testutils.setUpIocRegistry()
        self.cfg.init(video_dir=videoTestDir, scheduler_job_waitinterval=0)
        pythonioc.registerServiceInstance(reactor, 'reactor', overwrite=True)
        pythonioc.registerService(videoservice.SlaveVideoService)
        pythonioc.registerServiceInstance(jobscheduler.JobScheduler(maxRunning=1), 'taskRunner', True)
        self.setUpMixin(SlaveVideoMixin)

    @defer.inlineCallbacks
    def test_on_VideoListRequest(self):
        yield self.mixin.on_VideoListRequest(video.VideoListRequest())
        msg = self.getMsgSentByMixin()
        self.assertEquals(2, len(msg.videos))
        
    @defer.inlineCallbacks
    def test_on_VideoDataRequest(self):
        yield self.mixin.on_VideoDataRequest(video.VideoDataRequest(videoNames=['tiny_test__id1234.mp4']))
        
        msg = self.getMsgSentByMixin()
        # the result has structure (success, result)
        self.assertEquals('tiny_test', msg.name)
        self.assertEquals('1234', msg.variant)
    
    @defer.inlineCallbacks
    def test_on_VideoAddToCollection(self):
        # let's make the video-slave believe it actually works in /tmp
        self.mixin._dir = '/tmp'
        self.slaveVideoService._dir = '/tmp'
        self.cfg.change(video_dir='/tmp', scheduler_job_waitinterval=0)
        # tell it to get a new temporary video 
        newFile = yield self.mixin.on_VideoAddToCollection(video.VideoAddToCollection(path=os.path.join(videoTestDir, 'tiny_test.mp4'),
                                                                                      videoName='somevideo',
                                                                                      taskId=123))
        
        self.assertTrue(os.path.exists(os.path.join('/tmp', newFile)))
         
        msg = self.getMsgSentByMixin()
        
        self.assertTrue(isinstance(msg, video.VideoData))
        self.assertEquals(123, msg.taskId)
        
        # wait until the last threaded jobs are done...
        yield utils.deferredSleep(self.clock, 0.25)
        while self.sched.isBusy():
            yield utils.deferredSleep(self.clock, 0.25)
        
            
class TestMasterVideoMixin(basetests.BaseDbTest, mixinhelpers.ProtocolMixinTestMixin):
    
    videoDao = pythonioc.Inject('videoDao')
    videoService = pythonioc.Inject(mastervideoservice.MasterVideoService)
    def setUp(self):
        basetests.BaseDbTest.setUp(self, False)
        self.clearDatabase()
        self.setUpMixin(video.MasterVideoMixin)
        pythonioc.registerService(mastervideoservice.MasterVideoService)
        
    @defer.inlineCallbacks
    def test_onVideoList(self):
        vidList = video.VideoList()
        yield self.videoDao.addVideo('slaveA', domain.Video(name='vidA', variant='0', fname='vidA__id0.mp4'))
        yield self.videoDao.addVideo('someotherslave', domain.Video(name='vidA', variant='0', fname='vidA__id0.mp4'))
        
        vidList.videos = [videoservice.SlaveVideoService._parseVideoName('vidA__id0.mp4')]
        yield self.mixin.on_VideoList(vidList)
        
        msg = self.getMsgSentByMixin()
        
        self.assertIsInstance(msg, video.VideoDataRequest)
        self.assertEquals(set(['vidA__id0.mp4']), msg.videoNames)
        
    @defer.inlineCallbacks
    def test_on_VideoData(self):
        
        rawData = {'fps': '25', 'extension': 'mp4',
                   'format': 'mov,mp4,m4a,3gp,3g2,mj2',
                   'variant': '1234', 'fsize': 41854,
                   'fileName': 'tiny_test__id1234.mp4',
                   'width': '640', 'audio_codec': 'aac',
                   'duration': '1.0', 'video_codec': 'h264',
                   'height': '360', 'bitrate': '334832.0',
                   'name': 'tiny_test'}
        
        self.proto.receiveRaw(video.VideoData(slaveName='someslave', **rawData))
        yield self.videoService.addBatchedVideo()
        
        vidNames = yield self.videoDao.getVideoNames()
        self.assertEquals(['tiny_test'], vidNames)
    
    def test_regex(self):
        m = videoservice.videoPattern.match('video-slave-c3.mp4')
        self.assertIsNotNone(m)

class TestCollectVideoDataJob(unittest.TestCase):
    
    sampleOutput = """
avprobe version 0.8.10-6:0.8.10-0ubuntu0.13.10.1, Copyright (c) 2007-2013 the Libav developers
  built on Feb  6 2014 20:53:28 with gcc 4.8.1
Input #0, avi, from 'buck480_10_10.avi':
  Metadata:
    encoder         : Lavf53.21.1
  Duration: 00:00:18.75, start: 0.000000, bitrate: 2221 kb/s
    Stream #0.0: Video: msmpeg4v2, yuv420p, 854x480, 24 fps, 24 tbr, 24 tbn, 24 tbc
    Stream #0.1: Audio: mp3, 48000 Hz, stereo, s16, 245 kb/s
[STREAM]
index=0
codec_name=msmpeg4v2
codec_long_name=MPEG-4 part 2 Microsoft variant version 2
codec_type=video
codec_time_base=1/24
codec_tag_string=MP42
codec_tag=0x3234504d
width=854
height=480
has_b_frames=0
pix_fmt=yuv420p
level=-99
r_frame_rate=24/1
avg_frame_rate=24/1
time_base=1/24
start_time=0.000000 
duration=18.750000 
nb_frames=450
[/STREAM]
[STREAM]
index=1
codec_name=mp3
codec_long_name=MP3 (MPEG audio layer 3)
codec_type=audio
codec_time_base=1/48000
codec_tag_string=U[0][0][0]
codec_tag=0x0055
sample_rate=48000.000000 
channels=2
bits_per_sample=0
r_frame_rate=0/0
avg_frame_rate=0/0
time_base=3/125
start_time=0.000000 
duration=N/A
nb_frames=503
[/STREAM]
[FORMAT]
filename=buck480_10_10.avi
nb_streams=2
format_name=avi
format_long_name=AVI format
start_time=0.000000 
duration=18.750000 
size=5205610.000000 
bit_rate=2221060.000000 
TAG:encoder=Lavf53.21.1
[/FORMAT]
"""
    
    def test_getStreamSections(self):
        job = videoservice.CollectVideoDataJob(directory=None, vidData=None)
        streams = job._getStreamSections(self.sampleOutput)
        
        self.assertEquals(2, len(streams))
        self.assertEquals('mp3', streams['audio']['codec_name'])
        self.assertEquals('msmpeg4v2', streams['video']['codec_name'])
        
    def test_getFormatSection(self):
        job = videoservice.CollectVideoDataJob(directory=None, vidData=None)
        formatSection = job._getFormatSection(self.sampleOutput)
        
        self.assertAlmostEqual(18.75, float(formatSection['duration']))
        self.assertEquals('buck480_10_10.avi', formatSection['filename'])
        
        
    def test_readMetaDataFromOutput(self):
        job = videoservice.CollectVideoDataJob(directory=None, vidData=None)
        
        metaData = job._readMetaDataFromOutput(self.sampleOutput)
        
        self.assertEquals('18.75', metaData['duration'])
        self.assertEquals('2221060.0', metaData['bitrate'])
        self.assertEquals('24', metaData['fps'])
        self.assertEquals('854', metaData['width'])
        self.assertEquals('480', metaData['height'])
        self.assertEquals('msmpeg4v2', metaData['video_codec'])
        self.assertEquals('mp3', metaData['audio_codec'])
        self.assertEquals('avi', metaData['format'])
