'''

@author: eh14
'''
from haec.network import jsonprotocol, multiproto
from twisted.test import proto_helpers

class SlaveConnectionMock(object):
    obj = None
    
    def __init__(self, name):
        self._name = name
    
    def sendObject(self, obj):
        self.obj = obj
        
    def getSlaveName(self):
        return self._name
    

class JsonProtocolMock(jsonprotocol.JsonProtocol):
    received = None
    def receiveRaw(self, message):
        self.received = message
        

def createMultiProto(*mixins):
    return multiproto.MultiProtocol(*mixins)


class ProtocolMixinTestMixin(object):
    """
    This class is a mixin for test cases to provide
    a protocol mixin, along with its multiprotocol, a connection,
    a string transport and a json reciever which automatically tracks
    and converts all sent messages.
    """ 
    
    
    def setUpMixin(self, mixinClass):
        self.mixin = mixinClass()
        self.proto = createMultiProto(self.mixin)
        self.proto.connectionMade()
        self.tr = proto_helpers.StringTransport()
        self.proto.transport = self.tr
        self.receiver = JsonProtocolMock()
        
    def sendToMixin(self, message):
        self.tr.clear()
        self.proto.receiveRaw(message)
         
    def getMsgSentByMixin(self):
        """
        Returns the last message that has been sent by the mixin.
        """ 
        self.receiver.dataReceived(self.tr.value())
        self.tr.clear()
        return self.receiver.received
    