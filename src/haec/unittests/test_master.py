'''
Created on Jun 19, 2014

@author: eh14
'''
from haec import master , config
from twisted.internet import reactor
import pythonioc
from haec.unittests.kplane import basetests

class TestMasterFactory(basetests.BaseDbTest, basetests.CambrionixMockMixin):
    kp = pythonioc.Inject('knowledgeplane')
    clock = pythonioc.Inject('reactor')
    
    fact = pythonioc.Inject('master')
    
    def setUp(self):
        basetests.BaseDbTest.setUp(self, True, insertServiceMocks=False)
        master.registerMasterServices()
        pythonioc.registerServiceInstance(reactor, 'reactor')
        pythonioc.registerServiceInstance(master.MasterFactory(), 'master', overwrite=True)
        self.fact.startFactory()
        
        self.startCambrionix()
        
    def tearDown(self):
        
        self.fact.stopFactory()
        
        self.stopCambrionix()
        
        for call in self.clock.getDelayedCalls():
            call.cancel()
        
    def test_buildProtocol(self):
        proto = self.fact.buildProtocol(None)
        self.assertIsNotNone(proto)
        
        
            
