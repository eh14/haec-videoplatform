'''

@author: eh14
'''
from twisted.trial import unittest
from twisted.python import threadable
from twisted.internet import defer, threads, reactor
import pythonioc
from haec.unittests import testutils
import time

class TestReactorThreads(unittest.TestCase):
    
    @defer.inlineCallbacks
    def test_isInIOThread(self):
        raise unittest.SkipTest("Not working, the test's reactor does not seem to register as IOThread")
        pythonioc.cleanServiceRegistry()
        testutils.setUpIocRegistry()
        # here we are in the reactor's thread
        self.assertTrue(threadable.isInIOThread())
        
        result = yield threads.deferToThread(threadable.isInIOThread)
        
        self.assertFalse(result)
        
        