'''

@author: eh14
'''
from haec.kplane import domain


def createVideoForProfile(name, profile, variant=''):
    if isinstance(profile, domain.Profile):
        profile = vars(profile)
        
    return domain.Video(name=name,
                 variant=variant,
                 video_codec=profile['vCodec'],
                 audio_codec=profile['aCodec'],
                 width=profile['width'],
                 height=profile['height'],
                 format=profile['fileFormat'],
                 )
