'''
Tests the testutils.. nothing much yet.
'''
from twisted.trial import unittest
from twisted.internet import defer, reactor
from haec import utils
import time


class Testtestutils(unittest.TestCase):
    timeout = 2
    
    @defer.inlineCallbacks
    def testDeferredSleep(self):
        now = time.time()
        yield utils.deferredSleep(reactor, 0.2)
        self.assertAlmostEqual(time.time(), now + 0.2, places=2)
