from twisted.trial import unittest
from haec.network import message

class SomeMessage(message.Message):
    value = None

class TestMessageClass(unittest.TestCase):
        
    def testMessageCreation(self):
        self.assertEquals('somevalue', SomeMessage(value='somevalue').value)
        self.assertRaises(Exception, SomeMessage, nonexisting=1)
