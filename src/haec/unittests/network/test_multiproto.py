from twisted.trial import unittest
from twisted.test import proto_helpers
from haec.network.multiproto import MultiProtocol, MultiProtocolMixinBase, \
    MultiProtocolBuilder
from haec.network.jsonprotocol import JsonProtocol
from haec.network import message
from twisted.internet.protocol import Factory


class ProtocolMixinA(MultiProtocolMixinBase):
    madeConn = False
    lostConn = False
    lostConnReason = None
    
    def connectionMade(self):
        self.madeConn = True
        
    def connectionLost(self, reason):
        self.lostConn = True
        self.lostConnReason = reason
        
class MessageA(message.Message):
    pass

class MessageSend(message.Message):
    pass

class ProtocolMixinB(MultiProtocolMixinBase):
    msgA = None
    def on_MessageA(self, message):
        self.msgA = message
        
    def on_MessageSend(self, message):
        self.sendObject(message)
        
class ProtocolMixinC(MultiProtocolMixinBase):
    def connectionMade(self):
        self.factory.value += 1
        
class ProtocolMixinCFactory(Factory):
    protocol = MultiProtocolBuilder(ProtocolMixinC)
    
    value = 0

class TestMultiProtocol(unittest.TestCase):
    def setUp(self):
        self.sender = JsonProtocol()
        self.tr = proto_helpers.StringTransport()
        self.sender.transport = self.tr

    def msgFromObject(self, obj):
        self.sender.sendObject(obj)
        return self.tr.value()
        
    def testEmptyMultiProtocol(self):
        proto = MultiProtocol()
        proto.dataReceived(self.msgFromObject(message.Message()))
        
        
    def test_connectionMadeAndLost(self):
        mixin = ProtocolMixinA()
        proto = MultiProtocol(mixin)
        
        self.assertFalse(mixin.madeConn)
        self.assertFalse(mixin.lostConn)
        
        proto.makeConnection(self.tr)
        
        self.assertTrue(mixin.madeConn)
        self.assertFalse(mixin.lostConn)

        mixin.connectionLost("someReason")        
        self.assertTrue(mixin.madeConn)
        self.assertTrue(mixin.lostConn)
        self.assertEquals("someReason", mixin.lostConnReason)
        
        
    def test_receiveMessages(self):
        msg = MessageA()
        
        mixin = ProtocolMixinB()
        proto = MultiProtocol(mixin)
        
        self.assertIsNone(mixin.msgA)
        proto.dataReceived(self.msgFromObject(msg))
        self.assertEquals(msg.__class__, mixin.msgA.__class__)
        

    def test_receiveAndSend(self):
        msg = MessageSend()
        
        mixin = ProtocolMixinB()
        proto = MultiProtocol(mixin)
        transport = proto_helpers.StringTransport()
        proto.transport = transport
        proto.dataReceived(self.msgFromObject(msg))
        self.assertTrue('MessageSend' in transport.value())

    def test_setFactory(self):
        
        factory = ProtocolMixinCFactory()
        proto = factory.buildProtocol('address')
        self.assertEquals(0, factory.value)
        proto.makeConnection(proto_helpers.StringTransport())
        self.assertEquals(1, factory.value)
