from twisted.trial import unittest
from twisted.internet.protocol import Factory
from haec.network import jsonprotocol, message
from twisted.test import proto_helpers

class SubMessage(message.Message):
    value = None
    
class SpecificProtocol(jsonprotocol.JsonProtocol):
    receivedMessage = None
    receivedRaw = None
    receivedCounter = 0
    sendObjectCounter = 0
    
    
    def stringReceived(self, string):
        jsonprotocol.JsonProtocol.stringReceived(self, string)
        
        self.receivedCounter += 1
        
    def sendObject(self, messageObj):
        jsonprotocol.JsonProtocol.sendObject(self, messageObj)
        
        self.sendObjectCounter += 1
        
    def on_SubMessage(self, msg):
        self.receivedMessage = msg
        
    def on_Raw(self, rawMsg):
        self.receivedRaw = rawMsg 
        
class JsonProtocolFactory(Factory):
    protocol = SpecificProtocol

class TestJsonProtocol(unittest.TestCase):
    def setUp(self):
        factory = JsonProtocolFactory()
        self.proto = factory.buildProtocol(('127.0.0.1', 0))
        self.tr = proto_helpers.StringTransport()
        self.proto.makeConnection(self.tr)
        
    def tearDown(self):
        self.proto.transport.loseConnection()
        
    def testSendAndReceive(self):
        self.proto.sendObject(SubMessage(value='hello world'))
        
        newProto = JsonProtocolFactory().buildProtocol(None)
        newProto.dataReceived(self.tr.value())
        
        self.assertTrue(newProto is not None and isinstance(newProto.receivedMessage, SubMessage))
        self.assertEquals('hello world', newProto.receivedMessage.value)
        
    def testMessageShattering(self):
        value = 'a' * (self.proto._maxLength + 1)
        self.assertTrue(len(value) > self.proto._maxLength)
        self.proto.sendObject(SubMessage(value=value))
        
        self.assertEquals(4, self.proto.sendObjectCounter)
        
        recvProtocol = JsonProtocolFactory().buildProtocol(None)
        wireValue = self.tr.value()
        for i in range(0, len(wireValue), 1000):
            recvProtocol.dataReceived(wireValue[i:i + 1000])
        
        # make sure it was not received as RAW (i.e. the type was not recognized)
        self.assertIsNone(recvProtocol.receivedRaw)
        
        self.assertEquals(4, recvProtocol.receivedCounter)
        
        self.assertEquals(value, recvProtocol.receivedMessage.value)
        

    def test_isHandlerFunc(self):
        c = jsonprotocol.JsonProtocol
        
        self.assertFalse(c.isHandlerFunc(''))
        self.assertFalse(c.isHandlerFunc('abc'))
        self.assertFalse(c.isHandlerFunc('on_'))
        
        self.assertTrue(c.isHandlerFunc('on_ab'))
        self.assertTrue(c.isHandlerFunc('on_Ab'))
        self.assertTrue(c.isHandlerFunc('on_Ab1233'))
        
    def test_getHandlerForType(self):
        c = jsonprotocol.JsonProtocol
        
        self.assertEquals('on_SomeType', c.getHandlerForType('SomeType'))
        self.assertRaises(AssertionError, c.getHandlerForType, '')
        
    def test_getTypeForHandler(self):
        c = jsonprotocol.JsonProtocol
        
        self.assertEquals('SomeType', c.getTypeForHandler('on_SomeType'))
        
        self.assertRaises(AssertionError, c.getTypeForHandler, 'somethingelse')
        self.assertRaises(AssertionError, c.getTypeForHandler , 'on_')
