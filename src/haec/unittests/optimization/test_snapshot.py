'''
Created on Aug 4, 2014

@author: eh14
'''

import numpy
from haec.optimization import optmodel, snapshots , snapshotcreator
import pythonioc
from haec import config
from haec.kplane import domain
from twisted.internet import defer
from haec.unittests.kplane import basetests

        
class TestSnapshot(basetests.BaseDbTest):
    
    vidDao = pythonioc.Inject('videoDao')
    slaveDao = pythonioc.Inject('slaveDao')
    
    cfg = pythonioc.Inject(config.Config)
    optModel = pythonioc.Inject(optmodel.OptModelProvider)
    clock = pythonioc.Inject('reactor')
    snapshotcreator = pythonioc.Inject(snapshotcreator.SystemSnapshotCreator)
    
    
    @defer.inlineCallbacks
    def setUp(self):
        basetests.BaseDbTest.setUp(self)
        yield self.clearDatabase()
        
        self._vidIds = []
        
    def _namedItemsToDict(self, items):
        return {item.name:item for item in items}

    def assertSimilar(self, expected, actual, delta):
        self.assertLess(abs(actual - expected), delta)
    @defer.inlineCallbacks    
    def _createDefaultSnapshot(self):
        
        vid1Id = yield self.vidDao.addVideo('slaveA', domain.Video(name='vid1', duration=10, fsize=self.optModel.model.MB))
        vid2Id = yield self.vidDao.addVideo('slaveB', domain.Video(name='vid2', duration=10, fsize=self.optModel.model.MB))
        
        # save the vidIds for later
        self._vidIds.append(vid1Id)
        self._vidIds.append(vid2Id)
        
        # slave A has both videos
        yield self.vidDao.addVideoToSlave(vid2Id, 'slaveA')
        
        yield self.slaveDao.setSlaveOnlineStatus('slaveA', True)
        yield self.slaveDao.setSlaveOnlineStatus('slaveB', True)
        
        # hit 'em
        for _i in range(2):
            self.hittracker.hit(vid1Id)
            self.hittracker.hit(vid2Id)
        
        
        snap = yield self.snapshotcreator.createSnapshot(self.optModel.model)
        defer.returnValue(snap)
        
    @defer.inlineCallbacks
    def test_getNumberOfVideos(self):
        snapshot = yield self._createDefaultSnapshot()
        self.assertEquals({'slaveA':2, 'slaveB':1}, snapshot.getNumberOfVideos())
        
    @defer.inlineCallbacks
    def test_getSlaveByteSeconds(self):
        snapshot = yield self._createDefaultSnapshot()
        seconds = snapshot.getSlaveByteSeconds()
        # slaveA byteseconds: byteseconds of vid1 and half of vid2, because vid2 is replicated on
        # slaveB as well
        self.assertSimilar(seconds['slaveA'], seconds['slaveB'] * 3, delta=1)
       
    @defer.inlineCallbacks 
    def test_getMByteSeconds(self):
        snapshot = yield self._createDefaultSnapshot()
        
        seconds = snapshot.getSlaveByteSeconds()
        
        # just check if it's working, the functionality is tested by the getSlaveByteSeconds testcase
        self.assertTrue('slaveA' in seconds)
        self.assertTrue('slaveB' in seconds)
        
    @defer.inlineCallbacks
    def test_getTotalVideoPopularity(self):
        snapshot = yield self._createDefaultSnapshot()
        popularity = snapshot.getTotalVideoPopularity()
        self.assertTrue(popularity > 0)
    
    @defer.inlineCallbacks    
    def test_getAvgPopularity(self):
        snapshot = yield self._createDefaultSnapshot()
        avgPopularity = snapshot.getAvgPopularity()
        self.assertTrue('slaveA' in avgPopularity)
        self.assertTrue('slaveB' in avgPopularity)
    
    @defer.inlineCallbacks    
    def test_getMappedVideoReplication(self):
        snapshot = yield self._createDefaultSnapshot()
        replication = snapshot.getMappedVideoReplication()
        self.assertEquals(3, replication['slaveA'].sum())
        self.assertEquals(2, replication['slaveB'].sum())
        
    @defer.inlineCallbacks
    def test_createMapForNonReplicatedVideos(self):
        snapshot = yield self._createDefaultSnapshot()
        mapping = snapshot.createMapForNonReplicatedVideos()
        self.assertEquals(2, mapping.columns.size)
        self.assertTrue(numpy.isnan(mapping.ix[self._vidIds[0], 'slaveB']))
        self.assertEquals(1, mapping.ix[self._vidIds[0], 'slaveA'])
        
    @defer.inlineCallbacks
    def test_createMapForNonReplicatedVideos_allReplicated(self):
        
        # create the default videos and slaves
        yield self._createDefaultSnapshot()
        
        # add video 1 to slave-B
        yield self.vidDao.addVideoToSlave(self._vidIds[0], 'slaveB')
        
        # create the snapshot
        snap = yield self.snapshotcreator.createSnapshot(self.optModel.model)

        mp = snap.createMapForNonReplicatedVideos()
        self.assertEquals(2, mp.columns.size)
        self.assertEquals(0, mp.index.size)
        
    @defer.inlineCallbacks
    def test_getSumOfVideoSize(self):
        snapshot = yield self._createDefaultSnapshot()
        self.assertEquals(0, snapshot.getSumOfVideoSize([]))
        self.assertEquals(self.optModel.model.MB, snapshot.getSumOfVideoSize([self._vidIds[0]]))
        self.assertEquals(self.optModel.model.MB * 2, snapshot.getSumOfVideoSize(self._vidIds))
        
