from haec.kplane import domain
from haec.optimization import snapshotcreator, optmodel
import pythonioc
from twisted.internet import defer
from haec.unittests.kplane import basetests
from haec import config

class TestSnapshotcreator(basetests.BaseDbTest):
    
    vidDao = pythonioc.Inject('videoDao')
    slaveDao = pythonioc.Inject('slaveDao')
    hittacker = pythonioc.Inject('hitTracker')
    cfg = pythonioc.Inject(config.Config)
    snapshotcreator = pythonioc.Inject(snapshotcreator.SystemSnapshotCreator)
    
    @defer.inlineCallbacks
    def setUp(self):
        
        pythonioc.registerService(snapshotcreator.SystemSnapshotCreator)
        basetests.BaseDbTest.setUp(self, dropTables=False)
        
        yield self.clearDatabase()
        
        self.optmodel = optmodel.OptimizationModel()
        
    @defer.inlineCallbacks
    def test_createSnapshot_empty(self):
        df = yield self.snapshotcreator.createSnapshot(self.optmodel).mapping
        self.assertTrue(df.empty)
        
    @defer.inlineCallbacks
    def test_createSnapshot(self):
        size = 10 * 1024 * 1024
        vid1Id = yield self.vidDao.addVideo('slaveA', domain.Video(name='vid1', duration=60, fsize=size))
        vid2Id = yield self.vidDao.addVideo('slaveA', domain.Video(name='vid2', duration=30, fsize=size))
        vid3Id = yield self.vidDao.addVideo('slaveA', domain.Video(name='vid3', duration=10, fsize=size))
        yield self.vidDao.addVideo('slaveB', domain.Video(name='vid3'))
        yield self.vidDao.addVideo('slaveC', domain.Video(name='vid3'))

        yield self.slaveDao.setSlaveOnlineStatus('slaveC', False)
                
        hitperiod = 180
        self.optmodel.hitratePeriod = 180
        
        for _i in range(hitperiod):
            self.hittracker.hit(vid1Id)
            self.hittracker.hit(vid2Id)
            self.hittracker.hit(vid3Id)
        
        snap = yield self.snapshotcreator.createSnapshot(self.optmodel)
        self.assertTrue('slaveC' not in snap.onlineSlaveNames)
        self.assertTrue('slaveC' in snap.allSlaveNames)
        self.assertTrue(snap.mapping.loc[vid1Id, 'popularity'] > 1.0)
        
        # popularity:
        # 180 times last minute, current-part is 180*3*0,75=405
        # 180 times last 30 minutes -> last-part is (180/30)*30*0.25=4.5
        # hits in time = (current-part+last-part)/180 = (405+4.5)/180 = 2.275 
        self.assertAlmostEquals(size * 2.275, snap.mapping.loc[vid1Id, 'totalbyteseconds'], 5)
        self.assertEquals(size, snap.mapping.loc[vid1Id, 'size'])
        
        # concurrent reqeusts on average: hits in time * video length
        self.assertEquals(2.275 * 60.0, snap.mapping.loc[vid1Id, 'concurrentRequests'])
        self.assertEquals(60, snap.mapping.loc[vid1Id, 'length'])
            
    @defer.inlineCallbacks
    def test_getVideoSlaveMapping(self):
        vid1Id = yield self.vidDao.addVideo('slaveA', domain.Video(name='vid1'))
        vid2Id = yield self.vidDao.addVideo('slaveA', domain.Video(name='vid2'))
        
        mapping = yield self.vidDao.getVideoSlaveMapping()
        self.assertEquals([(vid1Id, 'slaveA'), (vid2Id, 'slaveA')], mapping)
        


