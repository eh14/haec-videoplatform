'''
Created on Aug 4, 2014

@author: eh14
'''

import pandas
from twisted.trial import unittest
from haec.optimization import optmodel, snapshotcreator, strategies
import pythonioc
from haec.unittests.kplane import basetests
from twisted.internet import defer
from haec.kplane import domain
from haec.services import taskservice, activityservice, localtaskservice, \
    slaveconnservice, eventservice
from haec.unittests.services import servicemocks
from haec.unittests.protomixins import mixinhelpers
from haec.protomixins import psoptimizer
import json

class TestVideoDataMixin(basetests.BaseDbTest):
    videoDao = pythonioc.Inject('videoDao')
    slaveDao = pythonioc.Inject('slaveDao')
    
    snapshotcreator = pythonioc.Inject(snapshotcreator.SystemSnapshotCreator)
    optmodel = pythonioc.Inject(optmodel.OptModelProvider)

    clock = pythonioc.Inject('reactor')    
    slaveConnService = pythonioc.Inject(slaveconnservice.SlaveConnService)
    
    @defer.inlineCallbacks
    def setUp(self):
        basetests.BaseDbTest.setUp(self, False)
        yield self.slaveDao.deleteAll()
        yield self.videoDao.deleteAll()
        pythonioc.registerService(optmodel.OptModelProvider)
        pythonioc.registerService(snapshotcreator.SystemSnapshotCreator)
        
        self.strat = strategies._VideoDataMixin()
        
    @defer.inlineCallbacks
    def test_createAndSendVideoData(self):
        vidId = yield self.videoDao.addVideo("slave-1", domain.Video(name='vid1', fname='videofile.avi'),)
        yield self.slaveDao.setSlaveStatus("slave-1", 'online')
        servicemocks.registerSlaveConnService('slave-1')
        
        snapshot = yield self.snapshotcreator.createSnapshot(self.optmodel.model)
        yield self.strat._createVideoData(snapshot)
        
        self.strat.sendVideodataToSlaveByName('slave-1')
        self.clock.advance(1)
        
        msg = self.slaveConnService.getConnectedSlaveByName('slave-1').obj
        data = psoptimizer.VideoData(json.loads(msg.videoData))

        self.assertEquals(vidId, data._videoIds[0])
        self.assertEquals(1, data.getNumberOfVideos())
        
        
class TestNoStrategy(unittest.TestCase):
    def test_execute(self):
        """ 
        most of the methods are not implemented, so let's run them all here just
        to check for stupid typos
        """
        strat = strategies.NoStrategy()
        strat.deploy()
        strat.undeploy()
        strat.doOptimize()
        self.assertEquals(psoptimizer.MODE_DISABLED, strat.getPsoMode())

class TestAllOnStrategy(basetests.BaseDbTest):
    slaveDao = pythonioc.Inject('slaveDao')
    taskDao = pythonioc.Inject('taskDao')
    clock = pythonioc.Inject('reactor')
    eventService = pythonioc.Inject(eventservice.EventService)
    slaveConnService = pythonioc.Inject(slaveconnservice.SlaveConnService)
    
    @defer.inlineCallbacks
    def setUp(self):
        pythonioc.registerService(taskservice.TaskService)
        pythonioc.registerService(localtaskservice.LocalTaskService)
        pythonioc.registerService(activityservice.ActivityService)
        basetests.BaseDbTest.setUp(self, False)
        yield self.slaveDao.deleteAll()
        yield self.taskDao.deleteAll()
        self.strat = strategies.AllOnStrategy()
        
    @defer.inlineCallbacks
    def test_deploy(self):
        yield self.slaveDao.addSlave(domain.Slave(name='slave-1', status='offline'))
        yield self.slaveDao.addSlave(domain.Slave(name='slave-2', status='online'))
        servicemocks.registerSlaveConnService('slave-2')
        
        yield self.strat.deploy()
        self.clock.advance(1)
        self.assertIsInstance(self.slaveConnService.getConnectedSlaveByName('slave-2').obj, psoptimizer.PsoResetSlaveData)
        
        tasks = yield self.taskDao.getAllTasks()
        self.assertEquals(1, len(tasks))
        
        # at this point we should send the trigger for slave-1 but 
        # recreating the slave connection service does not work in the testing 
        # environment, so we'll just check for the existing slave.
        self.eventService.triggerEvent('slave-online', 'slave-2')
        self.clock.advance(1)
        self.assertIsInstance(self.slaveConnService.getConnectedSlaveByName('slave-2').obj, psoptimizer.PsoResetSlaveData)
        
    @defer.inlineCallbacks
    def test_undeploy(self):
        servicemocks.registerSlaveConnService('slave-1')
        yield self.strat.deploy()
        self.strat.undeploy()
        
        self.assertEquals(0, len(self.eventService._events['slave-online']))
        
    @defer.inlineCallbacks
    def test_doOptimize(self):
        yield self.slaveDao.addSlave(domain.Slave(name='slave-1', status='offline'))
        servicemocks.registerSlaveConnService()
        
        yield self.strat.doOptimize()
        
        self.clock.advance(1)
        
        tasks = yield self.taskDao.getAllTasks()
        self.assertEquals(1, len(tasks))
        
class TestAllOnBalancedStrategy(basetests.BaseDbTest):
    slaveDao = pythonioc.Inject('slaveDao')
    taskDao = pythonioc.Inject('taskDao')
    clock = pythonioc.Inject('reactor')
    eventService = pythonioc.Inject(eventservice.EventService)
    slaveConnService = pythonioc.Inject(slaveconnservice.SlaveConnService)
    
    @defer.inlineCallbacks
    def setUp(self):
        pythonioc.registerService(taskservice.TaskService)
        pythonioc.registerService(localtaskservice.LocalTaskService)
        pythonioc.registerService(snapshotcreator.SystemSnapshotCreator)
        pythonioc.registerService(optmodel.OptModelProvider)
        pythonioc.registerService(activityservice.ActivityService)
        
        basetests.BaseDbTest.setUp(self, False)
        yield self.slaveDao.deleteAll()
        yield self.taskDao.deleteAll()
        self.strat = strategies.AllOnBalancedStrategy()
        
    @defer.inlineCallbacks
    def test_deploy(self):
        yield self.slaveDao.addSlave(domain.Slave(name='slave-1', status='offline'))
        yield self.slaveDao.addSlave(domain.Slave(name='slave-2', status='online'))
        servicemocks.registerSlaveConnService('slave-2')
        
        yield self.strat.deploy()
        self.clock.advance(1)
        
        # one boot task
        tasks = yield self.taskDao.getAllTasks()
        self.assertEquals(1, len(tasks))
        
        # lets optimize so the slave data is available
        yield self.strat.doOptimize()
        
        # then trigger an event and check whether the data is being send.
        self.eventService.triggerEvent('slave-online', 'slave-2')
        self.clock.advance(1)
        self.assertIsInstance(self.slaveConnService.getConnectedSlaveByName('slave-2').obj, psoptimizer.PsoResetVideoData)
        
    @defer.inlineCallbacks
    def test_undeploy(self):
        servicemocks.registerSlaveConnService('slave-1')
        yield self.strat.deploy()
        self.strat.undeploy()
        
        self.assertEquals(0, len(self.eventService._events['slave-online']))
        
    @defer.inlineCallbacks
    def test_doOptimize(self):
        yield self.slaveDao.addSlave(domain.Slave(name='slave-1', status='offline'))
        servicemocks.registerSlaveConnService()
        
        yield self.strat.doOptimize()
        
        self.clock.advance(1)
        
        tasks = yield self.taskDao.getAllTasks()
        self.assertEquals(1, len(tasks))

class TestPopularityStrategy(basetests.BaseDbTest):
    snapshotcreator = pythonioc.Inject(snapshotcreator.SystemSnapshotCreator)
    optmodel = pythonioc.Inject(optmodel.OptModelProvider)
    hittracker = pythonioc.Inject('hitTracker')
    vidDao = pythonioc.Inject('videoDao')
    slaveDao = pythonioc.Inject('slaveDao')
    clock = pythonioc.Inject('reactor')
    taskDao = pythonioc.Inject('taskDao')
    taskService = pythonioc.Inject(taskservice.TaskService)
    
    @defer.inlineCallbacks
    def setUp(self):
        pythonioc.registerService(activityservice.ActivityService)
        pythonioc.registerService(optmodel.OptModelProvider)
        pythonioc.registerService(snapshotcreator.SystemSnapshotCreator)
        pythonioc.registerService(taskservice.TaskService)
        basetests.BaseDbTest.setUp(self, False)
        yield self.slaveDao.deleteAll()
        yield self.videoDao.deleteAll()
        yield self.taskDao.deleteAll()
        servicemocks.registerSlaveConnService('slaveA', 'slaveB')
        
        self.strat = strategies.PopularityStrategy()
        
    @defer.inlineCallbacks
    def test_getASlaves(self):
        # empty, no videos:
        snappi = yield self.snapshotcreator.createSnapshot(self.optmodel.model)
        self.strat._refreshASlaves(snappi)
        self.assertEquals([], self.strat._currentASlaves)
        self.assertEquals([], self.strat._currentSlavesWithAVids)
        
        slaves = yield self.slaveDao.getSlaves()
        self.assertEquals(0, len(slaves))
        self.assertDeferredEquals(0, self.videoDao.getTotalNumberOfVideos())
        
        size = 10 * 1024 * 1024
        vidId1 = yield self.vidDao.addVideo('slaveA', domain.Video(name='vid1', duration=60, fsize=size))
        vidId2 = yield self.vidDao.addVideo('slaveA', domain.Video(name='vid2', duration=60, fsize=size))
        vidId3 = yield self.vidDao.addVideo('slaveB', domain.Video(name='vid3', duration=60, fsize=size))
        yield self.vidDao.addVideo('slaveC', domain.Video(name='vid4', duration=60, fsize=size))

        yield self.slaveDao.setSlaveOnlineStatus('slaveA', True)
        yield self.slaveDao.setSlaveOnlineStatus('slaveB', True)
        
        # get the a-slaves
        snappi = yield self.snapshotcreator.createSnapshot(self.optmodel.model)
        self.strat._updateVideoThreshold(snappi)
        self.strat._refreshASlaves(snappi)
        # no hits, no a-slaves at all.
        self.assertEquals([], self.strat._currentASlaves)
        self.assertEquals([], self.strat._currentSlavesWithAVids)
        
        # hit videos 1 to 3
        self.hittracker.hit(vidId1)
        self.hittracker.hit(vidId2)
        self.hittracker.hit(vidId3)
            
        snappi = yield self.snapshotcreator.createSnapshot(self.optmodel.model)
        self.strat._updateVideoThreshold(snappi)
        self.strat._refreshASlaves(snappi)
        self.assertEquals(['slaveA'], self.strat._currentASlaves)
        self.assertEquals(set(['slaveA', 'slaveB']), set(self.strat._currentSlavesWithAVids))
        
        
    @defer.inlineCallbacks
    def test_createSlaveData(self):
        size = 10 * 1024 * 1024
        id1 = yield self.vidDao.addVideo('slaveA', domain.Video(name='vid1', duration=60, fsize=size))
        id2 = yield self.vidDao.addVideo('slaveA', domain.Video(name='vid2', duration=30, fsize=size))
        id3 = yield self.vidDao.addVideo('slaveA', domain.Video(name='vid3', duration=10, fsize=size))
        yield self.vidDao.addVideo('slaveB', domain.Video(name='vid3'))
        yield self.vidDao.addVideo('slaveC', domain.Video(name='vid3'))

        yield self.slaveDao.setSlaveOnlineStatus('slaveA', True)
        yield self.slaveDao.setSlaveOnlineStatus('slaveB', True)
        yield self.slaveDao.setSlaveOnlineStatus('slaveC', False)
                
        for _i in range(500):
            self.hittracker.hit(id1)
            self.hittracker.hit(id1)
            self.hittracker.hit(id1)
            self.hittracker.hit(id2)
            self.hittracker.hit(id2)
            self.hittracker.hit(id3)
            
        snappi = yield self.snapshotcreator.createSnapshot(self.optmodel.model)
        self.strat._updateVideoThreshold(snappi)
        data = yield self.strat._createSlaveClassData(['slaveA'], snapshot=snappi)
        
        self.assertEquals('A', data['slaveA']['class'])
        self.assertEquals('B', data['slaveB']['class'])
        self.assertEquals(False, data['slaveC']['online'])
        
        
    @defer.inlineCallbacks
    def test_optimize(self):
        
        # mixinhelpers.createAndRegisterMasterMock('slaveA', 'slaveB')
        
        size = 10 * 1024 * 1024
        yield self.vidDao.addVideo('slaveA', domain.Video(name='vid1', duration=60, fsize=size))
        yield self.vidDao.addVideo('slaveA', domain.Video(name='vid2', duration=30, fsize=size))
        yield self.vidDao.addVideo('slaveA', domain.Video(name='vid3', duration=10, fsize=size))
        yield self.vidDao.addVideo('slaveB', domain.Video(name='vid3'))
        yield self.vidDao.addVideo('slaveC', domain.Video(name='vid3'))

        yield self.slaveDao.setSlaveOnlineStatus('slaveA', True)
        yield self.slaveDao.setSlaveOnlineStatus('slaveB', True)
        yield self.slaveDao.setSlaveOnlineStatus('slaveC', False)
                
        for _i in range(500):
            self.hittracker.hit(1)
            self.hittracker.hit(1)
            self.hittracker.hit(1)
            self.hittracker.hit(2)
            self.hittracker.hit(2)
            self.hittracker.hit(3)
            
        yield self.strat.doOptimize()
        
        self.clock.advance(1)
        
        tasks = yield self.taskDao.getAllTasks()
        
        self.assertEquals(0, len(tasks))
        
    @defer.inlineCallbacks
    def test_createVideoData(self):
        size = 10 * 1024 * 1024
        
        yield self.vidDao.addVideo('slaveA', domain.Video(name='vid1', fname='vid1file', duration=60, fsize=size))
        yield self.vidDao.addVideo('slaveA', domain.Video(name='vid2', fname='vid2file', duration=30, fsize=size))
        yield self.vidDao.addVideo('slaveA', domain.Video(name='vid3', fname='vid3file', duration=10, fsize=size))
        yield self.vidDao.addVideo('slaveB', domain.Video(name='vid3'))
        yield self.vidDao.addVideo('slaveC', domain.Video(name='vid3'))
        
        yield self.slaveDao.setSlaveOnlineStatus('slaveA', True)
        yield self.slaveDao.setSlaveOnlineStatus('slaveB', True)
        yield self.slaveDao.setSlaveOnlineStatus('slaveC', False)
                
        for _i in range(500):
            self.hittracker.hit(1)
            self.hittracker.hit(1)
            self.hittracker.hit(1)
            self.hittracker.hit(2)
            self.hittracker.hit(2)
            self.hittracker.hit(3)
            
        snappi = yield self.snapshotcreator.createSnapshot(self.optmodel.model)
        self.strat._updateVideoThreshold(snappi)
        data = yield self.strat._createVideoData(snappi)
        
        self.assertEquals(3, data.index.size)
        
        
    @defer.inlineCallbacks
    def test_updateVideoThreshold(self):
        model = self.optmodel.model
       
        # fix this number so we can work with the results here and
        # don't need to duplicate calculations. 
        model.recentHitPeriod = 120
        
        # no videos, no threshold
        snappi = yield self.snapshotcreator.createSnapshot(model)
        self.strat._updateVideoThreshold(snappi)
        self.assertEquals(0, model.videoPopularityThreshold)
        
        # add some slaves and videos
        size = 10 * 1024 * 1024
        id1 = yield self.vidDao.addVideo('slaveA', domain.Video(name='vid1', fname='vid1file', duration=60, fsize=size))
        yield self.vidDao.addVideo('slaveA', domain.Video(name='vid2', fname='vid2file', duration=60, fsize=size))
        yield self.slaveDao.setSlaveOnlineStatus('slaveA', True)
        
        # if we only have the same count of hits, all belong to class B
        snappi = yield self.snapshotcreator.createSnapshot(model)
        self.strat._updateVideoThreshold(snappi)
        self.assertEquals(1, model.videoPopularityThreshold)
                
        self.hittracker.hit(id1)
        
        snappi = yield self.snapshotcreator.createSnapshot(model)
        self.strat._updateVideoThreshold(snappi)
        self.assertAlmostEquals(1.51666 * model.classBVideoFraction, model.videoPopularityThreshold, 2)
        
    def test_sendVideoDataToSlave_jsonfail(self):
        """
        For bigger values, the json-convert failed. Test for regression.
        """
        
        
        self.strat._currentVideoData = pandas.DataFrame({'fname':['testsize'],
                                                         'size':[453434993L],
                                                         'totalbyteseconds':[34343434L],
                                                         'someslave':[1L],
                                                         'popularity':1L},
                                                        index=[1L])
        # print self.strat._currentVideoData['size'].dtype
        slave = mixinhelpers.SlaveConnectionMock('someslave')
        self.strat._sendVideoDataToSlave(slave)
        
        # print slave.obj
