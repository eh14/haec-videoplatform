'''

@author: eh14
'''
from haec.services import slaveconnservice
from haec.unittests.protomixins import mixinhelpers
import pythonioc
from twisted.internet import defer


def registerSlaveConnService(*slaveNames):
    service = slaveconnservice.SlaveConnService()
    for name in slaveNames:
        service.slaveConnected(mixinhelpers.SlaveConnectionMock(name))
        
    pythonioc.registerServiceInstance(service, 'slaveConnService', True)
    return service
    
class JobSchedulerMock(object):
    def __init__(self):
        self.deferredJobs = []
        
    def startJob(self, job):
        d = defer.Deferred()
        self.deferredJobs.append((d, job))
        
        return d
    
    def reset(self):
        self.deferredJobs = []
        
class CambrionixConnectorMock(object):
    """
    This mock does basically nothing, but prevents some jobs from
    failing due to unregistered services etc.
    """
    
    def restrictProfiles(self):
        pass
    
    def getNodeState(self):
        pass
    
    def turnOffNode(self, nodeName):
        pass
    
    def turnOnNode(self, nodeName):
        pass

    def getNetworkTopology(self):
        pass
    
class RealCambrionixConnectorMock(object):
    def __init__(self, cambrionixmock):
        self._camb = cambrionixmock
        
    def restrictProfiles(self):
        pass
    
    def getNodeState(self):
        return self._camb.getNodeStates()['data']
    
    def turnOffNode(self, nodeName):
        return self._camb.nodeDeactivate(nodeName)
    
    def turnOnNode(self, nodeName):
        return self._camb.nodeActivate(nodeName)

    def getNetworkTopology(self):
        return self._camb.getNetworkTopology()['data']
    
class TaskServiceMock(object):
    
    def wakeup(self):
        pass