'''

@author: eh14
'''
from haec.unittests.kplane import basetests
import pythonioc
from haec.services import activityservice, localtaskservice, taskservice
from twisted.internet import defer
from haec.kplane import domain
from haec.unittests import testutils
from haec.optimization import optmodel
from haec.unittests.services import servicemocks


class TestActivityService(basetests.BaseDbTest):

    actService = pythonioc.Inject(activityservice.ActivityService)
    slaveDao = pythonioc.Inject('slaveDao')
    clock = pythonioc.Inject('reactor')
    timeProvider = pythonioc.Inject('timeProvider')
    taskService = pythonioc.Inject(taskservice.TaskService)
    taskDao = pythonioc.Inject('taskDao')
    
    @defer.inlineCallbacks
    def setUp(self):
        basetests.BaseDbTest.setUp(self, False)
        pythonioc.registerService(activityservice.ActivityService)
        pythonioc.registerService(optmodel.OptModelProvider)
        pythonioc.registerService(localtaskservice.LocalTaskService)
        pythonioc.registerService(taskservice.TaskService)
        servicemocks.registerSlaveConnService('someslave')
        pythonioc.registerServiceInstance(testutils.FixedTimeProvider(), 'timeProvider', overwrite=True)
        
        yield self.clearDatabase()
    
    @defer.inlineCallbacks
    def test_updateSlavePurityAndActivity(self):
        yield self.slaveDao.addSlave(domain.Slave(name='someslave', status='online'))
        self.actService.updateSlavePurity('someslave')
        self.actService.updatePopClasses({'someslave':'A'})
        
        self.assertEquals(True, self.actService._slaveActivities['someslave'].pure)
        self.assertEquals('A', self.actService._slaveActivities['someslave'].getClass())
        self.assertEquals(False, self.actService._slaveActivities['someslave'].isIdle())
        slave = yield self.slaveDao.getSlaveByName('someslave')
        self.assertEquals('online', slave.status)
        
        
    @defer.inlineCallbacks
    def test_updateSlaveActivity(self):
        yield self.slaveDao.addSlave(domain.Slave(name='someslave', status='online'))
        self.actService.updateSlaveActivity('someslave', 100)
        self.assertEquals(False, self.actService._slaveActivities['someslave'].isIdle())
        self.assertEquals(100, self.actService._slaveActivities['someslave']._streams)
        
        
    @defer.inlineCallbacks
    def test_checkSlaveShutdown(self):
        yield self.slaveDao.addSlave(domain.Slave(name='someslave', status='online'))
        self.actService.updateSlavePurity('someslave')
        self.actService.updateSlaveActivity('someslave', 0)
        slave = yield self.slaveDao.getSlaveByName('someslave')
        self.assertEquals(True, slave.online)
        
        yield self.actService._checkSlaveShutdown('someslave')
        self.timeProvider.advance(100)
        yield self.actService._checkSlaveShutdown('someslave')
        self.clock.advance(100)
        
        def postCheck():
            slave = yield self.slaveDao.getSlaveByName('someslave')
            self.assertEquals(False, slave.online)
            self.assertEquals('shutdown', slave.status)
        yield testutils.doDeferredCheck(0.5, postCheck)
        
