'''
Created on Jun 17, 2014

@author: eh14
'''
from haec import config
from twisted.trial import unittest
from twisted.internet import defer
from haec.services import cambrionixservice
from haec.services.cambrionixservice import CambrionixException
from haec.unittests.kplane import basetests
import pythonioc

cfg = pythonioc.Inject(config.Config)
    
class TestCambrionixConnector(basetests.BaseDbTest, basetests.CambrionixMockMixin):
    
    def setUp(self):
        basetests.BaseDbTest.setUp(self)
        self.startCambrionix()
        
    def tearDown(self):
        self.stopCambrionix()

    @defer.inlineCallbacks
    def test_getNodeState(self):
        
        nodesState = yield self.cambrionixConnector.getNodeState()
        
        self.assertEquals('nodes-state', nodesState['code'])
        self.assertEquals('124', nodesState['data']['nodeA']['uptime'])
        
    @defer.inlineCallbacks
    def test_turnOnOffNode(self):
        self.assertDeferredRaises(CambrionixException, self.cambrionixConnector.turnOffNode, 'somenode')
        yield self.cambrionixConnector.turnOffNode('nodeA')
        nodeState = yield self.cambrionixConnector.getNodeState()
        self.assertEquals('0', nodeState['data']['nodeA']['status'])
        yield self.cambrionixConnector.turnOnNode('nodeA')
        nodeState = yield self.cambrionixConnector.getNodeState()
        self.assertEquals('1', nodeState['data']['nodeA']['status'])
        
    @defer.inlineCallbacks
    def test_getNetworkTopology(self):
        topo = yield self.cambrionixConnector.getNetworkTopology()
        
        self.assertEquals(1, len(topo['switch-S']))
        self.assertEquals('nodeA', topo['switch-S'][0])

class TestCambrionixConnector_offline(unittest.TestCase):
    def setUp(self):
        cfg.change(cambrionix_url='tcp://127.0.0.1:10059')
        self.cambrionixConnector = cambrionixservice.CambrionixConnector()
        
    @defer.inlineCallbacks
    def test_downCambrionix(self):
        """
        Tests the connector's behavior when the cambrionix is down
        """
        
        try:
            _result = yield self.cambrionixConnector.getNodeState()
            self.fail('this should raise an exception')
        except cambrionixservice.CambrionixException:
            # ok
            pass
        
class TestCambrionixService(basetests.BaseDbTest, basetests.CambrionixMockMixin):
    
    cambService = pythonioc.Inject(cambrionixservice.CambrionixService)
    timeProvider = pythonioc.Inject('timeProvider')
    sensorStore = pythonioc.Inject('sensorStore')
    def setUp(self):
        basetests.BaseDbTest.setUp(self, True)
        self.sensorStore.initBuffers({'slave-s':['nodeA']})
        pythonioc.registerService(cambrionixservice.CambrionixService)

        self.startCambrionix()
        
    def tearDown(self):
        self.stopCambrionix()
    
    @defer.inlineCallbacks
    def test_readCambrionixValues(self):
         
        self.assertEquals(0, self.sensorStore.getNumPowerValues())
        yield self.cambService.readCambrionixValues(False)
        yield self.cambService.readCambrionixValues(False)
        
        self.sensorStore.sampleValues(self.timeProvider.now())
        
        # make sure the values are in the database at least once.
        self.assertTrue(1 <= self.sensorStore.getNumPowerValues())
         
