from twisted.internet import reactor, defer
from twisted.trial import unittest
from twisted.web import server

from haec.services import videostreamer, activitytracker
from haec import config
import os
import pythonioc
from twisted.web.client import getPage
from haec.unittests import testutils
video_dir = os.path.join(os.path.dirname(__file__), '../protomixins/testvideos')

cfg = pythonioc.Inject(config.Config)

class R(object):
    pass

class TestCORSResource(unittest.TestCase):
    
    timeout = 3
    
    actTracker = pythonioc.Inject(activitytracker.ActivityTracker)
    
    def setUp(self):
        pythonioc.registerService(activitytracker.ActivityTracker)
        cfg.change(name='testname', video_dir=video_dir)
        root = videostreamer.CORSResource(cfg.vals.video_dir)
        self.listener = reactor.listenTCP(0, server.Site(root))  # @UndefinedVariable
        
        self.addCleanup(self.listener.stopListening)
        
        # clean the current streams
        self.actTracker.clear()
        
    def _getBaseUrl(self):
        return 'http://127.0.0.1:%s/' % self.listener.getHost().port
    
    def _getFileUrl(self, name):
        return self._getBaseUrl() + name

    @defer.inlineCallbacks    
    def test_getName(self):
        """
        tests the special "name"-url of the fileserver that 
        simply returns the name of the node for integration testing purposes.
        """
        response = yield getPage(self._getFileUrl('__get_hostname'))
        self.assertEquals('testname', response)
        
    @defer.inlineCallbacks 
    def test_streaming(self):
        yield getPage(self._getFileUrl('tiny_test.mp4'))
        self.assertTrue('tiny_test.mp4' in self.actTracker._currentStreams)
        
    @defer.inlineCallbacks
    def test_VideoMoved(self):
        
        r = R()
        self.actTracker.streamStarted('tiny_test.mp4', r)
        
        callbackCalled = [False]
        def callback(result):
            callbackCalled[0] = True
        d = self.actTracker.blockVideo('tiny_test.mp4')
        d.addCallback(callback)
        response = yield testutils.getPage(self._getFileUrl('tiny_test.mp4'))
        self.assertEquals(301, response.code)
        self.assertFalse(callbackCalled[0])
        
        self.actTracker.streamFinished('tiny_test.mp4', r)
        self.assertTrue(callbackCalled[0])
        
