'''

@author: eh14
'''
from haec.unittests.kplane import basetests
import pythonioc
from haec.kplane import domain
from haec.services import profileservice
from twisted.internet import defer
from haec import utils


class TestProfileService(basetests.BaseDbTest):
    
    db = pythonioc.Inject('dbprovider')
    profService = pythonioc.Inject(profileservice.ProfileService)
    reactor = pythonioc.Inject('reactor')
    def setUp(self):
        basetests.BaseDbTest.setUp(self, False)
        
    @defer.inlineCallbacks
    def test_initProfiles(self):
        with self.db() as db:
            db.query(domain.Profile).delete()
        
        profs = yield self.profService.getProfileDictList()
        self.assertTrue(len(profs) > 0)
        
    def test_getQualityModes(self):
        m = self.profService.getQualityModes()
        self.assertTrue(len(m) > 0)
        
    @defer.inlineCallbacks
    def test_getProfileForId(self):
        profs = yield self.profService.getProfileDictList()
        
        prof = yield self.profService.getProfileForId(profs[0]['id'])
        self.assertEquals(profs[0]['name'], prof.name)
        
    @defer.inlineCallbacks
    def test_getProfileForNameOrId(self):
        profs = yield self.profService.getProfileDictList()

        name = yield self.profService.getProfileForNameOrId(profileservice.ProfileService.makeShortName(profs[0]['name']))
        idName = yield self.profService.getProfileForNameOrId(profs[0]['id'])
        
        self.assertEquals(name, idName)
        