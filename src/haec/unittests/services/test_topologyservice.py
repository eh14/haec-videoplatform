from twisted.trial import unittest
from haec.unittests.services import cambrionixmock, servicemocks
from haec.services import topologyservice, eventservice
from twisted.internet import defer
import pythonioc
from haec.unittests import testutils



class TestTopologyService(unittest.TestCase):
    cambrionixConnector = pythonioc.Inject('cambrionixConnector')
    timeProvider = pythonioc.Inject('timeProvider')
    
    
    def setUp(self):
        pythonioc.cleanServiceRegistry()
        pythonioc.registerServiceInstance(testutils.FixedTimeProvider(), 'timeProvider', overwrite=True)
        pythonioc.registerService(eventservice.EventService)
        self._camb = cambrionixmock.CambrionixMock(['slave-a', 'slave-b'])
        pythonioc.registerServiceInstance(servicemocks.RealCambrionixConnectorMock(self._camb), 'cambrionixConnector', overwrite=True)
        self._topo = topologyservice.TopologyService()
        
    @defer.inlineCallbacks
    def test_initTopology(self):
        self.assertEquals(0, len(self._topo._topoNodes))
        yield self._topo._initTopology()
        
        self.assertEquals(3, len(self._topo._topoNodes), str(self._topo._topoNodes))
        self.assertTrue('slave-a' in self._topo._topoNodes)
        self.assertTrue('slave-b' in self._topo._topoNodes)
        
    @defer.inlineCallbacks
    def test_updateNodeStates(self):
        yield self._camb.deactivateAllNodes()
        
        yield self._topo._initTopology()
        yield self._topo.updateNodeStates(self.cambrionixConnector.getNodeState())
        
        self.assertTrue(all([bool(n.status) == False for n in self._topo._topoNodes.itervalues()]))
        
        self._camb.nodeActivate('slave-a')
        self._camb.nodeActivate('switch-S')
        
        yield self._topo.updateNodeStates(self.cambrionixConnector.getNodeState())
        self.assertTrue(self._topo._topoNodes['slave-a'].status)
        self.assertTrue(self._topo._topoNodes['switch-S'].status)
        
        # now turn off slave-a, the switch should be turned off automatically
        yield self._camb.nodeDeactivate('slave-a')
        # update the node-states. Should turn off the switch
        yield self._topo.updateNodeStates(self.cambrionixConnector.getNodeState())
        
        # now update again, so the offline switch propagates to the topology-service.
        yield self._topo.updateNodeStates(self.cambrionixConnector.getNodeState())
        
        self.assertTrue(all([bool(n.status) == False for n in self._topo._topoNodes.itervalues()]))
        
    @defer.inlineCallbacks
    def test_activateNode(self):
        yield self._camb.deactivateAllNodes()
        yield self._topo._initTopology()
        
        # all should be off
        yield self._topo.updateNodeStates(self.cambrionixConnector.getNodeState())
        self.assertTrue(all([bool(n.status) == False for n in self._topo._topoNodes.itervalues()]))

        # turn on slave-a        
        yield self._topo.activateNode('slave-a')
        
        # switch-S should be turned on automatically
        yield self._topo.updateNodeStates(self.cambrionixConnector.getNodeState())
        self.assertTrue(self._topo._topoNodes['slave-a'].status)
        self.assertTrue(self._topo._topoNodes['switch-S'].status)
        self.assertFalse(self._topo._topoNodes['slave-b'].status)
        
    def test_status_setstatus(self):
        s = topologyservice._Status()
        self.assertFalse(s.status)
        
        s.setStatus(True)
        self.assertTrue(s.status)
        
    def test_status_isChanging(self):
        s = topologyservice._Status()
        
        self.assertFalse(s.isChanging())
        s.markChange()
        self.assertTrue(s.isChanging())
        
        # go to the future
        self.timeProvider.advance(s.CHANGE_TIMEOUT + 1)
        self.assertFalse(s.isChanging())
        
    def test_setStatus(self):
        s = topologyservice._Status()

        s.markChange()        
        self.assertTrue(s.isChanging())
        
        # set same status, should still be "changing"
        s.setStatus(False)
        self.assertTrue(s.isChanging())
        
        # change status, reset the changing.
        s.setStatus(True)
        self.assertFalse(s.isChanging())
        
        
    @defer.inlineCallbacks
    def test_getTopology(self):
        yield self._topo._initTopology()
        
        self.assertEquals({'switch-S':['slave-a', 'slave-b']},
                          self._topo.getTopology())
        
