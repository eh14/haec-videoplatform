from twisted.trial import unittest
from haec.services import valueaggregator


class FirstAggregator(valueaggregator.ValueAggregator):
    """
    Simple test aggregator, that uses the first value...
    """
    def aggregate(self, values, *args, **kwargs):
        return values[0]

class TestValueAggregator(unittest.TestCase):

        
    def test_Valuebuffer_getLast(self):
        
        buf = valueaggregator.ValueBuffer(default='kk')
        self.assertEquals('kk', buf.getLast())
        buf.setValue("dd")
        self.assertEquals('dd', buf.getLast())
        
    
    def test_flush(self):
        buf = valueaggregator.ValueBuffer()
        agg = valueaggregator.ValueAggregator(100, [buf])
        
        self.assertEquals(0, agg.getLast())
        
        
    def test_baseClass(self):
        buf = valueaggregator.ValueBuffer()
        agg = valueaggregator.ValueAggregator(100, [buf])
        self.assertRaises(NotImplementedError, agg.flush, 1)
        
    def test_moreFlush(self):
        buf = valueaggregator.ValueBuffer(maxMisses=2)
        agg = FirstAggregator(3, [buf])
        
        buf.setValue(123)
        self.assertEquals(0, agg.getLast())
        agg.flush(1)
        self.assertEquals(123, agg.getLast())
        self.assertEquals([(1, 123)], agg.getAll())
        
        agg.flush(2)
        self.assertEquals([(1, 123), (2, 123)], agg.getAll())
        agg.flush(3)
        self.assertEquals([(1, 123), (2, 123), (3, 123)], agg.getAll())
        
        # the next flush will return the default value for the buffer,
        # since we set it to 2 max misses.
        agg.flush(4)
        self.assertEquals([(2, 123), (3, 123), (4, 0)], agg.getAll())
        
        
    def test_AvgAggregator(self):
        buf1 = valueaggregator.ValueBuffer()
        buf2 = valueaggregator.ValueBuffer()
        
        agg = valueaggregator.AvgAggregator(2, [buf1, buf2])
        
        buf1.setValue(1)
        buf2.setValue(3)
        agg.flush(1)
        self.assertEquals(2, agg.getLast())

    def test_HashCollectingAggregator(self):
        buf1 = valueaggregator.ValueBuffer()
        buf2 = valueaggregator.ValueBuffer()
        
        agg = valueaggregator.HashCollectingAggregator(2, [buf1, buf2], sourceNames=['buf1', 'buf2'])
        
        buf1.setValue(5)
        buf2.setValue(6)
        agg.flush(15)
        
        self.assertEquals({'buf1':5, 'buf2':6}, agg.getLast())