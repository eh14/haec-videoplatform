'''

@author: eh14
'''
from haec.unittests.kplane import basetests
from haec.protomixins import sensors
import pythonioc
from haec.services import sensorservice
from haec import config
from haec.unittests.protomixins import mixinhelpers
from twisted.trial import unittest
from haec.unittests import testutils
from haec.kplane import sensorstore

cfg = pythonioc.Inject(config.Config)


class TestMasterSensorService(unittest.TestCase, mixinhelpers.ProtocolMixinTestMixin):
    
    sensorService = pythonioc.Inject(sensorservice.MasterSensorService)
    sensorStore = pythonioc.Inject('sensorStore')
    timeProvider = pythonioc.Inject('timeProvider')
    
    def _makeSensorEntry(self, **kwargs):
        return {k:0 if k not in kwargs else kwargs[k] 
                    for k in self.sensorStore._sensorTypes}
    
    def setUp(self):
        testutils.setUpIocRegistry()
        self.setUpMixin(sensors.SlaveSensorsMixin)
        pythonioc.registerService(sensorstore.SensorStore)
        pythonioc.registerService(sensorservice.MasterSensorService)
        self.sensorStore.initBuffers({'switch-S':['slaveA', 'slaveB', 'slaveC', 'slaveD']})
        
#     def test_getPowerValues(self):
#         self.sensorStore.addPowerValues(self.timeProvider.now(), {'slaveA':{'current':'340', 'energy':'123',
#                                                   'uptime':'12',
#                                                   'status':'0'
#                                                   },
#                                         'slaveB':{'current':'340', 'energy':'123',
#                                                   'uptime':'12',
#                                                   'status':'0'
#                                                   }
#                                         }
#                                   )
#         
#         self.sensorStore.addPowerValues(self.timeProvider.now() + 1, {'slaveB':{'current':'340', 'energy':'123',
#                                                   'uptime':'12',
#                                                   'status':'0'
#                                                   },
#                                         }
#                                   )
#         
#         
#         pv = self.sensorService.getPowerValues(self.timeProvider.now(), ['slaveA'])
#         self.assertEquals(1, len(pv))
#         self.assertEquals(1, len(pv['slaveA']))
#         
#         pv = self.sensorService.getPowerValues(self.timeProvider.now(), ['slaveA', 'slaveB'])
#         self.assertEquals(2, len(pv))
#         self.assertEquals(1, len(pv['slaveA']))
#         self.assertEquals(2, len(pv['slaveB']))
#         
#     def test_getAggregatedPowerValues(self):
#         values = self.sensorService.getAggregatedPowerValues(0)
#         self.assertEquals([], values)
#         
#         
#         self.sensorStore.addPowerValues(self.timeProvider.now(), {'slaveA':{'current':'340', 'energy':'123',
#                                                   'uptime':'12',
#                                                   'status':'0'
#                                                   },
#                                         'slaveB':{'current':'340', 'energy':'123',
#                                                   'uptime':'12',
#                                                   'status':'0'
#                                                   },
#                                         'slaveC':{'current':'340', 'energy':'123',
#                                                   'uptime':'12',
#                                                   'status':'0'
#                                                   }
#                                         }
#                                   )
#         
#         values = self.sensorService.getAggregatedPowerValues(self.timeProvider.now())
#         
#         self.assertEquals(340 * 3, values[0]['current'])
#         
#     def test_getRecentSensorValues(self):
#         self.sensorStore.addSensorValues('slaveA', self._makeSensorEntry(cpu_usage=100))
#         self.sensorStore.addSensorValues('slaveB', self._makeSensorEntry(cpu_usage=5))
#         self.sensorStore.addSensorValues('slaveC', self._makeSensorEntry(cpu_usage=20))
#         self.sensorStore.sampleValues(self.timeProvider.now())
#         
#         lastValues = self.sensorStore.slaveValues.getLast()
#         self.assertEquals(100, lastValues['slaveA']['cpu_usage'])        
#         self.assertEquals(None, lastValues['slaveD']['cpu_usage'])        
#         
#     def test_getRecentPowerValues(self):
#         self.sensorStore.addPowerValues(self.timeProvider.now(), {'slaveA':{'current':'340', 'energy':'123',
#                                                   'uptime':'12',
#                                                   'status':'0'
#                                                   },
#                                         'slaveB':{'current':'341', 'energy':'124',
#                                                   'uptime':'13',
#                                                   'status':'0'
#                                                   },
#                                         }
#                                   )
#         
#         self.sensorStore.addPowerValues(self.timeProvider.now() + 1, {'slaveA':{'current':'342', 'energy':'125',
#                                                   'uptime':'16',
#                                                   'status':'0'
#                                                   },
#                                                 'slaveB':{'current':'345', 'energy':'125',
#                                                   'uptime':'14',
#                                                   'status':'0'
#                                                   },
#                                         }
#                                   )
#         
#         powerData = self.sensorStore.getRecentPowerValues()
#         
#         self.assertEquals(2, len(powerData))
#         self.assertEquals(14, powerData['slaveB']['uptime'])
#         self.assertEquals(342, powerData['slaveA']['current'])
#    
