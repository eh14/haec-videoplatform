from twisted.trial import unittest
from haec.services import jobscheduler

import subprocess
from twisted.internet import reactor, defer
from haec.unittests import testutils
import pythonioc
import time
from haec import config

class BlockingJob(jobscheduler.ThreadedJob):
    """
    Test job that calls sleep and waits for it to return.
    It also tracks how many times it's being executed.
    """
    RunningJobs = [0]
    def _executeThreaded(self):
        self.p = subprocess.Popen(['sleep', '0.1'])
        self.RunningJobs[0] += 1
        defThreadPool = reactor.getThreadPool()  # @UndefinedVariable
        if self.RunningJobs[0] > defThreadPool.max:
            raise AssertionError('too many jobs running')
        self.p.wait()
        self.RunningJobs[0] -= 1
    
    def stop(self):
        self.p.kill()
        
        
class AddElementJob(jobscheduler.ThreadedJob):
    """
    Simple Testjob that adds passed element to passed list, waiting passed time
    before and afterwards.
    Used to test race conditions within the job scheduler.
    """
    def __init__(self, target, element, sleepTime):
        self.target = target
        self.element = element
        self.sleepTime = sleepTime
        
    def _executeThreaded(self):
        time.sleep(self.sleepTime)
        self.target.append(self.element)
        time.sleep(self.sleepTime)
        
    def __str__(self):
        return "adding %s (sleep %f)" % (self.element, self.sleepTime)

class SelfCancellingJob(jobscheduler.Job):
    def run(self):
        raise jobscheduler.JobCancelled()
    
    
class TestJobScheduler(unittest.TestCase):
    clock = pythonioc.Inject('reactor')
    timeout = 5
    cfg = pythonioc.Inject(config.Config)
    
    def setUp(self):
        testutils.setUpIocRegistry()
        
    def testSchedulerThreadPool(self):
        """
        run the scheduler with twice as many jobs, as the
        thread pool allows to run in parallel. 
        This test is supposed to ensure that the scheduler does not start
        more jobs than threads in the pool.
        
        *** 
            This test case is somewhat useless since we should trust the
            threadpool and the scheduler does not defer to threads by default
            anyway, so the responsibility is on the threaded job, not the scheduler.
        ***
        
        
        """
        
        sched = jobscheduler.JobScheduler()
        
        defThreadPool = reactor.getThreadPool()  # @UndefinedVariable
        defs = []
        for i in range(defThreadPool.max * 2):
            df = sched.startJob(BlockingJob())
            df.addBoth(lambda result: self.clock.advance(5))
            defs.append(df)
            
        self.clock.advance(10)
        return defer.DeferredList(defs)
    
    @defer.inlineCallbacks
    def test_compositeJob(self):
        target = []
        sleepTime = 0.03
        number = 7
        jobs = [AddElementJob(target, i, sleepTime) for i in range(number)]
        job = jobscheduler.CompositeJob(*jobs)
        sched = jobscheduler.JobScheduler()
        d = sched.startJob(job)
        self.clock.advance(10)
        yield d
        
        self.assertEquals(number, len(target))
        for i in range(len(target) - 1):
            self.assertTrue(target[i] < target[i + 1])
            
            
    @defer.inlineCallbacks
    def test_cancelledJob(self):
        sched = jobscheduler.JobScheduler()
        d = sched.startJob(SelfCancellingJob())
        self.clock.advance(10)
        try:
            yield d
            self.fail('expected to be cancelled')
        except defer.CancelledError as e:
            pass
        
    def test_jobCancel(self):
        sched = jobscheduler.JobScheduler()
        d = sched.startJob(jobscheduler.Job())
        
        cancelled = [False]
        def errback(reason):
            reason.trap(defer.CancelledError)
            cancelled[0] = True
            
        d.addErrback(errback)
        self.assertEquals(1, len(sched._waiting))
        self.assertEquals(0, len(sched._running))
        self.assertEquals(0, len(sched._done))
        d.cancel()
        
        self.assertTrue(cancelled[0])
        self.assertEquals(0, len(sched._waiting))
        self.assertEquals(0, len(sched._running))
        self.assertEquals(0, len(sched._done))
        
    @defer.inlineCallbacks
    def test_compositeJob_failing_regression(self):
        self.cfg.change(scheduler_job_waitinterval=0)
        # for this test we need the real reactor
        pythonioc.registerServiceInstance(reactor, 'reactor', overwrite=True)
        
        # failing job
        def fail():
            raise RuntimeError()
        
        # succeeding job
        def success():
            pass
        
        job = jobscheduler.CompositeJob(jobscheduler.SyncFunctionJob(fail), jobscheduler.SyncFunctionJob(success))
        sched = jobscheduler.JobScheduler()
        
        try:
            yield sched.startJob(job)
            self.fail('should fail')
        except RuntimeError:
            pass
            
        sched.shutdown()