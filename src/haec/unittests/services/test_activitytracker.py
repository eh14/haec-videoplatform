'''

@author: eh14
'''
from twisted.trial import unittest
from haec.services import activitytracker
import gc

class R(object):
    pass
class TestActivityTracker(unittest.TestCase):
    
    
    def activity(self):
        self.activity += 1
    def setUp(self):
        self.tracker = activitytracker.ActivityTracker()
        self.tracker.setActivityCallback(self.activity)
        self.activity = 0
        
    def test_streamStarted(self):
        r = R()
        
        self.assertEquals(0, self.activity)
        self.tracker.streamStarted('somefile', r)
        
        self.assertEquals(1, self.activity)
        self.assertEquals(1, self.tracker.sumStreams())

        
        
    def test_streamFinished(self):
        
        r = R()
        
        self.assertEquals(0, self.activity)
        self.tracker.streamStarted('somefile', r)
        
        self.tracker.streamFinished('somefile', r)

        self.assertEquals(0, self.tracker.sumStreams())
        self.assertEquals(2, self.activity)
        
        
    def test_cleanZero(self):
        r = R()
        
        self.assertEquals(0, self.activity)
        self.tracker.streamStarted('somefile', r)
        
        self.assertEquals(1, self.activity)
        self.assertEquals(1, self.tracker.sumStreams())

        # deleting it does not kick the activity-callback        
        del r
        self.assertEquals(0, self.tracker.sumStreams())
        self.assertEquals(1, self.activity)
        
        # but running the cleaner should
        self.tracker.cleanZero()
        self.assertEquals(2, self.activity)

    def test_blockVideo(self):
        d = self.tracker.blockVideo('not-blocked')
        self.assertTrue(d.called)
        self.assertEquals('not-blocked', d.result)
        
        # case (1), the request is finished appropriately
        r = R()
        self.tracker.streamStarted('blocked', r)
        d = self.tracker.blockVideo('blocked')
        # not called yet, it's blocked.
        self.assertFalse(d.called)
        self.tracker.streamFinished('blocked', r)
        self.assertTrue(d.called)
        
        # case (2), the request is not finished right, but cleaned up later
        r2 = R()
        self.tracker.streamStarted('blocked', r2)
        d = self.tracker.blockVideo('blocked')
        self.assertFalse(d.called)
        del r2
        self.assertFalse(d.called)
        self.tracker.cleanZero()
        self.assertTrue(d.called)
