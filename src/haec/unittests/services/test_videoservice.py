'''

@author: eh14
'''
from haec.services import videoservice, jobscheduler, mastervideoservice
import pythonioc
from haec.unittests import testutils
from haec import config, utils
import os
import shutil
from twisted.trial import unittest
from twisted.internet import defer, reactor
from haec.unittests.kplane import basetests
from haec.kplane import domain
import itertools
import uuid
from haec.optimization import snapshotcreator, optmodel
import time

cfg = pythonioc.Inject(config.Config)

_here = os.path.dirname(__file__)

class TestSlaveVideoService(unittest.TestCase):
    timeout = 3
    
    slaveVideoService = pythonioc.Inject(videoservice.SlaveVideoService)
    clock = pythonioc.Inject('reactor')
    
    def setUp(self):
        testutils.setUpIocRegistry()
        pythonioc.registerService(videoservice.SlaveVideoService)
        pythonioc.registerServiceInstance(jobscheduler.JobScheduler(maxRunning=1), 'taskRunner', True)
        
        
    def test_scanVideoDir(self):
        cfg.change(video_dir=os.path.join(_here, '..', 'protomixins/testvideos'))
        vids = self.slaveVideoService._scanVideoDir()
        self.assertTrue('tiny_test.mp4' in [v['fileName'] for v in vids])
        self.assertTrue('tiny_test__id1234.mp4' in [v['fileName'] for v in vids])
    
    
    def test_parseVideoName(self):
        # source name
        self.assertEquals({'name':'abc', 'variant':None, 'extension':'def', 'fileName':'abc.def'},
                          self.slaveVideoService._parseVideoName('abc.def'))
        
        # derived name (with id)
        data = self.slaveVideoService._parseVideoName('abc__id1234.mp4')
        self.assertEquals('abc', data['name'])
        self.assertEquals('1234', data['variant'])
        
    def test_parseVideoName_noVariant(self):
        data = self.slaveVideoService._parseVideoName('abc.mp4')
        self.assertIsNone(data['variant'])
        
    def test_parseVideoName_regression1(self):
        """
        regression test for video
        marguerite_ii_640x360.mp4 which could not be parsed.
        """
        data = self.slaveVideoService._parseVideoName('marguerite_ii_640x360.mp4')
        self.assertEquals('marguerite_ii_640x360', data['name'])
        
    def test_uniquifyVideoDir(self):
        scandir = '/tmp/' + str(uuid.uuid4())[:8]
        
            
        # set up the scandir
        cfg.change(video_dir=scandir)
        if os.path.exists(scandir):
            shutil.rmtree(scandir)
        os.mkdir(scandir)
        
        # create couple of videos
        def touch(name):
            open(os.path.join(scandir, name), 'a').close()
        touch('vid1.mp4')
        touch('vid2.mp4')
        touch('vid2.avi')
        touch('vid3__idthevariant.someext')
        
        vids = self.slaveVideoService._scanVideoDir()
        self.assertEquals(4, len(vids))
        vids.sort(lambda lhs, rhs:cmp(lhs['fileName'], rhs['fileName']))
        self.assertEquals('vid1', vids[0]['name'])
        self.assertEquals(None, vids[0]['variant'])
        
        uniquified = self.slaveVideoService._uniquifyVideoDir(vids)
        self.assertTrue(uniquified)
        
        # rescan it
        vids = self.slaveVideoService._scanVideoDir()
        vids.sort(lambda lhs, rhs:cmp(lhs['fileName'], rhs['fileName']))
        self.assertEquals('vid1.mp4', vids[0]['fileName'])
        self.assertEquals('vid2__idavi.avi', vids[1]['fileName'])
        self.assertEquals('vid2__idmp4.mp4', vids[2]['fileName'])
        self.assertEquals('vid3__idthevariant.someext', vids[3]['fileName'])
        
        # uniquifying is idempotent, nothing done second time.
        
        self.assertFalse(self.slaveVideoService._uniquifyVideoDir(vids))
    
    @defer.inlineCallbacks
    def test_scanVideoData(self):
        # set video directory
        cfg.change(video_dir=os.path.join(_here, '..', 'protomixins/testvideos'))
        
        # read video data
        fileName = 'tiny_test.mp4'
        videoData = self.slaveVideoService._parseVideoName(fileName)
        
        # try to scan it.
        d = self.slaveVideoService.scanVideoData(videoData)
        self.clock.advance(1)
        result = yield d
        self.assertEquals('tiny_test', result['name'])
        self.assertEquals('tiny_test.mp4', result['fileName'])
        self.assertEquals('', result['variant'])
        self.assertEquals('mp4', result['extension'])


class TestMasterVideoService(basetests.BaseDbTest):
    
    videoDao = pythonioc.Inject('videoDao')
    slaveDao = pythonioc.Inject('slaveDao')
    masterVideoService = pythonioc.Inject(mastervideoservice.MasterVideoService)
    reactor = pythonioc.Inject('reactor')
    timeProvider = pythonioc.Inject('timeProvider')
    db = pythonioc.Inject('dbprovider')
    
    @defer.inlineCallbacks
    def setUp(self):
        basetests.BaseDbTest.setUp(self, False)
        pythonioc.registerServiceInstance(reactor, 'reactor', overwrite=True)
        pythonioc.registerService(mastervideoservice.MasterVideoService)
        
        yield self.slaveDao.deleteAll()
        yield self.taskDao.deleteAll()
        yield self.videoDao.deleteAll()
        
        with self.db() as db:
            db.flush()
        
    @defer.inlineCallbacks
    def test_addBatchedVideo(self):
        vidNames = yield self.videoDao.getVideoNames()
        self.assertEquals(0, len(vidNames))
          
        self.masterVideoService.addVideoData('slaveA', domain.Video(name='vid'))

        yield self.masterVideoService.addBatchedVideo()
        
        vidNames = yield self.videoDao.getVideoNames()
        self.assertEquals(['vid'], vidNames)
        
        
    @defer.inlineCallbacks
    def test_profile_addBatchedVideo(self):
        for i in range(10):
            self.masterVideoService.addVideoData('slaveB', domain.Video(name='vid' + str(i)))
            self.masterVideoService.addVideoData('slaveA', domain.Video(name='vid' + str(i), variant='x'))
            self.masterVideoService.addVideoData('slaveC', domain.Video(name='vid-xx' + str(i)))
            
        yield self.masterVideoService.addBatchedVideo()
        
    @defer.inlineCallbacks
    def _addVideos(self, errors, videos):
        for vid in videos:
            yield self.masterVideoService.addVideoData(*vid)
            
        yield self.masterVideoService.addBatchedVideo()
        
    def test_uniqueSlaveName_racecondition(self):
        raise unittest.SkipTest("synchronized inside, otherwise we're just being optimistic")
        names = ['vid1', 'vid2']
        variants = ['1', '2', '3']
        
        vids = [('slave-X', domain.Video(name=name, variant=variant)) for name, variant in itertools.product(names, variants)]
        
        errors = [0]
        for _i in range(10):
            self._addVideos(errors, vids)
            
        def checker():  
            self.assertEquals(0, errors[0])
            
        return testutils.doDeferredCheck(0.5, checker)
    
    def test_uniqueVariantName_racecondition(self):
        raise unittest.SkipTest("synchronized inside, otherwise we're just being optimistic")
        slaves = ['Slave-X%d' % i for i in range(10)]
        vids = [(slaves[i % len(slaves)], domain.Video(name='vid1', variant='1'))
                    for i in range(20)]
            
        errors = [0]
        for _i in range(10):
            self._addVideos(errors, vids)
            
        @defer.inlineCallbacks
        def checker():            
            numVids = yield self.videoDao.getTotalNumberOfVideos()
            self.assertEquals(1, numVids)
            slaveNames = yield self.slaveDao.getSlaveNames()
            self.assertEquals(slaves, slaveNames)
            self.assertEquals(0, errors[0])
            
        return testutils.doDeferredCheck(0.5, checker)
    
    @defer.inlineCallbacks
    def test_getRandomVideoName(self):
        # exception when no videos available
        self.assertDeferredRaises(RuntimeError, self.masterVideoService.getRandomVideoName)
        
        # add a video
        yield self.videoDao.addVideo("slaveA", domain.Video(name='testvid'))
        name = yield self.masterVideoService.getRandomVideoName()
        self.assertEquals('testvid', name)
        
class TestSampler(basetests.BaseDbTest):
    videoDao = pythonioc.Inject('videoDao')
    masterVideoService = pythonioc.Inject(mastervideoservice.MasterVideoService)
    db = pythonioc.Inject('dbprovider')
    slaveDao = pythonioc.Inject('slaveDao')
    hitTracker = pythonioc.Inject('hitTracker')
    snapshotCreator = pythonioc.Inject(snapshotcreator.SystemSnapshotCreator)
    optModelProvider = pythonioc.Inject(optmodel.OptModelProvider)
    timeProvider = pythonioc.Inject('timeProvider')
    
    def __init__(self, *args, **kwargs):
        basetests.BaseDbTest.__init__(self, *args, **kwargs)
        
        self._currentSampler = None
        
    @defer.inlineCallbacks
    def setUp(self):
        basetests.BaseDbTest.setUp(self, False)
        yield self.clearDatabase()

    @utils.runAsDeferredThread
    def _createVideos(self, numVids):
        with self.db() as db:
            counter = 0
            for vid in range(numVids):
                
                # add a video with chaning size and duration
                db.add(domain.Video(name='video-%d' % vid,
                                    fsize=(counter + 1) * 100000000,
                                    duration=counter + 5))
                counter = (1 + counter) % 100
                
            db.flush()
            
    @defer.inlineCallbacks
    def _createSamples(self, numVids, sampler, numSamples=None):
        if numSamples is None:
            numSamples = numVids * 100
        yield self.videoDao.deleteAll()
        yield self._createVideos(numVids)
        self._currentSampler = sampler(numSamples)
        yield self._currentSampler._refresh()
        samples = list(self._currentSampler._samples)
        samples.sort()
        defer.returnValue(samples)
        
    @defer.inlineCallbacks
    def testVideoSampler(self):
        raise unittest.SkipTest('disabled for performance reasons')
        import matplotlib.pyplot as plt
        from matplotlib.backends.backend_pdf import PdfPages
        
        with PdfPages('/tmp/samples.pdf') as pdf:
            
            samplers = [
                        mastervideoservice.GumbelSampler,
                        mastervideoservice.NormSampler
                        ]
            
            for sampler in samplers:
                for i in [100,
                          1000,
                          2000
                          ]:
                    print "sampling", i, 'items with sampler', sampler.name
                    samples = yield self._createSamples(i, sampler)
                    plt.hist(samples, bins=i)
                    plt.title('Items %d, Samples %d, Sampler: %s' % (i, len(samples), sampler.name))
                    pdf.savefig()
                    plt.close()
                    
                    vids = yield self.videoDao.getVideos()
                    vids = {v.name:v.id for v in vids}
                    
                    self.hitTracker.reset()
                    print "....sampling done, now I'll request all of them"
                    # now request all samples using the hittracker
                    interval = 900.0 / len(samples)
                    
                    for i in range(len(samples) - 1):
                        sample = yield self._currentSampler.getNext()
                        self.timeProvider.advance(interval)
                        self.hitTracker.hit(vids[sample])
                        
                    print "...requesting done, creating snapshot"
                    snappi = yield  self.snapshotCreator.createSnapshot(self.optModelProvider.model)
                    indexValues = snappi.mapping.index.values - snappi.mapping.index.values.min()
                    plt.plot(indexValues, snappi.mapping['popularity'].values)
                    plt.title('Popularity after requesting all these videos in 900 seconds')
                    pdf.savefig()
                    plt.close()
                    print "... done"
                    
