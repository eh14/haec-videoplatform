'''

@author: eh14
'''
from haec.unittests.kplane import basetests
import pythonioc
from haec.optimization import snapshotcreator, optmodel
from haec.services import taskservice, statisticservice

class TestStatisticService(basetests.BaseDbTest):
    service = pythonioc.Inject(statisticservice.StatisticService)
    
    videoDao = pythonioc.Inject('videoDao')
    slaveDao = pythonioc.Inject('slaveDao')
    taskDao = pythonioc.Inject('taskDao')
    taskService = pythonioc.Inject(taskservice.TaskService)
    
    hitcounter = pythonioc.Inject('hitTracker')
    
    optProvider = pythonioc.Inject(optmodel.OptModelProvider)
    
    def setUp(self):
        pythonioc.registerService(statisticservice.StatisticService)
        pythonioc.registerService(taskservice.TaskService)
        pythonioc.registerService(optmodel.OptModelProvider)
        
        basetests.BaseDbTest.setUp(self, True)
        self.snapCreator = snapshotcreator.SystemSnapshotCreator()
        
