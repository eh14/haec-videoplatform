'''

@author: eh14
'''
from twisted.trial import unittest
from haec.services import portmapperservice
from twisted.internet import defer, reactor
from haec.unittests import testutils
import pythonioc
import netaddr
from haec import utils

class UpnpDeviceMock(object):
    mappings = []
    def addportmapping(self, external, protocol, ip, internal, name, leasetime):
        self.mappings.append((external, protocol, (ip, internal), name, leasetime))
        
    def getgenericportmapping(self, i):
        if i >= len(self.mappings):
            return
        else:
            return self.mappings[i]
        
        
class PortMapperServiceMock(portmapperservice.PortMapperService):
    def __init__(self):
        portmapperservice.PortMapperService.__init__(self)
        self.upnpdevice = UpnpDeviceMock()
        
    def _createUpnpDevice(self):
        return self.upnpdevice
        
class TestPortMapperService(unittest.TestCase):
    clock = pythonioc.Inject('reactor')
    
    def setUp(self):
        testutils.setUpIocRegistry()
        self._mapper = PortMapperServiceMock()
        
    @defer.inlineCallbacks
    def test_handleNewSlaves_concurrency(self):
        self._mapper._initialized = True
        self.assertEquals(0, len(self._mapper.upnpdevice.mappings))
        for i in range(10):
            self._mapper.registerSlave(slaveName='slave-%s' % i, slaveIp='ip-%s' % i, internal=i)
        
        self.clock.advance(1)

        for _i in range(10):        
            if len(self._mapper.upnpdevice.mappings) == 10:
                break
            yield utils.deferredSleep(reactor, 0.1)
        else:
            self.fail('requests not added')
            
    @defer.inlineCallbacks
    def test_readExistingSlaves(self):
        # reuse port from the mapper
        port = iter(self._mapper._slavePorts).next()
        self._mapper.upnpdevice.addportmapping(port, 'tcp', 'ip-0', 0, 'slave', 0)
        
        yield self._mapper.readExistingSlaves()
        
        # the port must be gone
        self.assertFalse(port in self._mapper._slavePorts)
        
        # the slave must be in the mapper's registry
        self.assertEquals(port, self._mapper._slaveMapping['slave'].external)
        
    @defer.inlineCallbacks
    def test_initExternalIp(self):
        yield self._mapper.initExternalIp()
        self.assertIsInstance(self._mapper._externalAddress, netaddr.IPAddress)
        
    @defer.inlineCallbacks
    def test_initInternalIp(self):
        yield self._mapper.initInternalIp()
        self.assertIsInstance(self._mapper._internalAddress, netaddr.IPNetwork)
        
    def test_getMappedSlave(self):
        
        # init the addresses manually manually
        self._mapper._internalAddress = netaddr.IPNetwork('192.168.0.10/255.255.255.0')
        self._mapper._externalAddress = netaddr.IPAddress('123.45.67.89')
        
        # register two slaves, one is mapped, one is not.
        self._mapper.registerSlave(slaveName='slave-1', slaveIp='slave-1-ip', internal=1, external=2)
        self._mapper.registerSlave(slaveName='slave-2', slaveIp='slave-2-ip', internal=3)
        
        
        # (1) with non-existing slave
        self.assertRaises(AttributeError, self._mapper.getMappedSlave, 'somehost', 0)
        
        # (2) intern + extern client
        self.assertEquals('slave-1-ip:1', self._mapper.getMappedSlave('slave-1', '192.168.0.5'))
        self.assertEquals('123.45.67.89:2', self._mapper.getMappedSlave('slave-1', '1.1.1.1'))
        
        # (3) intern + extern with errors in slave
        self.assertEquals('slave-2-ip:3', self._mapper.getMappedSlave('slave-2', '192.168.0.5'))
        self.assertRaises(RuntimeError, self._mapper.getMappedSlave, 'slave-2', '1.1.1.1')
        
        self.assertEquals('slave-1-ip:1', self._mapper.getMappedSlave('slave-1', '127.0.0.1'))
        
        # (4) intern with 127.0.0.1 and the like
        
    def test_getSystemAddress(self):
        self._mapper._internalAddress = None
        self._mapper._externalAddress = None
        
        self.assertRaises(RuntimeError, self._mapper.getSystemAddress, None)
        
        self._mapper._internalAddress = netaddr.IPNetwork('192.168.0.10/255.255.255.0')
        self._mapper._externalAddress = netaddr.IPAddress('123.45.67.89')
        
        self.assertEquals('123.45.67.89', self._mapper.getSystemAddress('1.1.1.1'))
        self.assertEquals('192.168.0.10', self._mapper.getSystemAddress('192.168.0.5'))
        
        
        