from haec.kplane import domain
import pythonioc
from twisted.internet import defer, reactor
from haec.unittests import testutils
from sqlalchemy.exc import IntegrityError
from haec.unittests.kplane import basetests
from haec.services import taskservice, localtaskservice
from haec.unittests.services import servicemocks


class TestTaskService(basetests.BaseDbTest):
    
    timeout = 5
   
    ts = pythonioc.Inject(taskservice.TaskService)
    
    videoDao = pythonioc.Inject('videoDao')
    slaveDao = pythonioc.Inject('slaveDao')
    taskDao = pythonioc.Inject('taskDao')
    
    clock = pythonioc.Inject('reactor')
    
    @defer.inlineCallbacks
    def setUp(self):
        basetests.BaseDbTest.setUp(self, dropTables=False)
        pythonioc.registerService(localtaskservice.LocalTaskService)
        pythonioc.registerService(taskservice.TaskService)
        pythonioc.registerServiceInstance(reactor, 'reactor', overwrite=True)
        
        yield self.clearDatabase()
        
        # set autorun to false
        self.ts.autoWakeup = False
        
        # kill the cleaner, otherwise it will end up in every task
        self.ts._cleanerLooper.stop()
        
    @defer.inlineCallbacks
    def test_addExtrinsicTask(self):
        
        yield self.ts.addExtrinsicTask('someId', 'someName')
        
        tasks = yield self.taskDao.getAllTasks()
        tasks = list(tasks)
        
        self.assertEquals(1, len(tasks))
        self.assertEquals('someId', tasks[0].taskId)

    @defer.inlineCallbacks
    def test_addExtrinsicTask_doubleAdd(self):
        yield self.ts.addExtrinsicTask('someId', 'someName')
        try:
            yield self.ts.addExtrinsicTask('someId', 'someName2')
            self.fail('Expected to fail')
        except IntegrityError:
            pass  # ok
        
    @defer.inlineCallbacks
    def test_addTask_duplicatetaskId(self):
        
        yield self.taskDao.addTask('1', 'task1', 'local', taskType='intrinsic')
        try:
            yield self.taskDao.addTask('1', 'task1', 'local', taskType='intrinsic')
            self.fail('Should raise an Exception')
        except:
            pass  # ok, expected!
        
        
    @defer.inlineCallbacks
    def test_runTask_local(self):
        videoId = yield self.videoDao.addVideo('slaveA', domain.Video(name='hello', fname='file.avi'))
        self.slaveDao.addSlave(domain.Slave(name='slaveB', status='online'))
        
        yield self.taskDao.addLocalActionTask('localtask', localtaskservice.LocalTaskService.addVideoToSlave, args=('slaveB', videoId))
        
        tasks = yield self.taskDao.getRunnableTasks()
        self.assertEquals(1, len(tasks))
        
        vidOnSlave = yield self.videoDao.isVideoOnSlave(videoId, 'slaveB')
        self.assertFalse(vidOnSlave)
        yield self.ts._runTask(tasks[0])
        # self.clock.advance(10)
        
        @defer.inlineCallbacks
        def check():
            task, = yield self.taskDao.getAllTasks()
            self.assertEquals('success', task.status)
            vidOnSlave = yield self.videoDao.isVideoOnSlave(videoId, 'slaveB')
            self.assertTrue(vidOnSlave)
        
        yield testutils.doDeferredCheck(0.5, check)
            
    @defer.inlineCallbacks
    def test_runTask_local_failing(self):
        
        targetSlave = 'nonexisting'
        videoId = yield self.videoDao.addVideo('slaveA', domain.Video(name='hello', fname='file.avi'))
        
        yield self.taskDao.addLocalActionTask('localtask', localtaskservice.LocalTaskService.addVideoToSlave, args=(targetSlave, videoId))
        tasks = yield self.taskDao.getRunnableTasks()
        self.assertEquals(1, len(tasks))
        
        vidOnSlave = yield self.videoDao.isVideoOnSlave(videoId, targetSlave)
        self.assertFalse(vidOnSlave)
        yield self.ts._runTask(tasks[0])
        # self.clock.advance(10)
        
        @defer.inlineCallbacks
        def check():
            task, = yield self.taskDao.getAllTasks()
            self.assertEquals('failure', task.status)
            
        # need to 
        yield testutils.doDeferredCheck(0.5, check)
        

    @defer.inlineCallbacks
    def test_runTask_remote_success(self):
                                    
        service = servicemocks.registerSlaveConnService('slave')
        
        yield self.taskDao.addCommandActionTask('someaction', 'slave', 'do-some-command-thing')
        
        tasks = yield self.taskDao.getRunnableTasks()
        self.assertEquals('slave', tasks[0].location)
        self.assertEquals(1, len(tasks))
        # run the task
        yield self.ts._runTask(tasks[0])
        
        # run the scheduler
        # self.clock.advance(10)
        
        task = yield self.taskDao.getTaskById(tasks[0].taskId)
        self.assertEquals('running', task.status)
        
        self.assertEquals(tasks[0].taskId, service.getConnectedSlaveByName('slave').obj.taskId)
        
    @defer.inlineCallbacks
    def test_runTask_remote_slave_not_found(self):
                                    
        yield self.taskDao.addCommandActionTask('someaction', 'slave', 'do-some-command-thing')
        
        tasks = yield self.taskDao.getRunnableTasks()
        self.assertEquals(1, len(tasks), tasks)
        # run the task
        
        servicemocks.registerSlaveConnService('non-existing-slave')
        
        yield self.ts._runTask(tasks[0])
        
        # run the scheduler
        # self.clock.advance(10)
        
        task, = yield self.taskDao.getAllTasks()
        self.assertEquals('failure', task.status)
 
    @defer.inlineCallbacks
    def test_getLocalTasks(self):
        yield self.taskDao.addCommandActionTask('someaction', 'slave', 'do-some-command-thing')
        tasks = yield self.taskDao.getLocalTasks()
        self.assertEquals(0, len(tasks))

        yield self.taskDao.addLocalActionTask('someaction', localtaskservice.LocalTaskService.addVideoToSlave, args=('slaveB', 0))
        tasks = yield self.taskDao.getLocalTasks()
        self.assertEquals(1, len(tasks))
        
