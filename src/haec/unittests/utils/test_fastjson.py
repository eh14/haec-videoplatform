'''

@author: eh14
'''
from twisted.trial import unittest
from haec.utils import fastjson
from twisted.internet import defer
import pandas
import datetime

class Test_Json(object):
    
    @defer.inlineCallbacks
    def _encodedecode(self, value):
        
        # start = time.time()
        enc = yield fastjson.encode(value)
        # print "encoding took", (time.time() - start)*1000
        
        # start = time.time()
        dec = yield fastjson.decode(enc)
        # print "decoding took", (time.time() - start)*1000
        self.assertEquals(dec, value)
        
        defer.returnValue(enc)
    
    @defer.inlineCallbacks
    def test_encode(self):
        result = yield self._encodedecode({'h':'w'}) 
        self.assertEquals('{"h": "w"}', result)
        
    @defer.inlineCallbacks
    def test_muchdata(self):
        yield self._encodedecode({'d-%d' % v:range(560) for v in range(30)})
        
    @defer.inlineCallbacks
    def test_pandas(self):
        # that's a bogus test!
        # The converting is actually done by pandas, not by json/cjson...
        converted = pandas.DataFrame({'col':[1L, 2L, 3L]}, index=[500L, 600L, 700L]).to_json(orient='split')
        yield self._encodedecode(converted)
        
    @defer.inlineCallbacks
    def test_datetime(self):
        raise unittest.SkipTest('datetime not working!!')
        blubb = {'time':datetime.datetime.now()}
        yield self._encodedecode(blubb)
        
        
class CJsonTest(Test_Json, unittest.TestCase):
    def setUp(self):
        fastjson._coder = fastjson._CJsonJson()
        
class JsonTest(Test_Json, unittest.TestCase):
    def setUp(self):
        fastjson._coder = fastjson._JsonJson()
        
        
