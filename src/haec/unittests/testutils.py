'''

@author: eh14
'''
import pythonioc
from twisted.internet.task import Clock
from twisted.internet import defer, reactor
from haec import services, config
import time
from haec.services import eventservice, profileservice, topologyservice
from twisted.web import client
from haec.unittests.services import servicemocks
from haec.frontend import migrationevents
from haec.optimization import snapshotcreator, optmodel

cfg = pythonioc.Inject(config.Config)

class ThreadObliviousClock(Clock):
    """
    Clock extension that does not care about threads, since the test
    cases usually work without threads.
    """
    
    def callFromThread(self, func, *args, **kwargs):
        func(*args, **kwargs)  

def setUpIocRegistry():
    pythonioc.cleanServiceRegistry()
    cfg.init()
    pythonioc.registerServiceInstance(ThreadObliviousClock(), 'reactor', overwrite=True)
    pythonioc.registerServiceInstance(FixedTimeProvider(), 'timeProvider', overwrite=True)
    pythonioc.registerService(eventservice.EventService, overwrite=True)
    pythonioc.registerService(servicemocks.CambrionixConnectorMock, overwrite=True)
    pythonioc.registerService(profileservice.ProfileService)
    pythonioc.registerService(topologyservice.TopologyService)
    pythonioc.registerService(migrationevents.MigrationEventService)
    pythonioc.registerService(snapshotcreator.SystemSnapshotCreator)
    pythonioc.registerService(optmodel.OptModelProvider)
    
def doDeferredCheck(delay, checker, *args, **kwargs):
    """
    Used to schedule checks after some delay as part of a unit-test. Normally,
    one would use Clock to simulate the reactor. When threads are used e.g.,
    `threads.deferToThread(..)`, the real reactor will be used and delayed actions
    need to be scheduled here.
    
    The result of this method is a Deferred and MUST be returned (or yielded if using defer.inlineCallbacks) from the
    test case, otherwise the reactor will be unclean.
    
    @param delay: delay in seconds
    @param checker: function to execute after delay
    """
    d = defer.Deferred()
    d.addBoth(lambda _:checker(*args, **kwargs))
    
    reactor.callLater(delay, d.callback, None)  # @UndefinedVariable
    return d    
    

class FixedTimeProvider(services.TimeProvider):
    """
    Time provider that always returns the same time. Can be set
    to fixed time for testing (and is in most cases).
    """
    def __init__(self):
        services.TimeProvider.__init__(self)
        self.__fixedNow = time.time()
        
    def _now(self):
        return self.__fixedNow
    
    def advance(self, seconds):
        self.__fixedNow += seconds
        
        
def activateLogging():
    """
    Simply activate the normal logging. Should only be inserted when something is 
    to be investigated... otherwise the testing-console will be dumped with logs.
    """
    
    from twisted.python import log
    import sys
    
    log.startLogging(sys.stdout)
    
    
    

# @defer.inlineCallbacks
def getPage(url):
    agent = client.Agent(reactor)
    return agent.request('GET',
                      url)
    # defer.returnValue(agent)
    
    
