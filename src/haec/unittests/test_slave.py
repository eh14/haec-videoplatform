from twisted.trial import unittest

from haec.slave import SlaveFactory
from haec.unittests import testutils


# used by environment when we use this module as the mapping-module
forwarding = {
           'slave':1234
           }

        
class TestSlaveFactory(unittest.TestCase):
    def setUp(self):
        testutils.setUpIocRegistry()
        self.fact = SlaveFactory()
        self.fact.startFactory()
         
    def tearDown(self):
        self.fact.stopFactory()
         
    def test_buildProtocol(self):
        proto = self.fact.buildProtocol("someaddress")
        self.assertIsNotNone(proto)
        