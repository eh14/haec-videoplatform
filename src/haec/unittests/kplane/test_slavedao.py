import pythonioc
from twisted.internet import defer

from haec.kplane import domain
from haec.unittests.kplane import basetests


class TestSlaveDao(basetests.BaseDbTest):
    dao = pythonioc.Inject('slaveDao')
    
    @defer.inlineCallbacks
    def setUp(self):
        basetests.BaseDbTest.setUp(self, dropTables=False)
        yield self.clearDatabase()
    
    @defer.inlineCallbacks
    def testGetSlaveNames(self):
        yield self.dao.addSlave(domain.Slave(name="name"))
        
        names = yield self.dao.getSlaveNames()
        self.assertEquals(['name'], names)
        
    @defer.inlineCallbacks
    def test_getAllSlaveStatus(self):
        yield self.dao.addSlave(domain.Slave(name='slaveA'))
        yield self.dao.addSlave(domain.Slave(name='slaveB', status='failure'))
        
        status = yield self.dao.getAllSlaveStatus()
        self.assertEquals(2, len(status))
        self.assertEquals('offline', status['slaveA'])
        self.assertEquals('failure', status['slaveB'])
    
    @defer.inlineCallbacks    
    def testGetSlaves(self):
        yield self.dao.addSlave(domain.Slave(name="name", webserver_port='1234'))
        
        slaves = yield self.dao.getSlaves()
        self.assertEquals(1, len(slaves))
        self.assertEquals('1234', slaves[0].webserver_port)
    
    @defer.inlineCallbacks
    def test_addOrUpdateSlave(self):
        yield self.dao.addOrUpdateSlave(name='name', webserver_port='1')
        yield self.dao.addOrUpdateSlave(name='name2', webserver_port='1')
        
        slaves = yield self.dao.getSlaves()
        self.assertEquals(2, len(slaves))
        yield self.dao.addOrUpdateSlave(name='name2', webserver_port='2')
        
        newSlave = yield self.dao.getSlaveByName('name2')
        
        self.assertEquals('2', newSlave.webserver_port)
        
        slaves = yield self.dao.getSlaves()
        self.assertEquals(2, len(slaves))
        
    @defer.inlineCallbacks    
    def test_setSlavesOffExcept(self):
        yield self.dao.addSlave(domain.Slave(name='slaveA', status='online'))
        yield self.dao.addSlave(domain.Slave(name='slaveB', status='online'))
        yield self.dao.addSlave(domain.Slave(name='slaveC', status='online'))
        
        slaves = yield self.dao.getSlaves()
        self.assertEquals(3, len(slaves))
        self.assertTrue(all([x.online for x in slaves]))
        
        yield self.dao.setSlavesOffExcept('slaveA', 'slaveB', 'slaveC', 'some-other')
        slaves = yield self.dao.getSlaves()
        self.assertEquals(3, len(slaves))
        self.assertTrue(all([x.online for x in slaves]))        

        yield self.dao.setSlavesOffExcept('slaveA', 'non-existent')
        slaves = yield self.dao.getSlaves()
        self.assertEquals(3, len(slaves))
        self.assertTrue(any([x.online for x in slaves]))        
        
        # from set, as in the master-factory
        yield self.dao.setSlavesOffExcept(*set(['slaveA', 'non-existent']))
        
        slaves = yield self.dao.getSlaves()
        self.assertEquals(3, len(slaves))
        self.assertTrue(any([x.online for x in slaves]))        
        
        # this should turn off all slaves
        yield self.dao.setSlavesOffExcept()
        slaves = yield self.dao.getSlaves()
        self.assertTrue(not any([x.online for x in slaves]))        
        
    @defer.inlineCallbacks
    def test_initializeTopology(self):
        
        topology = {'switch-x':[
                                'slave-x1',
                                'slave-x2'
                                ]
                    }
        
        yield self.dao.initializeTopology(topology)
        
        switches = yield self.dao.getSystemTopology()
        self.assertEquals(1, len(switches))
        self.assertEquals('switch-x', switches[0].name)
        self.assertEquals(2, len(switches[0].slaves))
        
        topology2 = {'switch-x':['slave-x2',
                                 'slave-x3'],
                    }
        yield self.dao.initializeTopology(topology2)
        switches = yield self.dao.getSystemTopology()
        self.assertEquals(1, len(switches))
        self.assertEquals(3, len(switches[0].slaves))


    @defer.inlineCallbacks
    def test_getSetSlaveStatus(self):
        yield self.dao.addSlave(domain.Slave(name='slaveA', status='online'))
        
        self.assertDeferredEquals('online', self.dao.getSlaveStatus('slaveA'))
        yield self.dao.setSlaveStatus('slaveA', 'boot')
        self.assertDeferredEquals('boot', self.dao.getSlaveStatus('slaveA'))
        
        
    @defer.inlineCallbacks
    def test_getFailedSlaves(self):
        yield self.dao.addSlave(domain.Slave(name='ok', status='online'))
        yield self.dao.addSlave(domain.Slave(name='failed', status='failure'))
        
        slaves = yield self.dao.getFailedSlaves()
        self.assertEquals(['failed'], slaves)
        