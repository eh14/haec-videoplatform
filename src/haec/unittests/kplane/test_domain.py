from haec.kplane import domain
from haec.unittests.kplane import basetests
import json

class TestDomain(basetests.BaseDbTest):
    
    def testCreateVideo(self):
        """
        simply creates a domain without a database etc. to check
        whether it can be used as a plain object
        """
        
        vid = domain.Video(name='hello world')
        self.assertEquals("hello world", vid.name)
        
    def test_JsonType(self):
        col = domain.JsonCol(1024)
        dd = {'a':'hello', '2':3}
        self.assertEquals(json.dumps(dd), col.process_bind_param(dd, None))
        self.assertEquals(dd, col.process_result_value(json.dumps(dd), None))

    def test_TripleInt(self):
        col = domain.NumberTuple(count=3, precision=2)
        self.assertEquals('1.00|2.00|3.00', col.process_bind_param([1, 2, 3], None))
        self.assertEquals([1, 2, 124], col.process_result_value('1|2|124', None))
        
