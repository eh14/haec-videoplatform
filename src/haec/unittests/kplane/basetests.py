'''

@author: eh14
'''
from twisted.trial import unittest
from haec import config
import pythonioc
from haec.unittests import testutils
from haec import kplane
from twisted.internet import defer
from haec.services import cambrionixservice
import threading
from haec.unittests.services import cambrionixmock

class BaseTest(unittest.TestCase):
    @defer.inlineCallbacks
    def assertDeferredRaises(self, exc, func, *args, **kwargs):
        try:
            yield func(*args, **kwargs)
            self.fail('Expected exception %s was not raised' % exc)
        except exc:
            pass
        
    @defer.inlineCallbacks
    def assertDeferredEquals(self, expected, actual, message=None):
        
        exValue = yield expected
        actValue = yield actual
        
        self.assertEquals(exValue, actValue, msg=message)
        
        
class BaseDbTest(BaseTest):
    
    cfg = pythonioc.Inject(config.Config)
    
    videoDao = pythonioc.Inject('videoDao')
    slaveDao = pythonioc.Inject('slaveDao')
    taskDao = pythonioc.Inject('taskDao')
    hittracker = pythonioc.Inject('hitTracker')
    db = pythonioc.Inject('dbprovider')
    
    def setUp(self, dropTables=False, dbEcho=False, insertServiceMocks=True):
        
        pythonioc.cleanServiceRegistry()
        if insertServiceMocks:
            testutils.setUpIocRegistry()
        
        # we need a real database here, not the memory. Otherwise the the threaded
        # jobs will fail since the database is empty.
        self.cfg.init(db_connectionurl='mysql://root:root@localhost:3306/vp_tests',
                   db_drop=dropTables,
                   db_create=True,
                   task_runtasks_interval=0,  # disable auto-run
                   slave_dnslookup='localhost',
                   db_echo=dbEcho,
                   scheduler_job_waitinterval=0,
                   )
        kplane.registerInGlobalRegistry(True)
        
        self.hittracker.reset()
        
        
    @defer.inlineCallbacks
    def clearDatabase(self):
        yield self.videoDao.deleteAll()
        yield self.taskDao.deleteAll()
        yield self.slaveDao.deleteAll()
        self.hittracker.reset()
        
class CambrionixMockMixin(object):
    
    def startCambrionix(self):
        self.cfg.change(cambrionix_url='tcp://127.0.0.1:10059')
        
        self.cambrionixConnector = cambrionixservice.CambrionixConnector()
        pythonioc.registerServiceInstance(self.cambrionixConnector,
                                          'cambrionixConnector',
                                          overwrite=True)
        self.__mockThread = threading.Thread(target=cambrionixmock.runCambrionixMock, args=(self.cambrionixConnector.getZmqContext(),))
        self.__mockThread.start()
        
        
    def stopCambrionix(self):
        sock = self.cambrionixConnector._createSocket()
        try:
            sock.send_json({'code':'exit'})
        finally:
            sock.close()
            self.__mockThread.join()
