'''
@author: eh14
'''
import datetime
import pandas
import pythonioc
import time
from twisted.trial import unittest

from haec import config
from haec.kplane import hitcounter
from haec.unittests import testutils


cfg = pythonioc.Inject(config.Config)

class TestHitCounter(unittest.TestCase):
    
    def setUp(self):
        testutils.setUpIocRegistry()
        self.c = hitcounter.HitCounter()
        
    def createDt(self, year=2014, month=11, day=2, hour=23, minute=45, second=40):
        return datetime.datetime(year, month, day, hour, minute, second)        
        
    def test_floorMinute(self):
        dt = self.createDt()

        dt2 = hitcounter._floorMinute(dt)
        self.assertEquals(0, dt2.second)
        
        self.assertTrue(dt2 < dt)
        
        dt = self.createDt(second=0)
        dt2 = hitcounter._floorMinute(dt)
        self.assertEquals(dt2, dt)
        
    def test_floor30Minute(self):
        # raise unittest.SkipTest('aggregator removed')
        dt = self.createDt()

        dt2 = hitcounter._floor30Minute(dt)
        self.assertEquals(30, dt2.minute)
        self.assertEquals(0, dt2.second)
        
        self.assertTrue(dt2 < dt)
        
        dt = self.createDt(minute=23)
        dt2 = hitcounter._floor30Minute(dt)
        self.assertEquals(0, dt2.minute)
        self.assertEquals(0, dt2.second)

    def test_floorHour(self):
        dt = self.createDt()

        dt2 = hitcounter._floorHour(dt)
        self.assertEquals(0, dt2.second)
        self.assertEquals(0, dt2.minute)
        
        self.assertTrue(dt2 < dt)
        
    def test_floorDay(self):
        dt = self.createDt()

        dt2 = hitcounter._floorDay(dt)
        self.assertEquals(0, dt2.second)
        self.assertEquals(0, dt2.minute)
        self.assertEquals(0, dt2.hour)
        
        self.assertTrue(dt2 < dt)
        
        
    def test_aggregate(self):
        dt = self.createDt(second=30)
        now = time.mktime(dt.timetuple())
        self.c.aggregate(now)
        self.assertEquals(0, len(self.c._hits))
        self.assertEquals(0, len(self.c._minutes))
        
        # hit it now
        self.c.hit(now)
        
        # aggregate current minute
        self.c.aggregate(now + 31)
        
        # hit should still be seen
        self.assertEquals([(now, 1)], self.c._hits.collection)
        
        # and no aggregation should be done
        self.assertEquals(0, len(self.c._minutes))
        
        # hit it again next minute
        self.c.hit(now + 60)
        
        # do some aggregation next minute
        self.c.aggregate(now + 31 + 60)
        
        # the old hits-value was deleted,
        # both values have been aggregated
        self.assertEquals([(now + 60, 1)], self.c._hits.collection)
        self.assertEquals([(now + 30, 1),
                           (now + 30 + 60, 1)], self.c._minutes.collection)
        
    def test_aggregate_minutes(self):
        dt = self.createDt(second=0)
        
        now = time.mktime(dt.timetuple())
        
        # minute aggregation
        for i in range(61):
            self.c.hit(now + i)
            
        self.c.aggregate(now + 60)
        self.assertEquals(1, len(self.c._minutes.collection))
        self.assertEquals(60, self.c._minutes.collection[0][1])
        
    def test_aggregate_30mins(self):
        raise unittest.SkipTest('aggregator removed')
        dt = self.createDt(second=0, minute=0, hour=0)
        
        now = time.mktime(dt.timetuple())
                    
        for minute in range(61):
            for second in range(60):
                hitTime = now + (60 * minute) + second
                self.c.hit(hitTime)
                
        self.c.aggregate(hitTime)
        self.assertEquals(60, len(self.c._minutes.collection))
        self.assertEquals(2, len(self.c._30minutes.collection))
        
        # both 30 minute intervals got 1800 each (1 per second)
        self.assertEquals(1800, self.c._30minutes.collection[0][1])
        self.assertEquals(1800, self.c._30minutes.collection[1][1])
        
  
        
    def test_aggregate_hours(self):
        dt = self.createDt(second=0, minute=0, hour=0)
        
        now = time.mktime(dt.timetuple())
                    
        for minute in range(121):
            for second in range(60):
                hitTime = now + (60 * minute) + second
                self.c.hit(hitTime)
                
        self.c.aggregate(hitTime)
        self.assertEquals(60, len(self.c._minutes.collection))
        self.assertEquals(2, len(self.c._hours.collection))
        
    def test_aggregate_days(self):
        dt = self.createDt(second=0, minute=0, hour=0)
        
        now = time.mktime(dt.timetuple())
                    
        # for two days, once per minute
        for hour in range(49):
            for minute in range(60):
                hitTime = now + (60 * minute) + 3600 * hour
                self.c.hit(hitTime)
                
        self.c.aggregate(hitTime)
        self.assertEquals(24, len(self.c._hours.collection))
        self.assertEquals(2, len(self.c._days.collection))
        self.assertEquals(60 * 24, self.c._days.collection[0][1])
        self.assertEquals(60 * 24, self.c._days.collection[1][1])
         
        
class TestAggregator(unittest.TestCase):
    def setUp(self):
        self.src = []
        self.a = hitcounter.Aggregator('test', self.src, hitcounter._floorMinute, 60)
        
    def createDt(self, year=2014, month=11, day=2, hour=23, minute=45, second=40):
        return datetime.datetime(year, month, day, hour, minute, second)        
   
   
    def simpleExtractCumulatedValues(self, start, end):
        """
        utility function that allows counting the values in a simple list
        """
        
        return self.a.extractCumulatedValues(start, end)
    
    def test_aggregateFromSource(self):
        self.assertEquals(0, self.simpleExtractCumulatedValues(0, 1))
        
        self.a.collection = [(1, 1), (2, 1), (3, 1), (5, 1)]
        self.assertEquals(1, self.simpleExtractCumulatedValues(0, 1))
        self.assertEquals([(1, 1), (2, 1), (3, 1), (5, 1)], self.a.collection)
        
        self.assertEquals(2, self.simpleExtractCumulatedValues(1, 3))
        self.assertEquals([(2, 1), (3, 1), (5, 1)], self.a.collection)
        
    def test_timedelta(self):
        dt = self.createDt()
        dt2 = self.createDt(hour=dt.hour - 1, month=dt.month - 1)
        td = dt - dt2
        
        self.assertEquals(3600, td.seconds)
        self.assertEquals(31, td.days)
        
    def test_sumLastItems(self):
        df = self.createDt()
        
        now = time.mktime(df.timetuple())
        self.a.collection.extend([(now, 100),
                                  (now + 50, 200)])

        # take only last
        self.assertEquals(200, self.a.sumLastItems(now))
        self.assertEquals(200, self.a.sumLastItems(now + 1))
        
        
        # select none 
        self.assertEquals(0, self.a.sumLastItems(now + 50))
        
        # select both
        self.assertEquals(300, self.a.sumLastItems(now - 1))
        
        
    
class TestHitTracker(unittest.TestCase):
    def setUp(self):
        self.t = hitcounter.HitTracker()
        
    def createDt(self, year=2014, month=11, day=2, hour=23, minute=45, second=40):
        return datetime.datetime(year, month, day, hour, minute, second)        
   
        
    def test_hit(self):        
        self.assertTrue('vidA' not in self.t._counters)
        self.t.hit('vidA')
        self.assertTrue('vidA' in self.t._counters)
        
    def test_constructHitMatrix(self):
        
        now = time.mktime(self.createDt().timetuple())
        
        # hit it now
        self.t.hit('vidA', now)
        self.t.hit('vidA', now)
        self.t.hit('vidA', now)
        
        matrix = self.t.constructHitMatrix(now)
        # those have full precision
        self.assertEquals(3, matrix.loc['vidA', '30minutes']) 
        self.assertEquals(3, matrix.loc['vidA', 'total']) 
        self.assertEquals(3, matrix.loc['vidA', 'minute'])
        
        # hour + day are not aggregated yet
        self.assertEquals(0, matrix.loc['vidA', 'hour']) 
        self.assertEquals(0, matrix.loc['vidA', 'day']) 
        
        # hit it in one hour
        self.t.hit('vidA', now + 1802)
        
        matrix = self.t.constructHitMatrix(now + 3601)
        
        self.assertEquals(4, matrix.loc['vidA', 'total']) 
        self.assertEquals(1, matrix.loc['vidA', '30minutes']) 
        self.assertEquals(0, matrix.loc['vidA', 'minute'])
        # the reason we're seing 1 instead of 3 is, because
        # the last hour value is flattened
        self.assertEquals(3, matrix.loc['vidA', 'hour'])

        

class TestHitCounterModule(unittest.TestCase):
    def test_CreateHitHistogram(self):
        df = pandas.DataFrame(data={'total':[10, 10, 20, 0],
                                    'minute':[1, 5, 10, 0]}, index=[1, 2, 3, 4])
        
        hist = hitcounter.createHitHistogram(df)
        self.assertEquals(2, hist.loc[10, 'total'])
        self.assertEquals(1, hist.loc[20, 'total'])
        self.assertEquals(1, hist.loc[20, 'total'])

        # some value which is never hit
        # not working (and necessary) anymore since we do not
        # reindex anymore
        # self.assertEquals(0, hist.loc[13, 'total'])
        
    def test_createHitHistogram_empty(self):
        df = pandas.DataFrame()
        
        # even an empty hitmatrix, creates an empty histogram
        hist = hitcounter.createHitHistogram(df)
        self.assertEquals(1, hist.index.size)

