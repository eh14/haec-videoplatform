from haec.kplane import domain
import pythonioc
from twisted.internet import defer
from haec.kplane.exceptions import ItemNotFoundError
from haec import config
from haec.unittests.kplane import basetests
from haec.services import taskservice
from haec.unittests import testutils

cfg = pythonioc.Inject(config.Config)

class TestTaskDao(basetests.BaseDbTest):
    timeout = 5
    ts = pythonioc.Inject(taskservice.TaskService)
    videoDao = pythonioc.Inject('videoDao')
    slaveDao = pythonioc.Inject('slaveDao')
    clock = pythonioc.Inject('reactor')
    
    taskDao = pythonioc.Inject('taskDao')
    db = pythonioc.Inject('dbprovider')
    timeProvider = pythonioc.Inject('timeProvider')
    
    
    @defer.inlineCallbacks
    def setUp(self):
        pythonioc.registerService(taskservice.TaskService)
        pythonioc.registerServiceInstance(testutils.FixedTimeProvider(), 'timeProvider', True)
        basetests.BaseDbTest.setUp(self, False, False)
        # set autorun to false
        self.ts.autoWakeup = False
        
        yield self.clearDatabase()
        
    @defer.inlineCallbacks
    def test_deleteAll(self):
        yield self.ts.addExtrinsicTask('someId', 'someName')
        
        tasks = yield self.taskDao.getAllTasks()
        tasks = list(tasks)
        self.assertEquals(1, len(tasks))


        yield self.taskDao.deleteAll()
        
        tasks = yield self.taskDao.getAllTasks()
        tasks = list(tasks)
        self.assertEquals(0, len(tasks))

    @defer.inlineCallbacks
    def test_getTaskById(self):
        yield self.ts.addExtrinsicTask('someId', 'someName')
        
        task = yield self.taskDao.getTaskById('someId')
        
        self.assertEquals('someName', task.name)
        
        try:
            yield self.taskDao.getTaskById('non-existing ID')
            self.fail('should raise Exception')
        except ItemNotFoundError:
            pass  # ok
        

    @defer.inlineCallbacks
    def test_updateTaskStarted(self):
        yield self.ts.addExtrinsicTask('someId', 'someName')
        
        task = yield self.taskDao.getTaskById('someId')
        self.assertEquals('waiting', task.status)
        
        yield self.taskDao.updateTaskStarted('someId', 10, 'slave-abc')
        
        task = yield self.taskDao.getTaskById('someId')
        self.assertEquals('running', task.status)
        
        
    @defer.inlineCallbacks
    def test_updateTaskFinished_success(self):
        yield self.ts.addExtrinsicTask('someId', 'someName')
        yield self.taskDao.updateTaskFinished('someId', True, 'info')
        
        task = yield self.taskDao.getTaskById('someId')
        self.assertEquals('success', task.status)
        self.assertEquals('info', task.message)
        
        
    @defer.inlineCallbacks
    def test_updateTaskFinished_failure(self):
        yield self.ts.addExtrinsicTask('someId', 'someName')
        yield self.taskDao.updateTaskFinished('someId', False, 'info')
        
        task = yield self.taskDao.getTaskById('someId')
        self.assertEquals('failure', task.status)
        self.assertEquals('info', task.message)
        
        
    @defer.inlineCallbacks
    def test_updateTaskCancelled(self):
        yield self.ts.addExtrinsicTask('someId', 'someName')
        yield self.taskDao.updateTaskCancelled('someId')
        
        task = yield self.taskDao.getTaskById('someId')  
        self.assertEquals('cancelled', task.status)
        
    @defer.inlineCallbacks
    def test_getNumberOfWaitingTasks(self):
        # create an extern task 
        yield self.ts.addExtrinsicTask('someId', 'someName')
        
        # --> should not be found as waiting
        waiting = yield self.taskDao.getNumberOfWaitingTasks()
        self.assertEquals(0, waiting)
        
        yield self.taskDao.addCommandActionTask('taskA', 'slave', 'do-some-command-thing')
        
        waiting = yield self.taskDao.getNumberOfWaitingTasks()
        self.assertEquals(1, waiting)
        
            
    @defer.inlineCallbacks
    def test_getRunnableTasks(self):
        waitingTask = yield self.taskDao.addTask('1', 'task1', 'local', taskType='intrinsic')
        readyTask = yield self.taskDao.addTask('2', 'task2', 'local', taskType='intrinsic')
        failedTask = yield self.taskDao.addTask('3', 'task3', 'local', taskType='intrinsic')
        
        yield self.taskDao.addTask('d1', 'dep1', 'local', taskDeps=[waitingTask], taskType='intrinsic')
        yield self.taskDao.addTask('d2', 'dep2', 'local', taskDeps=[readyTask], taskType='intrinsic')
        yield self.taskDao.addTask('d3', 'dep3', 'local', taskDeps=[failedTask], taskType='intrinsic')
        
        yield self.taskDao.updateTaskFinished('2', True)
        yield self.taskDao.updateTaskFinished('3', False)
        
        tasks = yield self.taskDao.getRunnableTasks()
        tasks = {t.taskId:t for t in tasks}
        
        self.assertEquals(['1', 'd2'], list(tasks.keys()))
        
    @defer.inlineCallbacks
    def test_getRunnableTasks_noexterns(self):
        yield self.ts.addExtrinsicTask('someId', 'someName')
        tasks = yield self.taskDao.getRunnableTasks()
        self.assertEquals(0, len(tasks))
        
        
           
    @defer.inlineCallbacks
    def test_updateTaskStates(self):
        
        taskA = yield self.taskDao.addCommandActionTask('taskA', 'slave', 'do-some-command-thing')
        taskAId = taskA.taskId
        taskB = yield self.taskDao.addCommandActionTask('taskB', 'slave', 'do-some-command-thing',
                                             taskDeps=[taskA])
        taskBId = taskB.taskId
        
        tasks = yield self.taskDao.getRunnableTasks()
        
        # only task A is running, let it fail:
        self.assertEquals(1, len(tasks))
        self.assertEquals(taskAId, tasks[0].taskId)
        yield self.taskDao.updateTaskFinished(taskAId, False)
        
        tasks = yield self.taskDao.getRunnableTasks()
        self.assertEquals(0, len(tasks))
        
        yield self.taskDao.updateTaskStates()
        
        task = yield self.taskDao.getTaskById(taskBId)
        self.assertEquals(taskBId, task.taskId)
        self.assertEquals('cancelled', task.status)
        
    @defer.inlineCallbacks
    def test_cleanTasks_wait_timeout(self):
        
        taskA = yield self.taskDao.addCommandActionTask('taskA', 'slave', 'do-some-command')
        
        # check it's waiting
        yield self.taskDao.cleanTasks()
        taskA2 = yield self.taskDao.getTaskById(taskA.taskId)
        self.assertEquals('waiting', taskA2.status)
        
        self.timeProvider.advance(cfg.vals.task_max_waittime + 1)
        
        yield self.taskDao.cleanTasks()
        taskA2 = yield self.taskDao.getTaskById(taskA.taskId)
        self.assertEquals('timeout', taskA2.status)
        
    @defer.inlineCallbacks
    def test_cleanTasks_running_timeout(self):
        
        taskA = yield self.taskDao.addCommandActionTask('taskA', 'slave', 'do-some-command')
        
        # check it's waiting
        yield self.taskDao.cleanTasks()
        taskA2 = yield self.taskDao.getTaskById(taskA.taskId)
        self.assertEquals('waiting', taskA2.status)
        yield self.taskDao.updateTaskStarted(taskA.taskId, 0)
        
        taskA2 = yield self.taskDao.getTaskById(taskA.taskId)
        self.assertEquals('running', taskA2.status)
        
        # set the clock!
        self.timeProvider.advance(cfg.vals.task_max_runtime + 1)
        
        yield self.taskDao.cleanTasks()
        taskA2 = yield self.taskDao.getTaskById(taskA.taskId)
        self.assertEquals('timeout', taskA2.status)
        
        
    @defer.inlineCallbacks
    def test_cleanTasks_ttl_timeout(self):
        
        taskA = yield self.taskDao.addCommandActionTask('taskA', 'slave', 'do-some-command')
        
        # check it's waiting
        yield self.taskDao.cleanTasks()
        taskA2 = yield self.taskDao.getTaskById(taskA.taskId)
        self.assertEquals('waiting', taskA2.status)
        
        # let it timeout first
        self.timeProvider.advance(cfg.vals.task_max_waittime + 1)
        yield self.taskDao.cleanTasks()
        
        # let it delete
        self.timeProvider.advance(cfg.vals.task_maxage + 1)
        yield self.taskDao.cleanTasks()
        
        try:
            yield self.taskDao.getTaskById(taskA.taskId)
            self.fail('task should be deleted...')
        except ItemNotFoundError:
            pass
        
    @defer.inlineCallbacks
    def test_getNumberOfRunningTasksPerSlave(self):
        self.slaveDao.addSlave(domain.Slave(name='slaveA', status='online'))
        self.slaveDao.addSlave(domain.Slave(name='slaveB', status='online'))
        task = yield self.taskDao.addCommandActionTask('taskA', 'slaveA', 'do-some-command-thing')
        
        yield self.taskDao.updateTaskStarted(task.taskId, 0, task.location)
        
        tasks = yield self.taskDao.getNumberOfRunningTasksPerSlave()
        self.assertEquals({'slaveA':1}, tasks)
        
    @defer.inlineCallbacks
    def test_getTasksPerLocation(self):
        yield self.taskDao.addTask('1', '', 'A', 'extrinsic', status='running')
        yield self.taskDao.addTask('1a', '', 'A', 'extrinsic', status='waiting')
        yield self.taskDao.addTask('2', '', 'A', 'extrinsic', status='running')
        yield self.taskDao.addTask('3', '', 'B', 'extrinsic', status='running')
        yield self.taskDao.addTask('4', '', '', 'extrinsic', status='running')
        
        
        tasks = yield self.taskDao.getTasksPerLocation()
        self.assertEquals({'A':2, 'B':1}, tasks)
        
        tasks = yield self.taskDao.getTasksPerLocation(waiting=True)
        self.assertEquals({'A':3, 'B':1}, tasks)
        
        
    @defer.inlineCallbacks
    def test_getNumTasksForLocation(self):
        yield self.taskDao.addTask('1', '', 'A', 'extrinsic', status='running')
        yield self.taskDao.addTask('1a', '', 'A', 'extrinsic', status='waiting')
        yield self.taskDao.addTask('1b', '', 'A', 'extrinsic', status='failure')
        yield self.taskDao.addTask('1c', '', 'A', 'extrinsic', status='cancelled')
        yield self.taskDao.addTask('1d', '', 'A', 'extrinsic', status='timeout')
        yield self.taskDao.addTask('2', '', 'B', 'extrinsic', status='running')
        
        tasks = yield self.taskDao.getNumTasksForLocation('A')
        self.assertEquals(2, tasks)