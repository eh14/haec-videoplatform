import pythonioc
from twisted.trial import unittest

from haec.kplane import sensorstore
from haec.unittests.kplane import basetests


class TestSensorStore(basetests.BaseDbTest):
    timeProvider = pythonioc.Inject('timeProvider')
    
    def setUp(self):
        basetests.BaseDbTest.setUp(self)
        
        self.store = sensorstore.SensorStore()
        self.store.initBuffers({'switch-S':['slaveA', 'slaveB', 'slaveC', 'slaveD']})
    
    def tearDown(self):
        self.store = None
        
    def _makeSensorEntry(self, **kwargs):
        return {k:0 if k not in kwargs else kwargs[k] 
                    for k in self.store._sensorTypes}
        
    def testAddSensorValues(self):
        self.store.addSensorValues('slaveA', self._makeSensorEntry(cpu_usage=1))
        self.store.sampleValues(self.timeProvider.now())
        self.assertEquals(1, self.store.getNumSensorValues())
        self.assertEquals(0, self.store.getNumPowerValues())
        
    def test_avgCpuLoad(self):
        self.store.addSensorValues('slaveA', self._makeSensorEntry(cpu_usage=0))
        self.store.addSensorValues('slaveB', self._makeSensorEntry(cpu_usage=1))
        self.store.addSensorValues('slaveC', self._makeSensorEntry(cpu_usage=10))
        self.store.addSensorValues('slaveD', self._makeSensorEntry(cpu_usage=5))
        
        self.store.sampleValues(self.timeProvider.now())
        
        self.assertEquals(4, self.store.cpuAvg.getLast())
        
    def test_avgCpuLoad_missing(self):
        
        self.store.addSensorValues('slaveC', self._makeSensorEntry(cpu_usage=10))
        self.store.addSensorValues('slaveD', self._makeSensorEntry(cpu_usage=4))
        self.store.sampleValues(self.timeProvider.now())
        
        # since the others are not set yet, they should not be considered when doing the avg.
        self.assertEquals(7, self.store.cpuAvg.getLast())
        
    def test_avgCpuLoad_empty(self):
        self.assertEquals(0, self.store.cpuAvg.getLast())
    
    def test_minAvgMaxCpu(self):
        raise unittest.SkipTest('aggregator removed from sensorstore')
        for i in range(10):
            self.store.addSensorValues('slaveA', self._makeSensorEntry(cpu_usage=100))
            self.store.addSensorValues('slaveB', self._makeSensorEntry(cpu_usage=i))
            self.store.addSensorValues('slaveC', self._makeSensorEntry(cpu_usage=(i * 10) % 20))
            self.store.addSensorValues('slaveD', self._makeSensorEntry(cpu_usage=5 - i))
            self.store.sampleValues(self.timeProvider.now())
        

        values = self.store.minAvgMaxCpu.getAll()
        
        self.assertEquals(10, len(values))
        self.assertEquals(26.25, values[0][1][1])
        self.assertTrue(all([v[1][2] == 100 for v in values]))
        self.assertEquals(-4, values[9][1][0])
        
        
    def test_addPowerValues(self):
        self.store.addPowerValues(self.timeProvider.now(), {'slaveA':{'current':'340', 'energy':'123',
                                                  'uptime':'12',
                                                  'status':'0'
                                                  },
                                        'slaveB':{'current':'340', 'energy':'123',
                                                  'uptime':'12',
                                                  'status':'0'
                                                  },
                                        'slaveC':{'current':'340', 'energy':'123',
                                                  'uptime':'12',
                                                  'status':'0'
                                                  }
                                        }
                                  )
        
        self.assertEquals(0, self.store.getNumSensorValues())
        self.assertEquals(1, self.store.getNumPowerValues())
        
    def test_addPowerAndSensorValuesSeparate(self):
        """
        Ensures the power and sensor values do not interfere each other
        """
        self.store.addSensorValues('slaveA', self._makeSensorEntry(cpu_usage=2))
        self.store.addPowerValues(self.timeProvider.now(), {'slaveA':{'current':'340', 'energy':'123',
                                                  'uptime':'12',
                                                  'status':'0'
                                           },
                                        }
                                  )
        
        self.store.sampleValues(self.timeProvider.now())
        self.assertEquals(1, self.store.getNumSensorValues())
        self.assertEquals(1, self.store.getNumPowerValues())
        
