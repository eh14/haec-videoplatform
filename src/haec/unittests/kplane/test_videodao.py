'''
Created on May 8, 2014

@author: eh14
'''

import pythonioc
from sqlalchemy import func
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import DetachedInstanceError
from twisted.internet import defer

from haec.kplane import domain 
from haec.unittests.kplane import basetests


class TestVideoDao(basetests.BaseDbTest):
    
    slaveDao = pythonioc.Inject('slaveDao')
    dao = pythonioc.Inject('videoDao')
    
    db = pythonioc.Inject('dbprovider')
    
    
    
    @defer.inlineCallbacks
    def setUp(self):
        basetests.BaseDbTest.setUp(self, False, False)
        yield self.clearDatabase()
    
    @defer.inlineCallbacks
    def test_addVideo(self):
        yield self.dao.addVideo('slave', domain.Video(name='hello', variant='0', fname='file.avi'))
        
        vidNames = yield self.dao.getVideoNames()
        self.assertEquals(['hello'], vidNames)
        
        vids = yield self.dao.getVideos()
        self.assertEquals('hello', vids[0].name)
        
    @defer.inlineCallbacks
    def test_addVideoDuplicate(self):
        yield self.dao.addVideo('slaveA', domain.Video(name='hello', fname='file.avi'))
        yield self.dao.addVideo('slaveB', domain.Video(name='hello', fname='file2.avi'))
        vids = yield self.dao.getVideos()
        self.assertEquals(1, len(vids))
        self.assertEquals('file.avi', vids[0].fname)
        
    @defer.inlineCallbacks
    def test_getVideoById(self):
        yield self.dao.addVideo('slaveA', domain.Video(name='hello', fname='file.avi'))
        slaveAVids = yield self.dao.getVideosBySlaveName('slaveA')
        self.assertEquals(1, len(slaveAVids))
        
        vid = yield self.dao.getVideoById(slaveAVids[0].id)
        self.assertEquals('hello', vid.name)
        self.assertEquals('file.avi', vid.fname)
        
    @defer.inlineCallbacks
    def test_getUnreplicatedVideosForSlave(self):
        vidId = yield self.dao.addVideo('slaveA', domain.Video(name='hello', fname='file.avi'))
        yield self.slaveDao.setSlaveOnlineStatus('slaveA', True)
        yield self.dao.addVideo('slaveC', domain.Video(name='anothervid', fname='file.avi'))
        
        unreplicated = yield self.dao.getUnreplicatedVideosForSlave('slaveA')
        
        self.assertEquals(1, len(unreplicated))
        self.assertEquals(vidId, unreplicated[0].id)

        # replicate it on slaveB, the list should be empty  
        yield self.dao.addVideo('slaveB', domain.Video(name='hello', fname='file.avi'))
        yield self.slaveDao.setSlaveOnlineStatus('slaveB', True)
        
        unreplicated = yield self.dao.getUnreplicatedVideosForSlave('slaveA')
        self.assertEquals(0, len(unreplicated))
        
        # turn off slaveB, so it is again unreplicated
        yield self.slaveDao.setSlaveOnlineStatus('slaveB', False)
        unreplicated = yield self.dao.getUnreplicatedVideosForSlave('slaveA')
        self.assertEquals(1, len(unreplicated))
        
        self.assertEquals(vidId, unreplicated[0].id)
        
    @defer.inlineCallbacks
    def test_getVideosBySlaveName(self):
        yield self.dao.addVideo('slaveA', domain.Video(name='1', variant='0', fname='file.avi'))
        yield self.dao.addVideo('slaveB', domain.Video(name='2', variant='0', fname='file.avi'))
        
        slaveAVids = yield self.dao.getVideosBySlaveName('slaveA')
        self.assertEquals(1, len(slaveAVids))
        self.assertEquals('1', slaveAVids[0].name)
        
    @defer.inlineCallbacks
    def test_getVideosBySlave(self):
        videoa = domain.Video(name="a")
        videob = domain.Video(name="b")
        videoc = domain.Video(name="c")
        
        yield self.dao.addVideo('1', videoa)
        yield self.dao.addVideo('2', videob)
        yield self.dao.addVideo('1', videoc)
        
        vids = yield self.dao.getVideos()
        self.assertEquals(3, len(vids))
        
        slave1Vids = yield self.dao.getVideosBySlaveName('1')
        self.assertEquals(2, len(slave1Vids))
        slave2Vids = yield self.dao.getVideosBySlaveName('2')
        self.assertEquals(1, len(slave2Vids))
        
    @defer.inlineCallbacks
    def test_removeVideos(self):
        yield self.dao.addVideo('slaveA', domain.Video(name='1', variant='0', fname='file.avi'))
        yield self.dao.addVideo('slaveB', domain.Video(name='2', variant='0', fname='file.avi'))
        yield self.dao.addVideo('slaveA', domain.Video(name='2', variant='0', fname='file2.avi'))
        
        self.dao.removeVideos('slaveA', 'file.avi')
        
    @defer.inlineCallbacks
    def test_getSlavesForVideo(self):
        
        yield self.dao.addVideo('slaveA', domain.Video(name='1', variant='', fname='file'))
        yield self.dao.addVideo('slaveB', domain.Video(name='1', variant='', fname='file'))
        yield self.dao.addVideo('slaveC', domain.Video(name='1', variant='1', fname='file'))
        
        slaves = yield self.dao.getSlavesForVideo(name='1', variant='')
        self.assertEquals(set(['slaveA', 'slaveB']), set([s.name for s in slaves]))
        
        slaves = yield self.dao.getSlavesForVideo(name='1', variant='1')
        self.assertListEqual(['slaveC'], [s.name for s in slaves])
        
    @defer.inlineCallbacks
    def test_getVideo(self):
        yield self.dao.addVideo('slaveA', domain.Video(name='1', variant='', fname='file'))
        
        vid = yield self.dao.getVideo('1', '')
        
        self.assertEquals('file', vid.fname)
        

        self.assertRaises(DetachedInstanceError, getattr, vid, 'slaves')
                
        vid = yield self.dao.getVideo('1', '', loadSlaves=True)
        self.assertEquals('slaveA', vid.slaves[0].name)
        
    @defer.inlineCallbacks
    def test_getVideos(self):
        
        vid = domain.Video(name='1', variant='0', fname='file.avi')
        
        yield self.dao.addVideo('slaveA', vid)
        
        vids = yield self.dao.getVideos()
        savedVid = vids[0]
        
        self.assertEquals(vid.name, savedVid.name)
        self.assertEquals(vid.variant, savedVid.variant)
        self.assertEquals(vid.fname, savedVid.fname)
    
    @defer.inlineCallbacks
    def test_isVideoOnSlave(self):
        vid1Id = yield self.dao.addVideo('slaveA', domain.Video(name='vid1', variant='', width=100, fname='file.avi'))
        vid2Id = yield self.dao.addVideo('slaveB', domain.Video(name='vid2', variant='', width=100, fname='file.avi'))
        
        self.assertNotEqual(vid1Id, vid2Id)
        vidOnSlave = yield self.dao.isVideoOnSlave(vid1Id, 'slaveB')
        self.assertFalse(vidOnSlave)
        vidOnSlave = yield self.dao.isVideoOnSlave(vid2Id, 'slaveA')
        self.assertFalse(vidOnSlave)
        
        vidOnSlave = yield self.dao.isVideoOnSlave(vid1Id, 'slaveA')
        self.assertTrue(vidOnSlave)
        vidOnSlave = yield self.dao.isVideoOnSlave(vid1Id, 'slaveB')
        self.assertTrue(self.dao.isVideoOnSlave(vid2Id, 'slaveB'))
               
    @defer.inlineCallbacks         
    def test_searchVideos(self):
        yield self.dao.addVideo('slaveA', domain.Video(name='someVid', variant='', width=100, fname='file.avi'))
        yield self.dao.addVideo('slaveA', domain.Video(name='someVid', variant='123', width=200, fname='file__id123.avi'))
        yield self.dao.addVideo('slaveA', domain.Video(name='someVid_', variant='', width=100, fname='somefile.avi'))
        
        result = yield self.dao.searchVideos(width=100)
        self.assertEquals(2, len(result))
        
        result = yield self.dao.searchVideos(namePattern='some*')
        self.assertEquals(3, len(result))
        
    @defer.inlineCallbacks
    def test_searchVideos_codecs(self):
        yield self.dao.addVideo('slave', domain.Video(name='someVid1', variant='', video_codec='codec1'))
        yield self.dao.addVideo('slave', domain.Video(name='someVid2', variant='', video_codec='codec2'))
        
        result = yield self.dao.searchVideos()
        self.assertEquals(2, len(result))
        
        vids = yield self.dao.searchVideos(videoCodec='codec2')
        self.assertEquals('someVid2', vids[0].name)
        
    @defer.inlineCallbacks
    def test_searchVideos_loadSlaves(self):
        yield self.dao.addVideo('slaveA', domain.Video(name='someVid2', variant='', video_codec='codec2'))

        vids = yield self.dao.searchVideos(namePattern='*Vid2', loadSlaves=True)
        self.assertEquals(1, len(vids))
        self.assertEquals(1, len(vids[0].slaves))
        self.assertEquals('slaveA', vids[0].slaves[0].name)

    @defer.inlineCallbacks
    def test_getAudioVideoCodecNames(self):
        yield self.dao.addVideo('slave', domain.Video(name='someVid', video_codec='A', audio_codec='B'))
        yield self.dao.addVideo('slave', domain.Video(name='someVid', variant='1', video_codec='C', audio_codec='D'))
        
        codecs = yield self.dao.getAudioVideoCodecNames()
        self.assertEquals(2, len(codecs))
        self.assertListEqual(['A', 'C'], codecs['video'])
        self.assertListEqual(['B', 'D'], codecs['audio'])
        
    @defer.inlineCallbacks
    def test_getVideoVariantsForName(self):
        yield self.dao.addVideo('slave', domain.Video(name='someVid', variant=''))
        yield self.dao.addVideo('slaveA', domain.Video(name='someVid', variant='1'))
        yield self.dao.addVideo('slaveB', domain.Video(name='someVid', variant='1'))
        yield self.dao.addVideo('slaveC', domain.Video(name='someVid', variant='1'))
        
        vids = yield self.dao.getVideoVariantsForName('hello')
        self.assertEquals(0, len(vids))
        
        vids = yield self.dao.getVideoVariantsForName('someVid')
        self.assertEquals(2, len(vids))
        self.assertEquals(['someVid'] * 2, [v.name for v in vids])


    @defer.inlineCallbacks
    def test_uniqueVariantConstraint(self):
        # raise unittest.SkipTest('uniqe-constraint disabled')
        yield self.dao.addVideo('slaveA', domain.Video(name='someVid', variant=''))
        yield self.dao.addVideo('slaveA', domain.Video(name='someVid', variant='1'))

        # add the same variant should raise an integrity error
        with self.db() as db:
            # we need to generate the insert directly here, since the
            # addVideo-method of the dao will check for duplicates!
            q = domain.Video.__table__.insert().values(name='someVid', variant='')
            self.assertRaises(IntegrityError, db.execute, q)
            
    @defer.inlineCallbacks
    def test_getVideosForVariant(self):
        yield self.dao.addVideo('slaveA', domain.Video(name='someVid', variant=''))
        yield self.dao.addVideo('slaveB', domain.Video(name='someVid', variant=''))
        yield self.dao.addVideo('slaveA', domain.Video(name='someVid2', variant='1'))
        yield self.dao.addVideo('slaveB', domain.Video(name='someVid', variant='1'))
        
        
        vids = yield self.dao.getVideosForVariant(name='someVid', variant='')
        self.assertEquals(1, len(vids))
        vids = yield self.dao.getVideosForVariant(name='someVid', variant='1')
        self.assertEquals(1, len(vids))
        vids = yield self.dao.getVideosForVariant(name='someVid2', variant='1')
        self.assertEquals(1, len(vids))
        
        vids = yield self.dao.getVideosForVariant(name='someVid2', variant='2')
        self.assertEquals(0, len(vids))
        vids = yield self.dao.getVideosForVariant(name='someVid1', variant='1')
        self.assertEquals(0, len(vids))
    
    @defer.inlineCallbacks
    def test_getAllVideosForName(self):
        yield self.dao.addVideo('slaveA', domain.Video(name='someVid', variant=''))
        yield self.dao.addVideo('slaveB', domain.Video(name='someVid', variant='1'))
        yield self.dao.addVideo('slaveB', domain.Video(name='anotherVid', variant='1'))
        
        allVids = yield self.dao.getAllVideosForName('someVid')
        self.assertEquals(2, len(allVids))
        allVids = yield self.dao.getAllVideosForName('some*')
        self.assertEquals(2, len(allVids))
        allVids = yield self.dao.getAllVideosForName('*Vid')
        self.assertEquals(3, len(allVids))
        
    @defer.inlineCallbacks
    def test_getAllVideosForName_loadSlaves(self):
        yield self.dao.addVideo('slaveA', domain.Video(name='someVid', variant=''))
        yield self.dao.addVideo('slaveB', domain.Video(name='someVid', variant=''))
        yield self.dao.addVideo('slaveB', domain.Video(name='anotherVid', variant='1'))
        
        vids = yield self.dao.getAllVideosForName('someVid', loadSlaves=True)
        vid1 = vids[0]
        self.assertEquals(2, len(vid1.slaves))
        self.assertTrue(vid1.slaves[0].name in ['slaveA', 'slaveB'])
        self.assertTrue(vid1.slaves[1].name in ['slaveA', 'slaveB'])
        
        vids = yield self.dao.getAllVideosForName('anotherVid', loadSlaves=True)
        vid2 = vids[0]
        self.assertEquals('slaveB', vid2.slaves[0].name)
        
    @defer.inlineCallbacks
    def test_getTotalNumberOfVideos(self):
        numVids = yield self.dao.getTotalNumberOfVideos()
        self.assertEquals(0, numVids)
        
        yield self.dao.addVideo('slaveA', domain.Video(name='someVid', variant=''))
        numVids = yield self.dao.getTotalNumberOfVideos()
        self.assertEquals(1, numVids)
        
        yield self.dao.addVideo('slaveB', domain.Video(name='someVid', variant='1'))
        numVids = yield self.dao.getTotalNumberOfVideos()
        self.assertEquals(2, numVids)
                
    @defer.inlineCallbacks
    def test_addremoveVideoFromSlave(self):
        vidId1 = yield self.dao.addVideo('slaveA', domain.Video(name='vidA', variant=''))
        vidId2 = yield self.dao.addVideo('slaveB', domain.Video(name='vidA', variant=''))
        
        self.assertEquals(vidId1, vidId2)
        slaves = yield self.dao.getSlavesForVideo('vidA', '')
        self.assertEquals(2, len(slaves))
        
        yield self.dao.removeVideoFromSlave(vidId1, 'slaveB')
        slaves = yield self.dao.getSlavesForVideo('vidA', '')
        self.assertEquals(1, len(slaves))
        
        yield self.dao.addVideoToSlave(vidId1, 'slaveB')
        slaves = yield self.dao.getSlavesForVideo('vidA', '')
        self.assertEquals(2, len(slaves))
        
        yield self.dao.removeVideoFromSlave(vidId1, 'slaveA')
        slaves = yield self.dao.getSlavesForVideo('vidA', '')
        self.assertEquals(1, len(slaves))
        
        # remove the video from the last slave --> will remove the video
        yield self.dao.removeVideoFromSlave(vidId1, 'slaveB')
        vids = yield self.dao.getAllVideosForName('vidA')
        self.assertEquals(0, len(vids))
        
    @defer.inlineCallbacks
    def test_uniquevideo2slaveMapping(self):
        yield self.slaveDao.addSlave(domain.Slave(name='slaveA'))
        vidId = yield self.dao.addVideo('slaveA', domain.Video(name='vidA', variant=''))
        
        yield self.dao.addVideoToSlave(vidId, 'slaveA')
        
        with self.db() as db:
            numEntries = db.query(func.count('*')).one()[0]
            self.assertEquals(1, numEntries)
            
            q = domain.video2slaveTable.insert().values(video_id=vidId, slave_name='slaveA')
            self.assertRaises(IntegrityError, db.execute, q)
        
        
    def _addVideos(self, errors, vids):
        try:
            self.dao.addVideos(vids)
        except:
            errors[0] += 1
            raise

    @defer.inlineCallbacks
    def test_getFilenameForId(self):
        name = yield self.dao.getFilenameForId("someId")
        self.assertIsNone(name)
        
        vidId1 = yield self.dao.addVideo('slaveA', domain.Video(name='vidA', fname='file'))
        
        name = yield self.dao.getFilenameForId(vidId1)
        self.assertEquals('file', name)
        
    def test_syncAddVideo_single(self):
        vid = domain.Video(name='vidA')
        with self.dao.db() as db:
            self.dao.syncAddVideo(db, vid)
            
    def test_syncAddVideo_conflict(self):
        vid = domain.Video(name='vidA')
        with self.dao.db() as db:
            self.dao.syncAddVideo(db, vid)
            
        vid = domain.Video(name='vidA')
        with self.dao.db() as db:
            self.dao.syncAddVideo(db, vid)
            
    @defer.inlineCallbacks
    def test_cleanOrphanVideos(self):
        
        # initially empty
        self.assertDeferredEquals(0, self.dao.getTotalNumberOfVideos())
        
        
        # add two videos
        yield self.dao.addVideo('slaveA', domain.Video(name='vidA'))
        yield self.dao.addVideo('slaveB', domain.Video(name='vidB'))
        self.assertDeferredEquals(2, self.dao.getTotalNumberOfVideos())
        
        # delete one slave
        with self.db() as db:
            db.query(domain.Slave).filter(domain.Slave.name == 'slaveB').delete()
            
        self.assertDeferredEquals(2, self.dao.getTotalNumberOfVideos())
        yield self.dao.cleanOrphanVideos()
        self.assertDeferredEquals(1, self.dao.getTotalNumberOfVideos())
        
