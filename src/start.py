'''
Created on Mar 25, 2014

@author: eh14
'''

from twisted.python import log
import sys


#
# workaround for delayed ntpdate-issue:
# use another reactor/poller
import pythonioc
from haec import config

from twisted.internet import selectreactor
selectreactor.install()

# from twutils import reactorprof
# reactorprof.install()

def main():
    """
    Starts up the appropriate server role.
    """
    
    cfg = pythonioc.Inject(config.Config)
    cfg.initFromCmd(exit=True)
    
    log.startLogging(sys.stdout)
    cfg.dump()
    
    pythonioc.registerServiceInstance(cfg, overwrite=True)

    if cfg.vals.role == 'master':
        from haec import master
        master.run()
    elif cfg.vals.role == 'slave':
        from haec import slave
        slave.run()
    else:
        raise AttributeError('invalid role')


if __name__ == '__main__':
    main()
