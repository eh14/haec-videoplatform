from watchdog.observers import Observer
from functools import wraps
import time
import glob
import os
import fnmatch

class _Handler(object):
    def __init__(self, func, args, kwargs, pattern):
        self.f = func
        self.args = args
        self.kwargs = kwargs
        self.p = pattern
    
    def dispatch(self, event):
        name = os.path.basename(event.src_path)
        
        # check if any pattern matches
        if any([fnmatch.fnmatch(name, p) for p in self.p]):
            print name, "changed, running function"
            self.f(*self.args, **self.kwargs) 
            
        


def watch(path, pattern='*', now=True):
    """
    Runs the task repeatedly if any file is changed.
    @param path: path to observe
    @param pattern: globbing pattern of included files (all by default)
    """
    
    if not hasattr(pattern, '__iter__'):
        pattern = [pattern]
    
    def outer(func):
        
        @wraps(func)
        def inner(*args, **kwargs):
            if now:
                func(*args, **kwargs)
                
            print "Starting observer for path %s (pattern %s)" % (path, pattern)
            observer = Observer()
            handler = _Handler(func, args, kwargs, pattern)
            observer.schedule(handler, path=path, recursive=True)
            observer.start()
            # wait for the interrupt
            try:
                while True:
                    time.sleep(1)
            except KeyboardInterrupt:
                observer.stop()
            observer.join()
            
            
            
        return inner
    
    return outer
    
