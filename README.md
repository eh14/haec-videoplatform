# Videoplatform project for HAEC #
This is still in development, horribly documented and probably buggy. Its main purpose was to be used as a demonstrator-application for different research-projects from TU Dresden

## Dependencies
System-packages:

* python (developed on 2, 3 might work)
* python-twisted-core
* python-twisted-web
* python-pip
* sshpass
* mysql-server
* sqlite
* libmysqlclient-dev

Python packages:

* Django 1.6.2
* jsonpickle
* requests
* SQLAlchemy
* pandas
* JPype1
* miniupnpc
* pythonioc
* libav-tools
* netaddr
* netifaces
* psutil
* pyzmq
* networkx


## Howto
Most of the tasks are automated with *fabric*. Just go to the folder, and do `fab -l` for a list of all tasks.

Most importantly: run `fab check` to run all unittests. `fab itest` runs all integration tests (starts a local cluster, mocks theatre, cambrionix etc.).

`fab startMockCluster [videodir]` starts a local cluster with one slave, where the slave's video directory points to [videodir].

For questions, issues etc., feel free to create tickets or write some wiki pages.