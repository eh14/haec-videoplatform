from twisted.trial import unittest
import requests

import config
import json
import time
from django.http.response import HttpResponseNotAllowed, HttpResponseBadRequest

class TestStatistics(unittest.TestCase):
        
    def test_getSlaveStatus(self):
        response = requests.get('http://localhost:%s/statistics/slaveStatus' % config.masterWebPort)
        self.assertEquals(200, response.status_code)
        
    def test_getSlaveStatus_regression_type(self):
        response = requests.get('http://localhost:%s/statistics/slaveStatus' % config.masterWebPort)
        self.assertEquals(200, response.status_code)
        
        values = json.loads(response.text)
        self.assertNumber(values[0]['current'])
        self.assertNumber(values[0]['energy'])
        self.assertNumber(values[0]['cpu_usage'])
        self.assertNumber(values[0]['memory_usage'])
        self.assertNumber(values[0]['network_out'])
        self.assertNumber(values[0]['network_in'])
        
    def assertNumber(self, n):
        self.assertTrue(isinstance(n, float) or 
                        isinstance(n, int) or
                        isinstance(n, long),
                            'expected Number, was %s' % (n.__class__.__name__))

    def test_setSlaveStatus(self):
        response = requests.get('http://localhost:%s/statistics/slaveStatus' % config.masterWebPort)
        
        values = json.loads(response.text)

        # helper function that helps us to find the slave we're looking
        # for in the list of values from the respones.        
        def findSlaveInValues(values):
            for value in values:
                if value['name'] == 'slave-y1':
                    return value
            else:
                raise Exception("slave-y1 not found in result")
                    
        self.assertEquals('slave-y1', findSlaveInValues(values)['name'])
        self.assertTrue(values[0]['poweron'] in [False, True])
        
        # some invalid requests
        # GET is wrong
        self.assertEquals(HttpResponseNotAllowed.status_code, requests.get('http://localhost:%s/statistics/setSlaveStatus' % config.masterWebPort).status_code)
        
        # missing status 
        self.assertEquals(HttpResponseBadRequest.status_code, requests.post('http://localhost:%s/statistics/setSlaveStatus?slaveNames=slave-y1' % config.masterWebPort).status_code)
        
        # missing slave Name
        self.assertEquals(HttpResponseBadRequest.status_code, requests.post('http://localhost:%s/statistics/setSlaveStatus?status=1' % config.masterWebPort).status_code)
        
        # nonexistent slave
        self.assertEquals(HttpResponseBadRequest.status_code, requests.post('http://localhost:%s/statistics/setSlaveStatus?slaveNames=slave-x2&status=1' % config.masterWebPort).status_code)
        
        self.assertEquals(200, requests.post('http://localhost:%s/statistics/setSlaveStatus?slaveNames=slave-y1&status=off' % config.masterWebPort).status_code)
        # wait some time so the server pulls an update from the cambrionix
        time.sleep(0.5)
        response = requests.get('http://localhost:%s/statistics/slaveStatus' % config.masterWebPort)
        values = json.loads(response.text)
        self.assertEquals(False, findSlaveInValues(values)['poweron'])
        
        self.assertEquals(200, requests.post('http://localhost:%s/statistics/setSlaveStatus?slaveNames=slave-y1&status=on' % config.masterWebPort).status_code)
        # wait some time so the server pulls an update from the cambrionix
        time.sleep(0.5)
        response = requests.get('http://localhost:%s/statistics/slaveStatus' % config.masterWebPort)
        values = json.loads(response.text)
        self.assertEquals(True, findSlaveInValues(values)['poweron'])

    def test_show_statistics(self):
        response = requests.get('http://localhost:%s/statistics/show' % config.masterWebPort)
        self.assertEquals(200, response.status_code)
        
