'''

@author: eh14
'''


import base

class TestOptViews (base.BaseIntegrationTest):
    
    def test_slaveList(self):
        self.getPathAndAssert200('statistics/slaveList')

    def test_slaveStatus(self):
        self.getPathAndAssert200('statistics/slaveStatus')