from twisted.trial import unittest
from twisted.web.client import getPage
import requests

import config

class TestHome(unittest.TestCase):
    
    def test_index(self):
        response = requests.get('http://localhost:%s/' % config.masterWebPort)
        self.assertEquals(200, response.status_code)
        
        response = requests.get('http://localhost:%s' % config.masterWebPort)
        self.assertEquals(200, response.status_code)
        
    def test_showHome(self):
        response = requests.get('http://localhost:%s/home/' % config.masterWebPort)
        self.assertEquals(200, response.status_code)
        
        
    def test_showDashboard(self):
        response = requests.get('http://localhost:%s/home/dashboard/' % config.masterWebPort)
        self.assertEquals(200, response.status_code)