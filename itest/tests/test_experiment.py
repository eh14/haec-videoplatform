'''

@author: eh14
'''
import base
import json

class TestExperimentclass (base.BaseIntegrationTest):
    def test_randomVideo(self):
        vids = json.loads(self.get('_itest/getallvideos').text)
        names = [n['name'] for n in vids]
        r = self.get('exp/randomVideo', allow_redirects=False)
        self.assertEquals(302, r.status_code)
        
        # assert at least any of the names is in the redirect url
        self.assertTrue(any([name in r.headers['location'] for name in names]))
        
    def test_serverStats(self):
        r = self.get('exp/serverStats', allow_redirects=False)
        r = json.loads(r.text)
        self.assertTrue('slave-y1' in r)
        self.assertTrue('switch-S' in r)
