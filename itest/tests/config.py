"""
Configuration of the environment
that is used to set it up as well as
run the tests.
"""
import os


masterNodePort = '7900'
masterWebPort = '9990'

slaveNodePort = '7901'
slaveWebPort = '9980'

THEATRE_PORT = 8100


itest_tmp = '/tmp/_trial_tmp_itest'
video_dir = os.path.join('/tmp', 'slave_videos')
originalVidDir = os.path.join(os.path.dirname(__file__), '../', 'videos')