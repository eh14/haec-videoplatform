import requests

import base
import uuid
import os
import shutil
import time

videoDir = os.path.join(os.path.dirname(__file__), '..', 'videos')

class TestTaskInterface(base.BaseIntegrationTest):
    def setUp(self):
        response = requests.put(self._url('tasks/removeAllTasks'))
        self.assertEquals(200, response.status_code)
    
    def test_taskNew(self):
        response = requests.post(self._url('_itest/taskAdd?taskId=1&srcName=name'))
        self.assertEquals(200, response.status_code)
        tasks = self._getTasks()
        self.assertTrue('1' in tasks.keys())
        self.assertEquals('waiting' , tasks['1']['status'])
        
    def test_taskStarted(self):
        response = requests.post(self._url('_itest/taskAdd?taskId=2&srcName=name'))
        self.assertEquals(200, response.status_code)
        
        response = requests.put(self._url('_itest/taskStart?taskId=2&estimatedTime=100000'))
        self.assertEquals(200, response.status_code)
        tasks = self._getTasks()
        self.assertTrue('2' in tasks.keys())
        self.assertEquals('running' , tasks['2']['status'])
        self.assertEquals(100000, tasks['2']['estimatedTime'])
        
    def test_taskFinished_failure(self):
        response = requests.post(self._url('_itest/taskAdd?taskId=3&srcName=name'))
        self.assertEquals(200, response.status_code)
        
        response = requests.put(self._url('_itest/taskStart?taskId=3&estimatedTime=10000'))
        self.assertEquals(200, response.status_code)
        
        # test failure case here first
        response = requests.put(self._url('_itest/taskFinish?taskId=3&success=0'))
        self.assertEquals(200, response.status_code, response.text)
        tasks = self._getTasks()
        self.assertTrue('3' in tasks.keys())
        self.assertEquals('failure' , tasks['3']['status'])
        
    def test_taskFinished_success(self):
        
        videoName = 'testvideo' + str(uuid.uuid4())[:8]
        response = requests.post(self._url('_itest/taskAdd?taskId=4&srcName=%s' % videoName))
        self.assertEquals(200, response.status_code)
         
        response = requests.put(self._url('_itest/taskStart?taskId=4&estimatedTime=500000'))
        self.assertEquals(200, response.status_code)
        
        response = requests.put(self._url('_itest/taskFinish?taskId=4&success=1&slaveName=slave-y1'))
        self.assertEquals(200, response.status_code)
        
        for _i in range(20):
            time.sleep(0.5)
            task = self._taskDetails('4')
            if task['status'] != 'success':
                continue
            else:
                break
        else:
            self.fail('Seems like the video was not added to the video store')

    def test_taskFinished_failWithoutStart(self):
        response = requests.post(self._url('_itest/taskAdd?taskId=5&srcName=name'))
        self.assertEquals(200, response.status_code)
        
        tasks = self._getTasks()
        self.assertTrue('5' in tasks.keys())
        self.assertEquals('waiting' , tasks['5']['status'])
        
        response = requests.put(self._url('_itest/taskFinish?taskId=5&success=0'))
        
        self.assertEquals(200, response.status_code, response.text)
        tasks = self._getTasks()
        self.assertTrue('5' in tasks.keys())
        self.assertEquals('failure' , tasks['5']['status'])