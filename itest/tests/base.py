'''
Created on Jul 11, 2014

@author: eh14
'''
from twisted.trial import unittest

import config
import requests
import json
import shutil
import os
import time

class BaseIntegrationTest(unittest.TestCase):
    def _url(self, suffix):
        return ('http://localhost:%s/' % config.masterWebPort) + suffix
    
    def get(self, urlSuffix, **args):
        return requests.get(self._url(urlSuffix), **args)
    
    def _getTasks(self):
        taskList = requests.get(self._url('tasks/list'))
        self.assertEquals(200, taskList.status_code)
        return {task['taskId']:task for task in json.loads(taskList.text)}
    
    def _taskDetails(self, taskId):
        response = requests.get(self._url('tasks/details?taskId=%s' % taskId))
        self.assertEquals(200, response.status_code)
        
        return json.loads(response.text)
        
    
    def _getVidVariants(self, name):
        
        vidList = requests.get(self._url('video/variantsForName?name=' + name))
        vidList = json.loads(vidList.text)
        return vidList
    
    def _numOfFilesInDir(self, dirName):
        return len([name for name in os.listdir(dirName) if os.path.isfile(os.path.join(dirName, name))]) 

    def _resetVideoFolderAndRescan(self):
        
        time.sleep(0.5)
        shutil.rmtree(config.video_dir)
        shutil.copytree(config.originalVidDir, config.video_dir)
        time.sleep(0.5)
                
        assert self._numOfFilesInDir(config.originalVidDir) == self._numOfFilesInDir(config.video_dir), "error resetting the video dir"
        rescanResponse = requests.post(self._url('_itest/rescanvideodir'))
        self.assertEquals(200, rescanResponse.status_code)
        
        for _i in range(20):
            allVideosResponse = requests.get(self._url('_itest/getallvideos'))
            numFiles = self._numOfFilesInDir(config.video_dir)
            allVideos = json.loads(allVideosResponse.text)
            # print "vids: ", [(v['name'], v['variant']) for v in json.loads(allVideosResponse.text)]
            if numFiles == len(allVideos):
                break
            # print "vids: ", [(v['name'], v['variant']) for v in json.loads(allVideosResponse.text)]
            time.sleep(0.5)
        else:
            self.fail('Error resetting video dir')
        
        
    def getPathAndAssert200(self, path):
        """
        Sends a GET to passed path and checks for 200.
        Timeout is 5s
        """
        resp = requests.get('http://localhost:%s/%s' % (config.masterWebPort, path.strip('/')), timeout=5)
        
        self.assertEquals(200, resp.status_code)
        
        return resp