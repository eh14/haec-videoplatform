from twisted.trial import unittest
import requests

import config
import json
import time
from haec import config as haec_config
import pythonioc

cfg = pythonioc.Inject(haec_config.Config)

class TestVideo(unittest.TestCase):

    def setUp(self):    
        cfg.init(video_dir=config.video_dir)
        
    def _url(self, suffix):
        return ('http://localhost:%s/' % config.masterWebPort) + suffix
    
    def test_streamVideo_badrequest(self):
        
        # no arguments -> bad request
        response = requests.get('http://localhost:%s/video/stream' % config.masterWebPort)
        self.assertEquals(400, response.status_code)
        
        # invalid name/variant --> 404
        response = requests.get('http://localhost:%s/video/stream?name=nonexisting-name&variant=' % config.masterWebPort)
        self.assertEquals(404, response.status_code)
        
        response = requests.get('http://localhost:%s/video/stream?variant=someviant&name=tiny_test' % config.masterWebPort)
        self.assertEquals(404, response.status_code)
        
    def test_streamVideo_redirect(self):
        # request real video with redirect
        response = requests.get('http://localhost:%s/video/stream?variant=&name=tiny_test' % config.masterWebPort, allow_redirects=False)
        self.assertEquals(302, response.status_code)
        self.assertEquals("http://localhost:%s/tiny_test.mp4?download=False" % config.slaveWebPort, response.headers.get('location'))
        
    def test_streamVideo_noredirect(self):
        response = requests.get('http://localhost:%s/video/stream?variant=&name=tiny_test&redirect=False' % config.masterWebPort, allow_redirects=False)
        self.assertEquals(200, response.status_code)
        self.assertEquals("http://localhost:%s/tiny_test.mp4?download=False" % config.slaveWebPort, response.content)

    def test_doSearch(self):
        response = requests.get('http://localhost:%s/video/_doSearch/' % config.masterWebPort)
        self.assertEquals(200, response.status_code)
        
        
    def test_search(self):
        response = requests.get('http://localhost:%s/video/search/' % config.masterWebPort)
        self.assertEquals(200, response.status_code)
        
    def test_details(self):
        response = requests.get('http://localhost:%s/video/details/' % config.masterWebPort)
        self.assertEquals(200, response.status_code)
        
    def test_requestVideo(self):
        response = requests.get('http://localhost:%s/video/transcoding?name=tiny_test&profile=2&qualityMode=normal' % config.masterWebPort,
                                allow_redirects=False)

        self.assertEquals(404, response.status_code, response.text)

    def test_requestVideo_existing(self):
        response = requests.get('http://localhost:%s/video/transcoding?name=tiny_test&profile=1&qualityMode=normal' % config.masterWebPort, allow_redirects=False)
        
        # the video exists, we're getting a redirect
        
        self.assertEquals(302, response.status_code)
        
        # both of our test videos have same dimensions,
        # so we won't know which one the server will chose (variant '' or variant '1234')
        self.assertTrue(('http://localhost:%s/video/stream?name=tiny_test' % config.masterWebPort) in response.headers.get('location'))
        
        
