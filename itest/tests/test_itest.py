import requests

import base
import config
from fabric.operations import local
import time
import json
from fabric.context_managers import quiet

class TestItestInterface(base.BaseIntegrationTest):
    """
    Tests the REST interface that is used for the integration test
    (not THIS integration test, but the integration test, testing the whole
    Haec-Cubie! Sorry for the misleading name)
    """
    def test_rescanVideoDir(self):
        self._resetVideoFolderAndRescan()
        
        response = requests.get('http://localhost:%s/_itest/getallvideos' % config.masterWebPort)
        self.assertEquals(200, response.status_code)
        
        videos = json.loads(response.text)
        videos = {v['name']:v for v in  videos}
        self.assertEquals(1, len(videos))
        self.assertEquals(['tiny_test'], videos.keys())
        
        existingVideo = 'tiny_test.mp4'
        newVideo = 'newvideo.mp4'
        try:
            # create a new video
            with quiet():
                local('cp %s/%s %s/%s' % (config.video_dir, existingVideo, config.video_dir, newVideo))
            
            response = requests.post('http://localhost:%s/_itest/rescanvideodir?slaveName=slave-y1' % config.masterWebPort)
            self.assertEquals(200, response.status_code)
            
            for _i in range(20):
                time.sleep(0.5)
                response = requests.get('http://localhost:%s/_itest/getallvideos' % config.masterWebPort)
                videos = json.loads(response.text)
                videos = {v['name']:v for v in videos}
                
                if len(videos) > 1:
                    self.assertTrue('newvideo' in videos)
            
        finally:
            with quiet():
                local('rm %s/%s' % (config.video_dir, newVideo))
        
    def test_rescanVideoDir_all(self):
        """
        tests to call the function without parameter. No fancy video-copy-and-then-check-again here..
        """
        
        response = requests.post('http://localhost:%s/_itest/rescanvideodir' % config.masterWebPort)
        self.assertEquals(200, response.status_code)
        
    def test_resetVideoHits(self):
        response = requests.post('http://localhost:%s/_itest/resetVideoHits'%config.masterWebPort)
        self.assertEquals(200, response.status_code)
        
        response = requests.get('http://localhost:%s/video/rawstats/'%config.masterWebPort)
        
        # @TODO: what about a decent check?
        self.assertEquals(200, response.status_code)
        