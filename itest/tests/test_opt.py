'''

@author: eh14
'''

import base

class TestOptViews (base.BaseIntegrationTest):
    
    def test_show(self):
        self.getPathAndAssert200('opt/show')

    def test_stats(self):
        self.getPathAndAssert200('opt/stats')